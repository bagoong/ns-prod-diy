define('BrandListing.ListView', [
    'Backbone',
    'brand_listing.tpl',
    'BrandListing.CommonQuestions.View',
    'Backbone.CollectionView',
    'Backbone.CompositeView',
    'jQuery',
    'underscore'
], function BrandListingListView(
    Backbone,
    brandListingTpl,
    BrandListingView,
    CollectionView,
    CompositeView,
    jQuery,
    _
) {
    'use strict';

    return Backbone.View.extend({

        title: 'DIY Home Center | The Best Brands for Your Deck & Home',

        initialize: function initialize(options) {
            CompositeView.add(this);
            this.application = options.application;
            this.collection = options.collection;
        },

        childViews: {
            'Brand.Collection': function BrandCollection() {
                return new CollectionView({
                    'childView': BrandListingView,
                    'collection': this.collection,
                    'viewsPerRow': Infinity
                });
            }
        },

        events: {
            'click .alphabetical-index li': 'goToSection'
        },

        goToSection: function goToSection(event) {
            var letter = jQuery(event.currentTarget).html();
            letter = letter.toLowerCase();
            jQuery(window).scrollTop(jQuery('#brandletter-' + letter).position().top);
        },

        getAllLetters: function getAllLetters() {
            return [
                {letter: 'a', enabled: 'disabled'},
                {letter: 'b', enabled: 'disabled'},
                {letter: 'c', enabled: 'disabled'},
                {letter: 'd', enabled: 'disabled'},
                {letter: 'e', enabled: 'disabled'},
                {letter: 'f', enabled: 'disabled'},
                {letter: 'g', enabled: 'disabled'},
                {letter: 'h', enabled: 'disabled'},
                {letter: 'i', enabled: 'disabled'},
                {letter: 'j', enabled: 'disabled'},
                {letter: 'k', enabled: 'disabled'},
                {letter: 'l', enabled: 'disabled'},
                {letter: 'm', enabled: 'disabled'},
                {letter: 'n', enabled: 'disabled'},
                {letter: 'o', enabled: 'disabled'},
                {letter: 'p', enabled: 'disabled'},
                {letter: 'q', enabled: 'disabled'},
                {letter: 'r', enabled: 'disabled'},
                {letter: 's', enabled: 'disabled'},
                {letter: 't', enabled: 'disabled'},
                {letter: 'u', enabled: 'disabled'},
                {letter: 'v', enabled: 'disabled'},
                {letter: 'w', enabled: 'disabled'},
                {letter: 'x', enabled: 'disabled'},
                {letter: 'y', enabled: 'disabled'},
                {letter: 'z', enabled: 'disabled'}
            ];
        },

        getContext: function getContext() {
            var alphabeticalArrays = [];
            var brandAlphabeticalGroupList = [];
            var brandList = [];
            var letters;

            _.each(this.collection.models, function eachModel(model) {
                var id = model.get('id');
                var name = model.get('name');
                var logoId = model.get('logo').internalid;
                var logoName = model.get('logo').name;
                var urlComponent = model.get('urlcomponent');
                var brand = {id: id, name: name, logo: {logoId: logoId, logoName: logoName}, urlComponent: urlComponent};
                brandList.push(brand);

                alphabeticalArrays.push(name.toLowerCase().substring(0, 1));
            });

            alphabeticalArrays = _.uniq(alphabeticalArrays, function eachAlphabeticalArrays(i) { return i; });

            _.each(alphabeticalArrays, function eachLetter(letter) {
                var brandGroupList = [];
                var brandGrouping;

                _.each(brandList, function eachBrand(brand) {
                    if (brand.name.toLowerCase().substring(0, 1) === letter) {
                        brandGroupList.push(brand);
                    }
                });

                brandGrouping = {letter: letter, brandGroupList: brandGroupList};
                brandAlphabeticalGroupList.push(brandGrouping);
            });

            letters = this.getAllLetters();

            _.each(letters, function eachLetter(letter) {
                _.each(alphabeticalArrays, function eachAlphaLetter(alPhaletter) {
                    if (letter.letter === alPhaletter) {
                        letter.enabled = 'enabled';
                    }
                });
            });

            return {
                brandAlphabeticalGroupList: brandAlphabeticalGroupList,
                letters: letters
            };
        },

        template: brandListingTpl

    });
});