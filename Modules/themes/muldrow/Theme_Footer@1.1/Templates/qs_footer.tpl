{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div id="banner-footer" class="content-banner banner-footer" data-cms-area="global_banner_footer" data-cms-area-filters="global"></div>
<div class="footer-content">
	<div class="footer-content-nav">

		<div class="footer-thirdcol">
			<ul class="footer-content-nav-list about-us">
				<li class="first">
					<a href="" class="first" data-toggle="collapse" data-target=".about-us .footer-collapsed-list" data-type="collapse">Customer Care</a>
				</li>
				<li class="footer-collapsed-list">
					<a href="">Contact Us</a>
				</li>
				<li class="footer-collapsed-list">
					<a href="">Delivery Information</a>
				</li>
				<li class="footer-collapsed-list">
					<a href="">Returns & Damage</a>
				</li>
				<li class="footer-collapsed-list">
					<a href="">Customer Reviews</a>
				</li>
			</ul>

			<ul class="footer-content-nav-list my-account">
				<li class="first">
					<a href="" class="first" data-toggle="collapse" data-target=".my-account .footer-collapsed-list" data-type="collapse">About us</a>
				</li>
				<li class="footer-collapsed-list">
					<a href="">Our Team</a>
				</li>
				<li class="footer-collapsed-list">
					<a href="">Preferred Builder Program</a>
				</li>
			</ul>

			<div class="footer-content-copyright">
				<ul>
					<li><a href="">{{translate 'Privacy Policy'}}</a></li>
					<li>|</li>
					<li><a href="">{{translate 'Terms & Conditions'}}</a></li>
					<!--<li><a href="">{{translate 'Sitemap'}}</a></li>-->
				</ul>
				<ul>
					<li>{{translate '&copy; 2008-2015 '}}DIY Home Center, LLC. All Rights Reserved.</li>
				</ul>
			</div>
		</div>

		<div class="footer-thirdcol">
			<ul class="footer-content-nav-list customer-service">
				<li class="first">
					<a href="" class="first" data-toggle="collapse" data-target=".customer-service .footer-collapsed-list" data-type="collapse">Need Help?</a>
				</li>
				<li class="footer-collapsed-list">
					<a href="" class="phone-number">888-349-4660</a><br>Mon-Fri | 8am-5pm CST
				</li>
			</ul>

			<ul class="footer-content-nav-list company-name">
				<li class="first" data-toggle="collapse" data-target=".company-name .footer-collapsed-list" data-type="collapse">Stay Connected</li>
				<li class="footer-collapsed-list">
					<ul class="footer-social">
						<li class="footer-social-item">
							<a class="" href="https://www.twitter.com/" target="_blank">
								<i class="footer-social-twitter-icon"></i>
							</a>
						</li>
						<li class="footer-social-item">
							<a href="">
								<i class="footer-social-google-icon"></i>
							</a>
						</li>
						<li class="footer-social-item">
							<a href="">
								<i class="footer-social-facebook-icon"></i>
							</a>
						</li>
						<li class="footer-social-item">
							<a href="">
								<i class="footer-social-pinterest-icon"></i>
							</a>
						</li>
					</ul>
				</li>
			</ul>
			<ul class="footer-content-nav-list footer-content-nav-list-last newsletter-signup">
				<li class="first" data-toggle="collapse" data-target=".newsletter-signup .footer-collapsed-list" data-type="collapse">Join our Mailing List</li>
				<li class="footer-collapsed-list" data-view="Newsletter.SignUp"></li>
			</ul>
		</div>

		<div class="footer-thirdcol">
			<img class="shopper-approved" src="http://shopping.na1.netsuite.com/c.530610/site/images/shopper-approved-logo.png">
			<img class="shopper-reviews" src="http://shopping.na1.netsuite.com/c.530610/site/images/shopper-approved-reviews.png">
			<img class="bbb-rating" src="http://shopping.na1.netsuite.com/c.530610/site/images/bbb-rating.png">
			<img class="norton-secured" src="http://shopping.na1.netsuite.com/c.530610/site/images/norton-logo.png">
		</div>
	</div>


	<div class="footer-mobile">
		<h2>Need Help?</h2>
		<a href="" class="phone-number">888-349-4660</a> Mon-Fri | 8am-5pm CST

		<ul>
			<li><a href="#">Contact Us</a></li>
			<li><a href="#">Returns & Damage</a></li>
			<li><a href="#">Delivery Information</a></li>
			<li><a href="#">Customer Reviews</a></li>
			<br class="clr">
		</ul>
	</div>

</div>
