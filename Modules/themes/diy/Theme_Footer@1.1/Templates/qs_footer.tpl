{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}
<div class="home-bottom blue-bg">
	<div class="additional-contents-wrap">
		<ul class="additional-contents">
			<li>
				<img src="/c.530610/site/images/blog-icon.png">
				<h3>The DIY Blog</h3>
				<p> Nulla at felis non ante ornare gravida. Sed aliquam nisi et scelerisque ullamcorper.</p>
				<p><a href="">Catch Up On The Latest >></a></p>
			</li>
			<li>
				<img src="/c.530610/site/images/hammer-icon2.png">
				<h3>Our Preferred Builder Program</h3>
				<p>Save money, get money back, and enjoy free manufacturer samples and product literature. </p>
				<p><a href="">Sign Up And Start Saving Now >></a></p>
			</li>
			<li style="border-left:1px solid #4e6371;">
				<img src="/c.530610/site/images/rating-stars.png">
				<h3 style="margin-top:35px;">DIY Customer Reviews</h3>
				<p>If you're looking for DIYHomeCenter.com reviews, check out what our Customers are saying!</p>
				<p><a href="">Read Our Reviews >></a></p>
			</li>
			<br class="clr">
		</ul>
	</div>
</div>
<div id="banner-footer" class="content-banner banner-footer" data-cms-area="global_banner_footer" data-cms-area-filters="global"></div>
<div class="footer-content">
	<div class="footer-content-nav">

		<div class="footer-thirdcol">
			<ul class="footer-content-nav-list about-us">
				<li class="first">
					<a href="" class="first" data-toggle="collapse" data-target=".about-us .footer-collapsed-list" data-type="collapse">Customer Care</a>
				</li>
				<li class="footer-collapsed-list">
					<a href="">Contact Us</a>
				</li>
				<li class="footer-collapsed-list">
					<a href="">Delivery Information</a>
				</li>
				<li class="footer-collapsed-list">
					<a href="">Returns & Damage</a>
				</li>
				<li class="footer-collapsed-list">
					<a href="">Customer Reviews</a>
				</li>
			</ul>

			<ul class="footer-content-nav-list my-account">
				<li class="first">
					<a href="" class="first" data-toggle="collapse" data-target=".my-account .footer-collapsed-list" data-type="collapse">Company Info</a>
				</li>
				<li class="footer-collapsed-list">
					<a href="">About us</a>
				</li>
				<li class="footer-collapsed-list">
					<a href="">Our Team</a>
				</li>
				<li class="footer-collapsed-list">
					<a href="">Preferred Builder Program</a>
				</li>
			</ul>

			<div class="footer-content-copyright">
				<ul>
					<li><a href="">{{translate 'Privacy Policy'}}</a></li>
					<li class="separator">|</li>
					<li><a href="">{{translate 'Terms & Conditions'}}</a></li>
					<!--<li><a href="">{{translate 'Sitemap'}}</a></li>-->
				</ul>
				<ul>
					<li>{{translate '&copy; 2008-2015 '}}DIY Home Center, LLC. All Rights Reserved.</li>
				</ul>
			</div>
		</div>

		<div class="footer-thirdcol">
			<ul class="footer-content-nav-list customer-service">
				<li class="first">
					<a href="" class="first" data-toggle="collapse" data-target=".customer-service .footer-collapsed-list" data-type="collapse">Need Help?</a>
				</li>
				<li class="footer-collapsed-list">
					<a class="phone-icon" href="">&nbsp;</a> <a href="" class="phone-number">888-349-4660</a><br>Mon-Fri | 8am-5pm CST
				</li>
			</ul>

			<ul class="footer-content-nav-list company-name">
				<li class="first" data-toggle="collapse" data-target=".company-name .footer-collapsed-list" data-type="collapse">Stay Connected</li>
				<li class="footer-collapsed-list">
					<ul class="footer-social">
						<li class="footer-social-item">
							<a class="twitter" href="https://www.twitter.com/" target="_blank">
								<!--<i class="footer-social-twitter-icon"></i>-->
							</a>
						</li>
						<li class="footer-social-item">
							<a class="facebook" href="">
								<!--<i class="footer-social-facebook-icon"></i>-->
							</a>
						</li>
						<li class="footer-social-item">
							<a class="pinterest" href="">
								<!--<i class="footer-social-google-icon"></i>-->
							</a>
						</li>
						<li class="footer-social-item">
							<a class="blog" href="">
								<!--<i class="footer-social-pinterest-icon"></i>-->
							</a>
						</li>
					</ul>
				</li>
			</ul>
			<ul class="footer-content-nav-list footer-content-nav-list-last newsletter-signup">
				<li class="first" data-toggle="collapse" data-target=".newsletter-signup .footer-collapsed-list" data-type="collapse">Join our Mailing List</li>
				<li class="footer-collapsed-list" data-view="Newsletter.SignUp"></li>
			</ul>
		</div>

		<div class="footer-thirdcol">

			<img class="shopper-approved" src="/c.530610/site/images/shopper-approved-logo.png">

			<a href="http://www.shopperapproved.com/reviews/diyhomecenter.com/" class="shopperlink shopper-approved-reviews"><img src="//www.shopperapproved.com/seals/5876-r.gif" style="border: 0" alt="Customer Reviews" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by Shopper Approved \251 '+d.getFullYear()+'.'); return false;" /></a><script type="text/javascript">(function() { var js = window.document.createElement("script"); js.src = '//www.shopperapproved.com/seals/certificate.js'; js.type = "text/javascript"; document.getElementsByTagName("head")[0].appendChild(js); })();</script>

			<div style="padding-top: 2px; padding-bottom: 0px; padding-left: 70px; text-decoration: none; border-bottom: 0px solid #fff; outline: none;">
	          <script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.diyhomecenter.com&amp;size=M&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script>
	        </div>


			<!--<img class="shopper-approved" src="http://shopping.na1.netsuite.com/c.530610/site/images/shopper-approved-logo.png">-->
			<!--<img class="shopper-reviews" src="http://shopping.na1.netsuite.com/c.530610/site/images/shopper-approved-reviews.png">-->
			<img class="bbb-rating" src="/c.530610/site/images/bbb-rating.png">
			<img class="norton-secured" src="/c.530610/site/images/norton-logo.png">
		</div>
	</div>


	<div class="footer-mobile">
		<h2>Need Help?</h2>
		<a class="phone-icon" href="">&nbsp;</a><a href="" class="phone-number">888-349-4660</a> Mon-Fri | 8am-5pm CST

		<ul class="footermobile-nav">
			<li><a href="#">Contact Us</a></li>
			<li><a href="#">Returns & Damage</a></li>
			<li><a href="#">Delivery Information</a></li>
			<li><a href="#">Customer Reviews</a></li>
			<br class="clr">
		</ul>

		<div class="stay-connected-wrap">
			<span>Stay Connected</span>
	        <ul class="footer-social">
    			<li class="footer-social-item">
    				<a class="twitter" href="https://www.twitter.com/" target="_blank">
    					<!--<i class="footer-social-twitter-icon"></i>-->
    				</a>
    			</li>
    			<li class="footer-social-item">
    				<a class="facebook" href="">
    					<!--<i class="footer-social-facebook-icon"></i>-->
    				</a>
    			</li>
    			<li class="footer-social-item">
    				<a class="pinterest" href="">
    					<!--<i class="footer-social-google-icon"></i>-->
    				</a>
    			</li>
    			<li class="footer-social-item">
    				<a class="blog" href="">
    					<!--<i class="footer-social-pinterest-icon"></i>-->
    				</a>
    			</li>
    		</ul>
    		<br class="clr"/>
		</div>

		<div class="mailing-list-wrap">
			<span>Join Our Mailing List</span>
			<ul class="footer-content-nav-list footer-content-nav-list-last newsletter-signup">
				<li class="footer-collapsed-list" data-view="Newsletter.SignUp"></li>
				<br class="clr">
			</ul>
		</div>

		<div class="footnotes-wrap">
			<p><a href="#">Privacy Policy</a> | <a href="#">Terms & Conditions</a></p>
			<p class="copyright">{{translate '&copy; 2008-2015 '}}DIY Home Center, LLC. All Rights Reserved.</p>
		</div>
	</div>

</div>
