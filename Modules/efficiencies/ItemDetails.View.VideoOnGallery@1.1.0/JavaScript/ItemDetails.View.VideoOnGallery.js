define('ItemDetails.View.VideoOnGallery', [
    'ItemDetails.View',
    'ItemDetails.ImageGallery.View.Video',
    'SC.Configuration',
    'underscore',
    'Utils'
], function ItemDetailsViewVideo(
    ItemDetailsView,
    ItemDetailsImageGalleryViewVideo,
    Configuration,
    _,
    Utils
) {
    'use strict';
    _.extend(ItemDetailsView.prototype.childViews, {
        'ItemDetails.ImageGallery': function ItemDetailsImageGallery() {
            var images = this.model.get('_images', true);
            var hasVideo = false;
            var itemOptionImage = [];
            var optionLabel = [];

            // check if has item options selected
            if(this.model.itemOptions) {
                _.each(this.model.itemOptions, function (option) {
                    // replace item option's label spaces and special characters (alphanumeric only)
                    optionLabel.push(option.label.replace(/[^A-Z0-9]+/ig, ""));
                });
                var optionToCompare = optionLabel.join('');
                    optionLabel.reverse();
                var optionToCompare2 = optionLabel.join('');

                var obj = _.find(images, function(image){
                    if(image.url.search(optionToCompare) > -1 || image.url.search(optionToCompare2) > -1) {
                        itemOptionImage.push(image);
                    }
                });

                if(itemOptionImage.length > 0) {
                    images = itemOptionImage;
                }
            }

            if (this.model.get('custitem_ef_pdp_video_url')) {
                // Push the video
                images.push({
                    altimagetext: _('Video').translate(),
                    url: this.model.get('custitem_ef_pdp_video_url'),
                    isVideo: true,
                    thumb: Utils.getAbsoluteUrl('img/default-video-thumbnail.jpg')
                });
                // If we have a video, we should remove image not available. And leave the video as main
                images = _.filter(images, function filterImages(image) {
                    return image.url !== Configuration.imageNotAvailable;
                });
            }

            return new ItemDetailsImageGalleryViewVideo({
                images: images,
                hasVideo: hasVideo
            });
        }
    });
});