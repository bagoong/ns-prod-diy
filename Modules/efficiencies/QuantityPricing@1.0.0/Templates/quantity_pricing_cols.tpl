{{#if pricingSchedule}}
<div class="quantity-pricing">

    <div class="quantity-pricing-cms-intro" data-cms-area="quantity_pricing_intro" data-cms-area-filters="page_type"></div>
    <div class="quantity-pricing-table-wrapper">
        <table class="quantity-pricing-table">
            <thead class="quantity-pricing-quantities">
                <tr>
                    <th class="quantity-pricing-table-cell">{{translate 'Qty'}}</th>
                    <th class="quantity-pricing-table-cell">{{translate 'Price (per unit)'}}</th>
                </tr>
            </thead>
            <tbody class="quantity-pricing-prices">
                {{#each pricingSchedule}}
                <tr>
                {{#if @last}}
                    <td class="quantity-pricing-table-cell">{{minimum}} +</td>
                {{else}}
                    <td class="quantity-pricing-table-cell">{{minimum}} - {{maximum}}</td>
                {{/if}}

                    <td class="quantity-pricing-table-cell">{{price}}</td>
                </tr>
                {{/each}}
            </tbody>
        </table>
    </div>
</div>
{{/if}}