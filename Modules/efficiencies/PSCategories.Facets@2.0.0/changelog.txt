1.0.0 - Initial Release
1.0.1 - Code style format
2.0.0 - Mont blanc release
    - Includes Manual product sequencing for sorting items by manual relevance, when Search api cannot sort by relevance
    (original relevance only applies when having keyword search)
    - Fixes for HTML outputting on category pages for category description
    - Fixes a bug on not-found categories