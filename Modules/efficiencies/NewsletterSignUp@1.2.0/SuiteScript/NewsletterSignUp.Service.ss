function service() {
    'use strict';

    var method;
    var data;
    var url;
    var Application = require('Application');
    var NewsletterSignUpConfig = require('Newsletter.SignUp.Configuration');
    var _ = require('underscore');
    var response;
    var responseCode;
    var currentDomainMatch = session.getSiteSettings(['touchpoints'])
        .touchpoints.login
        .match(/^https?\:\/\/([^\/?#]+)(?:[\/?#]|$)/i);
    var currentDomain = currentDomainMatch && currentDomainMatch[0];

    try {
        method = request.getMethod();

        if (method === 'POST') {
            data = JSON.parse(request.getBody());

            data.email = _.escape(data.email);
            data.firstname = 'NSFirstName';
            data.lastname = 'NSLastName';

            url = currentDomain + '/app/site/crm/externalleadpage.nl?compid=' +
                nlapiGetContext().getCompany() + '&formid=' + NewsletterSignUpConfig.formId + '&h=' +
                NewsletterSignUpConfig.hash + '&globalsubscriptionstatus=1';

            if (context.getFeature('SUBSIDIARIES')) {
                data.subsidiary = session.getShopperSubsidiary();
            }

            try {
                response = nlapiRequestURL(url, data);
                responseCode = parseInt(response.getCode(), 10);

                // Just in case someday it accepts the redirect. 206 is netsuite error ('partial content')
                if (response === 200 || responseCode === 302 || responseCode === 201) {
                    return Application.sendContent({
                        successMessage: 'Thanks for contact us'
                    });
                }
            } catch (e) {
                // If the form submit SUCCEEDS!!! it will throw an exception
                // Because of the url redirect
                if (e instanceof nlobjError && e.getCode().toString() === 'ILLEGAL_URL_REDIRECT') {
                    return Application.sendContent({
                        successMessage: 'Thanks for signing up to our newsletter!'
                    });
                }

                return Application.sendError({
                    // @property {Number} status
                    status: 500,
                    code: 'ERR_FORM',
                    message: 'Something went wrong processing your form, please try again later.'
                });
            }


            return Application.sendError({
                // @property {Number} status
                status: 500,
                code: 'ERR_FORM',
                message: 'Something went wrong processing your form, please try again later.'
            });
        }

        return Application.sendError(methodNotAllowedError);
    } catch(ex) {
        return Application.sendError(ex);
    }
}
