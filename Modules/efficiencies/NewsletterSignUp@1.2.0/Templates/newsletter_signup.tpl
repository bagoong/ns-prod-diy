<form action="#" data-validation="control-group">
    <div>
        <input type="email" name="email" class="footer-newsletter-email-form-input" placeholder="{{ translate 'enter your email address'}}" data-validation="control">
        <button type="submit" class="footer-newsletter-email-form-submit">{{ translate 'Subscribe'}}</button>
    </div>
    <div class="hide message"></div>
</form>
