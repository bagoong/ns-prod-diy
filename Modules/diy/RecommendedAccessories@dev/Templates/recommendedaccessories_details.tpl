<li class="recommendedaccessories-list-details-results-table-column">
    <a class="item-views-recommended-accessories-thumbnail" href="/{{url_component}}">
    <div class="recommendedaccessories-list-details-container">
        <img class="recommendedaccessories-img" src="{{img_src}}?resizeid=4&resizeh=175&resizew=175">
        <div class="recommendedaccessories-title"><span>{{name}}</span></div>
        <div class="recommendedaccessories-price"><span>{{price}}</span></div>
    </div>
    </a>
</li>