<aside class="recommendedaccessories-container">
	{{#if isRecommendedAccessories}}
	    <h1 class="recommendedaccessories-header">{{translate 'Recommended Accessories'}}</h1>
	    <div class="recommendedaccessories-list-results">
	        <ul data-view="RecommendedAccessories.Collection" class="recommendedaccessories-items" id="recommendedaccessoriesslider-items"></ul>
	    </div>
    {{/if}}
</aside>
