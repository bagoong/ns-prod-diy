define('RecommendedAccessories.Model',[
    'SC.Model',
    'SearchHelper',
    'ItemsResultHelper',
    'underscore'
], function RecommendedAccessoriesModel(
    SCModel,
    SearchHelper,
    ItemsResultHelper,
    _
) {
    'use strict';

    return SCModel.extend ({
        name: 'RecommendedAccessories',

        record: 'customrecord_diy_recommended_accessories',

        fieldsets: {
            basic: [
                'internalid',
                'item',
                'accessories',
                'sort_order'
            ]
        },

        sorting: [
            {fieldName: 'custrecord_diy_ra_sort_order', sort:'asc'}
        ],

        filters: [
            {fieldName: 'isinactive', operator:'is', value1:'F'}],

        columns:{
            internalid:{fieldName:'internalid'},
            item:{fieldName:'custrecord_diy_ra_item'},
            accessories:{fieldName:'custrecord_diy_ra_accessories'},
            sort_order:{fieldName:'custrecord_diy_ra_sort_order'},
        },

        list:function list(internalid) {
            var filters = _.clone(this.filters);
            var ItemResult;
            var results;

            //nlapiLogExecution('DEBUG', 'RecommendedAccessories: internalid', internalid);

            if (internalid == '' || internalid == null) {
                return [];
            }

            filters.push({
                fieldName: 'custrecord_diy_ra_item',
                operator: 'anyof',
                value1: internalid
            });

            var Search = new SearchHelper(this.record, filters, this.columns, this.fieldsets.basic);
            Search.search();

            results = Search.getResultsForListHeader();

            ItemResult = new ItemsResultHelper(this.record, this.columns.accessories, 'accessories', 'item');
            ItemResult.processResults(results.records);

            return results.records;
        }

    });

});