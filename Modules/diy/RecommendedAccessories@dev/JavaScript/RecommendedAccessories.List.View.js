define('RecommendedAccessories.List.View', [
	'Backbone',
	'SC.Configuration',
	'recommendedaccessories_list.tpl',
	'Backbone.CollectionView',
	'Backbone.CompositeView',
	'RecommendedAccessories.Details.View',
	'underscore',
	'jQuery'
], function RecommendedAccessoriesListView(
	Backbone,
	Configuration,
	recommendedaccessories_list_tpl,
	CollectionView,
	CompositeView,
	RecommendedAccessoriesDetailsView,
	_,
	jQuery

) {
		var bxSliderInstance;
		return Backbone.View.extend({
			initialize: function(options){
				CompositeView.add(this);
				this.application = options.application;
				this.collection = options.collection;

				this.listenTo(this.collection, 'sync', jQuery.proxy(this.render,this));
				var self = this;

				this.listenTo(this, 'afterCompositeViewRender', function() {

					_.defer(function() {
						if (bxSliderInstance && bxSliderInstance.length) {
							//console.log('bxSliderInstance', bxSliderInstance);
							bxSliderInstance.destroySlider();
						}
						var recommendedAccessoriesCarousel = jQuery('#recommendedaccessoriesslider-items');

						bxSliderInstance = _.initBxSlider(recommendedAccessoriesCarousel, Configuration.recommendedAccessoriesSliderDefaults);
					});

					if (self.collection.length === 0) {
						jQuery('.recommendedaccessories-section').hide();
					} else {
						jQuery('.recommendedaccessories-section').show();
					}

				});

			},
			
			childViews: {
				'RecommendedAccessories.Collection': function(){
					return new CollectionView({
						'childView': RecommendedAccessoriesDetailsView,
						'collection': this.collection,
						'viewsPerRow': Infinity
					});
				}
			},

			getContext: function ()
			{
				var recommendedAccessoriesCollection = this.collection.models.length;
				var isRecommendedAccessories = recommendedAccessoriesCollection > 0;

				return {
					isRecommendedAccessories: isRecommendedAccessories
				};
			},

			template: recommendedaccessories_list_tpl
		});
	}
);