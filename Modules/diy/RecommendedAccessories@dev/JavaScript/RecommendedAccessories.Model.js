define('RecommendedAccessories.Model',[
	'Backbone',
	'underscore',
	'Utils'
], function RecommendedAccessoriesModel(
	Backbone,
	_,
	Utils
) {
	'use strict';

	return Backbone.Model.extend ({
		urlRoot: Utils.getAbsoluteUrl('services/RecommendedAccessories.Service.ss')
	});
});