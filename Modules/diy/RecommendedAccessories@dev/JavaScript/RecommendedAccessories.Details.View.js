define('RecommendedAccessories.Details.View', [
	'Backbone',
	'recommendedaccessories_details.tpl',
	'jQuery',
	'Utils'
], function VideoDetailsView(
	Backbone,
	recommendedaccessories_details_template,
	jQuery,
	Utils
){
		return Backbone.View.extend({
			getContext: function(){
				var item = this.model.get('item');
				var imageUrl;
				var img_src;
				//console.log('item',item);

				//var img_src = item.itemimages_detail.Main.url;

				var itemImagesDetail = item.itemimages_detail || {};
				itemImagesDetail = itemImagesDetail.media || itemImagesDetail;
				itemImagesDetail = this.itemImageFlatten(itemImagesDetail);

				_.each(itemImagesDetail, function eachObjImage(obj) {
					if (obj.url) {
						imageUrl = obj.url;
						return;
					}
				});

				if (imageUrl) {
					img_src = imageUrl;
				} else {
					img_src = Utils.getAbsoluteUrl('img/no_image_available.jpeg');
				}

				return {
					name: item.storedisplayname2,
					url_component:item.urlcomponent,
					img_src:img_src,
					price:item.pricelevel1_formatted
				}

			},

			template: recommendedaccessories_details_template,

			itemImageFlatten: function itemImageFlatten(images) {
				if ('url' in images && 'altimagetext' in images) {
					return [images];
				}

				return _.flatten(_.map(images, function flatten(item) {
					if (_.isArray(item)) {
						return item;
					}

					return itemImageFlatten(item);
				}));
			}


		});
	}
);