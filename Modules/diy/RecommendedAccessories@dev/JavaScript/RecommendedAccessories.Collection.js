define('RecommendedAccessories.Collection',[
  'Backbone.CachedCollection',
  'RecommendedAccessories.Model',
  'underscore',
  'Utils'
], function RecommendedAccessoriesCollection(
    BackboneCachedCollection,
    Model,
    _,
    Utils
) {

  'use strict';

  return BackboneCachedCollection.extend ({
        model: Model,
        url: Utils.getAbsoluteUrl('services/RecommendedAccessories.Service.ss')
  });
});