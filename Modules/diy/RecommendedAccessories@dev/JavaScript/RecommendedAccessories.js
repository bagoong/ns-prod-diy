define('RecommendedAccessories', [
    'ItemDetails.View',
    'RecommendedAccessories.Model',
    'RecommendedAccessories.Collection',
    'RecommendedAccessories.List.View',
    'SC.Configuration',
    'underscore',
    'jQuery',
    'Utils'
], function RecommendedAccessories(
    ItemDetailsView,
    Model,
    Collection,
    RecommendedAccessoriesListView,
    Configuration,
    _,
    jQuery
) {
    'use strict';

    return {
        mountToApp: function () {
            ItemDetailsView.prototype.initialize =
                _.wrap(ItemDetailsView.prototype.initialize, function wrapInitialize(fn) {

                    fn.apply(this, _.toArray(arguments).slice(1));

                    this.on('afterViewRender', function() {
                        this.$el
                            .find('.item-details-banner-details-bottom')
                            .append('<div class="recommendedaccessories-section" data-view="RecommendedAccessories"></div>');
                    });
                });



            _.extend(ItemDetailsView.prototype.childViews, {
                'RecommendedAccessories': function addAccessories() {
                    var self = this;
                    var collection = new Collection();
                    collection.fetch({
                        data: {
                            internalid: self.model.get('internalid')
                        }
                    });

                    return new RecommendedAccessoriesListView ({
                        collection: collection,
                        application:self.application
                    });
                }
            });


        }
    }
});