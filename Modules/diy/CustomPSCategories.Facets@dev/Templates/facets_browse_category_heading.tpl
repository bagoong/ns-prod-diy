<section class="category-list-header">
	<div class="category-main-image">{{#if image}}<img src="{{image}}" />{{/if}}</div>
	<div class="category-main-description">
	    	<h1>{{{pageTitle}}}</h1>
	</div>
</section>
