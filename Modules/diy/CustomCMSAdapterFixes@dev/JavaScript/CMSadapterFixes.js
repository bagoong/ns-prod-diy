define('CMSadapterFixes', [
    'jQuery',
    'underscore',
    'Backbone',
    'Utils'
], function CMSadapterFixes(
    jQuery,
    _,
    Backbone,
    Utils
) {
    'use strict';

    return {
        mountToApp: function mountToApp() {
            /**
             * This CMS fix is for when templates change according to device type
             * and CMS zones appear/desappear accordingly
             * Otherwise, zones get empty on resize
             *
             * This file is CUSTOM and is OVERRIDING the efficiencies module file
             * See ns.package.json file to see which file is being overriden.
             * 
             * LINES 32-36 have been added
             */
            var previousDevice = Utils.getDeviceType();
            jQuery(window).on('resize', _.throttle(function adapterTrigger() {
       
                var newDevice = Utils.getDeviceType();
                if (newDevice !== previousDevice) {
                    previousDevice = newDevice;
                    if(CMS) {
                        CMS.trigger('adapter:page:changed');
                    } else {
                        Backbone.Events.trigger('adapter:page:changed');
                    }
                }
            }, 300));
        }
    };
});