{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="footer-bottom blue-bg">
	<div data-cms-area="bottom-top" data-cms-area-filters="global">
	</div>
</div>
<div id="banner-footer" class="content-banner banner-footer" data-cms-area="global_banner_footer" data-cms-area-filters="global"></div>
<div class="footer-content">
	<div class="footer-content-nav">

		<div class="footer-thirdcol">
			<div data-cms-area="footer-column1" data-cms-area-filters="global"></div>
		</div>

		<div class="footer-thirdcol">
			<div data-cms-area="footer-column2" data-cms-area-filters="global"></div>
			<!--
			<ul class="footer-content-nav-list footer-content-nav-list-last newsletter-signup">
				<li class="first" data-toggle="collapse" data-target=".newsletter-signup .footer-collapsed-list" data-type="collapse">Join our Mailing List</li>
				<li class="footer-collapsed-list" data-view="Newsletter.SignUp"></li>
			</ul>
			-->
		</div>

		<div class="footer-thirdcol">
			<div data-cms-area="footer-column3" data-cms-area-filters="global"></div>
		</div>
	</div>


	<div class="footer-mobile">
		<h2>Need Help?</h2>
		<a class="phone-icon" href="">&nbsp;</a><a href="" class="phone-number">888-349-4660</a> Mon-Fri | 8am-5pm CST

		<ul class="footermobile-nav">
			<li><a href="#/contact-us" data-hashtag="#/contact-us" data-touchpoint="home">Contact Us</a></li>
			<li><a href="/returns-damage" data-hashtag="#/returns-damage" data-touchpoint="home">Returns &amp; Damage</a></li>
			<li><a href="/delivery-information" data-hashtag="#/delivery-information" data-touchpoint="home">Delivery Information</a></li>
			<li><a href="/customer-reviews" data-hashtag="#/customer-reviews" data-touchpoint="home">Customer Reviews</a></li>
			<br class="clr">
		</ul>
		
		<div class="stay-connected-wrap">
			<span>Stay Connected</span> 
	        <ul class="footer-social">
    			<li class="footer-social-item">
    				<a class="twitter" href="https://www.twitter.com/" target="_blank">
    					<!--<i class="footer-social-twitter-icon"></i>-->
    				</a>
    			</li>
    			<li class="footer-social-item">
    				<a class="facebook" href="https://www.facebook.com/diyhomecenter" target="_blank">
    					<!--<i class="footer-social-facebook-icon"></i>-->
    				</a>
    			</li>
    			<li class="footer-social-item">
    				<a class="pinterest" href="https://www.pinterest.com/diyhomecenter" target="_blank">
    					<!--<i class="footer-social-google-icon"></i>-->
    				</a>
    			</li>
    			<li class="footer-social-item">
    				<a class="blog" href="http://www.diyhomecenterblog.com" target="_blank">
    					<!--<i class="footer-social-pinterest-icon"></i>-->
    				</a>
    			</li>
    		</ul>
    		<br class="clr"/>
		</div>

		<!--
		<div class="mailing-list-wrap">
			<span>Join Our Mailing List</span> 
			<ul class="footer-content-nav-list footer-content-nav-list-last newsletter-signup">
				<li class="footer-collapsed-list" data-view="Newsletter.SignUp"></li>
				<br class="clr">
			</ul>
		</div>
		-->

		<div class="footnotes-wrap">
			<p><a href="/privacy-policy">Privacy Policy</a> <span class="separator">&nbsp;|&nbsp;</span> <a href="/terms-conditions">Terms & Conditions</a></p>
			<p class="copyright">{{translate '&copy; 2008-2016 '}}DIY Home Center, LLC. All Rights Reserved.</p>
		</div>
	</div>

</div>
