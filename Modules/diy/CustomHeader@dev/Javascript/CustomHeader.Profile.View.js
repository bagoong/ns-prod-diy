define('CustomHeader.Profile.View', [
    'Header.Menu.View',
    'Header.Profile.View',
    'Profile.Model',
    'SC.Configuration',

    'Backbone',
    'underscore',
    'jQuery'
], function CustomHeaderProfileView(
	HeaderMenuView,
	HeaderProfileView,
	ProfileModel,
	Configuration,

	Backbone,
	_,
	jQuery
) {
    'use strict';

    HeaderProfileView.prototype.installPlugin('postContext', {
		name: 'headerProfilePostContext',
		priority: 10,
		execute: function execute(context) {
			var profile = ProfileModel.getInstance();
			var is_loading = !_.getPathFromObject(Configuration, 'performance.waitForUserProfile', true) && ProfileModel.getPromise().state() !== 'resolved'
			var is_loged_in = profile.get('isLoggedIn') === 'T' && profile.get('isGuest') === 'F';

			var isInBuilderProgram = _.find(profile.get('customFieldValues'), function (v) {
				return v.name === 'custentity_in_builder_program';
			});
			var isInBuilderProgramBool = (isInBuilderProgram && isInBuilderProgram.value == 'T') ? true : false;
			
			_.extend(context, {
				showProBuilder: (is_loading || is_loged_in) && isInBuilderProgramBool
			});
		}
	});
});