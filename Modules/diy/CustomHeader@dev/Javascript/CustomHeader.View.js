define('CustomHeader.View', [
    'Header.View',
    'Profile.Model',
    'Backbone.CompositeView',
    'SC.Configuration',
    'Utils',
    'underscore'
], function CustomHeaderView(
    HeaderView,
    ProfileModel,
    BackboneCompositeView,
    Configuration,
    Utils,
    _
) {
    'use strict';

    HeaderView.prototype.initialize = function initialize() {
        var self = this;
        BackboneCompositeView.add(this);

        this.render = _(this.render).wrap(function wrapFn(originalRender) {
            originalRender.apply(self, Array.prototype.slice.call(arguments, 1));
        });
        
        
        $(window).on('resize', function resize() {
            if(self.options.application && self.options.application.getLayout()) {
                if (typeof RelatedItemSlider !== 'undefined' && typeof RelatedItemSlider.getCurrentSlide !== 'undefined') {
                    RelatedItemSlider.destroySlider();
                    _.initBxSlider(RelatedItemSlider, Configuration.getBxSliderDefaults());
                }

                if (typeof CorRelatedItemSlider !== 'undefined' && typeof CorRelatedItemSlider.getCurrentSlide !== 'undefined') {
                    CorRelatedItemSlider.destroySlider();
                    _.initBxSlider(CorRelatedItemSlider, Configuration.getBxSliderDefaults());
                }

                if (typeof RecentlyViewItemSlider !== 'undefined' && typeof RecentlyViewItemSlider.getCurrentSlide !== 'undefined') {
                    RecentlyViewItemSlider.destroySlider();
                    _.initBxSlider(RecentlyViewItemSlider, Configuration.getBxSliderDefaults());
                }
            }
        });
    };
    
    HeaderView.prototype.installPlugin('postContext', {
        name: 'headerFreeShipping',
        priority: 10,
        execute: function execute(context, model) {
            var freeShippingConfig = SC.ENVIRONMENT.published.FREE_SHIPPING;
            var freeShippingTitle;
            var freeShippingBodyText;
            var freeShippingLinkText;
            var freeShippingLinkValue;
            var profile = ProfileModel.getInstance();
            var isLoggedIn = profile.get('isLoggedIn') === 'T' && profile.get('isGuest') === 'F';
            var wishListLink = Configuration.siteSettings.touchpoints.customercenter + '#/wishlist/?';
            var isHome = true;
    
            if (freeShippingConfig) {
                freeShippingTitle = freeShippingConfig.title;
                freeShippingBodyText = freeShippingConfig.body;
                freeShippingLinkText = freeShippingConfig.linktext;
                freeShippingLinkValue = freeShippingConfig.linkvalue;
            }
            
            if (isLoggedIn) {
                isHome = false;
            } else {
                isHome = true;
            }

            _.extend(context, {
                freeShippingTitle: freeShippingTitle,
                freeShippingBodyText: freeShippingBodyText,
                freeShippingLinkText: freeShippingLinkText,
                freeShippingLinkValue: freeShippingLinkValue,
                wishListLink: wishListLink,
                isHome: isHome
            });
        }
    });
    
});
