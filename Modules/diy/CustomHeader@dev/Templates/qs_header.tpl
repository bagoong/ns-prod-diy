{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="header-message" data-type="message-placeholder"></div>

<div class="header-top-wrap">
	<div class="header-top-content {{#if showProBuilder}}header-diy-pro-builder-row{{/if}}">
		<div class="col first {{#if showProBuilder}} wprobuilder {{/if}}">
			<a data-toggle="modal" data-target="#header-free-shipping-modal"><p>Free Shipping On All Products*</p></a>
		</div>
		<div class="col {{#if showProBuilder}} wprobuilderphone {{/if}}">
			<a href="#">
				<div class="image-wrap">
					<img src="/c.530610/site/images/phone-icon.png" align="absmiddle"/>
				</div>
				<p>888-349-4660 <span>M-F | 8-5 CST</span></p>
			</a>
		</div>
		<div class="col {{#if showProBuilder}} wprobuilder {{/if}}">
			<a href="/builder-program">
				<div class="image-wrap">
					<img src="/c.530610/site/images/hammer-icon.png" align="absmiddle"/>
				</div>
				<p>Pro Rewards</p>
			</a>
		</div>
		<div class="col last wish-mobile {{#if showProBuilder}} wprobuilder {{/if}}">
			{{#if isHome}}
			<a href="{{wishListLink}}">
				<div class="image-wrap">
					<img src="/c.530610/site/images/star-icon.png" class="star-icon" align="absmiddle"/>
				</div>
				<p>Wish Lists</p>
			</a>
			{{else}}
			<a href="{{wishListLink}}">
				<div class="image-wrap">
					<img src="/c.530610/site/images/star-icon.png" class="star-icon" align="absmiddle"/>
				</div>
				<p>Wish Lists</p>
			</a>
			{{/if}}
		</div>
		{{#if showProBuilder}}
			<div class="col build-button">
				<a href="#" class="action-build-button">
					<div class="image-wrap">
						<img src="/c.530610/site/images/check-white.png"/>
					</div>
					<p>Pro Builder</p>
				</a>
			</div>
		{{/if}}
		<br class="clr">
	</div>
</div>

<div class="header-main-wrapper">

	<nav class="header-main-nav {{#if isLoggedIn}}header-main-nav-logged-in{{/if}}">

		<div id="banner-header-top" class="content-banner banner-header-top" data-cms-area="header_banner_top" data-cms-area-filters="global"></div>

		<div class="header-sidebar-toggle-wrapper">
			<button class="header-sidebar-toggle" data-action="header-sidebar-show">
				<i class="header-sidebar-toggle-icon"></i>
			</button>
		</div>

		<div class="header-content">
			<div class="header-logo-wrapper">
				<div data-view="Header.Logo"></div>
			</div>


			<div class="header-right-menu">

				<div class="header-menu-profile" data-view="Header.Profile">
				</div>

				{{#if isLoggedIn}}
					{{#if userName}}
					<div class="header-menu-welcome-message-tablet">
						{{ translate 'Welcome' }} {{userName}}
					</div>
					{{/if}}
				{{/if}}

				{{#if showLanguagesOrCurrencies}}
				<div class="header-menu-settings">
					<a href="#" class="header-menu-settings-link" data-toggle="dropdown" title="{{translate 'Settings'}}">
						<i class="header-menu-settings-icon"></i>
						<i class="header-menu-settings-carret"></i>
					</a>
					<div class="header-menu-settings-dropdown">
						<h5 class="header-menu-settings-dropdown-title">{{translate 'Site Settings'}}</h5>
						{{#if showLanguages}}
							<div data-view="Global.HostSelector"></div>
						{{/if}}
						{{#if showCurrencies}}
							<div data-view="Global.CurrencySelector"></div>
						{{/if}}
					</div>
				</div>
				{{/if}}

				<!-- div class="header-menu-searchmobile">
					<button class="header-menu-searchmobile-link" data-action="show-sitesearch" title="{{translate 'Search'}}">
						<i class="header-menu-searchmobile-icon"></i>
					</button>
				</div -->
                <div class="header-menu-quote" data-view="RequestQuoteWizardHeaderLink">
                </div>

				<div class="header-menu-cart">
					<div class="header-menu-cart-dropdown" >
						<div data-view="Header.MiniCart"></div>
					</div>
				</div>
			</div>

			<!--
			<div class="clearfix"></div>
			-->

			<div class="site-search-wrapper pull-right">
				<div data-view="SiteSearch" data-type="SiteSearch"></div>
			</div>

		</div>

		<div id="banner-header-bottom" class="content-banner banner-header-bottom" data-cms-area="header_banner_bottom" data-cms-area-filters="global"></div>
	</nav>
</div>

<div class="header-sidebar-overlay" data-action="header-sidebar-hide"></div>
<div class="header-secondary-wrapper" data-view="Header.Menu" data-phone-template="header_sidebar" data-tablet-template="header_sidebar"></div>

{{#if isSitewideMsgAvailable}}
	<div class="site-message">
		{{sitewideMsg}}
	</div>
{{/if}}

<!-- Modal -->
<div class="modal fade modal-return-policy" id="header-free-shipping-modal" role="dialog">
	<div class="modal-dialog global-views-modal">
		<!-- Modal content-->
		<div class="global-views-modal-content">
			<div id="modal-header" class="global-views-modal-content-header">
				<button type="button" class="global-views-modal-content-header-close" data-dismiss="modal" aria-hidden="true"> × </button>
			</div>
			<div class="return-policy-view-container global-views-modal-content-body">

				<h2 class="return-policy-view-title">{{{freeShippingTitle}}}</h2>
				<p>{{{freeShippingBodyText}}}</p>

				<a href="{{freeShippingLinkValue}}">{{{freeShippingLinkText}}}</a>

				<button class="return-policy-view-button" data-dismiss="modal">{{translate 'Close'}}</button>
			</div>
		</div>

	</div>
</div>
