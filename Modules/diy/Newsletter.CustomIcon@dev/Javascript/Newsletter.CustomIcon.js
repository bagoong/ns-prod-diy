define('Newsletter.CustomIcon', [
    'Newsletter.SignUp.View',
    'Newsletter.CustomIcon.View',
    'underscore',
    'Backbone.CompositeView' //when no childviews
], function (
    NewsletterSignUpView,
    NewsletterCustomIcon,
    _,
    BackboneCompositeView
) {
    'use strict';

    return {
        mountToApp: function mountToApp() {

            NewsletterSignUpView.prototype.initialize =
                _.wrap(NewsletterSignUpView.prototype.initialize, function wrapInitialize(fn) {

                    //copy functions of original modules
                    fn.apply(this, _.toArray(arguments).slice(1));

                    //when no childviews
                    if (!NewsletterSignUpView.prototype.childViews) {                        
                        NewsletterSignUpView.prototype.childViews = {};
                        BackboneCompositeView.add(this);
                    }

                    //extend only for object and array

                    /*
                    _.extend(NewsletterSignUpView.prototype.childViews, {
                        'Newsletter.CustomIcon': function NewsletterIconChildView() {
                            return new NewsletterCustomIcon({                                
                                model: this.model,
                                img: 'http://shopping.na1.netsuite.com/c.530610/site/images/mail-icon.png'
                            });
                        }
                    });
                    */

                    //insert new elements 
                    this.on('afterViewRender', function afterViewRender() {
                        //alert('x');
                        this.$el.find('.footer-newsletter-email-form-submit')
                                .html('<img src="/site/images/mail-icon.png" align="absmiddle">');
                    });
                });
        }
    };
});