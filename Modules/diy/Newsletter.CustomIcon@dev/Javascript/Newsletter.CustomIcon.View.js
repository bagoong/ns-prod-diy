define('Newsletter.CustomIcon.View', [
    'Backbone',
    'customicon.tpl'
], function (
    Backbone,
    Template
) {

    'use strict';

    return Backbone.View.extend({
        template: Template,

        initialize: function() {
            //alert('sf');
        },

        getContext: function getContext() {

            
            return {
                url: this.options.img
            }
            
        }
    });
});