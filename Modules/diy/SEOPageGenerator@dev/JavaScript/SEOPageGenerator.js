define('SEOPageGenerator', [
   'underscore'
], function SEOPageGenerator(
   _
) {

	return {
       mountToApp: function(application) {
           if (SC.isPageGenerator()) {
               _.extend(application.Configuration.searchApiMasterOptions.Facets, {
                   "facet.exclude": "custitem_baluster_length,custitem_depth_inches,custitem_final_rail_height_inches,custitem_furniture_type,custitem_height_inches,custitem_length_feet,custitem_manufacturer,pricelevel5,custitem_screw_size,custitemcustitem_shape,custitemcustitem_width_feet,custitemcustitem_width_inches"
               });
           }
       }
   }

});