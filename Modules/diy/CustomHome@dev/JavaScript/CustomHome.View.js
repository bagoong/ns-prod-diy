define('CustomHome.View', [
    'Home.View',
    'CustomHome.MobileMenu.View',
    'Backbone.CompositeView',
    'jQuery',
    'underscore'
], function CustomHomeView(
	HomeView,
	MobileHomeMenu,
	BackboneCompositeView,
	jQuery,
	_
) {
    'use strict';

    _.extend(HomeView.prototype.events, {
	     'mouseenter [data-action="hotspot"]': 'togglePart',
	     'click [data-action="hotspot"]': 'hidePart',
	     'mouseleave .hotspot-part': 'hidePart'
	});

    _.extend(HomeView.prototype, {

	    togglePart: function togglePart(e) {
		    var parentId = jQuery(e.target).parent().attr('id');
		    var partId = jQuery(e.target).attr('id');

		    jQuery('.hotspot-part').stop().hide(400);
		    jQuery('.hotspot').removeClass('close-hotspot');
		    jQuery('.' + parentId + ' .hotspot-part.' + partId).stop().show(400);
		    jQuery(e.target).addClass('close-hotspot');
	    },

        hidePart: function hidePart() {
	        jQuery('.hotspot-part').stop().hide(400);
	        jQuery('.hotspot').removeClass('close-hotspot');
        }
    });

    HomeView.prototype.initialize =
		_.wrap(HomeView.prototype.initialize, function wrapInitialize(fn) {
			fn.apply(this, _.toArray(arguments).slice(1));
			BackboneCompositeView.add(this);
	});

    HomeView.prototype.childViews = {
        'HomeMenu.View': function MobileHomeMenuChildView() {
            var self = this;
		    return new MobileHomeMenu({
			    model: self.model,
			    menu: 'Shop By Brand'
			});
	    }
    };

    return {
        mountToApp: function mountToApp() {
        }
    };
});