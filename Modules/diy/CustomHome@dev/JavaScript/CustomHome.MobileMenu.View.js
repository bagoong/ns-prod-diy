define('CustomHome.MobileMenu.View', [
    'Backbone',
    'mobilemenu.tpl',
    'jQuery'
], function CustomHomeMobileMenuView(
    Backbone,
    Template,
    jQuery
) {
    'use strict';

    return Backbone.View.extend({
        template: Template,
        events: {
            'click [data-action="test"]': 'toggleTest'
        },

        initialize: function() {
            //alert('sf');
        },

        toggleTest: function() {
            alert('sf');
        },

        getContext: function getContext() {

            return {
                menu: this.options.menu
            }
        }
    });
});