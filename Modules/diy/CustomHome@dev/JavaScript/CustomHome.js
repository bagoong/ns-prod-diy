define('CustomHome', [
    'CustomHome.View',
    'CustomHome.Model',
    'SC.Configuration'
], function CustomHome(
    CustomHomeView,
    CustomHomeModel,
    Configuration
) {
    'use strict';
    
    return {
        mountToApp: function mountToApp() {
            var sliderConfig = SC.ENVIRONMENT.published.HomeSlider_Config;
            if (sliderConfig) {
                _.extend(Configuration, {
                    homePage: {
                        carouselImages: sliderConfig
                    }
                });
            }
        }
    };
});