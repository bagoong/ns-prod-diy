define('CustomHome.Model', [
    'Backbone',
    'underscore',
    'Utils'
], function CustomHomeModel(
    Backbone,
    _,
    Utils
) {
    'use strict';

    return Backbone.Model.extend({
        urlRoot: Utils.getAbsoluteUrl('services/CustomHome.Service.ss')
    });
});