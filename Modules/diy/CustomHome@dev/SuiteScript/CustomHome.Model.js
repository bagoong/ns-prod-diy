define('CustomHome.Model', [
    'SC.Model'
], function CustomHomeModel(
     SCModel
) {
    'use strict';

    return SCModel.extend({

        name: 'CustomHomeModel',

        // getConfiguration: function getConfiguration(currentHostString) {
         //    try {
		// 		var url = currentHostString + '/site/configurations/homeSlider.json';
         //        var request = nlapiRequestURL(url);
         //        var ret = JSON.parse(request.getBody());
		// 		return ret || [];
		// 	}
		// 	catch (e) {
         //        nlapiLogExecution('error', 'HomeImageSliderModel error', e);
		// 		return e;
		// 	}
		// }

        getConfiguration: function getConfiguraton() {
            return homeSliderConfig;
        }
	});
});
