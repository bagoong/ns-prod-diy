define('CustomHome.Configuration', [
    'Configuration'
], function CustomHomeConfiguration(
    Configuration
) {
    'use strict';

    Configuration.publish.push({
        key: 'HomeSlider_Config',
        model: 'CustomHome.Model',
        call: 'getConfiguration'
    });

});
