{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}


<div class="home">
	<div class="home-slider-container">
		<div class="home-image-slider">
			<ul data-slider class="home-image-slider-list">
				{{#each carouselImages}}
					<li data-action="test">
						<div class="home-slide-main-container">
							<img src="{{image}}"/>

							<!--<img src="http://shopping.na1.netsuite.com/c.530610/site/images/homebanner1.jpg"/>-->
                            {{#if title}}
							<div class="home-slide-caption">
								<h2 class="home-slide-caption-title">{{{title}}}</h2>
								<p>{{{text}}}</p>

								<div class="home-slide-caption-button-container">
									<a href="{{{link}}}" class="home-slide-caption-button">Shop This Look</a>
								</div>
							</div>
							{{/if}}

							<div class="hotspots" id="{{@index}}">
							    {{#each hotspots}}
							        <div class="hotspot" data-action="hotspot" style="top:{{top}}; left:{{left}};" id="{{id}}">
                                    </div>
							    {{/each}}
							</div>

                            <div class="parts {{@index}}">
							{{#each parts}}
                            	<div class="hotspot-part {{id}}" id="part-{{id}}" style="top:{{top}}; left: {{left}};">
                            		<div class="left-arrow">
                            		</div>
                            		<div class="item-contents-wrap">
                            			<div class="item-image">
                            				<img src="{{image}}" alt="" width="146">
                            			</div>
                            			<div class="item-contents">
                            				<p class="item-name">{{itemName}}</p>
                            				<p class="item-price">{{price}}</p>
                            				<p class="desc">{{desc}}</p>
                            				<p class="shop-now"><a href="{{{link}}}">Shop Now >></a></p>
                            			</div>
                            			<br class="clr">
                            		</div>
                            	</div>
                            {{/each}}

						</div>
						{{#if title}}
						<div class="home-slide-caption-mobile">
							<h2 class="home-slide-caption-title">{{{title}}}</h2>
							<p>{{{text}}}</p>
							<a href="{{{link}}}">Shop Now >></a>
						</div>
						{{/if}}
					</li>
				{{/each}}
			</ul>
		</div>
	</div>
</div>

<div data-view="HomeMenu.View"></div>

<div data-cms-area="home-top" data-cms-area-filters="path">
</div>

<div data-cms-area="home-mid" data-cms-area-filters="path">
</div>

<div data-cms-area="home-bottom" data-cms-area-filters="path">
</div>

<div class="mobile-content">
	<div class="additional-contents-wrap">
		<ul class="additional-contents">
			<li>
				<img src="/site/images/blog-icon.png">
				<p>DIY Blog</p>
			</li>
			<li>
				<img src="/site/images/hammer-icon2.png">
				<p>For Builders</p>
			</li>
			<li style="border-right:none;">
				<img src="/site/images/rating-stars3.png">
				<p>Reviews</p>
			</li>
			<br class="clr">
		</ul>
	</div>
</div>


