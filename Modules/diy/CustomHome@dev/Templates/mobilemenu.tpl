<div class="home-mobile-navigation testtemplate">

	<ul>
		<li>
			<a href="/brands">Shop by Brand</a>
		</li>
		<li>
			<a href="/building-products">Building Products</a>
		</li>
		<li>
			<a href="/deck-products">Deck Products</a>
		</li>
		<li>
        	<a href="/outdoor-living">Outdoor Living</a>
        </li>
        <li>
			<a href="/home-garden">Home &amp; Garden</a>
		</li>
        <li>
			<a href="/tools-fasteners">Tools &amp; Fasteners</a>
		</li>
	    <li>
    		<a href="/savings">Savings</a>
    	</li>
	</ul>


</div>