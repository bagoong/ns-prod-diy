{{#each hotspots}}
	<div class="hotspot" data-action="hotspot" style="top:{{top}}; left:{{left}};" id="{{id}}">

	</div>
{{/each}}

{{#each parts}}
	<div class="hotspot-part {{id}}" id="part-{{id}}" style="top:{{top}}; left: {{left}};">
		<div class="left-arrow">
		</div>
		<div class="item-contents-wrap">
			<div class="item-image">
				<img src="{{image}}" alt="" width="146">
			</div>
			<div class="item-contents">
				<p class="item-name">{{item-name}}</p>
				<p class="item-price">{{price}}</p>
				<p class="desc">{{desc}}</p>
				<p class="shop-now"><a href="#">Shop Now >></a></p>
			</div>
			<br class="clr">
		</div>
	</div>
{{/each}}