define('Facets.CategoryCell.View', [
    'Facets.Browse.View',
    'facets_category_cell.tpl',
    'Backbone',
    'underscore'
], function FacetsCategoryCellView(
    FacetBrowse,
    facetsCategoryCellTpl,
	Backbone,
    _
) {
    'use strict';
	// @class Facets.CategoryCell.View @extends Backbone.

    return Backbone.View.extend({
        template: facetsCategoryCellTpl,

        getContext: function getContext() {
            
            //console.log(this.options.subcat && this.options.subcat.values);

            return {
                subcats: this.options.subcat && this.options.subcat.values
            };
        }
    });
});