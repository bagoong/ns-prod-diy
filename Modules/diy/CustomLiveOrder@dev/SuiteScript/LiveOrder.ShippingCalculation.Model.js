
define(
	'LiveOrder.ShippingCalculation.Model'
,	[
		'SC.Model'
	,	'Application'
	,	'Profile.Model'
	,	'Utils'
	,	'LiveOrder.Model'
	,	'underscore'
	]
,	function LiveOrderShippingCalculationModel (
		SCModel
	,	Application
	,	Profile
	,	Utils
	,	LiveOrder
	,	_
	)
{
	'use strict';

	var isUpdating = false;
    var isCartEstimator = false;

	/* EVENTS */
    // Application.once('before:LiveOrder.getLines', function beforeLiveOrderCalculateShippingCost(Model, lines) {
    //     Model.applyCustomShippingCalculations(lines);
    // });

    Application.on('after:LiveOrder.get', function(Model, response) {
    	//console.log('shipping after get PRE');
        //nlapiLogExecution('DEBUG', 'after:LiveOrder.get - SC', JSON.stringify(SC));

        // Shipping Estimator flow
        Model.populateDefaultShipMethods(response);

    	//nlapiLogExecution('DEBUG', 'after:LiveOrder.get - response.shipmethods PRE', JSON.stringify(response.shipmethods));
    	//nlapiLogExecution('DEBUG', 'after:LiveOrder.get - response.shipmethod PRE', JSON.stringify(response.shipmethod));

    	Model.applyCustomShippingCalculations(Model, response);
    	//nlapiLogExecution('DEBUG', 'after:LiveOrder.get - response.shipmethods POST', JSON.stringify(response.shipmethods));
    	//nlapiLogExecution('DEBUG', 'after:LiveOrder.get - response.shipmethod POST', JSON.stringify(response.shipmethod));
    	//console.log('shipping after get POST');

    	var shipMethod = _.find(response.shipmethods, function(ship) {
            return ship.internalid == response.shipmethod;
        });
        //nlapiLogExecution('DEBUG', 'after:LiveOrder.get - shipMethod POST', JSON.stringify(shipMethod));
        //nlapiLogExecution('DEBUG', 'after:LiveOrder.get - isCartEstimator POST', isCartEstimator);

        if (shipMethod != null && shipMethod.rate != null && !isUpdating && !isCartEstimator) {
            try {
                //nlapiLogExecution('DEBUG', 'after:LiveOrder.get - setCustomFieldValues shipMethod.rate', shipMethod.rate);
                isUpdating = true;

                order.setCustomFieldValues({custbody_ns_ps_shipping_price: shipMethod.rate.toString() });
                order.setCustomFieldValues({custbody_ns_ps_shipping_carrier: shipMethod.shipcarrier });
                order.setCustomFieldValues({custbody_ns_ps_shipping_method: shipMethod.internalid });

                _.extend(response, Model.get());
                isUpdating = false;
            }
            catch (err) {
                //nlapiLogExecution('DEBUG', 'err', err);
            }        	
        }

        if (response && response.options) {
            if (response.options.custbody_ns_ps_shipping_price)   {
                delete response.options.custbody_ns_ps_shipping_price;
            }
            if (response.options.custbody_ns_ps_shipping_carrier)   {
                delete response.options.custbody_ns_ps_shipping_carrier;
            }
            if (response.options.custbody_ns_ps_shipping_method)   {
                delete response.options.custbody_ns_ps_shipping_method;
            }
        }

        return response;
    });

    Application.on('after:LiveOrder.update', function(Model, data, response) {
    	
    	//console.log('shipping after update');

        // Shipping Estimator flow
        Model.populateDefaultShipMethods(response);        

    	var shipMethod = _.find(response.shipmethods, function(ship) { 
            return ship.internalid == response.shipmethod;
        });
        if (shipMethod) {
        	//console.log('shipping after update - shipMethod.rate: ' + shipMethod.rate);
            if (shipMethod.rate && !isCartEstimator)    {
                try {
                    order.setCustomFieldValues({custbody_ns_ps_shipping_price: shipMethod.rate.toString() });    
                    order.setCustomFieldValues({custbody_ns_ps_shipping_carrier: shipMethod.shipcarrier });
                    order.setCustomFieldValues({custbody_ns_ps_shipping_method: shipMethod.internalid });
                }
                catch (err) {
                    nlapiLogExecution('DEBUG', 'err', err);
                }
            }            
        }
    });

    Application.on('before:LiveOrder.update', function(Model, data) {

        if (data && data.options) {
            if (data.options.custbody_ns_ps_shipping_price)   {
                delete data.options.custbody_ns_ps_shipping_price;
            }
            if (data.options.custbody_ns_ps_shipping_carrier)   {
                delete data.options.custbody_ns_ps_shipping_carrier;
            }
            if (data.options.custbody_ns_ps_shipping_method)   {
                delete data.options.custbody_ns_ps_shipping_method;
            }
        }
    });

	_.extend(LiveOrder, {

        // This is for the Ship Estimator use case
        populateDefaultShipMethods: function populateDefaultShipMethods(response)  
        {
            if (response.shipmethods && response.shipmethods.length == 0)   {
                var tempShips = [];
                tempShips.push({
                    internalid: '18423',
                    name: 'Commercial Freight (LTL)',
                    shipcarrier: 'nonups',
                    rate: 0,
                    rate_formatted: 'Free!'
                });
                tempShips.push({
                    internalid: '23707',
                    name: 'FedEx/UPS Ground',
                    shipcarrier: 'nonups',
                    rate: 0,
                    rate_formatted: 'Free!'
                });
                tempShips.push({
                    internalid: '23708',
                    name: 'Freight Quote',
                    shipcarrier: 'nonups',
                    rate: 0,
                    rate_formatted: ''
                });

                response.shipmethods = tempShips;
            }
        },

		// First we must look at the Shipping Address, then we can dive into the Cart Items
		applyCustomShippingCalculations: function applyCustomShippingCalculations(Model, response)	
		{
			//nlapiLogExecution('DEBUG', '*** applyCustomShippingCalculations: START');
			var orderSummary = order.getFieldValues(['summary']);
			var orderSubtotal = _.pluck(orderSummary, 'subtotal');
			//nlapiLogExecution('DEBUG', '0 - applyCustomShippingCalculations: orderSubtotal', orderSubtotal);

			// 0 - Must have lines in order.  Note that initially we read Profile.isLoggedIn, but when session expires and post-login,
			// the isLoggedIn flag is not immediately set to true.
			if (_.isEmpty(response.lines))	{
				//TODO: Message
				//nlapiLogExecution('DEBUG', '*** applyCustomShippingCalculations: proceed no further');
				return;
			}

			// 1 - Ship Address
			var shipAddress = order.getFieldValues(['shipaddress']);
			var state;
			if (shipAddress && shipAddress.shipaddress && shipAddress.shipaddress.state)	{
				state = shipAddress.shipaddress.state;
			} else if (shipAddress && shipAddress.shipaddress && shipAddress.shipaddress.zip)	{
				// This is for the Shipping Estimator
                var estimatedZip = shipAddress.shipaddress.zip;
                if (estimatedZip && estimatedZip.length >= 2)   {
                    var zipPrefix = estimatedZip.substring(0, 2);
                    if ('96' == zipPrefix)  {
                        state = 'HI';
                    } else if ('99' == zipPrefix)   {
                        state = 'AK';
                    }
                }
                // This is a hack to identify that we are coming from the Estimator
                isCartEstimator = true;

			} else   {
                //TODO: error message?  or is this shouldn't even happen?

            }
        	//nlapiLogExecution('DEBUG', '1 - applyCustomShippingCalculations: state', state);
        	//nlapiLogExecution('DEBUG', '1 - applyCustomShippingCalculations: shipAddress', JSON.stringify(shipAddress));

        	// 2 - Items
			var fieldValues = order.getFieldValues(
	        	{
	        		items: [
	                    'orderitemid'
	                ,   'internalid'
	                ,	'options'
	                ,	'custitem_freight_quote_required'
	                ,	'custitem_expedited_delivery_available'
	                ,	'custitem_free_shipping_floor'
	                ,	'custitem_below_free_ship_floor_charge'
	                ,	'custitem_ships_to_hi_or_ak'
	                ,	'custitem_hi_ak_ship_charge_each'
	                ,	'custitem_hi_ak_base_shipping_charge'
	            	]
	        	}
        	);

			// Process Hawaii and Alaska differently
        	if ('HI' == state || 'AK' == state)	{
        		this.processShipToHawaiiOrAlaska(fieldValues, response);
        		return;
        	}

			// 2.1 - Item - if one Item in the order has Freight Required = T
        	var atLeastOneItemFreightRequired = false;
        	_.each(fieldValues.items, function(item)	{
        		//nlapiLogExecution('DEBUG', '2 - applyCustomShippingCalculations: item.internalid', item['internalid']);
        		//nlapiLogExecution('DEBUG', '2 - applyCustomShippingCalculations: item.custitem_freight_quote_required', item['custitem_freight_quote_required']);
        		if (item['custitem_freight_quote_required'])	{
        			atLeastOneItemFreightRequired = true;
        		}
        	});
        	//nlapiLogExecution('DEBUG', '2 - applyCustomShippingCalculations: atLeastOneItemFreightRequired', atLeastOneItemFreightRequired);

        	//If atLeastOneItemFreightRequired == true, then only Shipping Method = Freight Quote (id=23708) is available + MSG in Cart
        	if (atLeastOneItemFreightRequired)	{

        		//nlapiLogExecution('DEBUG', '2 - applyCustomShippingCalculations: defaultShipMethods', JSON.stringify(response.shipmethods));
        		this.getHighestShippingFloorValue(fieldValues, response, orderSubtotal, true);
	            //nlapiLogExecution('DEBUG', '2 - applyCustomShippingCalculations: newShipMethods', JSON.stringify(response.shipmethods));
        	}
        	else	{
        		this.getHighestShippingFloorValue(fieldValues, response, orderSubtotal, false);
        	}

        	// 2.2 - Item - Check Expedited Shipping Availability
        	this.checkExpeditedAvailability(fieldValues, response);
				
			//nlapiLogExecution('DEBUG', '*** applyCustomShippingCalculations: END');
		},

		getHighestShippingFloorValue: function getHighestShippingFloorValue(fieldValues, response, orderSubtotal, isForFreightQuote)	
		{
			//nlapiLogExecution('DEBUG', '++ getHighestShippingFloorValue: START');
			var highestShipFloor = 0;
			var shipFloorCostToApply = 0;

			// Figure out the Highest Shipping Floor and the Charge To Apply
			_.each(fieldValues.items, function(item)	{
				var freeShipFloor = item['custitem_free_shipping_floor'];
				var shipFloorCharge = item['custitem_below_free_ship_floor_charge'];

        		//nlapiLogExecution('DEBUG', '2.1 - getHighestShippingFloorValue: freeShipFloor', freeShipFloor);
        		//nlapiLogExecution('DEBUG', '2.1 - getHighestShippingFloorValue: shipFloorCharge', shipFloorCharge);

        		if (freeShipFloor > highestShipFloor)	{
        			highestShipFloor = freeShipFloor;
        			shipFloorCostToApply = shipFloorCharge;
        			//nlapiLogExecution('DEBUG', '2.1 - getHighestShippingFloorValue: NEW freeShipFloor', highestShipFloor);
        			//nlapiLogExecution('DEBUG', '2.1 - getHighestShippingFloorValue: NEW shipFloorCharge', shipFloorCostToApply);
        		}
        	});
        	//nlapiLogExecution('DEBUG', '2.1 - getHighestShippingFloorValue: FINAL highestShipFloor', highestShipFloor);
        	//nlapiLogExecution('DEBUG', '2.1 - getHighestShippingFloorValue: PRE-FINAL shipFloorCostToApply', shipFloorCostToApply);

        	//nlapiLogExecution('DEBUG', '2.1 - getHighestShippingFloorValue: defaultShipMethods', JSON.stringify(response.shipmethods));

        	// If Order SubTotal < Shipping Floor Charge, then use the Shipping Floor Charge, otherwise Shipping Cost is zero.
            var isFloorMet = orderSubtotal > highestShipFloor;
        	if (isFloorMet)	{
        		shipFloorCostToApply = 0;
        	}
        	//nlapiLogExecution('DEBUG', '2.1 - getHighestShippingFloorValue: FINAL shipFloorCostToApply', shipFloorCostToApply);

            //nlapiLogExecution('DEBUG', '2.1 - getHighestShippingFloorValue: isForFreightQuote', isForFreightQuote);
            //nlapiLogExecution('DEBUG', '2.1 - getHighestShippingFloorValue: isFloorMet', isFloorMet);

            if (isForFreightQuote)  {
                if (isFloorMet) {
                    // Set the Rate
                    _.each(response.shipmethods, function(shipmethod) {
                        //nlapiLogExecution('DEBUG', '2.1 - getHighestShippingFloorValue: shipmethod.internalid', shipmethod.internalid);
                        // Ship Method is Commercial Freight
                        if ('18423' == shipmethod.internalid)   {
                            shipmethod.rate = shipFloorCostToApply; // will be 0.00
                            shipmethod.rate_formatted = 'Free!';
                        }
                    });

                    // Set the Shipping Method
                    var commercialFreighShipMethods = _.findWhere(response.shipmethods, {internalid: '18423'});
                    //nlapiLogExecution('DEBUG', '2.1 - getHighestShippingFloorValue: commercialFreighShipMethods', JSON.stringify(commercialFreighShipMethods));
                    if (commercialFreighShipMethods) {
                        response.shipmethods = [];
                        response.shipmethods.push(commercialFreighShipMethods);
                        response.shipmethod = '18423';

                        // Ship Estimator specific fields
                        response.estimatedShipCost = shipFloorCostToApply; // will be 0.00
                        response.estimatedShipCost_formatted = 'Free';
                        response.notAllItemsMarkedShipsToAlaskaHawaiiErrorShipEstimatorMsg = '';
                        response.freightQuoteRequiredMessageToDisplay = '';
                    }
                } else  {
                    // Set the Rate
                    _.each(response.shipmethods, function(shipmethod) {
                        // Ship Method is Freight Quote
                        if ('23708' == shipmethod.internalid)   {
                            shipmethod.rate = 0; // will be 0.00
                            shipmethod.rate_formatted = '';
                        }
                    });

                    // Ship Method is Freight Quote
                    var freighQuoteShipMethods = _.findWhere(response.shipmethods, {internalid: '23708'});
                    // assign filtered shipping methods to shipmethods
                    if (freighQuoteShipMethods) {
                        response.shipmethods = [];
                        response.shipmethods.push(freighQuoteShipMethods);
                        response.shipmethod = '23708';
                        response.freightQuoteRequiredMessageToDisplay = 'Because of length or weight restrictions, one or more of the items you’ve chosen needs to be shipped via commercial freight and a freight quote is required for your order. You can continue checkout of your order and we will follow up in one business day with the amount of the freight quote. Don’t worry, we won’t finalize or ship your order until you’ve had a chance to review and approve everything. Feel free to contact us at (888) 349-4660 with any questions.';

                        // Ship Estimator specific fields
                        response.estimatedShipCost = shipFloorCostToApply; // will be 0.00
                        response.estimatedShipCost_formatted = 'n/a';
                        response.notAllItemsMarkedShipsToAlaskaHawaiiErrorShipEstimatorMsg = '';
                    }
                }
            } else {
                // Set the Rate
                _.each(response.shipmethods, function(shipmethod) {
                    if ('23707' == shipmethod.internalid)   {
                        shipmethod.rate = shipFloorCostToApply;
                        if (shipFloorCostToApply == 0)  {
                            shipmethod.rate_formatted = 'Free!';
                        } else {
                            shipmethod.rate_formatted = Utils.formatCurrency(shipFloorCostToApply);
                        }                      
                    }
                });

                // Set the Shipping Method
                var groundShipMethods = _.findWhere(response.shipmethods, {internalid: '23707'});
                if (groundShipMethods) {
                    response.shipmethods = [];
                    response.shipmethods.push(groundShipMethods);
                    response.shipmethod = '23707';

                    // Ship Estimator specific fields
                    response.estimatedShipCost = shipFloorCostToApply;
                    response.estimatedShipCost_formatted = Utils.formatCurrency(shipFloorCostToApply);
                    response.notAllItemsMarkedShipsToAlaskaHawaiiErrorShipEstimatorMsg = '';
                    response.freightQuoteRequiredMessageToDisplay = '';
                }
            }
        	
            //nlapiLogExecution('DEBUG', '2.1 - getHighestShippingFloorValue: newShipMethods', JSON.stringify(response.shipmethods));
			//nlapiLogExecution('DEBUG', '++ getHighestShippingFloorValue: END');
		},

		processShipToHawaiiOrAlaska: function processShipToHawaiiOrAlaska(fieldValues, response)	
		{
			//nlapiLogExecution('DEBUG', '++ processShipToHawaiiOrAlaska: START');

			var atLeastOneItemDoesNotHaveShipsToHiAk = false;
        	_.each(fieldValues.items, function(item)	{
        		//nlapiLogExecution('DEBUG', '++ processShipToHawaiiOrAlaska: item.internalid', item['internalid']);
        		//nlapiLogExecution('DEBUG', '++ processShipToHawaiiOrAlaska: item.custitem_ships_to_hi_or_ak', item['custitem_ships_to_hi_or_ak']);
        		if (!item['custitem_ships_to_hi_or_ak'])	{
        			atLeastOneItemDoesNotHaveShipsToHiAk = true;
        		}
        	});
        	//nlapiLogExecution('DEBUG', '++ processShipToHawaiiOrAlaska: atLeastOneItemDoesNotHaveShipsToHiAk', atLeastOneItemDoesNotHaveShipsToHiAk);

        	//If atLeastOneItemDoesNotHaveShipsToHiAk == true, then Error Message (TODO: and block?)
        	var hawaiiAndAlaskaTotalShipCharge = 0;
        	if (atLeastOneItemDoesNotHaveShipsToHiAk)	{
        		var notAllItemsMarkedShipsToAlaskaHawaiiErrorMsg = 'Oh no!  Unfortunately, one or more of the items in your shopping cart is not available to ship to a Hawaii or Alaska address and we are unable to continue your order at this time.  This may be due to the length or weight of a particular item.  If you would like to discuss what options are available, please feel free to call us at (888) 349-4660, Monday thru Friday, 8AM-5PM CST.';
        		response.notAllItemsMarkedShipsToAlaskaHawaiiErrorMsg = notAllItemsMarkedShipsToAlaskaHawaiiErrorMsg;
        		response.shipmethods = []; // TODO: verify whether this is what we want to doresponse.shipmethods = [];
	            response.shipmethod = '';
                response.notAllItemsMarkedShipsToAlaskaHawaii = true;

                // Ship Estimator specific fields
                response.estimatedShipCost = 0;
                response.estimatedShipCost_formatted = '';
                response.freightQuoteRequiredMessageToDisplay = '';
                response.notAllItemsMarkedShipsToAlaskaHawaiiErrorShipEstimatorMsg = 'Most items can be shipped to Alaska and Hawaii. However, some large or hazardous material items may have limited shipping options. You cannot complete this order online. Please call us at (888) 349-4660 so that we can discuss potential options with you.'
        	} else	{
        		// Add up all Items' Base Charge + Per Pound Charge
        		var baseChargeToApply = 0;
        		_.each(fieldValues.items, function(item)	{
	        		var itemBaseCharge = item['custitem_hi_ak_base_shipping_charge'];
	        		var itemComputedPerPoundCharge = item['custitem_hi_ak_ship_charge_each'];

	        		// Find the highest item Base Charge to apply
	        		if (itemBaseCharge > baseChargeToApply)	{
	        			baseChargeToApply = itemBaseCharge;
	        		}

	        		//nlapiLogExecution('DEBUG', '++ processShipToHawaiiOrAlaska: hawaiiAndAlaskaTotalShipCharge', hawaiiAndAlaskaTotalShipCharge);
	        		//nlapiLogExecution('DEBUG', '++ processShipToHawaiiOrAlaska: itemBaseCharge', itemBaseCharge);
	        		//nlapiLogExecution('DEBUG', '++ processShipToHawaiiOrAlaska: itemComputedPerPoundCharge', itemComputedPerPoundCharge);

	        		hawaiiAndAlaskaTotalShipCharge += itemComputedPerPoundCharge;
        		});
        		hawaiiAndAlaskaTotalShipCharge += baseChargeToApply;
        		//nlapiLogExecution('DEBUG', '++ processShipToHawaiiOrAlaska: hawaiiAndAlaskaTotalShipCharge', hawaiiAndAlaskaTotalShipCharge);

        		// Ship Method is Ground (id=23707), set the rate
        		_.each(response.shipmethods, function(shipmethod) {
	        		if ('23707' == shipmethod.internalid)	{
	        			shipmethod.rate = hawaiiAndAlaskaTotalShipCharge;
	        			shipmethod.rate_formatted = Utils.formatCurrency(hawaiiAndAlaskaTotalShipCharge);
	        			shipmethod.isActive = true;
	        		}
	        	});	
	        	// Ship Method is Ground (id=23707), set the ship method
	    		var tempShipMethods = _.findWhere(response.shipmethods, {internalid: '23707'});
	            if (tempShipMethods) {
	            	response.shipmethods = [];
	                response.shipmethods.push(tempShipMethods);
	                response.shipmethod = '23707';

                    // Ship Estimator specific fields
                    response.estimatedShipCost = hawaiiAndAlaskaTotalShipCharge;
                    response.estimatedShipCost_formatted = Utils.formatCurrency(hawaiiAndAlaskaTotalShipCharge);
                    response.notAllItemsMarkedShipsToAlaskaHawaiiErrorShipEstimatorMsg = '';
                    response.freightQuoteRequiredMessageToDisplay = '';
	            }
        	}
        	//nlapiLogExecution('DEBUG', '++ processShipToHawaiiOrAlaska: END');
		},

		checkExpeditedAvailability: function checkExpeditedAvailability(fieldValues, response)	
		{
			//nlapiLogExecution('DEBUG', '++ atLeastOneItemFreightRequired: START');

			var itemAvailableForExpeditedShippingCount = 0;
			var allItemInOrderAvailableForExpeditedShipping = false;
			var atLeastOneItemInOrderAvailableForExpeditedShipping = false;
			var itemCartCount = fieldValues.items.length;

        	_.each(fieldValues.items, function(item)	{
        		//nlapiLogExecution('DEBUG', '++ atLeastOneItemFreightRequired: item.internalid', item['internalid']);
        		//nlapiLogExecution('DEBUG', '++ atLeastOneItemFreightRequired: item.custitem_expedited_delivery_available', item['custitem_expedited_delivery_available']);
        		if (item['custitem_expedited_delivery_available'])	{
        			itemAvailableForExpeditedShippingCount++;
        		}
        	});

        	// Now determine whether we have ONE OR MORE BUT NOT ALL or ALL
        	if (itemCartCount == itemAvailableForExpeditedShippingCount)	{
        		allItemInOrderAvailableForExpeditedShipping = true;
        	} else if (itemAvailableForExpeditedShippingCount < itemCartCount && itemAvailableForExpeditedShippingCount >= 1)	{
        		atLeastOneItemInOrderAvailableForExpeditedShipping = true;
        	}

        	// Message to display
        	var expeditedMessageToDisplay = '';
        	if (allItemInOrderAvailableForExpeditedShipping)	{
        		expeditedMessageToDisplay = "Great news! Expedited shipping is available as an option for all items in your shopping cart. To receive a quote, please give us a quick call at <a href='tel:+18883494660'>(888) 349-4660</a> so that we can discuss pricing and delivery timeframes with you.";
        	} else if (atLeastOneItemInOrderAvailableForExpeditedShipping)	{
        		expeditedMessageToDisplay = "Great news! Expedited shipping is available as an option for some, but not all of the items in your shopping cart. To receive a quote, please give us a quick call at <a href='tel:+18883494660'>(888) 349-4660</a> so that we can discuss pricing and delivery timeframes with you.";
        	}
        	response.expeditedMessageToDisplay = expeditedMessageToDisplay;

        	//nlapiLogExecution('DEBUG', '++ atLeastOneItemFreightRequired: expeditedMessageToDisplay', expeditedMessageToDisplay);
        	//nlapiLogExecution('DEBUG', '++ atLeastOneItemFreightRequired: END');
		}

	});
});
