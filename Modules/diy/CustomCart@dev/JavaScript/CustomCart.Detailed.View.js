/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Cart
define('CustomCart.Detailed.View'
,	[	'ErrorManagement'
	,	'Backbone.CompositeView'
	,	'Cart.Summary.View'
	,	'GlobalViews.Message.View'
	,	'Backbone.CollectionView'
	,	'ItemViews.Cell.Actionable.View'
	,	'ItemViews.RelatedItem.View'
	,	'RecentlyViewedItems.View'
	,	'ItemRelations.Related.View'
	,	'ItemRelations.Correlated.View'
	,	'Cart.Item.Summary.View'
	,	'Cart.Item.Actions.View'
	,	'SC.Configuration'
	,	'Backbone.FormView'

	,	'Cart.Detailed.View'
	,	'LiveOrder.Model'

	,	'cart_detailed.tpl'

	,	'jQuery'
	,	'Backbone'
	,	'underscore'
	,	'Utils'

	,	'jQuery.scStickyButton'
	]
,	function (
		ErrorManagement
	,	BackboneCompositeView
	,	CartSummaryView
	,	GlobalViewsMessageView
	,	BackboneCollectionView
	,	ItemViewsActionableView
	,	ItemViewsRelatedItemView
	,	RecentlyViewedItemsView
	,	ItemRelationsRelatedView
	,	ItemRelationsCorrelatedView
	,	CartItemSummaryView
	,	CartItemActionsView
	,	Configuration
	,	BackboneFormView

	,	CartDetailedView
	,	LiveOrder

	,	cart_detailed_tpl

	,	jQuery
	,	Backbone
	,	_
	)
{
	'use strict';

	//Browser title change
	CartDetailedView.prototype.title = 'Shopping Cart | DIYHomeCenter.com'

	// CUSTOMIZATION
	CartDetailedView.prototype.estimateTaxShip = function estimateTaxShip(e)	{

		var defaultCountry = 'US';
		var options = jQuery(e.target).serializeObject()
		,	address_internalid = options.zip + '-' + defaultCountry + '-null';

		e.preventDefault();

		this.model.get('addresses').push({
			internalid: address_internalid
		,	zip: options.zip
		,	country: defaultCountry
		});

		this.model.set('shipaddress', address_internalid);

		var promise = this.saveForm(e);

		if (promise)
		{
			this.swapEstimationStatus();
			promise.done(jQuery.proxy(this, 'showContent'));
		}

	};

	//rail length sibling indentFix
	CartDetailedView.prototype.indentFix = function indentFix() {

		// TODO: AH REVIEW
		if (SC.isPageGenerator()) {
			return;
		}

		var indentInterval = setInterval(function(){
			railLengthSiblingIndentFix();
			// if ($('.item-views-cell-actionable-selected-options-cell:has(span:contains("Rail Length")) + .item-views-cell-actionable-selected-options-cell .item-views-selected-option-color-label').hasClass('rail-length-sibling-indent-fix')) {
			// 	clearInterval(indentInterval);
			// };
		}, 100);

		function railLengthSiblingIndentFix(){
			$('.item-views-cell-actionable-selected-options-cell + .item-views-cell-actionable-selected-options-cell .item-views-selected-option-color-label').addClass('rail-length-sibling-indent-fix');

			$('.item-views-cell-actionable-selected-options-cell:has(label:contains("Frame Color")) + .item-views-cell-actionable-selected-options-cell .rail-length-sibling-indent-fix + li').addClass('frame-color-sibling-indent-reverse');
			$('.item-views-selected-option-color-label:has(.adjust-label-width) + li + .item-views-selected-option-color-text').addClass('adjust-small-word');
		};

	}();

});
