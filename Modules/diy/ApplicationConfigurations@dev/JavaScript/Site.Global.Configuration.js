define('Site.Global.Configuration', [
    'SC.Configuration',
    
    'item_views_option_tile.tpl',
    'item_views_selected_option.tpl',
    'item_views_option_color.tpl',
    'item_views_selected_option_color.tpl',
    'item_views_option_text.tpl',
    'item_views_option_dropdown.tpl',
    
    'facets_item_cell_grid.tpl',
    'facets_item_cell_list.tpl',
    'facets_item_cell_table.tpl',
    
    'underscore'
], function SiteGlobalConfiguration(
    Configuration,
        
    itemViewsOptionTileTemplate,
    itemViewsSelectedOptionTemplate,
    itemViewsOptionColorTemplate,
    itemViewsSelectedOptionColorTemplate,
    itemViewsOptionTextTemplate,
    itemViewsOptionDropdownTemplate,
    
    facets_item_cell_grid_tpl,
    facets_item_cell_list_tpl,
    facets_item_cell_table_tpl
) {
    'use strict';
    
    
    jQuery('<link />', {
    rel: "stylesheet"
    , href: "//fast.fonts.net/cssapi/a0ed2f16-dbd4-470c-bcc1-f5af4c6436d8.css"
    }).appendTo(jQuery('head'));
    
    var colors = {
        // custcol_adl_color
        custcol_adl_color:   {
            'Black': '#272324',
            'Bronze': '#70665a',
            'Clay': '#ddcbb3',
            'Cool White': '#f9f8f3',
            'Matte Black': '#10110c',
            'Tan': '#f7f5e6',
            'Warm White': '#ffe2b5',
            'White': '#fcfcfc',
            'Warm White Lights': '#ffe2b5',
            'Cool White Lights': '#f9f8f3',
            'Blue': '#95d1f5',
            'Copper': '#b74811'
        },
        // custcol_aqu_color
        custcol_aqu_color:   {
            'Black': '#ededed',
            'Blue Stone': '#ededed',
            'Burnt Umber': '#ededed',
            'Crushed Coral': '#ededed',
            'Desert Granite': '#ededed',
            'European Terra Cotta': '#ededed',
            'Gray Slate': '#ededed',
            'Green Slate': '#ededed',
            'Mixed': '#ededed',
            'Mixed/Black': '#ededed',
            'Natural Limestone': '#ededed',
            'Powdered Terra Cotta': '#ededed',
            'Sandstone': '#ededed',
            'Terra Cotta': '#ededed'
        },
        // custcol_awg_color
        custcol_awg_color:   {
            'Black': '#ededed',
            'Blue': '#ededed',
            'Crystal White': '#f6f5fb',
            'Desert': '#836e4f',
            'Fire Red': '#de2a33',
            'Great Lakes': '#7c674a',
            'Grey': '#8a8b86',
            'Ice Blue': '#3a56a9',
            'Mountain': '#95968e',
            'SOL White': '#dfc47f',
            'Southern': '#685848',
            'Tan': '#d8c8a4'
        },
        // custcol_azk_color
        custcol_azk_color:   {
            'Architectural Bronze': '#7f6032',
            'Black': '#000000',
            'Brownstone': '#8f745f',
            'Kona': '#45302b',
            'Slate Grey': '#8c817d',
            'White': '#ffffff'
        },
        // custcol_bwp_color
        custcol_bwp_color:   {
            'Beige': '#d0b387',
            'Bronze': '#bf9543',
            'Gray': '#b4b6b3',
            'Oat': '#b8a784',
            'White': '#ffffff'
        },
        // custcol_cop_color
        custcol_cop_color:   {
            'Brushed Nickel': '#ededed',
            'Electroless Nickel': '#ededed',
            'Oil Rubbed Bronze': '#ededed'
        },
        // custcol_dck_base_color
        custcol_dck_base_color:   {
            'Black': '#202020',
            'Bronze': '#4d4946',
            'White': '#fefefe'
        },
        // custcol_dck_color
        custcol_dck_color:   {
            'Black': '#202020',
            'Bronze': '#4d4946',
            'Cedar': '#92653c',
            'Clay': '#7f7d71',
            'Copper': '#cf8868',
            'Dark Walnut': '#55403b',
            'Forest': '#213637',
            'Gray': '#909189',
            'Green': '#7d8375',
            'Mahogany': '#684935',
            'Matte Black': '#202020',
            'Matte Gray': '#747570',
            'Matte White': '#e8e9e4',
            'Pewter': '#706f74',
            'Real Patina': '#98c8bc',
            'Redwood': '#8a5042',
            'Rust': '#795149',
            'Satin Black': '#2e2e2e',
            'Stainless': '#d6dde7',
            'Stainless Plated': '#555344',
            'Textured White': '#dedfda',
            'Verde Green': '#53594f',
            'Weathered Brown': '#372523',
            'White': '#fefefe',
            'Wood Grain': '#c48562'
        },
        // custcol_dkr_color
        custcol_dkr_color:   {
            'Autumn Bluff': '#aa846d',
            'Brown Granite': '#908268',
            'Cottage Gray': '#a2a6a9',
            'Fieldstone': '#9c9289',
            'Green': '#36aa59',
            'Green Turf': '#0e4832',
            'Riverbed': '#a7928d',
            'Sandstone': '#bfad95',
            'Tan': '#f1decf'
        },
        // custcol_dtx_color
        custcol_dtx_color:   {
            'Bird&#39;s Eye': '#ebe8e1 url(site/swatches/trex-furniture/birds-eye.jpg) center center',
            'Bravada Salsa': '#ededed url(site/swatches/trex-furniture/bravada-salsa.jpg) center center',
            'Charcoal Black': '#2a2627 url(site/swatches/trex-furniture/charcoal-black.jpg) center center',
            'Chili': '#833f2a url(site/swatches/trex-furniture/chili.jpg) center center',
            'Classic White': '#f6f6f6 url(site/swatches/trex-furniture/classic-white.jpg) center center',
            'Island Mist': '#736b69 url(site/swatches/trex-furniture/island-mist.jpg) center center',
            'Island Mist Table Top with Sapphire Sling': '#ededed url(site/swatches/trex-furniture/island-mist-table-top-with-sapphire-sling.jpg) center center',
            'Lava Rock': '#200403 url(site/swatches/trex-furniture/lava-rock.jpg) center center',
            'Macaw': '#a1b039 url(site/swatches/trex-furniture/macaw.jpg) center center',
            'Navy': '#13182c url(site/swatches/trex-furniture/navy.jpg) center center',
            'Rainforest Canopy': '#303a32 url(site/swatches/trex-furniture/rainforest-canopy.jpg) center center',
            'Sand Castle': '#c7bdb1 url(site/swatches/trex-furniture/sand-castle.jpg) center center',
            'Satin Bronze': '#332f30 url(site/swatches/trex-furniture/satin-bronze.jpg) center center',
            'Satin Silver': '#a9a8b0 url(site/swatches/trex-furniture/satin-silver.jpg) center center',
            'Satin White': '#efebe8 url(site/swatches/trex-furniture/satin-white.jpg) center center',
            'Sesame': '#99a69d url(site/swatches/trex-furniture/sesame.jpg) center center',
            'Spa': '#a9b6ad url(site/swatches/trex-furniture/spa.jpg) center center',
            'Spiced Rum': '#805d47 url(site/swatches/trex-furniture/spiced-rum.jpg) center center',
            'Stepping Stone': '#8b8784 url(site/swatches/trex-furniture/stepping-stone.jpg) center center',
            'Tangerine': '#f59737 url(site/swatches/trex-furniture/tangerine.jpg) center center',
            'Tiki Torch': '#a57c5c url(site/swatches/trex-furniture/tiki-torch.jpg) center center',
            'Tiki Torch Table Top with Sapphire Sling': '#ededed url(site/swatches/trex-furniture/tiki-torch-table-top-with-sapphire-sling.jpg) center center',
            'Tree House': '#6e4b35 url(site/swatches/trex-furniture/tree-house.jpg) center center',
            'Vintage Lantern': '#4d342d url(site/swatches/trex-furniture/vintage-lantern.jpg) center center'
        },
        // custcol_dtx_cushion_color
        custcol_dtx_cushion_color:   {
            'Bird&#39;s Eye': '#ebe8e1 url(site/swatches/trex-furniture/birds-eye.jpg) center center',
            'Bravada Salsa': '#ededed url(site/swatches/trex-furniture/brada-salsa.jpg) center center',
            'Chili': '#833f2a url(site/swatches/trex-furniture/chili.jpg) center center',
            'Macaw': '#a1b039 url(site/swatches/trex-furniture/macaw.jpg) center center',
            'Navy': '#13182c url(site/swatches/trex-furniture/navy.jpg) center center',
            'Sesame': '#99a69d url(site/swatches/trex-furniture/sesame.jpg) center center',
            'Spa': '#a9b6ad url(site/swatches/trex-furniture/spa.jpg) center center',
            'Tangerine': '#f59737 url(site/swatches/trex-furniture/tangerine.jpg) center center'
        },
        // custcol_dtx_frame_color
        custcol_dtx_frame_color:   {
            'Charcoal Black': '#2a2627 url(site/swatches/trex-furniture/charcoal-black.jpg) center center',
            'Classic White': '#f6f6f6 url(site/swatches/trex-furniture/classic-white.jpg) center center',
            'Rainforest Canopy': '#303a32 url(site/swatches/trex-furniture/rainforest-canopy.jpg) center center',
            'Sand Castle': '#c7bdb1 url(site/swatches/trex-furniture/sand-castle.jpg) center center',
            'Satin Bronze': '#332f30 url(site/swatches/trex-furniture/satin-bronze.jpg) center center',
            'Satin Silver': '#a9a8b0 url(site/swatches/trex-furniture/satin-silver.jpg) center center',
            'Satin White': '#efebe8 url(site/swatches/trex-furniture/satin-white.jpg) center center',
            'Textured Black': '#46454b url(site/swatches/trex-furniture/textured-black.jpg) center center',
            'Textured Bronze': '#3f3637 url(site/swatches/trex-furniture/textured-bronze.jpg) center center',
            'Textured Silver': '#d5d3d4 url(site/swatches/trex-furniture/textured-silver.jpg) center center',
            'Textured White': '#eae7e2 url(site/swatches/trex-furniture/textured-white.jpg) center center',
            'Tree House': '#6e4b35 url(site/swatches/trex-furniture/tree-house.jpg) center center',
            'Vintage Lantern': '#4d342d url(site/swatches/trex-furniture/vintage-lantern.jpg) center center'
        },
        // custcol_dtx_seat_color
        custcol_dtx_seat_color:   {
            'Driftwood': '#cebfaa url(site/swatches/trex-furniture/driftwood.jpg) center center',
            'Spiced Rum': '#805d47 url(site/swatches/trex-furniture/spiced-rum.jpg) center center',
            'Tiki Torch': '#a57c5c url(site/swatches/trex-furniture/tiki-torch.jpg) center center',
            'Island Mist': '#736b69 url(site/swatches/trex-furniture/island-mist.jpg) center center',
            'Lava Rock': '#200403 url(site/swatches/trex-furniture/lava-rock.jpg) center center',
            'Montego': '#9b8b69 url(site/swatches/trex-furniture/montego.jpg) center center',
            'Onyx': '#8e8781 url(site/swatches/trex-furniture/onyx.jpg) center center'
        },
        // custcol_dtx_slat_color
        custcol_dtx_slat_color:   {
            'Charcoal Black': '#2a2627 url(site/swatches/trex-furniture/charcoal-black.jpg) center center',
            'Charcoal White': '#2e2d2b url(site/swatches/trex-furniture/charcoal-white.jpg) center center',
            'Classic White': '#f6f6f6 url(site/swatches/trex-furniture/classic-white.jpg) center center',
            'Island Mist': '#736b69 url(site/swatches/trex-furniture/island-mist.jpg) center center',
            'Lava Rock': '#200403 url(site/swatches/trex-furniture/lava-rock.jpg) center center',
            'Rainforest Canopy': '#303a32 url(site/swatches/trex-furniture/rainforest-canopy.jpg) center center',
            'Sand Castle': '#c7bdb1 url(site/swatches/trex-furniture/sand-castle.jpg) center center',
            'Spiced Rum': '#805d47 url(site/swatches/trex-furniture/spiced-rum.jpg) center center',
            'Stepping Stone': '#8b8784 url(site/swatches/trex-furniture/stepping-stone.jpg) center center',
            'Tiki Torch': '#a57c5c url(site/swatches/trex-furniture/tiki-torch.jpg) center center',
            'Tree House': '#6e4b35 url(site/swatches/trex-furniture/tree-house.jpg) center center',
            'Vintage Lantern': '#4d342d url(site/swatches/trex-furniture/vintage-lantern.jpg) center center'
        },
        // custcol_dws_color
        custcol_dws_color:   {
            'Cedar': '#573601',
            'Concrete Grey': '#c2bbb5',
            'Cumaru': '#a36c33',
            'Garapa': '#b36421',
            'Hardwood Brown': '#a65f27',
            'Ipe': '#784122',
            'Massaranduba': '#843d37',
            'Outdoor Grey': '#575354',
            'Rosy Brown': '#4a1e13',
            'Shadowline Black': '#282828',
            'Stainless Steel': '#979589',
            'Tigerwood': '#a3471e'
        },
        // custcol_dws_fastener_color
        custcol_dws_fastener_color:   {
            'Black': '#040500',
            'Brown': '#904f25',
            'Grey': '#cac9ce'
        },
        // custcol_dws_head_color
        custcol_dws_head_color:   {
            'Black': '#040500',
            'Brown': '#904f25',
            'Cedar': '#573601',
            'Composite Grey': '#c0c0c0',
            'Composite Sand': '#a8a27e',
            'Hardwood Brown': '#a65f27',
            'Rosy Brown': '#4a1e13',
            'Stainless Steel': '#979589'
        },
        // custcol_fen_color
        custcol_fen_color:   {
            'Black': '#282924',
            'Bronze': '#997e6b',
            'Brown': '#75523f',
            'Gray': '#91a29c',
            'White': '#dbdfde'
        },
        // custcol_fmr_color
        custcol_fmr_color:   {
            'Acacia': '#6a514a',
            'Azek Acacia': '#6f564f',
            'Azek Brownstone': '#a9937c',
            'Azek Clay': '#b8ba9e',
            'Azek Cobre': '#916a40',
            'Azek Cypress': '#9b684d',
            'Azek Dark Hickory': '#53423b',
            'Azek Frontier ': '#dfdfdf',
            'Azek Hazelwood': '#8f8a74',
            'Azek Ivory': '#ded4c4',
            'Azek Kona': '#221713',
            'Azek Kona Wood Grain': '#221713',
            'Azek Mahogany': '#311d14',
            'Azek Morado': '#4f3826',
            'Azek Redland Rose': '#946052',
            'Azek Sedona': '#958170',
            'Azek Silver Oak': '#665f59',
            'Azek Slate Gray': '#6a6760',
            'Azek Tahoe': '#929492',
            'Azek Traditional': '#fcfcfc',
            'Azek White': '#eee8e4',
            'Brown': '#a58751',
            'Dark Brown': '#5f442f',
            'Dark Redwood': '#a97560',
            'EverGrain Cape Cod Grey': '#6d6a63',
            'EverGrain Cedar': '#ba8058',
            'EverGrain Redwood': '#855443',
            'EverGrain Weathered Wood': '#6b4d42',
            'Gray': '#808986',
            'Harbor Stone': '#8e9295',
            'Harvest Bronze': '#a1654b',
            'Koma Celuka Smooth': '#ffffff',
            'Koma Free Foam Smooth': '#ffffff',
            'Koma Smooth': '#ffffff',
            'Mahogany': '#8e5b40',
            'River Rock': '#b1b6b0',
            'Rustic Bark': '#955f43',
            'Sand Ridge': '#c5b4aa',
            'Sedona': '#8e7d6d',
            'Spiced Rum': '#61412a',
            'TimberTech Ashwood': '#535049',
            'TimberTech Cedar': '#c0a18d',
            'TimberTech Grey': '#9e9f9a',
            'TimberTech Harvest Bronze': '#9d6853',
            'TimberTech River Rock': '#b0b5b1',
            'TimberTech Rustic Bark': '#906143',
            'TimberTech Sand Ridge': '#c1b4a9',
            'TimberTech Twin Finish Redwood ': '#8f6e64',
            'TimberTech TwinFinish Cedar': '#c0a18d',
            'TimberTech TwinFinish Grey': '#9e9f9a',
            'Trex Accents Madeira': '#542920',
            'Trex Accents Saddle': '#6f3f19',
            'Trex Accents Winchester Gray': '#3b3b43',
            'Trex Accents Woodland Brown': '#3f271d',
            'Trex Enhance Beach Dune': '#936845',
            'Trex Enhance Clam Shell': '#515254',
            'Trex Enhance Saddle': '#744720',
            'Trex Select Madeira': '#75493c',
            'Trex Select Pebble Grey': '#8c8885',
            'Trex Select Saddle': '#996f49',
            'Trex Select Winchester Grey': '#5e5e68',
            'Trex Select Woodland Brown': '#513c37',
            'Trex Smooth': '#ffffff',
            'Trex Transcend Fire Pit': '#6f432e',
            'Trex Transcend Gravel Path': '#75706a',
            'Trex Transcend Havana Gold': '#8f6d46',
            'Trex Transcend Island Mist': '#8c827a',
            'Trex Transcend Lava Rock': '#592c1e',
            'Trex Transcend Rope Swing': '#9d8970',
            'Trex Transcend Spiced Rum': '#795437',
            'Trex Transcend Tiki Torch': '#644533',
            'Trex Transcend Tree House': '#865433',
            'Trex Transcend Vintage Lantern': '#562f34',
            'Trex Wood Grain': '#ffffff',
            'Versatex Smooth': '#ffffff',
            'Versatex Timber Ridge': '#e5dfd8',
            'Walnut Grove': '#5b3c28',
            'White': '#ffffff',
            'Azek Autumn Chestnut': '#5b4b40',
            'Azek Island Oak': '#3c3c3c',
            'Azek Moutain Redwood': '#32211a',
            'Azek Brazilian Walnut': '#2f261e',
            'Fiberon Burnt Umber': '#5b3f31',
            'Fiberon Cinnabar': '#623c30',
            'Fiberon Warm Sienna': '#704d39',
            'Fiberon Ipe': '#ad8562',
            'Fiberon Rosewood': '#654740',
            'Fiberon Tudor Brown': '#8e6554',
            'Fiberon Greystone': '#a9a7a0',
            'Fiberon Castle Gray': '#8a8f9a',
            'Fiberon Western Cedar': '#986b54',
            'Fiberon Gray Birch': '#717373',
            'Fiberon Brownstone': '#95765e',
            'Fiberon Sandstone': '#aba398',
            'Fiberon Flagstone': '#969796',
            'Fiberon Earthstone': '#635853',
            'Fiberon Mineral': '#a4a3a0',
            'Fiberon Fossil': '#b09e8b'
        },
        // custcol_frt_color
        custcol_frt_color:   {
            'Antique Bronze': '#6c6358',
            'Black': '#ededed',
            'Black Sand': '#2b2e35',
            'Clear': '#d6e7e7',
            'Gloss Black': '#242425',
            'Hunter Green': '#ededed',
            'Oil Rubbed Bronze': '#4e4e4f',
            'Rust': '#865550',
            'Smoke': '#ededed',
            'Taupe': '#ededed',
            'White': '#ffffff'
        },
        // custcol_gro_color
        custcol_gro_color:   {
            'Finished': '#f0a954',
            'Unfinished': '#e7b67d'
        },
        // custcol_hpl_color
        custcol_hpl_color:   {
            'Antique Bronze': '#493831',
            'Antique Copper': '#73583A',
            'Black': '#000000',
            'Brushed Stainless': '#9b9794',
            'Midnight Black': '#312E29',
            'Powder Coat White': '#f2f1f6',
            'Solid Copper': '#d29370',
            'Textured Black': '#333438',
            'White': '#ffffff'
        },
        // custcol_knp_color
        custcol_knp_color:   {
            'Black': '#ededed',
            'Titanium': '#ededed',
            'White': '#ededed'
        },
        // custcol_ome_color
        custcol_ome_color:   {
            'Alder': '#ededed',
            'Cherry': '#ededed',
            'Hickory': '#ededed',
            'Maple': '#ededed',
            'Paint Grade': '#ededed',
            'Red Oak': '#ededed'
        },
        // custcol_pal_color
        custcol_pal_color:   {
           'Palight Sand Smooth': '#b1afa3',
           'Palight Sand Wood Grain': '#b2b0a4',
           'Palight White Smooth': '#ececec',
           'Palight White Wood Grain': '#f2f2f0'
        },
        // custcol_pld_color
        custcol_pld_color:   {
            'Aruba': '#3cb2b5 url(site/swatches/polywood/aruba.jpg) center center',
            'Black': '#221f20 url(site/swatches/polywood/black.jpg) center center',
            'Green': '#39463c url(site/swatches/polywood/green.jpg) center center',
            'Lemon': '#fcda3e url(site/swatches/polywood/lemon.jpg) center center',
            'Lime': '#a2b865 url(site/swatches/polywood/lime.jpg) center center',
            'Mahogany': '#4a3227 url(site/swatches/polywood/mahogany.jpg) center center',
            'Pacific Blue': '#2c70b4 url(site/swatches/polywood/pacific-blue.jpg) center center',
            'Pacific Blue Aruba': '#ededed url(site/swatches/polywood/pacific-blue-aruba.jpg) center center',
            'Pacific Blue Lime': '#ededed url(site/swatches/polywood/pacific-blue-lime.jpg) center center',
            'Sand': '#c9bfb4 url(site/swatches/polywood/sand.jpg) center center',
            'Slate Grey': '#787672 url(site/swatches/polywood/slate-grey.jpg) center center',
            'Sunset Red': '#bf2c27 url(site/swatches/polywood/sunset-red.jpg) center center',
            'Sunset Red Tangerine': '#ededed url(site/swatches/polywood/sunset-red-tangerine.jpg) center center',
            'Tangerine': '#e18032 url(site/swatches/polywood/tangerine.jpg) center center',
            'Teak': '#69462f url(site/swatches/polywood/teak.jpg) center center',
            'White': '#ebe7e3 url(site/swatches/polywood/white.jpg) center center'
        },
        // custcol_pld_cushion_color
        custcol_pld_cushion_color:   {
            'Astoria Lagoon': '#ededed url(site/swatches/polywood/astoria-lagoon.jpg) center center',
            'Astoria Sunset': '#ededed url(site/swatches/polywood/astoria-sunset.jpg) center center',
            'Bird&#39;s Eye': '#e6e3c6 url(site/swatches/polywood/birds-eye.jpg) center center',
            'Canvas Black': '#262626 url(site/swatches/polywood/canvas-black.jpg) center center',
            'Canvas Charcoal': '#656565 url(site/swatches/polywood/canvas-charcoal.jpg) center center',
            'Canvas Ginkgo': '#80915e url(site/swatches/polywood/canvas-ginkgo.jpg) center center',
            'Canvas Tuscan': '#c56743 url(site/swatches/polywood/canvas-tuscan.jpg) center center',
            'Chili': '#a26148 url(site/swatches/polywood/chili.jpg) center center',
            'Forest Green': '#2c614a url(site/swatches/polywood/forest-green.jpg) center center',
            'Linen Chili': '#a15e46 url(site/swatches/polywood/linen-chili.jpg) center center',
            'Linen Sesame': '#ad946f url(site/swatches/polywood/linen-sesame.jpg) center center',
            'Logo Red': '#dd3042 url(site/swatches/polywood/logo-red.jpg) center center',
            'Navy': '#1c2739 url(site/swatches/polywood/navy.jpg) center center',
            'Pacific Blue': '#0588c0 url(site/swatches/polywood/pacific-blue-cushion.jpg) center center',
            'Sesame': '#ae946e url(site/swatches/polywood/sesame.jpg) center center',
            'Spa': '#b6cfbc url(site/swatches/polywood/spa.jpg) center center',
            'Sunflower Yellow': '#fac743 url(site/swatches/polywood/sunflower-yellow.jpg) center center',
            'Tangerine': '#f18a50 url(site/swatches/polywood/tangerine-cushion.jpg) center center'
        },
        // custcol_pld_frame_color
        custcol_pld_frame_color:   {
            'Aruba': '#3cb2b5 url(site/swatches/polywood/aruba.jpg) center center',
            'Black': '#221f20 url(site/swatches/polywood/black.jpg) center center',
            'Gloss White': '#f4f4f1 url(site/swatches/polywood/gloss-white.jpg) center center',
            'Green': '#39463c url(site/swatches/polywood/green.jpg) center center',
            'Lemon': '#fcda3e url(site/swatches/polywood/lemon.jpg) center center',
            'Lime': '#a2b865 url(site/swatches/polywood/lime.jpg) center center',
            'Mahogany': '#4a3227 url(site/swatches/polywood/mahogany.jpg) center center',
            'Pacific Blue': '#2c70b4 url(site/swatches/polywood/pacific-blue.jpg) center center',
            'Sand': '#c9bfb4 url(site/swatches/polywood/sand.jpg) center center',
            'Satin Mahogany': '#45291c url(site/swatches/polywood/satin-mahogany.jpg) center center',
            'Satin Tan ': '#c0b8a7 url(site/swatches/polywood/satin-tan.jpg) center center',
            'Satin White': '#eeeeee url(site/swatches/polywood/satin-white.jpg) center center',
            'Satin White with Natural Teak': '#ededed url(site/swatches/polywood/satin-white-with-natural-teak.jpg) center center',
            'Satin White with Natural Teak Arm Rest': '#ededed url(site/swatches/polywood/satin-white-with-natural-teak-arm-rest.jpg) center center',
            'Satin White with White': '#ededed url(site/swatches/polywood/satin-white-with-white.jpg) center center',
            'Satin White with White ': '#ededed url(site/swatches/polywood/satin-white-with-white.jpg) center center',
            'Satin White with White Arm Rest': '#ededed url(site/swatches/polywood/satin-white-with-white-arm-rest.jpg) center center',
            'Slate Grey': '#787672 url(site/swatches/polywood/slate-grey.jpg) center center',
            'Sunset Red': '#bf2c27 url(site/swatches/polywood/sunset-red.jpg) center center',
            'Tangerine': '#e18032 url(site/swatches/polywood/tangerine.jpg) center center',
            'Teak': '#69462f url(site/swatches/polywood/teak.jpg) center center',
            'Textured Black': '#3e3d42 url(site/swatches/polywood/textured-black.jpg) center center',
            'Textured Black with Natural Teak Arm Rest': '#ededed url(site/swatches/polywood/textured-black-with-natural-teak-arm-rest.jpg) center center',
            'Textured Bronze': '#5a4747 url(site/swatches/polywood/textured-bronze.jpg) center center',
            'Textured Bronze with Natural Teak': '#ededed url(site/swatches/polywood/textured-bronze-with-natural-teak.jpg) center center',
            'Textured Bronze with Natural Teak Arm Rest': '#ededed url(site/swatches/polywood/textured-bronze-with-natural-teak-arm-rest.jpg) center center',
            'Textured Silver': '#bfbcbd url(site/swatches/polywood/textured-silver.jpg) center center',
            'Textured Silver with White': '#ededed url(site/swatches/polywood/textured-silver-with-white.jpg) center center',
            'Textured Silver with White Arm Rest': '#ededed url(site/swatches/polywood/textured-silver-with-white-arm-rest.jpg) center center',
            'Textured White': '#ebe7e4 url(site/swatches/polywood/textured-white.jpg) center center',
            'White': '#ececec url(site/swatches/polywood/white.jpg) center center'
        },
        // custcol_pld_seat_color
        custcol_pld_seat_color:   {
            'Aruba': '#3cb2b5 url(site/swatches/polywood/aruba.jpg) center center',
            'Avocado Sling': '#b4ba4c url(site/swatches/polywood/avocado-sling.jpg) center center',
            'Black': '#221f20 url(site/swatches/polywood/black.jpg) center center',
            'Black Sling': '#1e1e1e url(site/swatches/polywood/black-sling.jpg) center center',
            'Burlap Sling': '#847144 url(site/swatches/polywood/burlap-sling.jpg) center center',
            'Citrus Sling': '#c66622 url(site/swatches/polywood/citrus-sling.jpg) center center',
            'Kiwi Sling': '#788040 url(site/swatches/polywood/kiwi-sling.jpg) center center',
            'Lemon': '#fcda3e url(site/swatches/polywood/lemon.jpg) center center',
            'Lime': '#a2b865 url(site/swatches/polywood/lime.jpg) center center',
            'Mahogany': '#4a3227 url(site/swatches/polywood/mahogany.jpg) center center',
            'Metallic Sling': '#6f6c66 url(site/swatches/polywood/metallic-sling.jpg) center center',
            'Navy Blue Sling': '#1c2739 url(site/swatches/polywood/navy-blue-sling.jpg) center center',
            'Navy Sling': '#1c2639 url(site/swatches/polywood/navy-sling.jpg) center center',
            'Pacific Blue': '#2c70b4 url(site/swatches/polywood/pacific-blue.jpg) center center',
            'Poolside Sling': '#8a9bbb url(site/swatches/polywood/poolside-sling.jpg) center center',
            'Royal Blue Sling': '#213884 url(site/swatches/polywood/royal-blue-sling.jpg) center center',
            'Salsa Sling': '#9d3720 url(site/swatches/polywood/salsa-sling.jpg) center center',
            'Sand': '#c9bfb4 url(site/swatches/polywood/sand.jpg) center center',
            'Slate Grey': '#787672 url(site/swatches/polywood/slate-grey.jpg) center center',
            'Sunset Red': '#bf2c27 url(site/swatches/polywood/sunset-red.jpg) center center',
            'Tangerine': '#e18032 url(site/swatches/polywood/tangerine.jpg) center center',
            'Teak': '#69462f url(site/swatches/polywood/teak.jpg) center center',
            'Vibrant Sling': '#ededed url(site/swatches/polywood/vibrant-sling.jpg) center center',
            'Vibrant Slings': '#ededed url(site/swatches/polywood/vibrant-slings.jpg) center center',
            'White': '#ececec url(site/swatches/polywood/white.jpg) center center',
            'White Sling': '#e2e3db url(site/swatches/polywood/white-sling.jpg) center center'
        },
        // custcol_pld_slat_color
        custcol_pld_slat_color:   {
            'Aruba': '#3cb2b5 url(site/swatches/polywood/aruba.jpg) center center',
            'Black': '#221f20 url(site/swatches/polywood/black.jpg) center center',
            'Green': '#39463c url(site/swatches/polywood/green.jpg) center center',
            'Lemon': '#fcda3e url(site/swatches/polywood/lemon.jpg) center center',
            'Lime': '#a2b865 url(site/swatches/polywood/lime.jpg) center center',
            'Mahogany': '#4a3227 url(site/swatches/polywood/mahogany.jpg) center center',
            'Natural Teak': '#bb9466 url(site/swatches/polywood/natural-teak.jpg) center center',
            'Pacific Blue': '#2c70b4 url(site/swatches/polywood/pacific-blue.jpg) center center',
            'Sand': '#c9bfb4 url(site/swatches/polywood/sand.jpg) center center',
            'Slate Grey': '#787672 url(site/swatches/polywood/slate-grey.jpg) center center',
            'Sunset Red': '#bf2c27 url(site/swatches/polywood/sunset-red.jpg) center center',
            'Tangerine': '#e18032 url(site/swatches/polywood/tangerine.jpg) center center',
            'Teak': '#69462f url(site/swatches/polywood/teak.jpg) center center',
            'White': '#ebe7e3 url(site/swatches/polywood/white.jpg) center center'
        },
        // custcol_pld_top_color
        custcol_pld_top_color:   {
            'Aruba': '#3cb2b5 url(site/swatches/polywood/aruba.jpg) center center',
            'Black': '#221f20 url(site/swatches/polywood/black.jpg) center center',
            'Green': '#39463c url(site/swatches/polywood/green.jpg) center center',
            'Lemon': '#fcda3e url(site/swatches/polywood/lemon.jpg) center center',
            'Lime': '#a2b865 url(site/swatches/polywood/lemon.jpg) center center',
            'Mahogany': '#4a3227 url(site/swatches/polywood/mahogany.jpg) center center',
            'Pacific Blue': '#2c70b4 url(site/swatches/polywood/pacific-blue.jpg) center center',
            'Sand': '#c9bfb4 url(site/swatches/polywood/sand.jpg) center center',
            'Slate Grey': '#787672 url(site/swatches/polywood/slate-grey.jpg) center center',
            'Sunset Red': '#bf2c27 url(site/swatches/polywood/sunset-red.jpg) center center',
            'Tangerine': '#e18032 url(site/swatches/polywood/xxx.jpg) center center',
            'Teak': '#69462f url(site/swatches/polywood/teak.jpg) center center',
            'White': '#ebe7e3 url(site/swatches/polywood/white.jpg) center center'
        },
        // custcol_pld_weave_color
        custcol_pld_weave_color:   {
            'Cahaba': '#3a3433 url(site/swatches/polywood/cahaba.jpg) center center',
            'Seagrass': '#a69578 url(site/swatches/polywood/seagrass.jpg) center center',
            'Tigerwood': '#7c624b url(site/swatches/polywood/tigerwood.jpg) center center',
            'White Loom': '#bcb5ae url(site/swatches/polywood/white-loom.jpg) center center'
        },
        // custcol_pnf_color
        custcol_pnf_color:   {
            'Adobe': '#9a846d',
            'Appalachian Oak': '#af7f3f',
            'Cedar': '#934c10',
            'Chestnut': '#b35f1f',
            'Clear': '#a68f65',
            'Cypress': '#602d14',
            'Dark Walnut': '#532a0e',
            'Dolomite': '#c7a490',
            'Eqyptian Alabaster': '#e6b477',
            'Golden Oak': '#f0b360',
            'Ipe': '#8c562a',
            'Mahogany': '#77402c',
            'Medium Walnut': '#8b5c26',
            'Mendocino Mist': '#7f7a5d',
            'Mission Oak': '#6f4936',
            'Nantucket Mist': '#b99a6e',
            'Natural': '#ab793e',
            'Pewter': '#70716c',
            'Rainier': '#8b5f20',
            'Redwood': '#5a220b',
            'Rosewood': '#782f19',
            'Sable': '#b27d31',
            'Sea Ranch': '#867c70',
            'Seal': '#50463a',
            'Sierra': '#934111',
            'Slate': '#3d5e4d',
            'Suede': '#a69079',
            'Tahoe': '#a26512',
            'Terra Cotta': '#795241',
            'Tigerwood': '#94473f',
            'Transparent Bark': '#692b38',
            'Transparent Cedar': '#bb770c',
            'Transparent Chestnut': '#995e1c',
            'Transparent Hickory': '#614110',
            'Transparent Mission Brown': '#a15e2a',
            'Transparent Natural': '#7b4322',
            'Transparent Redwood': '#834634',
            'Transparent Sable': '#6d4d14',
            'Transparent Sierra': '#8f3d0d',
            'Transparent Western Red Cedar': '#b05d1b',
            'Western Red Cedar': '#c4854f',
            'Yosemite': '#6c400f'
        },
        // custcol_rdi_color
        custcol_rdi_color:   {
            'Satin Black': '#343434',
            'White': '#eeeeee'
        },
        // custcol_rev_color
        custcol_rev_color:   {
            'Almond': '#e3cba5',
            'Black': '#404040',
            'Chrome': '#c6c5ca',
            'Cool White Lights': '#eef3ef',
            'Oil Rubbed Bronze': '#534042',
            'Satin Nickel': '#d0d1c9',
            'Warm White Lights': '#f3d59e',
            'White': '#eae9ee',
            'Silver': '#a19d9e',
            'Cool White': '#eef3ef',
            'Warm White': '#f3d59e'
        },
        // custcol_sfr_color
        custcol_sfr_color:   {
            'Black': '#3c3b40',
            'White': '#e3e5f1'
        },
        // custcol_srw_color
        custcol_srw_color:   {
            'Black': '#000000',
            'Brown': '#613c1f',
            'White': '#ffffff'
        },
        // custcol_srw_head_color
        custcol_srw_head_color:   {
            'Brown': '#613c1f',
            'Cedar': '#ccaf8f',
            'Desert Bronze': '#5e3519',
            'Fire Pit': '#532c1b',
            'Flint': '#78777c',
            'Gravel Path': '#78716b',
            'Gray': '#b6afa9',
            'Mountain Cedar': '#5d381e',
            'Red': '#602d29',
            'Redwood': '#7a4938',
            'River Rock': '#aca59f',
            'Rosewood': '#a85126',
            'Rustic Bark': '#6d504a',
            'Sand Ridge': '#9a9b93',
            'Stainless Steel': '#979589',
            'Tan': '#cbb38d',
            'Teak': '#986d5a',
            'Tree House': '#4f3327',
            'Vintage Lantern': '#502e2d',
            'Walnut': '#613c1f',
            'White': '#ffffff'
        },
        // custcol_sur_color
        custcol_sur_color:   {
            'Cape Cod Gray': '#a5aaa4',
            'Desert Sand': '#9a9084',
            'Earthtone': '#726957',
            'Graystone': '#76776f',
            'Ipe': '#50463d',
            'Mahogany': '#594841',
            'Redwood': '#785f5b',
            'Tan': '#a1927d',
            'Walnut': '#60564d'
        },
        // custcol_tbc_cushion_color
        custcol_tbc_cushion_color:   {
            'Air Blue': '#76b5eb',
            'Antique Beige': '#eae7c6',
            'Aruba': '#49cfd3',
            'Bay Brown': '#333123',
            'Camel': '#938b67',
            'Canvas': '#f1efe1',
            'Charcoal': '#778d80',
            'Coal': '#545953',
            'Cocoa': '#6b6941',
            'Dupione Dove': '#d4d9bb',
            'Dupione Papaya': '#b8474d',
            'Dupione Pearl': '#e4e2b2',
            'Dupione Sand': '#d3cc9a',
            'Dupione Walnut': '#545439',
            'Elegance Marble': '#ededed',
            'Fife Plum': '#341f30',
            'Heather Beige': '#9e9f72',
            'Henna': '#753036',
            'Hot Pink': '#de1d9c',
            'Linden': '#ced651',
            'Linen Chili': '#734c38',
            'Linen Straw': '#978455',
            'Linen Taupe': '#858f7a',
            'Mankala Dusk': '#ededed',
            'Parkway Carbon': '#ededed',
            'Sailcloth Suntan': '#b39670',
            'Sailor Sahara': '#ededed',
            'Saphire Blue': '#5490bf',
            'Stone Green': '#9dbb81',
            'Tangerine': '#cd6c25',
            'Tango Mink': '#ededed',
            'Taupe': '#869483'
        },
        // custcol_trx_color
        custcol_trx_color:   {
            'Black': '#000000',
            'Bronze': '#3b2c27',
            'Charcoal Black': '#000000',
            'Classic White': '#efebe8',
            'Fire Pit': '#532c1b',
            'Gravel Path': '#79766f',
            'Rope Swing': '#bda38a',
            'Tree House': '#9a5d1e',
            'Vintage Lantern': '#390104',
            'White': '#efebe8'
        },
        // custcol_tte_color
        custcol_tte_color:   {
            'Architectural Bronze': '#9b9b7f',
            'Black': '#000000',
            'Brownstone': '#917661',
            'Cedar': '#c0a18d',
            'Dark Grey': '#6c6c6c',
            'Gray': '#9e9f9a',
            'Grey': '#9e9f9a',
            'Kona': '#49342f',
            'Mountain Cedar': '#a06b41',
            'Rosewood': '#a0562f',
            'Sand': '#dfcac5',
            'Slate Grey': '#9e9e9e',
            'Teak': '#af7746',
            'Walnut': '#553a25',
            'White': '#ffffff'
        },
        // custcol_ven_color
        custcol_ven_color:   {
            'Black': '#171b1a',
            'Green': '#517d6e',
            'Slate Grey': '#1b1f1e',
            'Terra Cotta': '#83443b'
        },
        // custcol_wst_color
        custcol_wst_color:   {
            'Black Fine': '#161614',
            'Bronze Fine': '#3d352d',
            'Gloss White': '#ffffff',
            'Speckled Walnut': '#543620',
            'White Fine': '#dcdbda'
        }
    };
    
    /* Add your global config here. It will be merged in the specific app config, not here */
    
     
    var getBxSliderDefaults = function getBxSliderDefaults() {
     
        var bxSliderDefaults = {};
        var screen_width = jQuery(window).width();
     
        if (screen_width <= 480)
        {
            bxSliderDefaults = {
                minSlides: 2
                ,   slideWidth: 175
                ,   maxSlides: 2
                ,   forceStart: true
                ,   pager: false
                ,   touchEnabled:true
                ,   nextText: '<a class="item-details-carousel-next"><span class="control-text">' + _('next').translate() + '</span> <i class="carousel-next-arrow"></i></a>'
                ,   prevText: '<a class="item-details-carousel-prev"><i class="carousel-prev-arrow"></i> <span class="control-text">' + _('prev').translate() + '</span></a>'
                ,   controls: true
                ,   preloadImages: 'all'
                ,   responsive: true
                ,   slideMargin: 10
                ,   auto: true
            }
        }
        // Tablet Specific
        else if (screen_width < 768)
        {
            bxSliderDefaults = {
                minSlides: 3
                ,   slideWidth: 180
                ,   maxSlides: 3
                ,   forceStart: true
                ,   pager: false
                ,   touchEnabled:true
                ,   nextText: '<a class="item-details-carousel-next"><span class="control-text">' + _('next').translate() + '</span> <i class="carousel-next-arrow"></i></a>'
                ,   prevText: '<a class="item-details-carousel-prev"><i class="carousel-prev-arrow"></i> <span class="control-text">' + _('prev').translate() + '</span></a>'
                ,   controls: true
                ,   preloadImages: 'all'
                ,   responsive: true
                ,   slideMargin: 20
                ,   auto: true
            }
        }
        // Desktop Specific
        else if (screen_width < 1200)
        {
            bxSliderDefaults = {
                minSlides: 4
                ,   slideWidth: 210
                ,   maxSlides: 4
                ,   forceStart: true
                ,   pager: false
                ,   touchEnabled:true
                ,   nextText: '<a class="item-details-carousel-next"><span class="control-text">' + _('next').translate() + '</span> <i class="carousel-next-arrow"></i></a>'
                ,   prevText: '<a class="item-details-carousel-prev"><i class="carousel-prev-arrow"></i> <span class="control-text">' + _('prev').translate() + '</span></a>'
                ,   controls: true
                ,   preloadImages: 'all'
                ,   responsive: true
                ,   slideMargin: 20
                ,   auto: true
            }
        }
        // Default
        else
        {
            bxSliderDefaults = {
                minSlides: 2
                ,   slideWidth: 220
                ,   maxSlides: 5
                ,   forceStart: true
                ,   pager: false
                ,   touchEnabled:true
                ,   nextText: '<a class="item-details-carousel-next"><span class="control-text">' + _('next').translate() + '</span> <i class="carousel-next-arrow"></i></a>'
                ,   prevText: '<a class="item-details-carousel-prev"><i class="carousel-prev-arrow"></i> <span class="control-text">' + _('prev').translate() + '</span></a>'
                ,   controls: true
                ,   preloadImages: 'all'
                ,   responsive: true
                ,   auto: true
            }
        }
     
        return bxSliderDefaults;
    };
     
     
     
    
    return {
     
        getBxSliderDefaults: getBxSliderDefaults,
         
        bxSliderDefaults: getBxSliderDefaults(),
         
        colors: colors,
    
        //USE of AdvancedItemImagesModule
        advancedImageManagement: true,
        multiImageOption: [
            'custcol_adl_color',
            'custcol_ags_color',
            'custcol_aqu_color',
            'custcol_awg_color',
            'custcol_azk_color',
            'custcol_baluster_length',
            'custcol_base_material',
            'custcol_bwp_color',
            'custcol_cop_color',
            'custcol_dck_base_color',
            'custcol_dck_color',
            'custcol_dimensions',
            'custcol_dkr_color',
            'custcol_dtx_color',
            'custcol_dtx_cushion_color',
            'custcol_dtx_frame_color',
            'custcol_dtx_seat_color',
            'custcol_dtx_slat_color',
            'custcol_dws_color',
            'custcol_dws_fastener_color',
            'custcol_dws_head_color',
            'custcol_fen_color',
            'custcol_finish',
            'custcol_fmr_color',
            'custcol_frt_color',
            'custcol_grit',
            'custcol_gro_color',
            'custcol_height',
            'custcol_hg_test_list_option',
            'custcol_hpl_color',
            'custcol_knp_color',
            'custcol_length',
            'custcol_ome_color',
            'custcol_packaging_size',
            'custcol_pal_color',
            'custcol_pld_color',
            'custcol_pld_cushion_color',
            'custcol_pld_frame_color',
            'custcol_pld_seat_color',
            'custcol_pld_slat_color',
            'custcol_pld_top_color',
            'custcol_pld_weave_color',
            'custcol_pnf_color',
            'custcol_pond_size',
            'custcol_post_size',
            'custcol_post_type',
            'custcol_pump_size',
            'custcol_quantity',
            'custcol_rail_length',
            'custcol_railing_type',
            'custcol_rdi_color',
            'custcol_rev_color',
            'custcol_scrail_type',
            'custcol_screw_length',
            'custcol_screw_type',
            'custcol_sfr_color',
            'custcol_srw_color',
            'custcol_srw_head_color',
            'custcol_sur_color',
            'custcol_tbc_cushion_color',
            'custcol_trx_color',
            'custcol_tte_color',
            'custcol_type',
            'custcol_ven_color',
            'custcol_watts',
            'custcol_wst_color'
        ],
        showOnlyMainImages: true,
    
        // Use StoreEmail cookie to suggest email on login
        rememberEmailOnLoginPage:false,
    
        // 2016-09-27 (mta): Alex, all 4 of these elements need to be removed from the hamburger menu
        navigationData: [{
            text: _('Home').translate(),
            href: '/',
            'class': 'header-menu-shop-anchor',
            data: {
                touchpoint: 'home',
                hashtag: '#/'
            }
        }, {
            text: _('Shop By Brand').translate(),
            href: '/brands',
            'class': 'header-menu-shop-anchor',
            data: {
                touchpoint: 'home',
                hashtag: '#/brands'
            },
            categories: [
                {
                    text: _('Atlantic Water Gardens').translate(),
                    href: '/atlantic-water-gardens',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('Aurora Deck Lighting').translate(),
                    href: '/aurora-deck-lighting',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('Azek').translate(),
                    href: '/azek',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('CAMO Fasteners').translate(),
                    href: '/camo-deck-fasteners',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('Deckorators').translate(),
                    href: '/deckorators',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('DeckWise').translate(),
                    href: '/deckwise',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('DuPont Tyvek').translate(),
                    href: '/dupont-tyvek',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('FastenMaster').translate(),
                    href: '/fastenmaster',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('Feeney').translate(),
                    href: '/feeney',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('Fortress').translate(),
                    href: '/fortress',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('Grace').translate(),
                    href: '/grace',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('Highpoint Lighting').translate(),
                    href: '/highpoint-lighting',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('Penofin').translate(),
                    href: '/penofin',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('POLYWOOD').translate(),
                    href: '/polywood',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('RDI Railing').translate(),
                    href: '/rdi-railing',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('Rev-A-Shelf').translate(),
                    href: '/rev-a-shelf',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('Screw Products').translate(),
                    href: '/screw-products',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('Three Birds Casual').translate(),
                    href: '/three-birds-casual',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('TigerClaw').translate(),
                    href: '/tigerclaw',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('TimberTech').translate(),
                    href: '/timbertech',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('Trex').translate(),
                    href: '/trex',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('Trex Furniture').translate(),
                    href: '/trex-furniture',
                    'class': 'header-menu-level2-anchor'
                },
                {
                    text: _('Typar').translate(),
                    href: '/typar',
                    'class': 'header-menu-level2-anchor'
                }
            ]
        }],
    
        itemOptions: [
    
            // update the name of the custcol here in order to get this working correctly
            {
                cartOptionId: 'custcol_fmr_color',
                label: 'Color',
                url: 'frmcolor',
                colors: colors.custcol_fmr_color,
                showSelectorInList: false,
                templates: {
                    selector: itemViewsOptionDropdownTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_fmr_color',
                label: 'Color',
                url: 'frmcolor',
                colors: colors.custcol_fmr_color,
                showSelectorInList: false,
                templates: {
                    selector: itemViewsOptionDropdownTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
   
            }
        /*,
            {
                cartOptionId: 'custcol_pld_frame_color',
                label: 'Frame Color',
                url: 'pldfrmcolor',
                colors: colors.custcol_pld_frame_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
            }*/
    
        ,    
            {
                cartOptionId: 'custcol_pld_slat_color',
                label: 'Slat Color',
                url: 'pldslatcolor',
                colors: colors.custcol_pld_slat_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
            }
            ,
            {
                cartOptionId: 'custitem_pld_slat_color',
                label: 'Slat Color',
                url: 'pldslatcolor',
                colors: colors.custcol_pld_slat_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
            }
            ,
            {
                cartOptionId: 'custcol_adl_color',
                label: 'Color',
                url: 'adlcolor',
                colors: colors.custcol_adl_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
            }
            ,
            {
                cartOptionId: 'custitem_adl_color',
                label: 'Color',
                url: 'adlcolor',
                colors: colors.custcol_adl_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
            }
            ,
            {
                cartOptionId: 'custcol_ags_color',
                label: 'Color',
                url: 'agscolor',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_ags_color',
                label: 'Color',
                url: 'agscolor',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_aqu_color',
                label: 'Color',
                url: 'aqucolor',
                colors: colors.custcol_aqu_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
            }
            ,
            {
                cartOptionId: 'custitem_aqu_color',
                label: 'Color',
                url: 'aqucolor',
                colors: colors.custcol_aqu_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
            }
            ,
            {
                cartOptionId: 'custcol_awg_color',
                label: 'Color',
                url: 'awgcolor',
                colors: colors.custcol_awg_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_awg_color',
                label: 'Color',
                url: 'awgcolor',
                colors: colors.custcol_awg_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_azk_color',
                label: 'Color',
                url: 'azkcolor',
                colors: colors.custcol_azk_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_azk_color',
                label: 'Color',
                url: 'azkcolor',
                colors: colors.custcol_azk_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_baluster_length',
                label: 'Baluster Length',
                url: 'balusterlength',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_baluster_length',
                label: 'Baluster Length',
                url: 'balusterlength',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_base_material',
                label: 'Base Material',
                url: 'basematerial',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_base_material',
                label: 'Base Material',
                url: 'basematerial',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_bwp_color',
                label: 'Color',
                url: 'bwpcolor',
                colors: colors.custcol_bwp_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_bwp_color',
                label: 'Color',
                url: 'bwpcolor',
                colors: colors.custcol_bwp_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_cop_color',
                label: 'Color',
                url: 'copcolor',
                colors: colors.custcol_cop_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_cop_color',
                label: 'Color',
                url: 'copcolor',
                colors: colors.custcol_cop_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_dck_base_color',
                label: 'Base Color',
                url: 'dckbasecolor',
                colors: colors.custcol_dck_base_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_dck_base_color',
                label: 'Base Color',
                url: 'dckbasecolor',
                colors: colors.custcol_dck_base_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_dck_color',
                label: 'Color',
                url: 'dckcolor',
                colors: colors.custcol_dck_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_dck_color',
                label: 'Color',
                url: 'dckcolor',
                colors: colors.custcol_dck_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_dimensions',
                label: 'Dimensions',
                url: 'dimensions',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionDropdownTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_dimensions',
                label: 'Dimensions',
                url: 'dimensions',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionDropdownTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custitem_dimensions_matrix',
                label: 'Dimensions',
                url: 'dimensions',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionDropdownTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_dkr_color',
                label: 'Color',
                url: 'dkrcolor',
                colors: colors.custcol_dkr_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_dkr_color',
                label: 'Color',
                url: 'dkrcolor',
                colors: colors.custcol_dkr_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_dtx_color',
                label: 'Color',
                url: 'dtxcolor',
                colors: colors.custcol_dtx_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_dtx_color',
                label: 'Color',
                url: 'dtxcolor',
                colors: colors.custcol_dtx_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_dtx_cushion_color',
                label: 'Cushion Color',
                url: 'dtxcushioncolor',
                colors: colors.custcol_dtx_cushion_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_dtx_cushion_color',
                label: 'Cushion Color',
                url: 'dtxcushioncolor',
                colors: colors.custcol_dtx_cushion_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_dtx_frame_color',
                label: 'Frame Color',
                url: 'dtxframecolor',
                colors: colors.custcol_dtx_frame_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_dtx_frame_color',
                label: 'Frame Color',
                url: 'dtxframecolor',
                colors: colors.custcol_dtx_frame_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_dtx_seat_color',
                label: 'Seat Color',
                url: 'dtxseatcolor',
                colors: colors.custcol_dtx_seat_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_dtx_seat_color',
                label: 'Seat Color',
                url: 'dtxseatcolor',
                colors: colors.custcol_dtx_seat_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_dtx_slat_color',
                label: 'Slat Color',
                url: 'dtxslatcolor',
                colors: colors.custcol_dtx_slat_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_dtx_slat_color',
                label: 'Slat Color',
                url: 'dtxslatcolor',
                colors: colors.custcol_dtx_slat_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_dws_color',
                label: 'Color',
                url: 'dwscolor',
                colors: colors.custcol_dws_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_dws_color',
                label: 'Color',
                url: 'dwscolor',
                colors: colors.custcol_dws_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_dws_fastener_color',
                label: 'Fastener Color',
                url: 'dwsfastenercolor',
                colors: colors.custcol_dws_fastener_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_dws_fastener_color',
                label: 'Fastener Color',
                url: 'dwsfastenercolor',
                colors: colors.custcol_dws_fastener_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_dws_head_color',
                label: 'Head Color',
                url: 'dwsheadcolor',
                colors: colors.custcol_dws_head_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_dws_head_color',
                label: 'Head Color',
                url: 'dwsheadcolor',
                colors: colors.custcol_dws_head_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_fen_color',
                label: 'Color',
                url: 'fencolor',
                colors: colors.custcol_fen_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_fen_color',
                label: 'Color',
                url: 'fencolor',
                colors: colors.custcol_fen_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_finish',
                label: 'Finish',
                url: 'finish',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_finish',
                label: 'Finish',
                url: 'finish',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_fmr_color',
                label: 'Color',
                url: 'fmrcolor',
                colors: colors.custcol_fmr_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionDropdownTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_fmr_color',
                label: 'Color',
                url: 'fmrcolor',
                colors: colors.custcol_fmr_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionDropdownTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
   
            }
            ,
            {
                cartOptionId: 'custcol_frt_color',
                label: 'Color',
                url: 'frtcolor',
                colors: colors.custcol_frt_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_frt_color',
                label: 'Color',
                url: 'frtcolor',
                colors: colors.custcol_frt_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
   
            ,
            {
                cartOptionId: 'custcol_grit',
                label: 'Grit',
                url: 'grit',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
            {
                cartOptionId: 'custitem_grit',
                label: 'Grit',
                url: 'grit',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
   
            ,
            {
                cartOptionId: 'custcol_gro_color',
                label: 'Color',
                url: 'grocolor',
                colors: colors.custcol_gro_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_gro_color',
                label: 'Color',
                url: 'grocolor',
                colors: colors.custcol_gro_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
   
            ,
            {
                cartOptionId: 'custcol_height',
                label: 'Height',
                url: 'height',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
            {
                cartOptionId: 'custitem_height',
                label: 'Height',
                url: 'height',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            //himanshus test
            ,
            {
                cartOptionId: 'custcol_hg_test_list_option',
                label: 'HG Test Option',
                url: 'hgtest',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
            {
                cartOptionId: 'custcol_hpl_color',
                label: 'Color',
                url: 'hplcolor',
                colors: colors.custcol_hpl_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_hpl_color',
                label: 'Color',
                url: 'hplcolor',
                colors: colors.custcol_hpl_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
   
            {
                cartOptionId: 'custcol_knp_color',
                label: 'Color',
                url: 'knpcolor',
                colors: colors.custcol_knp_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_knp_color',
                label: 'Color',
                url: 'knpcolor',
                colors: colors.custcol_knp_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
   
            ,
            {
                cartOptionId: 'custcol_length',
                label: 'Length',
                url: 'length',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionDropdownTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_length',
                label: 'Length',
                url: 'length',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionDropdownTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
   
            }
   
            ,
            {
                cartOptionId: 'custcol_ome_color',
                label: 'Color',
                url: 'omecolor',
                colors: colors.custcol_ome_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
            }
            ,
            {
                cartOptionId: 'custitem_ome_color',
                label: 'Color',
                url: 'omecolor',
                colors: colors.custcol_ome_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
   
            ,
            {
                cartOptionId: 'custcol_packaging_size',
                label: 'Packaging Size',
                url: 'packagingsize',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionDropdownTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_packaging_size',
                label: 'Packaging Size',
                url: 'packagingsize',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionDropdownTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
   
            }
   
            ,
            {
                cartOptionId: 'custcol_pal_color',
                label: 'Color',
                url: 'palcolor',
                colors: colors.custcol_pal_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_pal_color',
                label: 'Color',
                url: 'palcolor',
                colors: colors.custcol_pal_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
   
            ,
            {
                cartOptionId: 'custcol_pld_color',
                label: 'Color',
                url: 'pldcolor',
                colors: colors.custcol_pld_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_pld_color',
                label: 'Color',
                url: 'pldcolor',
                colors: colors.custcol_pld_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
   
            ,
            {
                cartOptionId: 'custcol_pld_cushion_color',
                label: 'Cushion Color',
                url: 'pldcushioncolor',
                colors: colors.custcol_pld_cushion_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
            }
            ,
            {
                cartOptionId: 'custitem_pld_cushion_color',
                label: 'Cushion Color',
                url: 'pldcushioncolor',
                colors: colors.custcol_pld_cushion_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
   
            ,
            {
                cartOptionId: 'custcol_pld_frame_color',
                label: 'Frame Color',
                url: 'pldframecolor',
                colors: colors.custcol_pld_frame_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            //40
            ,
            {
                cartOptionId: 'custitem_pld_frame_color',
                label: 'Frame Color',
                url: 'pldframecolor',
                colors: colors.custcol_pld_frame_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
   
   
            {
                cartOptionId: 'custcol_pld_seat_color',
                label: 'Seat Color',
                url: 'pldseatcolor',
                colors: colors.custcol_pld_seat_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
            }
   
            ,
            {
                cartOptionId: 'custitem_pld_seat_color',
                label: 'Seat Color',
                url: 'pldseatcolor',
                colors: colors.custcol_pld_seat_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
            }
   
            ,
            {
                cartOptionId: 'custcol_pld_slat_color',
                label: 'Slat Color',
                url: 'pldslatcolor',
                colors: colors.custcol_pld_slat_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_pld_slat_color',
                label: 'Slat Color',
                url: 'pldslatcolor',
                colors: colors.custcol_pld_slat_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
   
   
            {
                cartOptionId: 'custcol_pld_top_color',
                label: 'Top Color',
                url: 'pldtopcolor',
                colors: colors.custcol_pld_top_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_pld_top_color',
                label: 'Top Color',
                url: 'pldtopcolor',
                colors: colors.custcol_pld_top_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
   
            {
                cartOptionId: 'custcol_pld_weave_color',
                label: 'Weave Color',
                url: 'pldweavecolor',
                colors: colors.custcol_pld_weave_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_pld_weave_color',
                label: 'Weave Color',
                url: 'pldweavecolor',
                colors: colors.custcol_pld_weave_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
   
            {
                cartOptionId: 'custcol_pnf_color',
                label: 'Color',
                url: 'pnfcolor',
                colors: colors.custcol_pnf_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_pnf_color',
                label: 'Color',
                url: 'pnfcolor',
                colors: colors.custcol_pnf_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
   
            {
                cartOptionId: 'custcol_pond_size',
                label: 'Pond Size',
                url: 'pondsize',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
            {
                cartOptionId: 'custitem_pond_size',
                label: 'Pond Size',
                url: 'pondsize',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
   
            {
                cartOptionId: 'custcol_post_size',
                label: 'Post Size',
                url: 'postsize',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
            {
                cartOptionId: 'custitem_post_size',
                label: 'Post Size',
                url: 'postsize',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
 
            {
                cartOptionId: 'custcol_post_type',
                label: 'Post Type',
                url: 'posttype',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
            {
                cartOptionId: 'custitem_post_type',
                label: 'Post Type',
                url: 'posttype',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
            {
                cartOptionId: 'custcol_pump_size',
                label: 'Pump Size',
                url: 'pumpsize',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
            {
                cartOptionId: 'custitem_pump_size',
                label: 'Pump Size',
                url: 'pumpsize',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
   
            {
                cartOptionId: 'custcol_quantity',
                label: 'Quantity Option',
                url: 'quantity',
                colors: colors.custcol_pld_slat_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
            {
                cartOptionId: 'custitem_quantity',
                label: 'Quantity',
                url: 'quantity',
                colors: colors.custcol_pld_slat_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
   
            {
                cartOptionId: 'custcol_rail_length',
                label: 'Rail Length',
                url: 'raillength',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
            {
                cartOptionId: 'custitem_rail_length',
                label: 'Rail Length',
                url: 'raillength',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
   
            {
                cartOptionId: 'custcol_railing_type',
                label: 'Railing Type',
                url: 'railingtype',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
            {
                cartOptionId: 'custitem_railing_type',
                label: 'Railing Type',
                url: 'railingtype',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
   
            {
                cartOptionId: 'custcol_rdi_color',
                label: 'Color',
                url: 'rdicolor',
                colors: colors.custcol_rdi_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_rdi_color',
                label: 'Color',
                url: 'rdicolor',
                colors: colors.custcol_rdi_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
   
            {
                cartOptionId: 'custcol_rev_color',
                label: 'Color',
                url: 'revcolor',
                colors: colors.custcol_rev_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_rev_color',
                label: 'Color',
                url: 'revcolor',
                colors: colors.custcol_rev_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
   
            {
                cartOptionId: 'custcol_scrail_type',
                label: 'Scrail Type',
                url: 'scrailtype',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
            {
                cartOptionId: 'custitem_scrail_type',
                label: 'Scrail Type',
                url: 'scrailtype',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
   
            {
                cartOptionId: 'custcol_screw_length',
                label: 'Screw Length',
                url: 'screwlength',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionDropdownTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_screw_length',
                label: 'Screw Length',
                url: 'screwlength',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionDropdownTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
   
            }
            ,
   
            {
                cartOptionId: 'custcol_screw_type',
                label: 'Screw Type',
                url: 'screwtype',
                colors: colors.custcol_pld_slat_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionDropdownTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
            {
                cartOptionId: 'custitem_screw_type',
                label: 'Screw Type',
                url: 'screwtype',
                colors: colors.custcol_pld_slat_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionDropdownTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
   
            {
                cartOptionId: 'custcol_sfr_color',
                label: 'Color',
                url: 'sfrcolor',
                colors: colors.custcol_sfr_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_sfr_color',
                label: 'Color',
                url: 'sfrcolor',
                colors: colors.custcol_sfr_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
   
            {
                cartOptionId: 'custcol_srw_color',
                label: 'Color',
                url: 'srwcolor',
                colors: colors.custcol_srw_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_srw_color',
                label: 'Color',
                url: 'srwcolor',
                colors: colors.custcol_srw_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
   
            {
                cartOptionId: 'custcol_srw_head_color',
                label: 'Head Color',
                url: 'srwheadcolor',
                colors: colors.custcol_srw_head_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_srw_head_color',
                label: 'Head Color',
                url: 'srwheadcolor',
                colors: colors.custcol_srw_head_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
   
            {
                cartOptionId: 'custcol_sur_color',
                label: 'Color',
                url: 'surcolor',
                colors: colors.custcol_sur_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_sur_color',
                label: 'Color',
                url: 'surcolor',
                colors: colors.custcol_sur_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
   
            {
                cartOptionId: 'custcol_tbc_cushion_color',
                label: 'Cushion Color',
                url: 'tbccushioncolor',
                colors: colors.custcol_tbc_cushion_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_tbc_cushion_color',
                label: 'Cushion Color',
                url: 'tbccushioncolor',
                colors: colors.custcol_tbc_cushion_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
   
            {
                cartOptionId: 'custcol_trx_color',
                label: 'Color',
                url: 'trxcolor',
                colors: colors.custcol_trx_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_trx_color',
                label: 'Color',
                url: 'trxcolor',
                colors: colors.custcol_trx_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
   
            {
                cartOptionId: 'custcol_tte_color',
                label: 'Color',
                url: 'ttecolor',
                colors: colors.custcol_tte_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_tte_color',
                label: 'Color',
                url: 'ttecolor',
                colors: colors.custcol_tte_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
            ,
   
            {
                cartOptionId: 'custcol_type',
                label: 'Type',
                url: 'type',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionDropdownTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_type',
                label: 'Type',
                url: 'type',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionDropdownTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
   
            }
            ,
 
            {
                cartOptionId: 'custitem_type_matrix',
                label: 'Type',
                url: 'type',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionDropdownTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
   
            }
            ,
   
            {
                cartOptionId: 'custcol_ven_color',
                label: 'Color',
                url: 'vencolor',
                colors: colors.custcol_ven_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
            }
            ,
            {
                cartOptionId: 'custitem_ven_color',
                label: 'Color',
                url: 'vencolor',
                colors: colors.custcol_ven_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
            }
            ,
   
            {
                cartOptionId: 'custcol_watts',
                label: 'Watts',
                url: 'watts',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
            {
                cartOptionId: 'custitem_watts',
                label: 'Watts',
                url: 'watts',
                colors: '',
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionTileTemplate,
                    selected: itemViewsSelectedOptionTemplate
                }
            }
            ,
   
            {
                cartOptionId: 'custcol_wst_color',
                label: 'Color',
                url: 'wstcolor',
                colors: colors.custcol_wst_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
    
            }
            ,
            {
                cartOptionId: 'custitem_wst_color',
                label: 'Color',
                url: 'wstcolor',
                colors: colors.custcol_wst_color,
                showSelectorInList: true,
                templates: {
                    selector: itemViewsOptionColorTemplate,
                    selected: itemViewsSelectedOptionColorTemplate
                }
   
            }
    
        ],
        extraModulesConfig: {
            Categories: {
                addToNavigationTabs: true,
                classLevels: [
                    'header-menu-level1-anchor',
                    'header-menu-level2-anchor',
                    'header-menu-level3-anchor'
                ],
                method: 'merge',
                mergeIndex: 2,
                categoryOnProductURL: false,
                levelsOnMenu: 3
            }
        },

        tracking: {
            googleTagManager: {
                id: 'GTM-NH74P9',
                dataLayerName: 'dataLayer'
            }
        }
    };
});