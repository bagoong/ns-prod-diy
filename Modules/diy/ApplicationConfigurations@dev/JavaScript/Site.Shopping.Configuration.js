define('Site.Shopping.Configuration', [
    'SC.Shopping.Configuration',
    'Site.Global.Configuration',

    'facets_faceted_navigation_item_range.tpl',
    'facets_faceted_navigation_item_color.tpl',

    'facets_item_cell_grid.tpl',
    'facets_item_cell_list.tpl',
    'facets_item_cell_table.tpl',

    'underscore',
    'Utils',
    
    'jQuery'
], function SiteCheckoutConfiguration(
    ShoppingConfiguration,
    GlobalConfiguration,

    facetsFacetedNavigationItemRangeTemplate,
    facetsFacetedNavigationItemColorTemplate,

    facets_item_cell_grid_tpl,
    facets_item_cell_list_tpl,
    facets_item_cell_table_tpl,

    _,
    Utils,

    jQuery

) {
    'use strict';

    ShoppingConfiguration.lightColors.push('White');

    var SiteApplicationConfiguration = {
        documentSliderDefaults: {
            minSlides: 5
            ,   slideWidth: 180
            ,   maxSlides: 5
            ,   forceStart: true
            ,   pager: false
            ,   touchEnabled: true
            ,   preloadImages: 'all'
            ,   slideMargin: 15
            ,   auto: true
            ,   pause: 5000
            ,   useCSS: true
            ,   autoHover: true
            ,   infiniteLoop: true
            ,   nextText: '<a class="item-details-gallery-next-icon document-details-carousel-next"><span class="control-text">' + _('next').translate() + '</span></a>'
            ,   prevText: '<a class="item-details-gallery-prev-icon document-details-carousel-prev"><span class="control-text">' + _('prev').translate() + '</span></a>'
            ,   controls: true
        },
        videoSliderDefaults: {
            minSlides: 5
            ,   slideWidth: 250
            ,   maxSlides: 4
            ,   forceStart: true
            ,   pager: false
            ,   touchEnabled: true
            ,   preloadImages: 'all'
            ,   slideMargin: 15
            ,   auto: true
            ,   pause: 5000
            ,   useCSS: true
            ,   autoHover: true
            ,   infiniteLoop: true
            ,   nextText: '<a class="item-details-gallery-next-icon video-details-carousel-next"><span class="control-text">' + _('next').translate() + '</span></a>'
            ,   prevText: '<a class="item-details-gallery-prev-icon video-details-carousel-prev"><span class="control-text">' + _('prev').translate() + '</span></a>'
            ,   controls: true
        },
        recommendedAccessoriesSliderDefaults: {
            minSlides: 5
            ,   slideWidth: 200
            ,   maxSlides: 5
            ,   forceStart: true
            ,   pager: false
            ,   touchEnabled: true
            ,   preloadImages: 'all'
            ,   slideMargin: 15
            ,   auto: true
            ,   pause: 5000
            ,   useCSS: true
            ,   autoHover: true
            ,   infiniteLoop: true
            ,   nextText: '<a class="item-details-gallery-next-icon rc-details-carousel-next"><span class="control-text">' + _('next').translate() + '</span></a>'
            ,   prevText: '<a class="item-details-gallery-prev-icon rc-details-carousel-prev"><span class="control-text">' + _('prev').translate() + '</span></a>'
            ,   controls: true
        },
        itemDetails: [

            // Tab 1: Description - handled in ItemDetails.View.js

            // Tab 2: Videos
            {
                name: _('Videos').translate()
            ,   contentFromKey: 'storedetaileddescription'
            ,   opened: true
            ,   itemprop: 'description'
            },

            // Tab 3: Documentation
            {
                name: _('Info & Guides').translate()
            ,   contentFromKey: ''
            ,   opened: true
            ,   itemprop: 'description'
            ,   isInfoGuideTab: true
            }

            // Tab 4: Specs - handled in ItemDetails.View.js

            // Tab 5: Quantity Pricing - handled in ItemDetails.View.js
        ],
        facets: [{
            id: 'category',
            name: _('Category').translate(),
            behavior: 'hierarchical',
            uncollapsible: true,
            titleToken: '$(0)',
            titleSeparator: ', ',
            showHeading: false
        }, {
            id: 'onlinecustomerprice',
            name: _('Price').translate(),
            behavior: 'range',
            macro: 'facetRange',
            template: facetsFacetedNavigationItemRangeTemplate,
            uncollapsible: true,
            titleToken: 'Price $(1) - $(0)',
            titleSeparator: ', ',
            priority:3,
            descending:98,
            parser: function parser(value) {
                return _.formatCurrency(value);
            }
        }, {
            // update the custom field id to match with the correct one
            id: 'custitem_manufacturer',
            name: _('Manufacturer').translate(),
            max: 5,
            behavior: 'multi',
            descending: 100,
            priority: 100
        }, {
            // this is for the case that onlinecustomerprice is not available in the account
            id: 'pricelevel5',
            name: _('Price').translate(),
            behavior: 'range',
            macro: 'facetRange',
            template: facetsFacetedNavigationItemRangeTemplate,
            uncollapsible: true,
            titleToken: 'Price $(1) - $(0)',
            titleSeparator: ', ',
            priority:99,
            descending: 99,
            parser: function parser(value) {
                return _.formatCurrency(value);
            }
        }, {
            // update the custom field id to match with the correct one
            id: 'custitem_length_feet',
            name: _('Length - Feet').translate(),
            max: 5,
            behavior: 'multi',
            priority:98,
            descending: 98
        }, {
            // update the custom field id to match with the correct one
            id: 'custitem_height_inches',
            name: _('Height - Inches').translate(),
            max: 5,
            behavior: 'multi',
            priority:97,
            descending: 97
        }, {
            // update the custom field id to match with the correct one
            id: 'custitem_baluster_length',
            name: _('Baluster Length Field').translate(),
            max: 5,
            behavior: 'multi',
            priority:96,
            descending: 96
        }, {
            // update the custom field id to match with the correct one
            id: 'custitem_final_rail_height_inches',
            name: _('Final Rail Height').translate(),
            max: 5,
            behavior: 'multi',
            priority:95,
            descending: 95
        }, {
            // update the custom field id to match with the correct one
            id: 'custitemcustitem_shape',
            name: _('Shape').translate(),
            max: 5,
            behavior: 'multi',
            priority:94,
            descending: 94
        }, {
            // update the custom field id to match with the correct one
            id: 'custitemcustitem_width_feet',
            name: _('Width - Feet').translate(),
            max: 5,
            behavior: 'multi',
            priority:93,
            descending: 93
        }, {
            // update the custom field id to match with the correct one
            id: 'custitemcustitem_width_inches',
            name: _('Width - Inches').translate(),
            max: 5,
            behavior: 'multi',
            priority:92,
            descending: 92
        }, {
            // update the custom field id to match with the correct one
            id: 'custitem_depth_inches',
            name: _('Depth - Inches').translate(),
            max: 5,
            behavior: 'multi',
            priority:91,
            descending: 91
        }, {
            // update the custom field id to match with the correct one
            id: 'custitem_furniture_type',
            name: _('Furniture Type').translate(),
            max: 5,
            behavior: 'multi',
            priority:90,
            descending: 90
        }, {
            // update the custom field id to match with the correct one
            id: 'custitem_seat_height_inches',
            name: _('Seat Height - Inches').translate(),
            max: 5,
            behavior: 'multi',
            priority:89,
            descending: 89
        }, {
            // update the custom field id to match with the correct one
            id: 'custitem_seat_width_inches',
            name: _('Seat Width - Inches').translate(),
            max: 5,
            behavior: 'multi',
            priority:88,
            descending: 88
        }, {
            // update the custom field id to match with the correct one
            id: 'custitem_seat_depth_inches',
            name: _('Seat Depth - Inches').translate(),
            max: 5,
            behavior: 'multi',
            priority:87,
            descending: 87
        }, {
            // update the custom field id to match with the correct one
            id: 'custitem_screw_size',
            name: _('Screw Size').translate(),
            max: 5,
            behavior: 'multi',
            priority:86,
            descending: 86
        }
            /*, {
            // update the custom field id to match with the correct one
            id: 'custitem_color',
            name: _('Color').translate(),
            template: facetsFacetedNavigationItemColorTemplate,
            colors: GlobalConfiguration.colors,
            descending: 1
        }, {
            // update the custom field id to match with the correct one
            id: 'custitem_baluster_shape',
            name: _('Baluster Shape').translate(),
            max: 5,
            behavior: 'multi',
            descending: 1
        }*/

        ],

        sortOptions: [
            {id: 'relevance:asc', name: _('Sort by relevance').translate(), isDefault: true}
            ,   {id: 'pricelevel5:desc', name: _('Sort by price, High to Low ').translate()}
            ,   {id: 'pricelevel5:asc', name: _('Sort by price, Low to High').translate()}
            ,   {id: 'displayname:asc', name: _('Sort by name, A - Z').translate()}
            ,   {id: 'displayname:desc', name: _('Sort by name, Z - A').translate()}
        ]
    };
    
    
    
    // Device Specific Settings
    // ------------------------

    /*---------------------------
    itemsDisplayOptions is set when the user loads the page with the current width of the screen device, NOT the width of the browser.
    This option is NOT responsive, so if the user loads the page with the desktop resolution, even if he resize the browser, he will still see the view of the desktop.

    Display type and columns supported by screen resolution:

    Mobile
    Display types -> List, Table
        List -> columns  [1, 2]
        Table -> columns [1, 2]

    Tablet
    Display types  -> List, Table
        List -> columns [1, 2, 3, 4, 6, 12]
        Table -> columns [1, 2, 3, 4, 6, 12]

    Desktop
    Display types   ->
        List -> columns [1, 2, 3, 4, 6, 12]
        Table -> columns [1, 2, 3, 4, 6, 12]
        Grid -> columns [1, 2, 3, 4, 6, 12]
    --------------------------*/

    if (!SC.isPageGenerator())
    {
        var screenType = Utils.getDeviceType();

        // Phone Specific
        if (screenType === 'phone')
        {
            _.extend(ShoppingConfiguration, {

                itemsDisplayOptions: [
                    { id: 'table', name: _('Table').translate(), template: facets_item_cell_table_tpl, columns: 2, icon: 'icon-display-table', isDefault: true }
                ,    { id: 'list', name: _('List').translate(), template: facets_item_cell_list_tpl, columns: 1, icon: 'icon-display-list' }
                ]

            ,   sortOptions: [
                    {id: 'relevance:asc', name: _('Sort by relevance').translate(), isDefault: true}
                ,   {id: 'onlinecustomerprice:asc', name: _('Sort by  price, low to high').translate()}
                ,   {id: 'onlinecustomerprice:desc', name: _('Sort by price, high to low ').translate()}
                ]

            ,   defaultPaginationSettings: {
                    showPageList: false
                ,   showPageIndicator: true
                }
            });
        }
        // Tablet Specific
        else if (screenType === 'tablet')
        {
            _.extend(ShoppingConfiguration, {

                itemsDisplayOptions: [
                    {id: 'table', name: _('Table').translate(), template: facets_item_cell_table_tpl, columns: 2, icon: 'icon-display-table'}
                ,   {id: 'grid', name: _('Grid').translate(), template: facets_item_cell_grid_tpl, columns: 4, icon: 'icon-display-grid', isDefault: true}
                ]

            ,   sortOptions: [
                    {id: 'relevance:asc', name: _('Sort by relevance').translate(), isDefault: true}
                ,   {id: 'onlinecustomerprice:asc', name: _('Sort by price, low to high').translate()}
                ,   {id: 'onlinecustomerprice:desc', name: _('Sort by price, high to low ').translate()}
                ]

            ,   defaultPaginationSettings: {
                    showPageList: true
                ,   pagesToShow: 4
                ,   showPageIndicator: true
                }
            });
        }
        // Desktop
        else
        {
            _.extend(ShoppingConfiguration, {

                itemsDisplayOptions: [
                    {id: 'table', name: _('Table').translate(), template: facets_item_cell_table_tpl, columns: 2, icon: 'icon-display-table'}
                ,   {id: 'grid', name: _('Grid').translate(), template: facets_item_cell_grid_tpl, columns: 4, icon: 'icon-display-grid', isDefault: true}
                ]
            });
        }
    }

    var extraModulesConfig = GlobalConfiguration.extraModulesConfig;
    delete GlobalConfiguration.extraModulesConfig;

    ShoppingConfiguration.facetsSeoLimits.numberOfFacetsGroups = 0;
    ShoppingConfiguration.modulesConfig = ShoppingConfiguration.modulesConfig || {};

    _.extend(ShoppingConfiguration.searchApiMasterOptions.Facets, {
        'facet.exclude' : ShoppingConfiguration.searchApiMasterOptions.Facets['facet.exclude']? ShoppingConfiguration.searchApiMasterOptions.Facets['facet.exclude']+',' + 'custitem_best_seller': 'custitem_best_seller'
    });

    //console.log(ShoppingConfiguration);

    _.extend(ShoppingConfiguration, GlobalConfiguration, SiteApplicationConfiguration);
    _.extend(ShoppingConfiguration.modulesConfig, extraModulesConfig);




    return {
        mountToApp: function mountToApp(application) {
            _.extend(application.Configuration, ShoppingConfiguration);
        }
    };
});