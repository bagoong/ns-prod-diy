define('Site.Checkout.Configuration', [
    'SC.Checkout.Configuration',
    'SC.Checkout.Configuration.Steps.OPC',
    'Site.Global.Configuration',
    'underscore'
], function SiteCheckoutConfiguration(
    CheckoutConfiguration,
    CheckoutConfigurationStepsOPC,
    GlobalConfiguration,
    _
) {
    'use strict';

    var SiteApplicationConfiguration = {
        checkoutSteps: CheckoutConfigurationStepsOPC
    };

    var extraModulesConfig = GlobalConfiguration.extraModulesConfig;
    delete GlobalConfiguration.extraModulesConfig;

    CheckoutConfiguration.modulesConfig = CheckoutConfiguration.modulesConfig || {};
    _.extend(CheckoutConfiguration, GlobalConfiguration, SiteApplicationConfiguration);
    _.extend(CheckoutConfiguration.modulesConfig, extraModulesConfig);

    return {
        mountToApp: function mountToApp(application) {
            _.extend(application.Configuration, CheckoutConfiguration);
        }
    };
});