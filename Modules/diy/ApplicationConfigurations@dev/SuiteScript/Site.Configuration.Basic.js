define('Site.Configuration', [
    'Configuration',
    'PSCategories.Configuration',
    'ContactUs.Configuration',
    'Newsletter.SignUp.Configuration'
], function SiteConfiguration(
    Configuration,
    CategoriesConfiguration,
    ContactUsConfiguration,
    NewsletterConfiguration
) {
    'use strict';


    /*
    Configuration.webFonts = {
        enabled: true,
        async: true,
        configuration: {
            google: { families: [ 'Roboto:400,700,300:latin' ] }
        }
    };
    */

    ContactUsConfiguration.formId = '1';
    ContactUsConfiguration.hash = '5b04e56061487343809e';

    NewsletterConfiguration.formId = '2';
    NewsletterConfiguration.hash = '7cbe9825db61c5b73ea0';

    CategoriesConfiguration.secureCategories = true;

});