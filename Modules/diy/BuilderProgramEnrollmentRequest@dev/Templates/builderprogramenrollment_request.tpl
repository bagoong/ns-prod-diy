
<section class="builderheader">

  <header>
    <h1>Preferred Builder Program</h1>
    <h4>For Professional Builders</h4>
  </header>

  <div class="header-content">
    <div class="col-md-8 imageHammer">
      <img src="{{iconhammer}}" alt="">
    </div>

    <div class="col-md-4 right-box">
      <h2>What You Get!</h2>
      
      <div class="right-content">
        <i class="icon-success"></i>
        <div class="content-data">
          <span>Annual Cash Rebate</span>
          <p>on order volume over $5,000</p>
        </div>
      </div>  
      
      <div class="right-content">
        <i class="icon-success"></i>
        <div class="content-data">
          <span>Instant Discounts (5%)</span>
          <p>and product information</p>
        </div>  
      </div>
      
      <div class="right-content">
        <i class="icon-success"></i>
        <div class="content-data">
          <span>Free Manufacturer Samples</span>
          <p>on order volume over $5,000</p>
        </div>  
      </div>

    </div>
  </div>

  <p class="builderheader-last-p">Our name is DIY Home Center - yet, our brands say it all! Tried, tested and trusted by professional builders we offer brands you recognize for quality and reliability.<br/>
Add that to fast (tipically free) delivery, personalized service and competitive prices - signing up for our Preferred Builders Program just makes sense!</p>

</section>

<section class="builderprogramenrollment-request">
    <header class="builderprogramenrollment-request-header">
        <h2 class="builderprogramenrollment-request-header-title">Preferred Builder Program Sign Up:</h2>
    </header>

    <form id="b2b-request-form" class="builderprogramenrollment-request-form" action="#" method="POST">
      <fieldset>

      <!--
      <small class="builderprogramenrollment-request-required">{{translate 'Required <span class="builderprogramenrollment-request-form-required">*</span>'}}</small>
      -->

        <!-- First Name -->
        <div class="builderprogramenrollment-request-form-controls-group" data-validation="control-group">
            <label class="builderprogramenrollment-request-form-label" for="firstname">
                {{translate 'First Name <small class="builderprogramenrollment-request-form-required ">*</small>'}}
            </label>
            <div class="builderprogramenrollment-request-form-controls" data-validation="control">
                <input {{#if hasAutoFocus}} autofocus {{/if}} type="text" name="firstname" id="firstname" class="builderprogramenrollment-request-form-input input-medium-form">
            </div>
        </div>
        <!-- Last Name -->
        <div class="builderprogramenrollment-request-form-controls-group" data-validation="control-group">
            <label class="builderprogramenrollment-request-form-label" for="lastname">
                {{translate 'Last Name <small class="builderprogramenrollment-request-form-required">*</small>'}}
            </label>
            <div class="builderprogramenrollment-request-form-controls" data-validation="control">
                <input {{#if hasAutoFocus}} autofocus {{/if}} type="text" name="lastname" id="lastname" class="builderprogramenrollment-request-form-input input-medium-form">
            </div>
        </div>
        <!-- Company Name -->
        <div class="builderprogramenrollment-request-form-controls-group" data-validation="control-group">
            <label class="builderprogramenrollment-request-form-label" for="companyname">
                {{translate 'Company Name <small class="builderprogramenrollment-request-form-required">*</small>'}}
            </label>
            <div class="builderprogramenrollment-request-form-controls" data-validation="control">
                <input {{#if hasAutoFocus}} autofocus {{/if}} type="text" name="companyname" id="companyname" class="builderprogramenrollment-request-form-input input-large-form">
            </div>
        </div>

        <!-- Bill Address 1 -->
        <div class="builderprogramenrollment-request-form-controls-group" data-validation="control-group">
            <label class="builderprogramenrollment-request-form-label" for="address1">
                {{translate 'Billing Address 1'}}
            </label>
            <div class="builderprogramenrollment-request-form-controls" data-validation="control">
                <input {{#if hasAutoFocus}} autofocus {{/if}} type="text" name="address1" id="address1" class="builderprogramenrollment-request-form-input input-large-form">
            </div>
        </div>

        <!-- Bill City -->
        <div class="builderprogramenrollment-request-form-controls-group" data-validation="control-group">
            <label class="builderprogramenrollment-request-form-label" for="city">
                {{translate 'City'}}
            </label>
            <div class="builderprogramenrollment-request-form-controls" data-validation="control">
                <input {{#if hasAutoFocus}} autofocus {{/if}} type="text" name="city" id="city" class="builderprogramenrollment-request-form-input input-medium-form">
            </div>
        </div>

        <!-- TODO: Bill State -->
        <div class="builderprogramenrollment-request-form-controls-group" data-validation="control-group">
            <label class="builderprogramenrollment-request-form-label" for="state">
                {{translate 'State'}}
            </label>
            <div class="builderprogramenrollment-request-form-controls" data-validation="control">
                <select name="state" id="state" class="builderprogramenrollment-request-form-input input-small-form" type="select-one">
                  <option value="" selected=""></option>
                  <option value="0">Alabama</option>
                  <option value="1">Alaska</option>
                  <option value="101">Alberta</option>
                  <option value="2">Arizona</option>
                  <option value="3">Arkansas</option>
                  <option value="53">Armed Forces Americas</option>
                  <option value="52">Armed Forces Europe</option>
                  <option value="54">Armed Forces Pacific</option>
                  <option value="102">British Columbia</option>
                  <option value="4">California</option>
                  <option value="5">Colorado</option>
                  <option value="6">Connecticut</option>
                  <option value="7">Delaware</option>
                  <option value="8">District of Columbia</option>
                  <option value="9">Florida</option>
                  <option value="10">Georgia</option>
                  <option value="11">Hawaii</option>
                  <option value="12">Idaho</option>
                  <option value="13">Illinois</option>
                  <option value="14">Indiana</option>
                  <option value="15">Iowa</option>
                  <option value="16">Kansas</option>
                  <option value="17">Kentucky</option>
                  <option value="18">Louisiana</option>
                  <option value="19">Maine</option>
                  <option value="103">Manitoba</option>
                  <option value="20">Maryland</option>
                  <option value="21">Massachusetts</option>
                  <option value="22">Michigan</option>
                  <option value="23">Minnesota</option>
                  <option value="24">Mississippi</option>
                  <option value="25">Missouri</option>
                  <option value="26">Montana</option>
                  <option value="27">Nebraska</option>
                  <option value="28">Nevada</option>
                  <option value="104">New Brunswick</option>
                  <option value="29">New Hampshire</option>
                  <option value="30">New Jersey</option>
                  <option value="31">New Mexico</option>
                  <option value="32">New York</option>
                  <option value="105">Newfoundland</option>
                  <option value="33">North Carolina</option>
                  <option value="34">North Dakota</option>
                  <option value="107">Northwest Territories</option>
                  <option value="106">Nova Scotia</option>
                  <option value="108">Nunavut</option>
                  <option value="35">Ohio</option>
                  <option value="36">Oklahoma</option>
                  <option value="109">Ontario</option>
                  <option value="37">Oregon</option>
                  <option value="38">Pennsylvania</option>
                  <option value="110">Prince Edward Island</option>
                  <option value="39">Puerto Rico</option>
                  <option value="111">Quebec</option>
                  <option value="40">Rhode Island</option>
                  <option value="112">Saskatchewan</option>
                  <option value="41">South Carolina</option>
                  <option value="42">South Dakota</option>
                  <option value="43">Tennessee</option>
                  <option value="44">Texas</option>
                  <option value="45">Utah</option>
                  <option value="46">Vermont</option>
                  <option value="47">Virginia</option>
                  <option value="48">Washington</option>
                  <option value="49">West Virginia</option>
                  <option value="50">Wisconsin</option>
                  <option value="51">Wyoming</option>
                  <option value="113">Yukon</option>
                </select>
            </div>
        </div>        

        <!-- Bill Zip -->
        <div class="builderprogramenrollment-request-form-controls-group  data-validation="control-group">
            <label class="builderprogramenrollment-request-form-label" for="zipcode">
                {{translate 'Zip Code'}}
            </label>
            <div class="builderprogramenrollment-request-form-controls" data-validation="control">
                <input {{#if hasAutoFocus}} autofocus {{/if}} type="text" name="zipcode" id="zipcode" class="builderprogramenrollment-request-form-input input-small-form">
            </div>
        </div>

        <!-- Phone Number -->
        <div class="builderprogramenrollment-request-form-controls-group" data-validation="control-group">
            <label class="builderprogramenrollment-request-form-label" for="phone">
                {{translate 'Phone Number'}}
            </label>
            <div class="builderprogramenrollment-request-form-controls" data-validation="control">
                <input type="text" name="phone" id="phone" class="builderprogramenrollment-request-form-input input-small-form" data-action="inputphone">
            </div>
        </div>
        <!-- Email -->
        <div class="builderprogramenrollment-request-form-controls-group" data-validation="control-group">
            <label class="builderprogramenrollment-request-form-label" for="email">
                {{translate 'Email Address <small class="builderprogramenrollment-request-form-required">*</small>'}}
            </label>
            <div class="builderprogramenrollment-request-form-controls" data-validation="control">
                <input type="email" name="email" id="email" class="builderprogramenrollment-request-form-input input-large-form" placeholder="{{translate 'your@email.com'}}">
            </div>
        </div>

        <!-- Federal Tax ID -->
        <div class="builderprogramenrollment-request-form-controls-group" data-validation="control-group">
            <label class="builderprogramenrollment-request-form-label" for="custentity_federal_tax_id">
                {{translate '9-Digit Federal Tax ID (US) <small class="builderprogramenrollment-request-form-required">*</small>'}}
            </label>
            <div class="builderprogramenrollment-request-form-controls" data-validation="control">
                <input {{#if hasAutoFocus}} autofocus {{/if}} type="text" name="custentity_federal_tax_id" id="custentity_federal_tax_id" class="builderprogramenrollment-request-form-input input-medium-form">
            </div>
        </div>

        <div class="builderprogramenrollment-request-form-controls-group" data-validation="control-group" style="display:none">
                <input type="text" name="custentity_builder_enrolment_req" id="custentity_builder_enrolment_req" class="builderprogramenrollment-request-form-input" value="T">
        </div>        

      </fieldset>

      <div class="builderprogramenrollment-request-form-submit">
            <button class="builderprogramenrollment-request-form-submit-button" type="submit">
            {{translate 'Submit Application'}}
            </button>
      </div>

        <div class="hide message"></div>

      <div class="builderprogramenrollment-view-alert">
          <div class="image-wrap"><img src="/c.530610/site/images/hammer-icon2.png"></div>
          We will review and approve your application right away. Start saving now! No gimmicks, just the details.

        <!--<img src="{{iconhammer}}">-->
      </div>
      <!--
      <div class="builderprogramenrollment-request-form-cancel">
            <button class="builderprogramenrollment-request-form-cancel-button" type="button" data-action="cancel-builderprogramenrollment-request" >Cancel</button>
      </div> 
      -->     
    </form>

</section>