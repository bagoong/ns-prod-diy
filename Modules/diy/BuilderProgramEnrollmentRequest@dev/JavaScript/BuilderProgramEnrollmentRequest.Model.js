/*
  © 2015 NetSuite Inc.
  User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
  provided, however, if you are an authorized user with a NetSuite account or log-in, you
  may use this code subject to the terms that govern your access and use.
*/

define('BuilderProgramEnrollmentRequest.Model',
  [
  'Backbone',
  'underscore'
  ],
  function (Backbone, _) {

    return Backbone.Model.extend({
      urlRoot: _.getAbsoluteUrl('services/BuilderProgramEnrollment.Request.Service.ss'),
      validation: {
        'firstname': {
          required: true,
          msg: 'Please enter a First Name'
        },
        'lastname': {
          required: true,
          msg: 'Please enter a Last Name'
        },
        'companyname': {
          required: true,
          msg: 'Please enter a Company Name'
        },
        'email': {
          required: true,
          msg: 'Please enter an Email'
        },
        'custentity_federal_tax_id': {
          required: true,
          msg: 'Please enter a 9-digit Federal Tax ID'
        },
        'phone': {
          required: true,
          fn: _.validatePhone,
          msg: 'Please enter a valid Phone Number'
        }
      }
    });
  }
);