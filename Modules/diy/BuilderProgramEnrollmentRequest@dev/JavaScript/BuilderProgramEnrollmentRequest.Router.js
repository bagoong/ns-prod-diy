/*
  © 2015 NetSuite Inc.
  User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
  provided, however, if you are an authorized user with a NetSuite account or log-in, you
  may use this code subject to the terms that govern your access and use.
*/

define(
  'BuilderProgramEnrollmentRequest.Router', [
    'Backbone',
    'BuilderProgramEnrollmentRequest.View',
    'BuilderProgramEnrollmentRequest.Model',
    'jQuery'
  ]
  , function (Backbone, View, Model, jQuery) {

  return Backbone.Router.extend({

      initialize: function(application) {
      this.application = application;
    },

    routes: {
      'builder-program': 'requestBuilderProgramEnrollment'
    },

    requestBuilderProgramEnrollment: function() {

      var view = new View({
        application: this.application
      });
      view.title = _('Builder Program Enrollment').translate();
      view.showContent();
    }

  });
});