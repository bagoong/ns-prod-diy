define('BuilderProgramEnrollmentRequest.View',
  [
    'Backbone',

    'builderprogramenrollment_request.tpl',
    'Backbone.FormView',
    'BuilderProgramEnrollmentRequest.Model'
  ],

  function (Backbone, builderprogramenrollment_request_template, FormView, Model) {

    return Backbone.View.extend({

      initialize: function(options) {
        this.model = new Model();
        FormView.add(this);
      },

      template: builderprogramenrollment_request_template,

      events: {
        'submit form': 'saveTheForm',
        'click [data-action="cancel-builderprogramenrollment-request"]': 'cancel',
        'blur [data-action="inputphone"]': 'formatPhone'
      },

      cancel: function() {
        Backbone.history.navigate('#/login-register', {trigger: true, replace: true});
      },

      //@method formatPhone Will try to reformat a phone number for a given phone Format,
      // If no format is given, it will try to use the one in site settings.
      //@param {jQuery.Event} e
      //@return {Void}
      formatPhone: function (e)
      {
        var $target = jQuery(e.target);
        $target.val(_($target.val()).formatPhone('123-456-7890'));
      },

      saveTheForm: function saveTheForm(e) {

            //jQuery('#b2b-request-form').submit();

            var self = this;
            var promise = FormView.saveForm.apply(this, arguments);

            e.preventDefault();

            return promise && promise.then(function promiseSuccessCallback(data) {
                if (data.successMessage) {
                  
                    self.showMessage(data.successMessage, 'success');
                    self.$('form').get(0).reset();
                } else {

                    self.showMessage('Error occurred, please try again.', 'error');
                }
            }, function promiseErrorCallback(jqXhr) {
                jqXhr.preventDefault = true;
                self.showMessage('Something went wrong processing your form, please try again later.', 'error');
            });
        },

        showMessage: function showMessage(message, type) {
            var $message = this.$('.message').removeClass('hide message-success message-error');
            if (type === 'success') {
                $message.addClass('message-success');
            } else {
                $message.addClass('message-error');
            }
            //$message.text(message).fadeIn(400).delay(3000).fadeOut();
            $message.text(message).fadeIn(400);
        },

        getBreadcrumbPages: function() {
          return [
            {text: 'Preferred Builder Program', href: '/builder-program'}
          ]
        },

        getContext: function() {

          return {
            id: this.model.get('internalid'),
            firstname: this.model.get('firstname'),
            lastname: this.model.get('lastname'),
            companyname: this.model.get('companyname'),
            email: this.model.get('email'),
            phone: this.model.get('phone'),
            address1: this.model.get('address1'),
            address2: this.model.get('address2'),
            city: this.model.get('city'),
            state: this.model.get('state'),
            zipcode: this.model.get('zipcode'),
            custentity_federal_tax_id: this.model.get('custentity_federal_tax_id'),
            custentity_builder_enrolment_req: this.model.get('custentity_builder_enrolment_req'),
            isperson: this.model.get('isperson'),
            iconhammer: _.getAbsoluteUrl('../site/images/iStock_hammer.jpg') 
          }
        }

    });
  }
)