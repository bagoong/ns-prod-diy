<div class="brandgrouping-sub-brands {{width}}">
    <!--<a href="/brandSubGroup?brandGroup={{associatedBrandGroup}}&brandSubGroup={{id}}">-->
    <a href="/{{brandURLComponent}}/{{urlcomponent}}">
        <div class="brandgrouping-sub-brands-image">
            {{#if image}}
            <img class="brandgrouping-sub-brands-logo-image" src="{{image}}">
            {{/if}}
        </div>
        <p class="title">{{name}}</p>
    </a>
</div>

