<div class="shopping-layout-breadcrumb">
    <ul class="global-views-breadcrumb" itemprop="breadcrumb">
        <li class="global-views-breadcrumb-item">
            <a href="/" data-touchpoint="home" data-hashtag="#"> Home </a>
        </li>
        <li class="global-views-breadcrumb-divider"><span class="global-views-breadcrumb-divider-icon"></span></li>
        <li class="global-views-breadcrumb-item"><a href="/brands"> Brands </a></li>
        <li class="global-views-breadcrumb-divider"><span class="global-views-breadcrumb-divider-icon"></span></li>
        <li class="global-views-breadcrumb-item"><a href="/{{brandURLComponent}}"> {{name}} </a></li>
        <li class="global-views-breadcrumb-divider"><span class="global-views-breadcrumb-divider-icon"></span></li>
        <li class="global-views-breadcrumb-item-active">Install Instructions</li>
    </ul>
</div>

<div class="brandgrouping-content">

    <div class="brandgrouping-container">

        <div class="brandgrouping-left-container">

            {{#if logo}}
            <div class="brandgrouping-logo-wrapper">
                <a href="/{{brandURLComponent}}">
                    <img class="brandgrouping-logo-image" src="{{logo}}">
                </a>
            </div>
            {{/if}}

            <nav class="brandgrouping-category-browse-facets">
                {{#if hasLearnAbout}}
                <div class="brandgrouping-learn-navigation">
                    <h3>Learn About It</h3>
                    {{#if hasCommonQuestions}}
                    <a href="{{brandURLComponent}}/common-questions">
                        <i class="infoIcon"></i><span>Common Questions</span>
                    </a>
                    {{/if}}
                    {{#if hasInstallInstructions}}
                    <a href="{{brandURLComponent}}/install-instructions">
                        <i class="infoIcon"></i><span>Install Instructions</span>
                    </a>
                    {{/if}}
                </div>
                {{/if}}
            </nav>

            <nav class="brandgrouping-category-browse-facets">
                <div class="brandgrouping-category-browse-navigation">
                    <h3>Categories</h3>
                    <div data-view="BrandGroup.Collection"></div>
                </div>
            </nav>
            <!--<div class="brandgrouping-logo">-->
            <!--{{#if logo}}-->
            <!--<img class="brandgrouping-logo-image" src="{{logo}}">-->
            <!--{{/if}}-->
            <!--</div>-->

            <!--<div class="brandgrouping-learn brandgrouping-row">-->
            <!--<h1>Learn About It</h1>-->
            <!--<a href="{{urlcomponent}}/common-questions">-->
            <!--<p><i class="infoIcon"></i>Common Questions</p>-->
            <!--</a>-->
            <!--<a href="{{urlcomponent}}/install-instructions">-->
            <!--<p><i class="infoIcon"></i>Install Instructions</p>-->
            <!--</a>-->
            <!--</div>-->

            <!--<h1 class="brandgrouping-header">Categories</h1>-->

            <!--<div data-view="BrandGroup.Collection"></div>-->
        </div>

        <div class="brandgrouping-right-container">
            <div class="brand-install-instructions">{{{installInstructions}}}</div>
        </div>
    </div>

</div>