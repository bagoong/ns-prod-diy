<div class="shopping-layout-breadcrumb">
    <ul class="global-views-breadcrumb" itemprop="breadcrumb">
        <li class="global-views-breadcrumb-item">
            <a href="/" data-touchpoint="home" data-hashtag="#"> Home </a>
        </li>
        <li class="global-views-breadcrumb-divider"><span class="global-views-breadcrumb-divider-icon"></span></li>
        <li class="global-views-breadcrumb-item-active"> Brands </li>
    </ul>
</div>
<div class="brandlisting-content">

    <div class="brandlisting-header" data-cms-area="brandlisting_header" data-cms-area-filters="path"></div>

    <div class="brandlisting-body" data-cms-area="brandlisting_body" data-cms-area-filters="path"></div>

    <div class="brandlisting-footer">
        <p>Alphabetical Brand Index</p>
        <div class="brandlisting-footer-alphabetical-index">
            <ul class="alphabetical-index">
                <li>#</li>
                {{#each letters}}
                    <li data-value="{{letter}}" class="{{enabled}}">{{letter}}</li>
                {{/each}}
            </ul>
        </div>

        {{#each brandAlphabeticalGroupList}}
        <div id="brandletter-{{letter}}" class="brandlisting-footer-alphabetical-result">
            <div class="brand-listing-letter-container">
                <span class="result-container-letter">{{letter}}</span>
            </div>
            <div class="brand-listing-brandgroup-container">
               <div class="brand-listing-brand-container">
                   {{#each brandGroupList}}
                   <div class="brand-listing-brand-details">
                       <a href="/{{urlComponent}}">
                           <img class="brand-cell-image"
                                src="{{resizeImage logo.logoName 'thumbnail'}}"
                           >
                           <p>{{name}}</p>
                       </a>
                   </div>
                   {{/each}}
               </div>
            </div>
        </div>
        {{/each}}

        <!--<div class="brandlisting-footer-alphabetical-result">-->
            <!--<ul class="brand-listing-alphabetical">-->
                <!--{{#each brandAlphabeticalGroupList}}-->
                <!--<li id="brandletter-{{letter}}">-->
                    <!--<div class="brand-listing-letter-container"><span class="result-container-letter">{{letter}}</span></div>-->
                    <!--<div class="brand-listing-brand-container">-->
                    <!--{{#each brandGroupList}}-->
                        <!--&lt;!&ndash;<a href="brands?brandId={{id}}">&ndash;&gt;-->
                        <!--<a href="/{{urlcomponent}}">-->
                        <!--<div class="brand-listing-brand-details">-->
                            <!--<img class="brand-cell-image"-->
                               <!--src="{{resizeImage logo.logoName 'thumbnail'}}"-->
                               <!--&gt;-->
                            <!--<p>{{name}}</p>-->
                        <!--</div>-->
                        <!--</a>-->
                    <!--{{/each}}-->
                    <!--</div>-->
                <!--</li>-->
                <!--{{/each}}-->
            <!--</ul>-->
        <!--</div>-->
    </div>

</div>