<div class="shopping-layout-breadcrumb">
    <ul class="global-views-breadcrumb" itemprop="breadcrumb">
        <li class="global-views-breadcrumb-item">
            <a href="/" data-touchpoint="home" data-hashtag="#"> Home </a>
        </li>
        <li class="global-views-breadcrumb-divider"><span class="global-views-breadcrumb-divider-icon"></span></li>
        <li class="global-views-breadcrumb-item"><a href="/brands"> Brands </a></li>
        <li class="global-views-breadcrumb-divider"><span class="global-views-breadcrumb-divider-icon"></span></li>
        <li class="global-views-breadcrumb-item-active">{{name}}</li>
    </ul>
</div>
<div class="brandgrouping-content">

    <div class="brandgrouping-container">
        <div class="brandgrouping-left-container">

            {{#if logo}}
            <div class="brandgrouping-logo-wrapper">
                <img class="brandgrouping-logo-image" src="{{logo}}">
            </div>
            {{/if}}

            <nav class="brandgrouping-category-browse-facets">
                {{#if hasLearnAbout}}
                <div class="brandgrouping-learn-navigation">
                    <h3>Learn About It</h3>
                    {{#if hasCommonQuestions}}
                    <a href="{{urlcomponent}}/common-questions">
                        <i class="infoIcon"></i><span>Common Questions</span>
                    </a>
                    {{/if}}
                    {{#if hasInstallInstructions}}
                    <a href="{{urlcomponent}}/install-instructions">
                        <i class="infoIcon"></i><span>Install Instructions</span>
                    </a>
                    {{/if}}
                </div>
                {{/if}}
            </nav>

            <nav class="brandgrouping-category-browse-facets">
                <div class="brandgrouping-category-browse-navigation">
                    <h3>Categories</h3>
                    <div data-view="BrandGroup.Collection"></div>
                </div>
            </nav>

        </div>

        <div class="brandgrouping-right-container">
            {{#if logo}}
            <div class="brandgrouping-logo-wrapper">
                <img class="brandgrouping-logo-image" src="{{logo}}">
            </div>
            {{/if}}

            <div class="brandgrouping-tags">
                <h1>{{title}}</h1>
            </div>

            <div class="brandgrouping-slider-container">
                {{#if brandSlider}}
                <div class="home-slider-container">
                    <div class="home-image-slider">
                        <ul data-slider class="home-image-slider-list">
                            {{#each brandSlider}}
                            <li>
                                <div class="home-slide-main-container">
                                    <a href="{{url}}">
                                        <img alt="" src="{{image.name}}">
                                        <div class="bx-caption">
                                            <span>{{title}}</span>
                                        </div>
                                    </a>
                                </div>
                            </li>
                            {{/each}}
                        </ul>
                    </div>
                </div>
                {{/if}}
            </div>

            <div class="brandgrouping-brand-description">
                <p>{{description}}</p>
            </div>

            <div class="brandgrouping-learn-navigation">
                {{#if hasLearnAbout}}
                <h3>Learn About It</h3>
                <ul>
                    {{#if hasCommonQuestions}}
                    <li>
                        <a href="{{urlcomponent}}/common-questions">
                            <i class="infoIcon"></i><span>Common Questions</span>
                        </a>
                    </li>
                    {{/if}}
                    {{#if hasInstallInstructions}}
                    <li>
                        <a href="{{urlcomponent}}/install-instructions">
                            <i class="infoIcon"></i><span>Install Instructions</span>
                        </a>
                    </li>
                    {{/if}}
                </ul>
                {{/if}}
            </div>

            <div class="brandgrouping-main-collection" data-view="BrandGroupMain.Collection"></div>
        </div>
    </div>

</div>