define('BrandGrouping.ListView',[
    'Backbone',
    'brand_group_listing.tpl',
    'BrandGroupNavigationList.View',
    'BrandGroupMainList.View',
    'Backbone.CollectionView',
    'Backbone.CompositeView',
    'Brand.Model',
    'SC.Configuration',
    'jQuery',
    'underscore'
], function BrandGroupingListView(
    Backbone,
    brandGroupListingTpl,
    BrandGroupNavigationListView,
    BrandGroupMainListView,
    CollectionView,
    CompositeView,
    BrandModel,
    Configuration,
    jQuery,
    _
) {
    'use strict';

    return Backbone.View.extend({

        title: 'DIY Home Center | The Best Brands for Your Deck & Home',

        initialize: function initialize(options) {
            CompositeView.add(this);
            this.collection = options.collection;
            this.brandModel = options.brandModel;
            this.title = this.brandModel.get('title') || 'DIY Home Center | The Best Brands for Your Deck & Home';
            this.listenTo(this.collection, 'sync', jQuery.proxy(this.render, this));
            this.implementBXSlider();
        },

        implementBXSlider: function implementBXSlider() {
            var self = this;
            this.windowWidth = jQuery(window).width();
            this.on('afterViewRender', function()
            {
                _.initBxSlider(self.$('[data-slider]'), {
                    nextText: '<a class="home-gallery-next-icon"></a>',
                    prevText: '<a class="home-gallery-prev-icon"></a>',
                    auto: true
                });
            });

            var windowResizeHandler = _.throttle(function ()
            {
                if (_.getDeviceType(this.windowWidth) === _.getDeviceType(jQuery(window).width()))
                {
                    return;
                }
                this.showContent();

                _.resetViewportWidth();

                this.windowWidth = jQuery(window).width();

            }, 1000);

            this._windowResizeHandler = _.bind(windowResizeHandler, this);

            jQuery(window).on('resize', this._windowResizeHandler);
        },

        destroy: function destroy() {
            Backbone.View.prototype.destroy.apply(this, arguments);
            jQuery(window).off('resize', this._windowResizeHandler);
        },

        childViews: {
            'BrandGroup.Collection': function(){
                return new CollectionView({
                    'childView': BrandGroupNavigationListView,
                    'collection': this.collection,
                    'viewsPerRow': Infinity
                });
            },
            'BrandGroupMain.Collection': function(){
                return new CollectionView({
                    'childView': BrandGroupMainListView,
                    'collection': this.collection,
                    'viewsPerRow': Infinity
                });
            }
        },

        getContext: function () {
            var hasCommonQuestions = this.brandModel.get('commonQuestions') && this.brandModel.get('commonQuestions').length;
            var hasInstallInstructions = this.brandModel.get('installInstructions') && this.brandModel.get('installInstructions').length;
            var hasLearnAbout = hasCommonQuestions || hasInstallInstructions;
            var brandSlider = [];
            var brandImage1 = this.brandModel.get('banner1Image');
            if (brandImage1.name) {
                brandSlider.push({image:brandImage1, url:this.brandModel.get('banner1URLComponent')});
            }
            var brandImage2 = this.brandModel.get('banner2Image');
            if (brandImage2.name) {
                brandSlider.push({image:brandImage2, url:this.brandModel.get('banner2URLComponent')});
            }
            var brandImage3 = this.brandModel.get('banner3Image');
            if (brandImage3.name) {
                brandSlider.push({image:brandImage3, url:this.brandModel.get('banner3URLComponent')});
            }
            var brandImage4 = this.brandModel.get('banner4Image');
            if (brandImage4.name) {
                brandSlider.push({image:brandImage4, url:this.brandModel.get('banner4URLComponent')});
            }
            var brandImage5 = this.brandModel.get('banner5Image');
            if (brandImage5.name) {
                brandSlider.push({image:brandImage5, url:this.brandModel.get('banner5URLComponent')});
            }


            return {
                name: this.brandModel.get('name') || 'None',
                title: this.brandModel.get('brandTitle'),
                description: this.brandModel.get('description'),
                logo: this.brandModel.get('logo').name,
                urlcomponent: this.brandModel.get('urlcomponent'),
                commonQuestions:this.brandModel.get('commonQuestions'),
                brandSlider:brandSlider,
                hasLearnAbout: hasLearnAbout,
                hasInstallInstructions: hasInstallInstructions,
                hasCommonQuestions: hasCommonQuestions
            };
        },

        template: brandGroupListingTpl

    });
  }
);