define('BrandListing.Router', [
    'SC.Configuration',
    'Backbone',

    'BrandListing.ListView',
    'BrandGrouping.ListView',
    'BrandLanding.ListView',
    'BrandLanding.Facet.View',
    'BrandListing.CommonQuestions.View',
    'BrandListing.InstallInstructions.View',

    'Brand.Model',
    'BrandGroup.Model',
    'BrandSubGroup.Model',
    'OrderedItems.Model',

    'Brand.Collection',
    'BrandGroup.Collection',
    'BrandItem.Collection',

    'Facets.Helper',
    'Facets.Model',
    'jQuery',
    'underscore'
], function BrandListingRouter(
    Configuration,
    Backbone,

    BrandListView,
    BrandGroupListView,
    BrandLandingListView,
    BrandLandingFacetView,
    BrandListingCommonQuestionsView,
    BrandListingInstallInstructionsView,

    BrandModel,
    BrandGroupModel,
    BrandSubGroupModel,
    OrderedItemModel,

    BrandCollection,
    BrandGroupCollection,
    BrandItemCollection,

    FacetHelper,
    FacetModel,
    jQuery,
    _
) {
    'use strict';

    return Backbone.Router.extend({
        initialize: function initialize(application) {
            this.application = application;
            this.translatorConfig = application.translatorConfig;
            this.globalBrands = SC.ENVIRONMENT.published.Brand_URL_Component;
        },

        routes: {
            'brands': 'brandListing'
        },

        testNestedURL: function testNestedURL(path) {
            var globalBrands = SC.ENVIRONMENT.published.Brand_URL_Component;
            var url = Backbone.history.fragment;
            var params = this.getUrlVars(url);

            var cutter = path.indexOf('/');
            var brand = path.substring(0, cutter);
            var brandSubGroup = path.substring(cutter + 1);
            var self = this;

            if (cutter < 0) {
                brand = brandSubGroup;

                _.each(globalBrands, function eachBrand(brandConfig){
                    if (brandConfig.urlcomponent == brand) {
                        self.brandGroupListPage(brandConfig.id);
                    }
                });
            } else if (brand != brandSubGroup){
                _.each(globalBrands, function eachBrand(brandConfig){
                    if (brandConfig.urlcomponent == brand) {
                        var brandId = brandConfig.id;
                        var brandSubGroups = brandConfig.brandSubGroups;
                        _.each(brandSubGroups, function eachBrandSubGroups(brandSubGroupConfig) {
                            if (brandSubGroupConfig.urlcomponent == brandSubGroup) {
                                self.brandSubGroupListPage(params, brandId, 0, brandSubGroupConfig.id);
                            }
                        });
                    }
                });
            } else {
                var brandCollection = new BrandCollection();
                this.brandListPage(brandCollection);
            }
        },

        getUrlVars: function getUrlVars(url) {
            var hash;
            var myJson = {};
            var hashes = url.slice(url.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                myJson[hash[0]] = hash[1];
            }
            return myJson;
        },

        brandListing: function brandListing(params) {
            var value;
            var brandCollection = new BrandCollection();

            if (params) {
                var position = params.indexOf('=');
                value =  params.substring(position + 1);
                if (value) {
                    this.brandGroupListPage(value);
                } else {
                    this.brandListPage(brandCollection);
                }
            } else {
                this.brandListPage(brandCollection);
            }


        },

        brandListPage: function brandListPage(brandCollection) {
            var brandListView = new BrandListView({
                collection:brandCollection,
                application: this.application
            });

            brandCollection.fetch().done(function() {
                brandListView.showContent();
            });
        },
        
        brandGroupListPage: function brandGroupListPage(value) {
            var brandModel = new BrandModel();
            var self = this;
            brandModel.fetch({
                data:{
                    id:value
                }
            }).done(function() {

                var brandGroupCollection = new BrandGroupCollection();
                var brandGroupListView = new BrandGroupListView({
                    brandModel:brandModel,
                    collection:brandGroupCollection,
                    application: self.application
                });

                brandGroupCollection.fetch({
                    data: {brandId: value}
                }).done(function() {
                    brandGroupListView.showContent();
                });

            });
        },

        brandSubGroupListPage: function brandSubGroupListPage(params, brandId, brandGroup, brandSubGroup) {
            var self = this;
            var url = Backbone.history.fragment;
            var translator = FacetHelper.parseUrl(url, this.translatorConfig);

            var brandItemCollection = new BrandItemCollection();
            var itemIds = [];

            brandItemCollection.fetch({
                data:{
                    brandSubgroupId:brandSubGroup
                }
            }).done(function() {

                _.each(brandItemCollection.models, function eachBrandItem(item) {
                    var itemId = item.get('columns').internalid.internalid;
                    itemIds.push(itemId);
                });

                // _.each([22049,22053,6677,550,6631,6627,6643,6650,6636], function eachIds(id) {
                //     itemIds.push(id);
                // });

                var orderedIds = _.uniq(itemIds,function(i){ return i; });


                if (params.show) {
                    translator.configuration.defaultShow = params.show;
                    translator.options.show = params.show;
                }

                translator.brandGroup = brandGroup;
                translator.brandSubGroup = brandSubGroup;

                var currCategory = [];
                //currCategory.type = 'facet';
                var model = new FacetModel();
                var itemListModel = new OrderedItemModel();

                var brandModel = new BrandModel();

                brandModel.fetch({
                    data:{ id:brandId }
                }).done(function() {
                    var brandGroupCollection = new BrandGroupCollection();
                    brandGroupCollection.fetch({
                        data: {brandId: brandId}
                    }).done(function() {
                        var brandSubGroupModel = new BrandSubGroupModel();

                        var view = new BrandLandingFacetView({
                            catModel: currCategory,
                            translator: translator,
                            translatorConfig: self.translatorConfig,
                            application: self.application,
                            model: model,
                            itemListModel: itemListModel,

                            brandModel:brandModel,
                            brandSubGroupModel:brandSubGroupModel,
                            collection:brandGroupCollection
                        });

                        brandSubGroupModel.fetch({
                            data: {brandSubGroupId: brandSubGroup}
                        }).done(function() {
                            var parallelLoadPromise = view.parallelLoadItemList(orderedIds, translator);
                            view.catModel.type = 'itemList';
                            view.catModel.custrecord_ecqs_category_page_title = 'Lorem Ipsum';

                            jQuery.when(parallelLoadPromise).done(function ()
                            {
                                view.model = view.itemListModel;
                                view.model.set('total', orderedIds.length);
                                view.title = brandSubGroupModel.get('name');
                                view.showContent();
                            });
                        });

                    });

                });

            });

        },

        commonQuestionPage: function commonQuestionPage() {
            var globalBrands = this.globalBrands;
            var url = Backbone.history.fragment;

            var cutter = url.indexOf('/');
            var brand = url.substring(0, cutter);
            if (brand) {
                var self = this;
                _.each(globalBrands, function eachBrand(brandConfig){
                    if (brandConfig.urlcomponent == brand) {

                        var brandModel = new BrandModel();
                        brandModel.fetch({
                            data:{
                                id:brandConfig.id
                            }
                        }).done(function() {

                            var brandGroupCollection = new BrandGroupCollection();
                            var brandListingCommonQuestionsView = new BrandListingCommonQuestionsView({
                                brandModel:brandModel,
                                collection:brandGroupCollection,
                                application: self.application
                            });

                            brandGroupCollection.fetch({
                                data: {brandId: brandConfig.id}
                            }).done(function() {
                                brandListingCommonQuestionsView.showContent();
                            });

                        });
                    }
                });
            }
        },

        installInstructionPage: function installInstructionPage() {
            var globalBrands = this.globalBrands;
            var url = Backbone.history.fragment;

            var cutter = url.indexOf('/');
            var brand = url.substring(0, cutter);
            if (brand) {
                var self = this;
                _.each(globalBrands, function eachBrand(brandConfig){
                    if (brandConfig.urlcomponent == brand) {

                        var brandModel = new BrandModel();
                        brandModel.fetch({
                            data:{
                                id:brandConfig.id
                            }
                        }).done(function() {

                            var brandGroupCollection = new BrandGroupCollection();
                            var brandListingInstallInstructionsView = new BrandListingInstallInstructionsView({
                                brandModel:brandModel,
                                collection:brandGroupCollection,
                                application: self.application
                            });

                            brandGroupCollection.fetch({
                                data: {brandId: brandConfig.id}
                            }).done(function() {
                                brandListingInstallInstructionsView.showContent();
                            });

                        });
                    }
                });
            }
        }
    });
});