define('BrandGroupNavigationList.View',[
    'Backbone',
    'brand_group_details.tpl',
    'BrandSubGroup.Collection',
    'Backbone.CollectionView',
    'Backbone.CompositeView',
    'BrandSubGroupNavigationList.View'
], function BrandGroupNavigationListView(
    Backbone,
    brand_group_details_tpl,
    BrandSubGroupCollection,
    CollectionView,
    CompositeView,
    BrandSubGroupNavigationListView
) {
    return Backbone.View.extend({
      initialize: function() {
          CompositeView.add(this);
          var self = this;
          // this.on('afterCompositeViewRender', function afterViewRender() {
          //     var id = self.model.get('id');
          //     setTimeout(function(){
          //         $('.brandgrouping-category-toggle-id-'+ id).slideToggle("slow");
          //     }, 1000);
          // });
      },

      childViews: {
          'BrandSubGroup.Collection': function(){
              var self = this;
              var brandSubGroupCollection = new BrandSubGroupCollection();

              var view = new CollectionView({
                  'childView': BrandSubGroupNavigationListView,
                  'collection': brandSubGroupCollection,
                  'viewsPerRow': Infinity
              });

              brandSubGroupCollection.fetch({
                  data: {
                      brandGroupId:self.model.get('id')
                  }
              }).done(function(){
                  _.each(brandSubGroupCollection.models, function eachModel(model) {
                      model.selectedBrandSubgroup = self.model.selectedBrandSubgroup
                  });



                  view.render();


              });

              return view;
          }
      },

      getContext: function () {


          return {
              name:this.model.get('name'),
              id:this.model.get('id')
          }
      },

      template: brand_group_details_tpl

    });
  }
);