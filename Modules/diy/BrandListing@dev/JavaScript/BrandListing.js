define('BrandListing',[
    'BrandListing.Router',
    'BrandListing.CommonQuestions.View',
    'BrandListing.InstallInstructions.View',
    'Brand.Collection',
    'BrandSubGroup.Collection',
    'SC.Configuration',
    'underscore'

], function BrandListing(
    Router,
    BrandListingCommonQuestionsView,
    BrandListingInstallInstructionsView,
    BrandCollection,
    BrandSubGroupCollection,
    Configuration,
    _
) {
    'use strict';
    return {      
      mountToApp: function(application) {

          application.on('afterModulesLoaded', function () {
              //console.log('afterModulesLoaded');


              var bootstappedBrands = SC.ENVIRONMENT.published.Brand_URL_Component;

              var facets_to_include;
              var components = _.compact(_.union(
                  [application.translatorConfig.fallbackUrl]
                  ,	facets_to_include || []
                  ,	_.pluck(application.translatorConfig.facets, 'url') || []
              ));

              var routerInstance = new Router(application);

              if (bootstappedBrands) {
                  _.each(bootstappedBrands, function eachBrands(brand) {

                      var urlToEmbed = brand.urlcomponent;
                      var facet_regex = '^\\b(' + urlToEmbed + ')\\b($|(\\/)($|' + components.join('|') + ')|\\?)';
                      Router.prototype.routes[ urlToEmbed  + '?*options'] = 'testNestedURL';
                      Router.prototype.routes[ urlToEmbed ] = 'testNestedURL';
                      routerInstance.route(new RegExp(facet_regex), 'testNestedURL');

                      _.each(brand.brandSubGroups, function eachBrandSubGroup(brandSubGroup) {

                          var urlToEmbed = brandSubGroup.brandURLComponent + "/" + brandSubGroup.urlcomponent;
                          var facet_regex = '^\\b(' + urlToEmbed + ')\\b($|(\\/)($|' + components.join('|') + ')|\\?)';
                          Router.prototype.routes[ urlToEmbed  + '?*options'] = 'testNestedURL';
                          Router.prototype.routes[ urlToEmbed ] = 'testNestedURL';
                          routerInstance.route(new RegExp(facet_regex), 'testNestedURL');

                      });

                      var otherPages = [{url:brand.urlcomponent+"/common-questions", type:"commonQuestionPage"}, {url:brand.urlcomponent+"/install-instructions", type:"installInstructionPage"}];

                      _.each(otherPages, function eachOtherPage(page) {

                          var urlToEmbed = page.url;
                          var facet_regex = '^\\b(' + urlToEmbed + ')\\b($|(\\/)($|' + components.join('|') + ')|\\?)';
                          Router.prototype.routes[ urlToEmbed  + '?*options'] = page.type;
                          Router.prototype.routes[ urlToEmbed ] = page.type;
                          routerInstance.route(new RegExp(facet_regex), page.type);

                      });

                  });

                  var urlToEmbed = 'brandListing';
                  var facet_regex = '^\\b(' + urlToEmbed + ')\\b($|(\\/)($|' + components.join('|') + ')|\\?)';
                  Router.prototype.routes[ urlToEmbed ] = 'brandListing';
                  routerInstance.route(new RegExp(facet_regex), 'brandListing');

                  return routerInstance;
              }



              // var brandSubGroupCollection = new BrandSubGroupCollection();
              // brandCollection.fetch().done(function() {
              //
              //
              //     _.each(brandCollection.models, function eachBrand(brand) {
              //         var customBrand = {id:brand.get('id'), urlcomponent:brand.get('urlcomponent'), brandSubGroups:{}};
              //         customBrands.push(customBrand);
              //     });
              //     //
              //     brandSubGroupCollection.fetch().done(function() {
              //         _.each(brandSubGroupCollection.models, function eachBrandSubGroup(brandSubGroup) {
              //
              //             var customBrandSubGroup = {id:brandSubGroup.get('id'),
              //                 urlcomponent:brandSubGroup.get('urlcomponent'),
              //                 brandURLComponent:brandSubGroup.get('brandURLComponent')};
              //             customBrandSubGroups.push(customBrandSubGroup);
              //         });
              //         _.each(customBrands, function eachBrand(brand) {
              //             var brandSubGroups = [];
              //             _.each(customBrandSubGroups, function eachBrandSubGroup(brandSubGroup) {
              //                 if (brand.urlcomponent == brandSubGroup.brandURLComponent) {
              //                     brandSubGroups.push(brandSubGroup);
              //                 }
              //             });
              //             brand.brandSubGroups = brandSubGroups;
              //
              //             var urlToEmbed = brand.urlcomponent;
              //             //console.log('urlToEmbed', urlToEmbed);
              //             var facet_regex = '^\\b(' + urlToEmbed + ')\\b($|(\\/)($|' + components.join('|') + ')|\\?)';
              //             Router.prototype.routes[ urlToEmbed  + '?*options'] = 'testNestedURL';
              //             Router.prototype.routes[ urlToEmbed ] = 'testNestedURL';
              //             routerInstance.route(new RegExp(facet_regex), 'testNestedURL');
              //         });
              //
              //         _.extend(Configuration, {
              //             brands:customBrands,
              //             hasBrandsDeployed:true
              //         });
              //
              //         _.each(customBrandSubGroups, function eachCustomBrandSubGroup(brandSubGroup) {
              //
              //             var urlToEmbed = brandSubGroup.brandURLComponent + "/" + brandSubGroup.urlcomponent;
              //             //console.log('urlToEmbed', urlToEmbed);
              //             var facet_regex = '^\\b(' + urlToEmbed + ')\\b($|(\\/)($|' + components.join('|') + ')|\\?)';
              //             Router.prototype.routes[ urlToEmbed  + '?*options'] = 'testNestedURL';
              //             Router.prototype.routes[ urlToEmbed ] = 'testNestedURL';
              //             routerInstance.route(new RegExp(facet_regex), 'testNestedURL');
              //         });
              //
              //         var urlToEmbed = 'brandListing';
              //         var facet_regex = '^\\b(' + urlToEmbed + ')\\b($|(\\/)($|' + components.join('|') + ')|\\?)';
              //         Router.prototype.routes[ urlToEmbed ] = 'brandListing';
              //         routerInstance.route(new RegExp(facet_regex), 'brandListing');
              //
              //         console.log('customBrands', customBrands);
              //
              //         return routerInstance;
              //
              //     });
              // });


          });

      }
    }
  }
);