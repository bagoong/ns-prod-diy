define('BrandSubGroupNavigationList.View',[
    'Backbone',
    'brand_sub_group_details.tpl'
], function BrandSubGroupNavigationListView(
    Backbone,
    brand_sub_group_details_tpl
) {
    return Backbone.View.extend({
        
      getContext: function () {

          var selected;

          if (this.model.get('id') == this.model.selectedBrandSubgroup) {
              selected = true;
          }

          return {
              name:this.model.get('name'),
              id:this.model.get('id'),
              urlcomponent:this.model.get('urlcomponent'),
              brandURLComponent:this.model.get('brandURLComponent'),
              associatedBrandGroup:this.model.get('associatedBrandGroup'),
              selected:selected
          }
      }, 

      template: brand_sub_group_details_tpl

    });
  }
);