define('BrandGroupMainList.View',[
        'Backbone',
        'brand_group_main_details.tpl',
        'BrandSubGroup.Collection',
        'BrandSubGroupNavigationListMain.View',
        'Backbone.CollectionView',
        'Backbone.CompositeView'

    ], function BrandGroupMainListView(
        Backbone,
        brand_group_main_details_tpl,
        BrandSubGroupCollection,
        BrandSubGroupNavigationListMainView,
        CollectionView,
        CompositeView

    ) {
        return Backbone.View.extend({
            initialize: function() {
                CompositeView.add(this);
            },

            childViews: {
                'BrandSubGroupMain.Collection': function(){
                    var self = this;
                    var brandSubGroupCollection = new BrandSubGroupCollection();

                    var view = new CollectionView({
                        'childView': BrandSubGroupNavigationListMainView,
                        'collection': brandSubGroupCollection,
                        'viewsPerRow': Infinity
                    });

                    brandSubGroupCollection.fetch({
                        data: {
                            brandGroupId:self.model.get('id')
                        }
                    }).done(function(){
                        view.render();
                    });
                    return view;
                }
            },

            getContext: function () {
                return {
                    name:this.model.get('name'),
                    description:this.model.get('description'),
                }
            },

            template: brand_group_main_details_tpl
        });
    }
);