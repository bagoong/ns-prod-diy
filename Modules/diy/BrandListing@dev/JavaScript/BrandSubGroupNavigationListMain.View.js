define('BrandSubGroupNavigationListMain.View',[
    'SC.Configuration',
    'Backbone',
    'brand_sub_group_main_details.tpl'
], function BrandSubGroupNavigationListMainView(
    Configuration,
    Backbone,
    brand_sub_group_main_details_tpl
) {
    return Backbone.View.extend({
        
        configureClass: function configureClass(width) {
            if (width == "100%") {
                return "colmd12";
            } else if (width == "50%") {
                return "colmd6";
            } else {
                return "colmd3";
            }
        },
        
        getContext: function () {
            var imagePath = this.model.get('image').name;
            if (!imagePath) {
                imagePath = Configuration.imageNotAvailable;
            }

            return {
                name:this.model.get('name'),
                width:this.configureClass(this.model.get('width')),
                image:imagePath,
                id:this.model.get('id'),
                associatedBrandGroup:this.model.get('associatedBrandGroup'),
                brandURLComponent:this.model.get('brandURLComponent'),
                urlcomponent:this.model.get('urlcomponent')
            }
        },

        template: brand_sub_group_main_details_tpl
    });
  }
);