define('BrandListing.InstallInstructions.View',[
    'Backbone',
    'BrandGroupNavigationList.View',
    'Backbone.CollectionView',
    'Backbone.CompositeView',
    'brand_install_instructions.tpl'
], function BrandListingInstallInstructionsView(
    Backbone,
    BrandGroupNavigationListView,
    CollectionView,
    CompositeView,
    brand_install_instructions_tpl

) {
    return Backbone.View.extend({

      initialize: function initialize(options) {
          CompositeView.add(this);
          this.collection = options.collection;
          this.brandModel = options.brandModel;

          this.listenTo(this.collection, 'sync', jQuery.proxy(this.render,this));
          
      },

        childViews: {
            'BrandGroup.Collection': function(){
                return new CollectionView({
                    'childView': BrandGroupNavigationListView,
                    'collection': this.collection,
                    'viewsPerRow': Infinity
                });
            }
        },

      getContext: function () {
          var hasCommonQuestions = this.brandModel.get('commonQuestions') && this.brandModel.get('commonQuestions').length;
          var hasInstallInstructions = this.brandModel.get('installInstructions') && this.brandModel.get('installInstructions').length;
          var hasLearnAbout = hasCommonQuestions || hasInstallInstructions;
          return {
              name: this.brandModel.get('name') || "None",
              logo: this.brandModel.get('logo').name,
              brandURLComponent: this.brandModel.get('urlcomponent'),
              installInstructions:this.brandModel.get('installInstructions'),
              hasLearnAbout: hasLearnAbout,
              hasInstallInstructions: hasInstallInstructions,
              hasCommonQuestions: hasCommonQuestions
          }
      },

      template: brand_install_instructions_tpl

    });
  }
);