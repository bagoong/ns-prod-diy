define('BrandListing.CommonQuestions.View',[
    'Backbone',
    'BrandGroupNavigationList.View',
    'Backbone.CollectionView',
    'Backbone.CompositeView',
    'brand_common_questions.tpl'
], function BrandListingCommonQuestionsView(
    Backbone,
    BrandGroupNavigationListView,
    CollectionView,
    CompositeView,
    brand_common_questions_tpl

) {
    return Backbone.View.extend({

      initialize: function initialize(options) {
          CompositeView.add(this);
          this.collection = options.collection;
          this.brandModel = options.brandModel;

          this.listenTo(this.collection, 'sync', jQuery.proxy(this.render,this));
          
      },

        childViews: {
            'BrandGroup.Collection': function(){
                return new CollectionView({
                    'childView': BrandGroupNavigationListView,
                    'collection': this.collection,
                    'viewsPerRow': Infinity
                });
            }
        },

      getContext: function () {
          var hasCommonQuestions = this.brandModel.get('commonQuestions') && this.brandModel.get('commonQuestions').length;
          var hasInstallInstructions = this.brandModel.get('installInstructions') && this.brandModel.get('installInstructions').length;
          var hasLearnAbout = hasCommonQuestions || hasInstallInstructions;
          return {
              name: this.brandModel.get('name') || "None",
              logo: this.brandModel.get('logo').name,
              brandURLComponent: this.brandModel.get('urlcomponent'),
              commonQuestions:this.brandModel.get('commonQuestions'),
              hasLearnAbout: hasLearnAbout,
              hasInstallInstructions: hasInstallInstructions,
              hasCommonQuestions: hasCommonQuestions
          }
      },

      template: brand_common_questions_tpl

    });
  }
);