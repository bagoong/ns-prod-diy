define('BrandLanding.Facet.CellView', [
    'ecft_subcategory_cell_template.tpl',

    'Backbone',
    'Backbone.CompositeView',
    'Backbone.CollectionView'
], function BrandLandingFacetCellView(
     ecft_subcategory_cell_template_tpl,

     Backbone,
     BackboneCompositeView,
     BackboneCollectionView
) {
    'use strict';

    // @class ECFTCategoryManager.SubcategoryCell.View @extends Backbone.View
    return Backbone.View.extend({

        template: ecft_subcategory_cell_template_tpl

        ,	initialize: function (options)
        {
            BackboneCompositeView.add(this);

            //console.log('ECFTCategoryManager.Subcategory options = ', options);

            this.subcategoryID = options.subcategoryID;

            this.subcategory = _.findWhere(ECQS.categories, {id : parseInt(this.subcategoryID)});

            //console.log('ECFTCategoryManager.Subcategory this.subcategory = ', this.subcategory);

        }

        // @method getContext @returns {Facets.FacetedNavigation.View.Context}
        ,	getContext: function ()
        {

            // @class Facets.FacetedNavigation.View.Context
            return {
                thumbnail : this.subcategory && this.subcategory.custrecord_ecqs_category_thumbnail ? this.subcategory.custrecord_ecqs_category_thumbnail.name : ''

            ,   url : this.subcategory && this.subcategory.custrecord_ecqs_category_url ? this.subcategory.custrecord_ecqs_category_url : ''

            ,   name : this.subcategory && this.subcategory.name ? this.subcategory.name : ''
        };
            // @class Facets.FacetedNavigation.View
        }
    });
});