define('BrandLanding',
  [
    'BrandLanding.Router'
  ],
  function (Router) {
    
    'use strict';
    return {      
      mountToApp: function(application) {
        return new Router(application);
      }
    }
  }
);