define('BrandLanding.Model',
  [
  'Backbone.CachedModel',
  'underscore'
  ],
  function (CachedModel, _) {
  	
    return CachedModel.extend({
      urlRoot: _.getAbsoluteUrl('services/Brand.BrandGroups.Service.ss')
    });
  }
);