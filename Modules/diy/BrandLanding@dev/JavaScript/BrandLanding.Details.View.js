define('BrandLanding.Details.View', [
    'Backbone',
    'brandlanding_details.tpl'
], function BrandLandingDetailsView(
     Backbone,
     brandLandingDetailsTpl
) {
    'use strict';

    return Backbone.View.extend({

        initialize: function initialize(options) {
            this.brandModel = options.brandModel;
            this.brandSubGroupModel = options.brandSubGroupModel;
        },

        getContext: function getContext() {
            return {
                brandName: this.brandModel.get('name'),
                brandId: this.brandModel.get('id'),
                brandLogo: this.brandModel.get('logo').name,
                brandSubGroupTitle: this.brandSubGroupModel.get('name'),
                brandSubGroupDescription: this.brandSubGroupModel.get('description')
            };
        },

        template: brandLandingDetailsTpl
    });
});