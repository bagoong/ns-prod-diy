define('BrandLanding.Facet.View', [
    'SC.Configuration',

    'Facets.Browse.View',
    'Facets.ItemListSortSelector.View',
    'Facets.ItemListShowSelector.View',
    'Facets.ItemListDisplaySelector.View',
    'GlobalViews.Pagination.View',

    'OrderedItems.Model',
    'LiveOrder.Model',

    'BrandLanding.Facet.SubView',
    'BrandLanding.Details.View',
    'BrandLanding.ListView',
    'GlobalViews.Breadcrumb.View',

    'jQuery',
    'Backbone',
    'Backbone.CompositeView',
    'Backbone.CollectionView',
    'underscore'

], function BrandLandingFacetView(
    Configuration,

    FacetsBrowseView,
    FacetsItemListSortSelectorView,
    FacetsItemListShowSelectorView,
    FacetsItemListDisplaySelectorView,
    GlobalViewsPaginationView,

    OrderedItemModel,
    LiveOrderModel,

    BrandLandingFacetSubView,
    BrandLandingDetailsView,
    BrandLandingListView,
    GlobalViewsBreadcrumbView,

    jQuery,
	Backbone,
	BackboneCompositeView,
    BackboneCollectionView,
	_
) {
    'use strict';

    var statuses = window.statuses = {};

    return FacetsBrowseView.extend({

        initialize: function initialize(options) {
            BackboneCompositeView.add(this);

            this.statuses = statuses;
            this.brandModel = options.brandModel;
            this.brandSubGroupModel = options.brandSubGroupModel;
            this.application = options.application;
            this.catModel = options.catModel;
            this.itemListModel = options.itemListModel;
            this.translator = options.translator;
            this.collection = options.collection;
            this.cart = LiveOrderModel.getInstance();

            this.childView = _.extend(FacetsBrowseView.prototype.childViews, {
                'BrandLanding.DetailsView': function newBrandLandingDetailsView() {
                    return new BrandLandingDetailsView( {
                        brandModel: this.brandModel,
                        brandSubGroupModel: this.brandSubGroupModel,
                        application: this.application
                    });
                },
                'BrandLanding.NavigationView': function newBrandLandingNavigationView() {
                    return new BrandLandingListView( {
                        brandModel: this.brandModel,
                        brandSubGroupModel: this.brandSubGroupModel,
                        collection: this.collection,
                        application: this.application
                    });
                }
            });

            this.on('afterViewRender', function afterViewRender() {
                this.$el
                    .find('.facets-facet-browse-results').addClass('brand-landing-details-container')
                    .prepend('<div class="brand-landing-details-view" data-view="BrandLanding.DetailsView"></div>');

                this.$el
                    .find('.facets-facet-browse-content')
                    .prepend('<div class="brand-landing-navigation-view" data-view="BrandLanding.NavigationView"></div>');

            });

            this.listenTo(this.collection, 'sync', jQuery.proxy(this.render, this));

            this.formatItemSorting();

            this.formatItemDisplay();

            this.formatItemShow();

            this.formatPager();
        },

        checkOccurrences: function checkOccurrences(string, subString, allowOverlapping) {
            var n = 0;
            var pos = 0;
            var step = allowOverlapping ? 1 : subString.length;

            string += '';
            subString += '';

            if (subString.length <= 0) return (string.length + 1);

            while (true) {
                pos = string.indexOf(subString, pos);
                if (pos >= 0) {
                    ++n;
                    pos += step;
                } else break;
            }
            return n;
        },

        formatItemSorting: function formatItemSorting() {
            var self = this;
            FacetsItemListSortSelectorView.prototype.installPlugin('postContext', {
                name: 'facetsItemListSortSelectorViewContext',
                priority: 10,
                execute: function execute(context, view) {

                    _.each(context.options, function eachProcessedOptions(obj) {
                        obj.configOptionUrl = obj.configOptionUrl.replace('/undefined', '');

                        var count = self.checkOccurrences(obj.configOptionUrl, "brandGroup");
                        if (count === 0) {
                            if (obj.configOptionUrl.indexOf('?') > 0) {
                                obj.configOptionUrl = obj.configOptionUrl + '&brandGroup=' + self.translator.brandGroup
                                    + '&brandSubGroup=' + self.translator.brandSubGroup;
                            } else {
                                obj.configOptionUrl = obj.configOptionUrl + '?brandGroup=' + self.translator.brandGroup
                                    + '&brandSubGroup=' + self.translator.brandSubGroup;
                            }
                        }
                    });

                    _.extend(context, {
                        options: context.options
                    });
                }
            });
        },

        formatItemDisplay: function formatItemDisplay() {
            var self = this;
            FacetsItemListDisplaySelectorView.prototype.installPlugin('postContext', {
                name: 'facetsItemListDisplaySelectorViewContext',
                priority: 10,
                execute: function execute(context, view) {

                    _.each(context.options, function eachProcessedOptions(obj) {
                        obj.configOptionUrl = obj.configOptionUrl.replace('/undefined', '');

                        var count = self.checkOccurrences(obj.configOptionUrl, "brandGroup");
                        if (count == 0) {
                            if (obj.configOptionUrl.indexOf('?') > 0) {
                                obj.configOptionUrl = obj.configOptionUrl + "&brandGroup="+self.translator.brandGroup
                                    + "&brandSubGroup="+self.translator.brandSubGroup;
                            } else {
                                obj.configOptionUrl = obj.configOptionUrl + "?brandGroup="+self.translator.brandGroup
                                    + "&brandSubGroup="+self.translator.brandSubGroup;
                            }
                        }
                    });

                    _.extend(context, {
                        options: context.options
                    });
                }
            });
        },

        formatItemShow: function formatItemShow() {
            var self = this;
            FacetsItemListShowSelectorView.prototype.installPlugin('postContext', {
                name: 'facetsItemListShowSelectorViewContext',
                priority: 10,
                execute: function execute(context, view) {

                    _.each(context.options, function eachProcessedOptions(obj) {
                        obj.configOptionUrl = obj.configOptionUrl.replace('/undefined', '');

                        var count = self.checkOccurrences(obj.configOptionUrl, "brandGroup");
                        if (count == 0) {
                            if (obj.configOptionUrl.indexOf('?') > 0) {
                                obj.configOptionUrl = obj.configOptionUrl + "&brandGroup="+self.translator.brandGroup
                                    + "&brandSubGroup="+self.translator.brandSubGroup;
                            } else {
                                obj.configOptionUrl = obj.configOptionUrl + "?brandGroup="+self.translator.brandGroup
                                    + "&brandSubGroup="+self.translator.brandSubGroup;
                            }
                        }
                    });

                    _.extend(context, {
                        options: context.options
                    });
                }
            });
        },

        formatPager: function formatPager() {
            var self = this;
            GlobalViewsPaginationView.prototype.installPlugin('postContext', {
                name: 'globalViewsPaginationViewContext',
                priority: 10,
                execute: function execute(context, view) {

                    context.currentPageURL = context.currentPageURL.replace('/undefined', '');
                    context.nextPageURL = context.nextPageURL.replace('/undefined', '');

                    var count = self.checkOccurrences(context.currentPageURL, "brandGroup");
                    if (count == 0) {
                        if (context.currentPageURL.indexOf('?') > 0) {
                            context.currentPageURL = context.currentPageURL + "&brandGroup="+self.translator.brandGroup
                                + "&brandSubGroup="+self.translator.brandSubGroup;

                            context.nextPageURL = context.nextPageURL + "&brandGroup="+self.translator.brandGroup
                                + "&brandSubGroup="+self.translator.brandSubGroup;
                        } else {
                            context.currentPageURL = context.currentPageURL + "?brandGroup="+self.translator.brandGroup
                                + "&brandSubGroup="+self.translator.brandSubGroup;

                            context.nextPageURL = context.nextPageURL + "?brandGroup="+self.translator.brandGroup
                                + "&brandSubGroup="+self.translator.brandSubGroup;
                        }
                    }

                    _.each(context.pages, function eachContextPages(page) {
                        page.URL = page.URL.replace('/undefined', '');
                        page.fixedURL = page.fixedURL.replace('/undefined', '');

                        var brandOccurencesOnURL = self.checkOccurrences(page.URL, "brandGroup");
                        var brandOccurencesOnFixedURL = self.checkOccurrences(page.fixedURL, "brandGroup");

                        if (brandOccurencesOnURL == 0 && brandOccurencesOnFixedURL == 0) {
                            if (page.URL.indexOf('?') > 0) {

                                page.URL = page.URL + "&brandGroup="+self.translator.brandGroup
                                    + "&brandSubGroup="+self.translator.brandSubGroup;

                                page.fixedURL = page.fixedURL + "&brandGroup="+self.translator.brandGroup
                                    + "&brandSubGroup="+self.translator.brandSubGroup;


                            } else {
                                page.URL = page.URL + "?brandGroup="+self.translator.brandGroup
                                    + "&brandSubGroup="+self.translator.brandSubGroup;

                                page.fixedURL = page.fixedURL + "?brandGroup="+self.translator.brandGroup
                                    + "&brandSubGroup="+self.translator.brandSubGroup;
                            }
                        }
                    });

                    _.extend(context, {
                        currentPageURL: context.currentPageURL,
                        nextPageURL:context.nextPageURL,
                        pages:context.pages
                    });
                }
            });
        },

        getBreadcrumbPages: function getBreadcrumbPages() {
            var breadcrumb = [];
            breadcrumb.push({categoryId:0, href:"/brands", text:"Brands"});
            breadcrumb.push({categoryId:0, href:"/"+this.brandModel.get('urlcomponent'), text:this.brandModel.get('name')});
            breadcrumb.push({categoryId:0, href:"/#", text:this.brandSubGroupModel.get('name')});
            return breadcrumb;
        },

        parallelLoadItemList: function parallelLoadItemList(ids, translator) {
            var self = this
            , 	itemIdCount = 1
            ,	numOfItemIds = ids.length
            ,	promises = []
            ,   page = translator.options.page || 1
            ,	maxNumOfCalls =  this.application.translatorConfig.defaultShow           //8   //numOfItemIds
            ,	numOfCalls = numOfItemIds / maxNumOfCalls > 1 ? maxNumOfCalls : numOfItemIds;

            this.catModel.itemIds = ids;
            this.catModel.initItemLoadCount = itemIdCount * numOfCalls;

            for (var i = 0; i < numOfCalls; i++) {
                var paginatedIndex = (page - 1) * numOfCalls;
                var itemIdArray = ids.slice((i * itemIdCount) + paginatedIndex, (i+1)*itemIdCount + paginatedIndex);

                //console.log(itemIdArray);
                if (itemIdArray.length > 0) {
                    var promise = self.loadItemListModel(itemIdArray, translator);
                    promises.push(promise);
                }
            }

            return jQuery.when.apply(jQuery, promises)
                .done(function() {
                    var items = [];

                    for (var i = 0; i < arguments.length; i++) {
                        var item = arguments[i][0] ? arguments[i][0].items : arguments[i].items;
                        items = items.concat(item);
                    }

                    if (translator.options.order == "pricelevel5:asc") {
                        ids = self.sortByPriceAscending(items);
                    }

                    if (translator.options.order == "pricelevel5:desc") {
                        ids = self.sortByPriceDescending(items);
                    }

                    if (translator.options.order == "displayname:desc") {
                        ids = self.sortByNameDescending(items);
                    }

                    if (translator.options.order == "displayname:asc") {
                        ids = self.sortByNameAscending(items);
                    }

                    var itemListModel = new OrderedItemModel({
                        ids: ids.join()
                    });

                    itemListModel.set('items', items);
                    itemListModel.set('total', items.length);
                    self.itemListModel = itemListModel;

                }).fail(function() {
                    // something went wrong here, handle it
                });
        },

        sortByPriceAscending: function sortByPriceAscending(items) {
            var pricelevel5Asc = items.slice(0);
            pricelevel5Asc.sort(function(a,b) {
                return a.onlinecustomerprice - b.onlinecustomerprice;
            });

            items = pricelevel5Asc;
            var newIds = [];
            _.each(items, function eachNewItem(item) {
                newIds.push(item.internalid);
            });

            return newIds;
        },

        sortByPriceDescending: function sortByPriceDescending(items) {
            var pricelevel5Desc = items.slice(0);
            pricelevel5Desc.sort(function(a,b) {
                return b.onlinecustomerprice - a.onlinecustomerprice;
            });

            items = pricelevel5Desc;
            var newIds = [];
            _.each(items, function eachNewItem(item) {
                newIds.push(item.internalid);
            });

            return newIds;
        },

        sortByNameDescending: function sortByNameDescending(items) {
            var displayNameDesc = items.slice(0);
            displayNameDesc.sort(function(a,b) {
                var x = a.displayname.toLowerCase();
                var y = b.displayname.toLowerCase();
                return x < y ? -1 : x > y ? 1 : 0;
            });

            items = displayNameDesc;
            var newIds = [];
            _.each(items, function eachNewItem(item) {
                newIds.push(item.internalid);
            });

            newIds.reverse();

            return newIds;
        },

        sortByNameAscending: function sortByNameAscending(items) {
            var displayNameAsc = items.slice(0);
            displayNameAsc.sort(function(a,b) {
                var x = a.displayname.toLowerCase();
                var y = b.displayname.toLowerCase();
                return x < y ? -1 : x > y ? 1 : 0;
            });

            items = displayNameAsc;
            var newIds = [];
            _.each(items, function eachNewItem(item) {
                newIds.push(item.internalid);
            });

            return newIds;
        },

        loadItemListModel: function loadItemListModel(itemIdArray, translator) {
            var itemListModel = new OrderedItemModel(
                {
                    ids: itemIdArray.join()
                })
            ,	modelData = translator.getApiParams();

            modelData.id = itemIdArray.join();		// max 10 ids per call
            modelData = _.omit(modelData, 'category');
            modelData.fieldset = 'search';
            modelData.limit = 100;

            return itemListModel.fetch({
                data: modelData
                ,	killerId: this.application.killerId
                ,	pageGeneratorPreload: true
            });
        },

        showContent: function showContent() {
            // If its a free text search it will work with the title
            var self = this
                ,	keywords = this.translator.getOptionValue('keywords')
                ,	resultCount = this.model.get('total')
                ,	categoryType = (self.catModel) ? self.catModel.type : 'facet';

            if (keywords)
            {
                keywords = decodeURIComponent(keywords);

                if (resultCount > 0)
                {
                    this.subtitle =  resultCount > 1 ? _('Results for "$(0)"').translate(keywords) : _('Result for "$(0)"').translate(keywords);
                }
                else
                {
                    this.subtitle = _('We couldn\'t find any items that match "$(0)"').translate(keywords);
                }
            }

            if (categoryType == 'itemList') {
                self.itemListModel.length = this.itemListModel.get('total');
            }
            this.totalPages = Math.ceil(resultCount / this.translator.getOptionValue('show'));



            this.application.getLayout().showContent(this).done(function ()
            {
                if(jQuery.fn.scPush)
                {
                    self.$el.find('[data-action="pushable"]').scPush({target: 'tablet'});
                }
            });
        },

	    getContext: function getContext() {
            var hasItemsAndFacets = true;
            var isEmptyList = true;
            var hasFacets = false;
            var isBrandPage = true;

            this.title = this.brandSubGroupModel.get('title');

            if (this.catModel.type == 'facet') {
                hasItemsAndFacets = !!((!_.isUndefined(this.model.get('items')) && this.model.get('items').length) && (!_.isUndefined(this.model.get('facets')) && this.model.get('facets').length));
                isEmptyList = (this.model.get('total') <= 0);
                hasFacets = !_.isUndefined(this.model.get('facets')) ? this.model.get('facets').length : false;
            } else {
                hasItemsAndFacets = !!(this.itemListModel.get('items').length);
                isEmptyList = (this.itemListModel.get('total') <= 0);
            }

            return {
                brandPage: isBrandPage,

                total: this.model.get('total'),

                isTotalProductsOne: this.model.get('total') === 1,

                hasFacets: hasFacets,

                hasItemsAndFacets: hasItemsAndFacets,

                keywords: this.translator.getOptionValue('keywords'),
                
                showResults: true,

                isEmptyList:  isEmptyList
            };
		}
	});
});
