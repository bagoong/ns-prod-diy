define('BrandLanding.Router', [
    'Backbone',
    'BrandListing.ListView',
    'BrandLanding.ListView',
    'BrandLanding.Facet.View',
    'Brand.Model',
    'BrandGroup.Model',
    'BrandGroup.Collection',
    'BrandSubGroup.Model',
    'BrandItem.Collection',
    'Brand.Collection',
    'OrderedItems.Model',
    'Facets.Helper',
    'Facets.Model',
    'jQuery',
    'underscore'
], function BrandLandingRouter(
    Backbone,
    BrandListView,
    BrandLandingListView,
    BrandLandingFacetView,
    BrandModel,
    BrandGroupModel,
    BrandGroupCollection,
    BrandSubGroupModel,
    BrandItemCollection,
    BrandCollection,
    OrderedItemModel,
    FacetHelper,
    FacetModel,
    jQuery,
    _
) {
    'use strict';

    return Backbone.Router.extend({

        initialize: function initialize(application) {
            this.application = application;
            this.translatorConfig = application.translatorConfig;
        },

        routes: {
            'brandSubGroup?*options': 'brandLanding'
        },

        getUrlVars: function getUrlVars(url) {
            var hash;
            var myJson = {};
            var hashes = url.slice(url.indexOf('?') + 1).split('&');
            var i;
            for (i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                myJson[hash[0]] = hash[1];
            }
            return myJson;
        },

        brandLanding: function brandLanding(params) {
            params = this.getUrlVars(params);
            var brandGroup;
            var brandSubGroup;
            if (params) {
                brandGroup =  params.brandGroup;
                brandSubGroup =  params.brandSubGroup;
                if (brandGroup && brandSubGroup) {
                    this.brandLandingPage(params, brandGroup, brandSubGroup);
                } else {
                    this.brandListPage();
                }
            } else {
                this.brandListPage();
            }
        },

        brandListPage: function brandListPage() {
            var brandCollection = new BrandCollection();
            var brandListView = new BrandListView({
                collection: brandCollection,
                application: this.application
            });

            brandCollection.fetch().done(function done() {
                brandListView.showContent();
            });
        },

        brandLandingPage: function brandLandingPage(params, brandGroup, brandSubGroup) {
            var brandGroupModel = new BrandGroupModel();
            var self = this;
            brandGroupModel.fetch({
                data: {
                    brandGroupId: brandGroup
                }
            }).done(function done() {
                var brandId = brandGroupModel.get('associatedBrand');
                self.brandGroupListPage(params, brandId, brandGroup, brandSubGroup);
            });
        },

        brandGroupListPage: function brandGroupListPage(params, brandId, brandGroup, brandSubGroup) {
            var self = this;
            var url = Backbone.history.fragment;
            var translator = FacetHelper.parseUrl(url, this.translatorConfig);

            var brandItemCollection = new BrandItemCollection();
            var itemIds = [];

            brandItemCollection.fetch({
                data: {
                    brandSubgroupId: brandSubGroup
                }
            }).done(function done() {
                var orderedIds;
                var currCategory = [];
                var model = new FacetModel();
                var itemListModel = new OrderedItemModel();
                var brandModel = new BrandModel();


                _.each(brandItemCollection.models, function eachBrandItem(item) {
                    var itemId = item.get('columns').internalid.internalid;
                    itemIds.push(itemId);
                });

                orderedIds = _.uniq(itemIds, function orderIds(i) { return i; });

                if (params.show) {
                    translator.configuration.defaultShow = params.show;
                    translator.options.show = params.show;
                }

                translator.brandGroup = brandGroup;
                translator.brandSubGroup = brandSubGroup;

                brandModel.fetch({
                    data: { id: brandId }
                }).done(function doneBrandModelFetch() {
                    var brandGroupCollection = new BrandGroupCollection();
                    brandGroupCollection.fetch({
                        data: {brandId: brandId}
                    }).done(function doneBrandGroupCollectionFetch() {
                        var brandSubGroupModel = new BrandSubGroupModel();

                        var view = new BrandLandingFacetView({
                            catModel: currCategory,
                            translator: translator,
                            translatorConfig: self.translatorConfig,
                            application: self.application,
                            model: model,
                            itemListModel: itemListModel,
                            brandModel: brandModel,
                            brandSubGroupModel: brandSubGroupModel,
                            collection: brandGroupCollection
                        });

                        brandSubGroupModel.fetch({
                            data: {brandSubGroupId: brandSubGroup}
                        }).done(function doneBrandSubGroupModelFetch() {
                            var parallelLoadPromise = view.parallelLoadItemList(orderedIds, translator);
                            view.catModel.type = 'itemList';

                            jQuery.when(parallelLoadPromise).done(function ()
                            {
                                view.model = view.itemListModel;
                                view.model.set('total', orderedIds.length);
                                view.showContent();
                            });
                        });
                    });
                });
            });
        }
    });
});