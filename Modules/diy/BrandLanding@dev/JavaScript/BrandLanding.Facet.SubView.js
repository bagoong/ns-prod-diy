define('BrandLanding.Facet.SubView', [
    'ecft_subcategory_template.tpl',

    'BrandLanding.Facet.CellView',

    'Backbone',
    'Backbone.CompositeView',
    'Backbone.CollectionView'
], function BrandLandingFacetSubView(
     ecft_subcategory_template_tpl,

     ECFTCategoryManagerSubcategoryCellView,

     Backbone,
     BackboneCompositeView,
     BackboneCollectionView
) {
    'use strict';

    // @class Facets.FacetedNavigation.View @extends Backbone.View
    return Backbone.View.extend({

        template: ecft_subcategory_template_tpl

    ,	initialize: function (options)
        {
            BackboneCompositeView.add(this);

            this.subcategories = options.subcategories;
            this.childView = ECFTCategoryManagerSubcategoryCellView;

            //console.log('ECFTCategoryManager.Subcategory subcategories = ', this.subcategories);
        }

    ,   render: function()
        {
            var self = this;

            if (this.template)
            {
                this._render();
            }

            var $content = this.$('#ecft-subcat-wrapper');

            ////console.log('$content', $content);

            _.each(this.subcategories, function(subcategory){
                //console.log('render, subcat id = ', subcategory.custrecord_ecqs_subcat_child.internalid);

                var child_view_instance = new (self.childView)({
                    subcategoryID: subcategory.custrecord_ecqs_subcat_child.internalid
                });

                //self.childCells.push(child_view_instance);

                child_view_instance.render();

                $content.append(child_view_instance.$el);

                //self.$el.append(child_view_instance.$el);

            });
        }

        // @method getContext @returns {Facets.FacetedNavigation.View.Context}
    ,	getContext: function ()
        {
            // @class Facets.FacetedNavigation.View.Context
            return {

            };
            // @class Facets.FacetedNavigation.View
        }
    });
});