define('BrandLanding.ListView',[
        'Backbone',
        'brand_landing.tpl',
        'BrandGroupNavigationList.View',
        'BrandGroupMainList.View',
        'Backbone.CollectionView',
        'Backbone.CompositeView',
        'Brand.Model',
        'BrandItem.Collection',
        'jQuery',
        'underscore'
    ], function BrandGroupingListView(
        Backbone,
        brand_landing_tpl,
        BrandGroupNavigationListView,
        BrandGroupMainListView,
        CollectionView,
        CompositeView,
        BrandModel,
        BrandItemCollection,
        jQuery,
        _
    ) {
        return Backbone.View.extend({

            initialize: function initialize(options) {
                CompositeView.add(this);
                this.collection = options.collection;
                this.brandModel = options.brandModel;
                this.brandSubGroupModel = options.brandSubGroupModel;
                this.listenTo(this.collection, 'sync', jQuery.proxy(this.render,this));
            },

            childViews: {
                'BrandGroup.Collection': function(){
                    var self = this;
                    _.each(this.collection.models, function eachModel(model) {
                        model.selectedBrandSubgroup = self.brandSubGroupModel.get('id')
                    });

                    return new CollectionView({
                        childView: BrandGroupNavigationListView,
                        collection: this.collection,
                        viewsPerRow: Infinity
                    });
                }
            },

            getContext: function getContext() {
                var hasCommonQuestions = this.brandModel.get('commonQuestions') && this.brandModel.get('commonQuestions').length;
                var hasInstallInstructions = this.brandModel.get('installInstructions') && this.brandModel.get('installInstructions').length;
                var hasLearnAbout = hasCommonQuestions || hasInstallInstructions;

                return {
                    brandName: this.brandModel.get('name'),
                    brandId: this.brandModel.get('id'),
                    brandLogo: this.brandModel.get('logo').name,
                    urlcomponent: this.brandModel.get('urlcomponent'),
                    hasLearnAbout: hasLearnAbout,
                    hasInstallInstructions: hasInstallInstructions,
                    hasCommonQuestions: hasCommonQuestions,
                    brandSubGroupName: this.brandSubGroupModel.get('name'),
                    brandSubGroupTitle: this.brandSubGroupModel.get('title'),
                    brandSubGroupDescription: this.brandSubGroupModel.get('description')
                };
            },

            template: brand_landing_tpl

        });
    }
);