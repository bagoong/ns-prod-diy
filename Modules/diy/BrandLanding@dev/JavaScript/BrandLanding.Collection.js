define('BrandLanding.Collection',
  [
    'Backbone',
    'BrandLanding.Model'
  ],
  function (Backbone, Model) {
    return Backbone.Collection.extend({
      model: Model,
      url: _.getAbsoluteUrl('services/Brand.BrandGroups.Service.ss')
    });
  }
);