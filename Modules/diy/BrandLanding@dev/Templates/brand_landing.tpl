<div class="brandgrouping-logo-wrapper">
    <a href="/{{urlcomponent}}">
        <img class="brandgrouping-logo-image" src="{{brandLogo}}">
    </a>
</div>

<nav class="brandgrouping-category-browse-facets">
    {{#if hasLearnAbout}}
    <div class="brandgrouping-learn-navigation">
        <h3>Learn About It</h3>
        {{#if hasCommonQuestions}}
        <a href="{{urlcomponent}}/common-questions">
            <i class="infoIcon"></i><span>Common Questions</span>
        </a>
        {{/if}}
        {{#if hasInstallInstructions}}
        <a href="{{urlcomponent}}/install-instructions">
            <i class="infoIcon"></i><span>Install Instructions</span>
        </a>
        {{/if}}
    </div>
    {{/if}}
</nav>

<nav class="brandgrouping-category-browse-facets">
    <div class="brandgrouping-category-browse-navigation">
        <h3>Categories</h3>
        <div data-view="BrandGroup.Collection"></div>
    </div>
</nav>