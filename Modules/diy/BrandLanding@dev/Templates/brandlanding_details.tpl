<div class="brandgrouping-tags">
    <h1>{{brandSubGroupTitle}}</h1>
</div>
{{#if brandSubGroupDescription}}
<div class="brandgrouping-subgroup-description">
    {{{brandSubGroupDescription}}}
</div>
{{/if}}