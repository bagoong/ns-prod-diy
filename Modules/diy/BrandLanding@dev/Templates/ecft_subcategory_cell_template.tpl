<div class="category-cell col-sm-4">
	<div class="category-cell-thumbnail thumbnail">
        <a href="{{url}}">
            <img src="{{thumbnail}}"/>
        </a>
	</div>
	<div class="category-cell-name">
        <a href="{{url}}">
            {{name}}
        </a>
	</div>
</div>