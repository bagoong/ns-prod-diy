define('OrderWizard.Router.Extended', [
	'OrderWizard.Router',
	'Wizard.Router',
	
	'underscore',
	'Backbone',
	'jQuery',
	'Utils'
],
function(
	OrderWizardRouter,
	WizardRouter,

	_,
	Backbone,
	jQuery,
	Utils
)
{
	'use strict';

	// NOTE: EXTENDING origina runStep function -- only changing the data-hashtag from 'orderhistory' to 'purchases'
	_.extend(OrderWizardRouter.prototype.runStep = function(options) {

		// Computes the position of the user in the flow
		var url = (options) ? Backbone.history.fragment.replace('?' + options, '') : Backbone.history.fragment
		,	position = this.getStepPosition(url)
		,	content = ''
		,	page_header = ''
		,	last_order_id = _.parseUrlOptions(options).last_order_id;

		if (last_order_id && !this.model.get('confirmation').get('internalid'))
		{
			if (this.profileModel.get('isGuest') !== 'T')
			{
				//checkout just finnished and user refreshed the doc.
				page_header = _('Your Order has been placed').translate();
				content = _('If you want to review your last order you can go to <a href="#" data-touchpoint="$(0)" data-hashtag="#/purchases/view/salesorder/$(1)">Your Account</a>. ')
					.translate('customercenter', last_order_id) +
					_('Or you can continue Shopping on our <a href="/" data-touchpoint="home">Home Page</a>. ').translate();
			}
			else
			{
				page_header = _('Your Shopping Cart is empty').translate();
				content = _('Continue Shopping on our <a href="/" data-touchpoint="home">Home Page</a>. ').translate();
			}

			var layout = this.application.getLayout();

			return layout.internalError && layout.internalError(content, page_header, _('Checkout').translate());
		}

		// if you have already placed the order you can not be in any other step than the last
		if (this.model && this.model.get('confirmation') && (this.model.get('confirmation').get('confirmationnumber') || this.model.get('confirmation').get('tranid')) && position.toLast !== 0)
		{
			window.location = Session.get('touchpoints.home');
			return;
		}

		WizardRouter.prototype.runStep.apply(this, arguments);

	});

	return {
		mountToApp: function(application){}
	}
});