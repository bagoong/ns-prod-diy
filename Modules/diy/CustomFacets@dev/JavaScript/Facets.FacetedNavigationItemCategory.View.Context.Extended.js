define('Facets.FacetedNavigationItemCategory.View.Context.Extended', [
	'Facets.FacetedNavigationItemCategory.View',
	'Backbone',
	'underscore',
	'jQuery'
],
function(
	FacetsFacetedNavigationItemCategoryView,
	Backbone,
	_,
	jQuery
)
{
	'use strict';

	FacetsFacetedNavigationItemCategoryView.prototype.getContext = _.wrap(
		FacetsFacetedNavigationItemCategoryView.prototype.getContext,
		getContext
	);

	function getContext(fn) {
		var context = fn.apply(this, _.toArray(arguments).slice(1));

        var original_values = _.isArray(this.model.get('values')) ? this.model.get('values') : [this.model.get('values')];

        _.each(original_values, function each(value) {
            if (value.url !== '') {
                value.showChildren = value.hasChildren;
            }
        });

		return context;
	}
	
});