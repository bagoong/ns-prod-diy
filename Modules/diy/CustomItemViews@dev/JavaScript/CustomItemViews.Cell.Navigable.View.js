/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module ItemViews
define('CustomItemViews.Cell.Navigable.View'
,	[	'ItemViews.SelectedOption.View'
	,	'ItemViews.Stock.View'
	,	'ItemViews.Price.View'
	,	'ItemViews.Cell.Navigable.View'

	,	'Backbone.CollectionView'
	,	'Backbone.CompositeView'

	,	'item_views_cell_navigable.tpl'
	,	'item_views_cell_actionable_selected_options_cell.tpl'

	,	'Backbone'
	,	'Utils'
	]
,	function (
		ItemViewsSelectedOptionView
	,	ItemViewsStockView
	,	ItemViewsPriceView
	,	ItemViewsCellNavigableView

	,	BackboneCollectionView
	,	BackboneCompositeView

	,	item_views_cell_navigable_tpl
	,	item_views_cell_actionable_selected_options_cell_tpl

	,	Backbone
	)
{
	'use strict';

	// CUSTOMIZATION
	ItemViewsCellNavigableView.prototype.getContext = function getContext()	{

		var item = this.model.get('item')
		,	line = this.model;

		// DIY Customization
		var itemStockStatus = item.get('custitem_stock_status');
		var backOrderedItemExpectedShipDate = item.get('custitem_backorder_expected_ship_date');
		var itemTimeToShip = item.get('custitem_time_to_ship');
		var shippingPromotion = item.get('custitem_shipping_promotion');
		var isShippingPromotionActive = shippingPromotion ? true : false;

		var isInStock = itemStockStatus == 'In Stock';
		var isBackordered = itemStockStatus == 'Backordered';

		//@class ItemViews.Navigable.View.Context
		return {
				//@property {String} itemId
				itemId: item.get('internalid')
				//@property {String} itemName
			,	itemName: item.get('_name')
				//@property {String} cellClassName
			,	cellClassName: this.options.cellClassName
				//@property {Boolean} isNavigable
			,	isNavigable: !!this.options.navigable && !!item.get('_isPurchasable')
				//@property {String} rateFormatted
			,	rateFormatted: line.get('rate_formatted')
				//@property {String} itemSKU
			,	itemSKU: item.get('_sku')
				//@property {Boolean} showOptions
			,	showOptions: !!(line.get('options') && line.get('options').length)
				//@property {String} itemImageURL
			,	itemImageURL: item.get('_thumbnail').url
				//@property {String} itemImageAltText
			,	itemImageAltText: item.get('_thumbnail').altimagetext
				//@property {String} itemURLAttributes
			,	itemURLAttributes: item.get('_linkAttributes')
				//@property {Number} quantity
			,	quantity: line.get('quantity')
				//@property {Boolean} showDetail2Title
			,	showDetail2Title: !!this.options.detail2Title
				//@property {String} detail2Title
			,	detail2Title: this.options.detail2Title
				//@property {String} detail2
			,	detail2: line.get(this.options.detail2)
				//@property {Boolean} showReason
			,	showBlockDetail2: !!line.get(this.options.detail2)
				//@property {Boolean} showDetail3Title
			,	showDetail3Title: !!this.options.detail3Title
				//@property {String} detail3Title
			,	detail3Title: this.options.detail3Title
				//@property {String} detail3
			,	detail3: line.get(this.options.detail3)
				//@property {Boolean} showComparePrice
			,	showComparePrice: line.get('amount') > line.get('total')
				//@property {String} comparePriceFormatted
			,	comparePriceFormatted: line.get('amount_formatted')

			// Customizations
			,	customManufacturer: item.get('custitem_manufacturer')
			,	itemStockStatus: itemStockStatus
			,	isInStock: isInStock
			,	isBackordered: isBackordered
			,	backOrderedItemExpectedShipDate: backOrderedItemExpectedShipDate
			,	itemTimeToShip: itemTimeToShip
			,	shippingPromotion: shippingPromotion
			,	isShippingPromotionActive: isShippingPromotionActive
		};
	};

});