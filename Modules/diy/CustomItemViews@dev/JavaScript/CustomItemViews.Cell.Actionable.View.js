/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module ItemViews
define('CustomItemViews.Cell.Actionable.View'
,	[
		'ItemViews.Price.View'
	,	'ItemViews.SelectedOption.View'
	,	'ItemViews.Stock.View'
	,	'ItemViews.Cell.Actionable.View'

	,	'Backbone.CollectionView'
	,	'Backbone.CompositeView'

	,	'item_views_cell_actionable.tpl'
	,	'item_views_cell_actionable_selected_options_cell.tpl'

	,	'Backbone'
	,	'underscore'
	]
,	function (
		ItemViewsPriceView
	,	ItemViewsSelectedOptionView
	,	ItemViewsStockView
	,	ItemViewsCellActionableView

	,	BackboneCollectionView
	,	BackboneCompositeView

	,	item_views_cell_actionable_tpl
	,	item_views_cell_actionable_selected_options_cell_tpl

	,	Backbone
	,	_
	)
{
	'use strict';

	// CUSTOMIZATION
	ItemViewsCellActionableView.prototype.getContext = function getContext()	{

		var item = this.model.get('item')
		,	thumbnail = item.get('_thumbnail');

		// DIY Customization
		var itemStockStatus = item.get('custitem_stock_status');
		var backOrderedItemExpectedShipDate = item.get('custitem_backorder_expected_ship_date');
		var itemTimeToShip = item.get('custitem_time_to_ship');
		var shippingPromotion = item.get('custitem_shipping_promotion');
		var isShippingPromotionActive = shippingPromotion ? true : false;

		var isInStock = itemStockStatus == 'In Stock';
		var isBackordered = itemStockStatus == 'Backordered';

		//@class ItemViews.Actionable.View.Context
		return {
				//@property {OrderLine.Model|Transaction.Line.Model} line
				line: this.model
				//@property {String} lineId
			,	lineId: this.model.get('internalid')
				//@property {ItemDetails.Model} item
			,	item: item
				//@property {String} itemId
			,	itemId: item.get('internalid')
				//@property {String} linkAttributes
			,	linkAttributes: item.get('_linkAttributes')
				//@property {String} imageUrl
			,	imageUrl: this.options.application.resizeImage(thumbnail.url, 'thumbnail')
				//@property {Boolean} isNavigable
			,	isNavigable: !!this.options.navigable && !!item.get('_isPurchasable')
				//@property {String} altImageText
			,	altImageText: thumbnail.altimagetext
				//@property {Boolean} showCustomAlert
			,	showCustomAlert: !!item.get('_cartCustomAlert')
				//@property {String} customAlertType
			,	customAlertType: item.get('_cartCustomAlertType') || 'info'
				//@property {Boolean} showActionsView
			,	showActionsView: !!this.options.ActionsView
				//@property {Boolean} showSummaryView
			,	showSummaryView: !!this.options.SummaryView
				//@property {Boolean} showAlert
			,	showAlert: !_.isUndefined(this.options.showAlert) ? !!this.options.showAlert : true
				//@property {Boolean} showGeneralClass
			,	showGeneralClass: !!this.options.generalClass
				//@property {String} generalClass
			,	generalClass: this.options.generalClass

			// Customizations
			,	customManufacturer: item.get('custitem_manufacturer')
			,	itemStockStatus: itemStockStatus
			,	isInStock: isInStock
			,	isBackordered: isBackordered
			,	backOrderedItemExpectedShipDate: backOrderedItemExpectedShipDate
			,	itemTimeToShip: itemTimeToShip
			,	shippingPromotion: shippingPromotion
			,	isShippingPromotionActive: isShippingPromotionActive
		};
	};

});