/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module ItemViews
define(
	'CustomItemViews.Price.View'
,	[
		'item_views_price.tpl'

	,	'Profile.Model'
	,	'Session'
	,	'SC.Configuration'
	,	'ItemViews.Price.View'

	,	'Backbone'
	,	'underscore'
	]
,	function(
		item_views_price_tpl

	,	ProfileModel
	,	Session
	,	Configuration
	,	ItemViewsPriceView

	,	Backbone
	,	_
	)
{
	'use strict';

	// CUSTOMIZATION
	ItemViewsPriceView.prototype.getContext = function getContext()	{

		var price_container_object = this.model.getPrice()
		,	is_price_range = !!(price_container_object.min && price_container_object.max)
		,	showComparePrice = false;

		if (this.options.linePrice && this.options.linePriceFormatted)
		{
			price_container_object.price = this.options.linePrice;
			price_container_object.price_formatted = this.options.linePriceFormatted;
		}

		if (!this.options.hideComparePrice)
		{
			showComparePrice = is_price_range ? price_container_object.max.price < price_container_object.compare_price : price_container_object.price < price_container_object.compare_price;
		}

		// DIY - CUSTOMIZATION - START
		var diyStockStatus;
		var diyIsInStock;
		var isMatrixChildItem = this.model.getSelectedMatrixChilds().length == 1;
		if (isMatrixChildItem)	{
			diyStockStatus = this.model.getSelectedMatrixChilds()[0].get('custitem_stock_status');
		} else {
			diyStockStatus = this.model.get('custitem_stock_status');
		}
		if ('In Stock' == diyStockStatus)	{
			diyIsInStock = true;
		} else {
			diyIsInStock = false;
		}
		// DIY - CUSTOMIZATION - END

		//@class ItemViews.Price.View.Context
		return {
				// @property {Boolean} isPriceEnabled
				isPriceEnabled: !ProfileModel.getInstance().hidePrices()
					// @property {String} urlLogin
			,	urlLogin: this.getUrlLogin()
				// @property {Boolean} isPriceRange
			,	isPriceRange: is_price_range
				// @property {Boolean} showComparePrice
			,	showComparePrice: showComparePrice
				// @property {Boolean} isInStock
			//,	isInStock: this.model.getStockInfo().isInStock
			,	isInStock: diyIsInStock
					// @property {ItemDetails.Model} model
			,	model: this.model
				// @property {String} currencyCode
			,	currencyCode: SC.getSessionInfo('currency') ? SC.getSessionInfo('currency').code : ''
				// @property {String} priceFormatted
			,	priceFormatted: price_container_object.price_formatted || ''
				// @property {String} comparePriceFormatted
			,	comparePriceFormatted: price_container_object.compare_price_formatted || ''
				// @property {String} minPriceFormatted
			,	minPriceFormatted: price_container_object.min ? price_container_object.min.price_formatted : ''
				// @property {String} maxPriceFormatted
			,	maxPriceFormatted: price_container_object.max ? price_container_object.max.price_formatted : ''
				// @property {Number} price
			,	price: price_container_object.price ? price_container_object.price : 0
				// @property {Number} comparePrice
			,	comparePrice: price_container_object.compare_price ? price_container_object.compare_price : 0
				// @property {Number} minPrice
			,	minPrice: price_container_object.min ? price_container_object.min.price : 0
				// @property {Number} maxPrice
			,	maxPrice: price_container_object.max ? price_container_object.max.price : 0
				// @property {Boolean} showHighlightedMessage
			,	showHighlightedMessage:	_.indexOf(ItemViewsPriceView.highlightedViews, this.options.origin) >= 0
		};
		//@class ItemViews.Price.View

	};
});