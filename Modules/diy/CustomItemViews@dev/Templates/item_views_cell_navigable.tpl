{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<tr class="item-views-cell-navigable {{cellClassName}} item-{{itemId}}" data-id="{{itemId}}" data-item-type="{{itemType}}">
	<td class="item-views-cell-navigable-item-image" name="item-image">
		<img src="{{resizeImage itemImageURL 'thumbnail'}}" alt="{{itemImageAltText}}">
	</td>
	<td class="item-views-cell-navigable-details" name="item-details">
		<p class="item-views-cell-navigable-product-name">
			{{#if isNavigable}}
				<a class="item-views-cell-navigable-product-title-anchor" {{{itemURLAttributes}}}>{{itemName}}</a>
			{{else}}
				<span class="item-views-cell-navigable-product-title">
					{{itemName}}
				</span>
			{{/if}}
		</p>

		<!-- custom field -->
		<div class="byItem">
			{{translate 'by '}}{{customManufacturer}}
		</div>
		<p>
			<span class="item-views-cell-navigable-sku-label">{{translate 'Item #: '}} </span>
			<span class="item-views-cell-navigable-sku-value">{{itemSKU}}</span>
		</p>
		<p>
			<div data-view="Item.Price"></div>
		</p>

		{{#if showOptions}}
			<div data-view="Item.Options"></div>
		{{/if}}

		<!-- custom field
		<div class="info-data">
			<div class="info-content">
				<span class="name-info">{{translate 'Frame color:'}}</span>
				<span class="color-info first"></span>
				<span class="name-color-info">{{translate 'Textured Silver'}}</span>
			</div>
			<div class="info-content">
				<span class="name-info">{{translate 'Slat color:'}}</span>
				<span class="color-info second"></span>
				<span class="name-color-info">{{translate 'Mahagony'}}</span>
			</div>
		</div>
		-->

		<!-- custom field -->
		<div class="back-order">
			<div class="back-data">
				{{#if isInStock}}
					<span class="expected-date">{{itemTimeToShip}}</span>
				{{else}}
					{{#if isBackordered}}
						<div class="back-title">
							<!--<span>-->
							<!--<img src="http://shopping.na1.netsuite.com/c.530610/site/images/icon-back-ordered.jpg" alt="">-->
							<!--</span>-->
							<i></i>
							<span>{{translate 'Back Ordered'}}</span>
						</div>
						<p class="in-date"><span class="in-date-label">{{translate 'Expected ship date:'}}</span> {{backOrderedItemExpectedShipDate}}</p>
					{{/if}}
				{{/if}}
			</div>
			{{#if isShippingPromotionActive}}
			<a href="" class="ships-free">
				<!--<span>-->
				<!--<img src="http://shopping.na1.netsuite.com/c.530610/site/images/icon-trak-ship-free.jpg" alt="">-->
				<!--</span>-->
				<!--<span class="text-icon">{{shippingPromotion}}</span>-->
				<i></i>
				<span>{{shippingPromotion}}</span>
			</a>
			{{/if}}
		</div>

		<p>
			<span class="item-views-cell-navigable-stock" data-view="ItemViews.Stock.View">
		</p>
	</td>
	<!--
	<td class="item-views-cell-navigable-item-unit-price" name="item-totalprice">
	{{#if showBlockDetail2}}
		<p>
		{{#if showDetail2Title}}
			<span class="item-views-cell-navigable-item-unit-price-label">{{detail2Title}} </span>
		{{/if}}
		<span class="item-views-cell-navigable-item-reason-value">{{detail2}}</span>
		</p>
	{{/if}}
	</td>
	-->
	<td class="item-views-cell-navigable-item-quantity" name="item-quantity">
		<p>
			<span class="item-views-cell-navigable-item-quantity-label">{{translate 'Qty:'}} </span>
			<span class="item-views-cell-navigable-item-quantity-value">{{quantity}}</span>
		</p>
	</td>
	<td class="item-views-cell-navigable-amount" name="item-amount">
		<p>
		{{#if showDetail3Title}}
			<span class="item-views-cell-navigable-item-amount-label">{{translate 'Item total:'}} </span>
		{{/if}}
		<span class="item-views-cell-navigable-item-amount-value">{{detail3}}</span>
		{{#if showComparePrice}}
			<small class="item-views-cell-navigable-item-old-price">{{comparePriceFormatted}}</small>
		{{/if}}
		</p>
	</td>
</tr>
