{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<tr id="{{lineId}}" data-item-id="{{itemId}}" data-type="order-item" {{#if showGeneralClass}} class="{{generalClass}}" {{/if}} >
	<td class="item-views-cell-actionable-table-first">
		<div class="item-views-cell-actionable-thumbnail">
			{{#if isNavigable}}
				<a {{linkAttributes}}>
					<img src="{{resizeImage item._thumbnail.url 'thumbnail'}}" alt="{{item._thumbnail.altimagetext}}">
				</a>
			{{else}}
				<img src="{{resizeImage item._thumbnail.url 'thumbnail'}}" alt="{{item._thumbnail.altimagetext}}">
			{{/if}}
		</div>
	</td>
	<td class="item-views-cell-actionable-table-middle">
		<div class="item-views-cell-actionable-name">
		{{#if isNavigable}}
			<a {{linkAttributes}} class="item-views-cell-actionable-name-link">
				{{item._name}}
			</a>
		{{else}}
				<span class="item-views-cell-actionable-name-viewonly">{{item._name}}</span>
		{{/if}}

		<!-- custom field -->
		<div class="byItem">
			{{translate 'by '}}{{customManufacturer}}
		</div>

		</div>

		<div class="item-views-cell-actionable-sku">
			<p>
				<span class="item-views-cell-actionable-sku-label">{{translate 'Item #: '}}</span>
				<span class="item-views-cell-actionable-sku-value">{{ item._sku}}</span>
			</p>
		</div>

		<div class="item-views-cell-actionable-price">
			<div data-view="Item.Price"></div>
		</div>

		<div class="item-views-cell-actionable-options">
			<div data-view="Item.SelectedOptions"></div>
		</div>

		<!-- custom field
		<div class="info-data">
			<div class="info-content">
				<span class="name-info">{{translate 'Frame color:'}}</span>
				<span class="color-info first"></span>
				<span class="name-color-info">{{translate 'Textured Silver'}}</span>
			</div>
			<div class="info-content">
				<span class="name-info">{{translate 'Slat color:'}}</span>
				<span class="color-info second"></span>
				<span class="name-color-info">{{translate 'Mahagony'}}</span>
			</div>
		</div>
		-->

		<!-- custom field -->

		<div class="back-order">
			<div class="back-data">
				{{#if isInStock}}
					<span class="expected-date">{{itemTimeToShip}}</span>
				{{else}}
					{{#if isBackordered}}
						<div class="back-title">
							<!--<span>-->
								<!--<img src="http://shopping.na1.netsuite.com/c.530610/site/images/icon-back-ordered.jpg" alt="">-->
							<!--</span>-->
							<i></i>
							<span>{{translate 'Back Ordered'}}</span>
						</div>
						<p class="in-date"><span class="in-date-label">{{translate 'Expected ship date:'}}</span> {{backOrderedItemExpectedShipDate}}</p>
					{{/if}}
				{{/if}}
			</div>
			{{#if isShippingPromotionActive}}
			<a href="" class="ships-free">
				<!--<span>-->
					<!--<img src="http://shopping.na1.netsuite.com/c.530610/site/images/icon-trak-ship-free.jpg" alt="">-->
				<!--</span>-->
				<!--<span class="text-icon">{{shippingPromotion}}</span>-->
				<i></i>
				<span>{{shippingPromotion}}</span>
			</a>
			{{/if}}
		</div>

		{{#if showSummaryView}}
			<div class="item-views-cell-actionable-summary" data-view="Item.Summary.View"></div>
		{{/if}}
		<div class="item-views-cell-actionable-stock" data-view="ItemViews.Stock.View">
		</div>

	</td>
	<td class="item-views-cell-actionable-table-last">
		<div data-view="Item.Actions.View"></div>

		{{#if showAlert}}
			<div class="item-views-cell-actionable-alert-placeholder" data-type="alert-placeholder"></div>
		{{/if}}

		{{#if showCustomAlert}}
			<div class="alert alert-{{customAlertType}}">
				{{item._cartCustomAlert}}
			</div>
		{{/if}}
	</td>
</tr>
