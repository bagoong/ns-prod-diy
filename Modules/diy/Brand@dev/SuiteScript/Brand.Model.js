define('Brand.Model', [
    'SC.Model',
    'StoreItem.Model',
    'SearchHelper',
    'underscore'
], function BrandModel(
	SCModel,
	StoreItem,
	SearchHelper,
	_
) {
    'use strict';

    return SCModel.extend({
        name: 'Brand',

        /** BRANDS **/

        brandRecord: 'customrecord_diy_brand',

        brandFieldSets: {
            basic: [
                'id',
                'name',
                'logo',
                'description',
                'title',
                'brandTitle',
                'urlcomponent',
                'commonQuestions',
                'installInstructions',
                'banner1Image',
                'banner1URLComponent',
                'banner2Image',
                'banner2URLComponent',
                'banner3Image',
                'banner3URLComponent',
                'banner4Image',
                'banner4URLComponent',
                'banner5Image',
                'banner5URLComponent'
			 ]
        },

        brandFilters: [
			{fieldName: 'isinactive', operator: 'is', value1: 'F'}
        ],

        getAllBrands: function getAllBrands() {
            var filters = _.clone(this.brandFilters);
            var Search = new SearchHelper(this.brandRecord, filters, this.brandColumns, this.brandFieldSets.basic);

            Search.search();

            return Search.getResults();
		 },

        brandColumns: {
            id: {fieldName: 'internalid'},
            name: { fieldName: 'name'},
            logo: {fieldName: 'custrecord_diy_brand_logo', type: 'file'},
            description: {fieldName: 'custrecord_diy_brand_description'},
            title: {fieldName: 'custrecord_diy_brand_title'},
            brandTitle: {fieldName: 'custrecord_brand_h1_title'},
            urlcomponent: {fieldName: 'custrecord_diy_brand_urlcomponent'},
            commonQuestions: {fieldName: 'custrecord_diy_brand_commonquestions'},
            installInstructions: {fieldName: 'custrecordcustrecord_diy_brand_instrct_p'},
            banner1Image: {fieldName: 'custrecord_diy_brand_image_1', type: 'file'},
            banner1URLComponent: {fieldName: 'custrecord_diy_brand_url_1'},
            banner2Image: {fieldName: 'custrecord_diy_brand_image_2', type: 'file'},
            banner2URLComponent: {fieldName: 'custrecord_diy_brand_url_2'},
            banner3Image: {fieldName: 'custrecord_diy_brand_image_3', type: 'file'},
            banner3URLComponent: {fieldName: 'custrecord_diy_brand_url_3'},
            banner4Image: {fieldName: 'custrecord_diy_brand_image_4', type: 'file'},
            banner4URLComponent: {fieldName: 'custrecord_diy_brand_url_4'},
            banner5Image: {fieldName: 'custrecord_diy_brand_image_5', type: 'file'},
            banner5URLComponent: {fieldName: 'custrecord_diy_brand_url_5'}

		 },

		 getBrandById: function getBrandById(id) {
     		var filters = _.clone(this.brandFilters);
     		var Search = new SearchHelper(this.brandRecord, filters, this.brandColumns, this.brandFieldSets.basic);

			Search.addFilter({
				fieldName: 'internalid',
				operator: 'is',
				value1: id
			});

			Search.search();

			return Search.getResult();
		},

		/** BRAND GROUPINGS **/

		brandGroupingRecord: 'customrecord_diy_brand_grouping',

		brandGroupingFieldSets: {
			basic: [
				'id',
				'name',
				'associatedBrand',
				'description',
				'brandGroupingSequence'
			]
		},

		brandGroupingFilters: [
			{fieldName: 'isinactive', operator:'is', value1:'F'}],

		brandGroupingColumns:{
			id: { fieldName: 'internalid' },
            name: { fieldName: 'name' },
            associatedBrand: { fieldName: 'custrecord_diy_brandgroup_brand' },
            description: { fieldName: 'custrecord_diy_brand_grouping_desc' },
            brandGroupingSequence: { fieldName: 'custrecord_diy_brand_group_sequence'}
		},

		getAllBrandGroupingsByBrandId: function getAllBrandGroupingsByBrandId(brandId) {
			
			var resultsObject = [];

			var cols = [];
			cols[0] = new nlobjSearchColumn('internalid', null, null);
			cols[1] = new nlobjSearchColumn('name', null, null);
			cols[2] = new nlobjSearchColumn('custrecord_diy_brandgroup_brand', null, null);
			cols[3] = new nlobjSearchColumn('custrecord_diy_brand_grouping_desc', null, null);
			cols[4] = new nlobjSearchColumn('custrecord_diy_brand_group_sequence', null, null);
			cols[4].setSort();

			var filters = [["custrecord_diy_brandgroup_brand","anyof",brandId],"AND",["isinactive","is","F"]];

			var results = nlapiSearchRecord('customrecord_diy_brand_grouping', null, filters, cols);
			
			for (var i = 0; results && i < results.length; i++)	{
				resultsObject.push({
					'id': results[i].getValue('internalid'),
					'name': results[i].getValue('name'),
					'associatedBrand': results[i].getValue('custrecord_diy_brandgroup_brand'),
					'description': results[i].getValue('custrecord_diy_brand_grouping_desc'),
					'brandGroupingSequence': results[i].getValue('custrecord_diy_brand_group_sequence')
				});
			}

			return resultsObject;
		},

		getBrandGroupById:function getBrandGroupById(brandGroupId) {
			var filters = _.clone(this.brandGroupingFilters);
			var Search = new SearchHelper(this.brandGroupingRecord, filters, this.brandGroupingColumns, this.brandGroupingFieldSets.basic);

			Search.addFilter({
				fieldName: 'internalid',
				operator: 'is',
				value1: brandGroupId
			});

			Search.search();

			return Search.getResult();
		},

		/** BRAND SUB-GROUPINGS **/

		brandSubGroupingRecord: 'customrecord_diy_brand_subgrouping',

		brandSubGroupingFieldSets: {
			basic: [
				'id',
				'name',
				'associatedBrandGroup',
				'description',
				'image',
				'title',
				'width',
				'urlcomponent',
				'brandURLComponent',
				'brandId',
				'brandGroupingSequence'
			]
		},

		brandSubGroupingFilters: [
			{fieldName: 'isinactive', operator:'is', value1:'F'}],

		 brandSubGroupingColumns: {
            id: { fieldName: 'internalid' },
            name: { fieldName: 'name' },
            associatedBrandGroup: { fieldName: 'custrecord_diy_brand_subgroup_brandgroup' },
            description: { fieldName: 'custrecord_diy_brand_subgrouping_desc' },
            image: {
                fieldName: 'custrecord_diy_brand_subgrouping_img',
                type: 'file'
            },
            title: { fieldName: 'custrecord_diy_brand_subgroup_title' },
            width: {
                fieldName: 'custrecord_diy_brand_subgrouping_width',
                type: 'getText'
            },
            urlcomponent: { fieldName: 'custrecord_brand_subgrouping_urlcompnent' },
            brandURLComponent: { fieldName: 'custrecord_diy_brand_subgroup_brandurl' },
            brandId: { fieldName: 'custrecord_diy_brand_subgroup_brandintid' },
            brandSubGroupSequence: { fieldName: 'custrecord_diy_brand_subgroup_sequence' }
        },

		getAllBrandSubGroupingsByBrandGroupingId: function getAllBrandSubGroupingsByBrandGroupingId(brandGroupingId) {
			var resultsObject = [];

			var cols = [];
			cols[0] = new nlobjSearchColumn('internalid', null, null);
			cols[1] = new nlobjSearchColumn('name', null, null);
			cols[2] = new nlobjSearchColumn('custrecord_diy_brand_subgroup_brandgroup', null, null);
			cols[3] = new nlobjSearchColumn('custrecord_diy_brand_subgrouping_desc', null, null);
			cols[4] = new nlobjSearchColumn('custrecord_diy_brand_subgrouping_img', null, null);
			cols[5] = new nlobjSearchColumn('custrecord_diy_brand_subgroup_title', null, null);
			cols[6] = new nlobjSearchColumn('custrecord_diy_brand_subgrouping_width', null, null);
			cols[7] = new nlobjSearchColumn('custrecord_brand_subgrouping_urlcompnent', null, null);
			cols[8] = new nlobjSearchColumn('custrecord_diy_brand_subgroup_brandurl', null, null);
			cols[9] = new nlobjSearchColumn('custrecord_diy_brand_subgroup_brandname', null, null);
			cols[10] = new nlobjSearchColumn('custrecord_diy_brand_subgroup_sequence', null, null);
			cols[10].setSort();

			var filters = [["custrecord_diy_brand_subgroup_brandgroup","anyof", brandGroupingId],"AND",["isinactive","is","F"]];

			var results = nlapiSearchRecord('customrecord_diy_brand_subgrouping', null, filters, cols);
			
			for (var i = 0; results && i < results.length; i++)	{

				var imageValue = results[i].getValue('custrecord_diy_brand_subgrouping_img');
				var imageText = results[i].getText('custrecord_diy_brand_subgrouping_img');

				var imageObj = {
					'internalid': imageValue,
					'name': imageText
				};

				resultsObject.push({
					'id': results[i].getValue('internalid'),
					'name': results[i].getValue('name'),
					'associatedBrandGroup': results[i].getValue('custrecord_diy_brand_subgroup_brandgroup'),
					'description': results[i].getValue('custrecord_diy_brand_subgrouping_desc'),
					'image': imageObj,
					'title': results[i].getValue('custrecord_diy_brand_subgroup_title'),
					'width': results[i].getText('custrecord_diy_brand_subgrouping_width'),
					'urlcomponent': results[i].getValue('custrecord_brand_subgrouping_urlcompnent'),
					'brandURLComponent': results[i].getValue('custrecord_diy_brand_subgroup_brandurl'),
					'brandId': results[i].getValue('custrecord_diy_brand_subgroup_brandname'),
					'brandSubGroupSequence': results[i].getValue('custrecord_diy_brand_subgroup_sequence')
				});
			}			

			return resultsObject;
		},

		getBrandSubGroupById: function getBrandSubGroupById(id) {
			var filters = _.clone(this.brandSubGroupingFilters);
			var Search = new SearchHelper(this.brandSubGroupingRecord, filters, this.brandSubGroupingColumns, this.brandSubGroupingFilters.basic);

			Search.addFilter({
				fieldName: 'internalid',
				operator: 'is',
				value1: id
			});

			Search.search();

			return Search.getResult();
		},

		getAllBrandSubGroupings:function getAllBrandSubGroupings() {
			var filters = _.clone(this.brandSubGroupingFilters);
			var Search = new SearchHelper(this.brandSubGroupingRecord, filters, this.brandSubGroupingColumns, this.brandSubGroupingFilters.basic);

			Search.search();

			return Search.getResults();
		},

		getAllBrandSubGroupURLS:function getAllBrandSubGroupURLS() {
			//nlapiLogExecution('DEBUG', 'Brand - Backend Model - getAllBrandSubGroupURLS: Flag');

			var brandSubGroupList = this.getAllBrandSubGroupings();
			var brandList = this.getAllBrands();
			var customBrands = [], customBrandSubGroups = [];

			_.each(brandSubGroupList, function eachBrandSubGroup(brandSubGroup){
				var customBrandSubGroup = {id:brandSubGroup.id,
										   urlcomponent:brandSubGroup.urlcomponent,
										   brandURLComponent:brandSubGroup.brandURLComponent};

				    customBrandSubGroups.push(customBrandSubGroup);
			});

			_.each(brandList, function eachResult(brand){
				var customBrand = {id:brand.id, urlcomponent:brand.urlcomponent, brandSubGroups:{}};


				var brandSubGroups = [];
				_.each(customBrandSubGroups, function eachBrandSubGroup(brandSubGroup) {
					if (brand.urlcomponent == brandSubGroup.brandURLComponent) {
						brandSubGroups.push(brandSubGroup);
					}
				});
				customBrand.brandSubGroups = brandSubGroups;

				customBrands.push(customBrand);
			});

			return customBrands;


		},

		getAllItemsByBrandSubGroupingId: function (brandSubGroupId, request) {

			var finalServiceUrl = nlapiResolveURL('SUITELET', 81, 1, true); // 81 = ID of NS Script Record
			finalServiceUrl += '&brandSubGroupingId=' + brandSubGroupId;
			finalServiceUrl += '&xml=T';

			var apiResponse;
			var responseData;
			var unknownError = {
				status: 500,
				code: 'ERR_UNKNONW',
				message: 'Internal error'
			};

			apiResponse = nlapiRequestURL(finalServiceUrl, null, request.getAllHeaders(), 'GET');

			if (parseInt(apiResponse.getCode(), 10) !== 200) {
				throw unknownError;
			}

			// Convert returnings Items into SCA-friendly StoreItem
			try {
				responseData = JSON.parse(apiResponse.getBody());

			} catch (e) {
				throw unknownError;
			}

			// Response Error Handling
			if (apiResponse.getHeader('Custom-Header-Status') && parseInt(apiResponse.getHeader('Custom-Header-Status'), 10) !== 200) {
				throw _.extend({}, {
					status: apiResponse.getHeader('Custom-Header-Status'),
					code: responseData.errorCode,
					message: responseData.errorMessage
				});
			}

			//nlapiLogExecution('DEBUG', 'Brand - Backend Model - responseData', JSON.stringify(responseData));
			//nlapiLogExecution('DEBUG', 'Brand - Backend Model - getAllItemsByBrandSubGroupingId: END');

			return responseData;
		},

		getCommonQuestionByBrandId: function (brandId, request) {

			//nlapiLogExecution('DEBUG', 'Brand - Backend Model - getAllItemsByBrandSubGroupingId: BEGIN');

			// Prepping the final URL with the brandSubGroupingId param
			var finalServiceUrl = nlapiResolveURL('SUITELET', 94, 1, true); // 81 = ID of NS Script Record
			finalServiceUrl += '&fileId=' + brandId;
			finalServiceUrl += '&xml=T';

			var apiResponse;
			var responseData;
			var unknownError = {
				status: 500,
				code: 'ERR_UNKNONW',
				message: 'Internal error'
			};

			apiResponse = nlapiRequestURL(finalServiceUrl, null, request.getAllHeaders(), 'GET');

			if (parseInt(apiResponse.getCode(), 10) !== 200) {
				throw unknownError;
			}

			// Convert returnings Items into SCA-friendly StoreItem
			try {
				//nlapiLogExecution('DEBUG', 'Common Question - Backend Model - responseData', apiResponse.getBody());
				responseData = JSON.parse(apiResponse.getBody());

			} catch (e) {
				throw unknownError;
			}

			// Response Error Handling
			if (apiResponse.getHeader('Custom-Header-Status') && parseInt(apiResponse.getHeader('Custom-Header-Status'), 10) !== 200) {
				throw _.extend({}, {
					status: apiResponse.getHeader('Custom-Header-Status'),
					code: responseData.errorCode,
					message: responseData.errorMessage
				});
			}

			return responseData;
		}

	});

});