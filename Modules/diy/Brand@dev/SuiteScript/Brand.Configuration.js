define('Brand.Configuration', [
    'Configuration'
], function BrandsConfiguration(
    Configuration
) {
    'use strict';

    Configuration.publish.push({
        key: 'Brand_URL_Component',
        model: 'Brand.Model',
        call: 'getAllBrandSubGroupURLS'
    });

});
