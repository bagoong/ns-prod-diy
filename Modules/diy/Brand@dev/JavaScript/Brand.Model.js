define('Brand.Model',[
	'Backbone',
	'underscore',
	'Utils'
], function BrandModel(
	Backbone,
	_,
	Utils
) {
	'use strict';

	return Backbone.Model.extend ({
		urlRoot: Utils.getAbsoluteUrl('services/Brand.Brands.Service.ss')
	});
});