define('BrandGroup.Collection',[
  'Backbone.CachedCollection',
  'BrandGroup.Model',
  'underscore',
  'Utils'
], function BrandGroupCollection(
    BackboneCachedCollection,
    Model,
    _,
    Utils
) {

  'use strict';

  return BackboneCachedCollection.extend ({
        model: Model,
        url: Utils.getAbsoluteUrl('services/Brand.BrandGroups.Service.ss')
  });
});