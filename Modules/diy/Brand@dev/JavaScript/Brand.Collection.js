define('Brand.Collection',[
  'Backbone.CachedCollection',
  'Brand.Model',
  'underscore',
  'Utils'
], function BrandCollection(
    BackboneCachedCollection,
    Model,
    _,
    Utils
) {

  'use strict';

  return BackboneCachedCollection.extend ({
        model: Model,
        url: Utils.getAbsoluteUrl('services/Brand.Brands.Service.ss')
  });
});