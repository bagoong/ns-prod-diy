define('BrandItem.Model',[
	'Backbone',
	'underscore',
	'Utils'
], function BrandItemModel(
	Backbone,
	_,
	Utils
) {
	'use strict';

	return Backbone.Model.extend ({
		urlRoot: Utils.getAbsoluteUrl('services/Brand.Items.Service.ss')
	});
});