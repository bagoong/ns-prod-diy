define('BrandItem.Collection',[
  'Backbone.CachedCollection',
  'BrandItem.Model',
  'underscore',
  'Utils'
], function BrandItemCollection(
    BackboneCachedCollection,
    Model,
    _,
    Utils
) {

  'use strict';

  return BackboneCachedCollection.extend ({
        model: Model,
        url: Utils.getAbsoluteUrl('services/Brand.Items.Service.ss')
  });
});