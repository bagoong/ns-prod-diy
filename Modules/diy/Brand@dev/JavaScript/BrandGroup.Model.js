define('BrandGroup.Model',[
	'Backbone',
	'underscore',
	'Utils'
], function BrandGroupModel(
	Backbone,
	_,
	Utils
) {
	'use strict';

	return Backbone.Model.extend ({
		urlRoot: Utils.getAbsoluteUrl('services/Brand.BrandGroups.Service.ss')
	});
});