define('BrandSubGroup.Model',[
	'Backbone',
	'underscore',
	'Utils'
], function BrandSubGroupModel(
	Backbone,
	_,
	Utils
) {
	'use strict';

	return Backbone.Model.extend ({
		urlRoot: Utils.getAbsoluteUrl('services/Brand.BrandSubgroups.Service.ss')
	});
});