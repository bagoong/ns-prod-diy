define('BrandSubGroup.Collection',[
  'Backbone.CachedCollection',
  'BrandSubGroup.Model',
  'underscore',
  'Utils'
], function BrandSubGroupCollection(
    BackboneCachedCollection,
    Model,
    _,
    Utils
) {

  'use strict';

  return BackboneCachedCollection.extend ({
        model: Model,
        url: Utils.getAbsoluteUrl('services/Brand.BrandSubgroups.Service.ss')
  });
});