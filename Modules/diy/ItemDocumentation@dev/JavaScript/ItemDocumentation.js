define('ItemDocumentation', [
    'ItemDetails.View',
    'ItemDocument.Model',
    'ItemDocument.Collection',
    'Document.List.View',
    'Video.List.View',
    'SC.Configuration',
    'underscore',
    'jQuery'

], function ItemDocumentation(
    ItemDetailsView,
    Model,
    ItemDocumentCollection,
    DocumentListView,
    VideoListView,
    Configuration,
    _,
    jQuery
) {
    'use strict';

    return {
        mountToApp: function () {
            // ItemDetailsView.prototype.initialize =
            //     _.wrap(ItemDetailsView.prototype.initialize, function wrapInitialize(fn) {
            //
            //         fn.apply(this, _.toArray(arguments).slice(1));
            //
            //         this.on('afterViewRender', function afterViewRender() {
            //             this.$el
            //                 .find('#item-details-info-tab-2')
            //                 .append('<div data-view="ItemDocumentation"></div>');
            //         });
            //     });

            _.extend(ItemDetailsView.prototype.childViews, {
                'ItemDocumentation': function addDocuments() {
                    var self = this;
                    var itemDocumentCollection = new ItemDocumentCollection();
                    itemDocumentCollection.fetch({
                        data: {
                            internalid: self.model.get('internalid'),
                            tabType: 1
                        }
                    });

                    return new DocumentListView({
                        collection: itemDocumentCollection,
                        application: self.application
                    });
                }
            });
        }
    }
});