define('ItemDocument.Collection',[
  'Backbone.CachedCollection',
  'ItemDocument.Model',
  'underscore',
  'Utils'
], function ItemDocumentCollection(
    BackboneCachedCollection,
    Model,
    _,
    Utils
) {

  'use strict';

  return BackboneCachedCollection.extend ({
        model: Model,
        url: Utils.getAbsoluteUrl('services/ItemDocument.Service.ss')
  });
});