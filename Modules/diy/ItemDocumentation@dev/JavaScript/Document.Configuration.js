define('Document.Configuration', [
], function(
) {
    'use strict';
    var configuration = {
        pdfLink: 'media/pdf/'
    };

    return configuration;

});