define('Document.List.View', [
		'Backbone',
		'SC.Configuration',
		'document_list.tpl',
		'Backbone.CollectionView',
		'Backbone.CompositeView',
		'Document.Details.View',
		'jQuery',
		'underscore'
	], function ItemDocumentListView(
		Backbone,
		Configuration,
		document_list_tpl,
		CollectionView,
		CompositeView,
		DocumentDetailsView,
		jQuery,
		_
	) {
		//var documentSliderInstance;
		return Backbone.View.extend({
			initialize: function(options){
				CompositeView.add(this);
				this.application = options.application;
				this.collection = options.collection;

				this.listenTo(this.collection, 'sync', jQuery.proxy(this.render,this));
				var self = this;
				this.listenTo(this, 'afterCompositeViewRender', function() {
					// _.defer(function() {
					// 	if (documentSliderInstance && documentSliderInstance.length) {
					// 		documentSliderInstance.destroySlider();
					// 	}
					// 	var documentCarousel = jQuery('#documentslider-items');
                    //
					// 	documentSliderInstance = _.initBxSlider(documentCarousel, Configuration.documentSliderDefaults);
					// });

					if (self.collection.length > 0) {
						jQuery('.info-and-guides-tab').show();
					} else {
						jQuery('.info-and-guides-tab').hide();
					}
				});



			},

			childViews: {
				'ItemDocument.Collection': function(){
					return new CollectionView({
						'childView': DocumentDetailsView,
						'collection': this.collection,
						'viewsPerRow': Infinity
					});
				}
			},

			template: document_list_tpl
		});
	}
);