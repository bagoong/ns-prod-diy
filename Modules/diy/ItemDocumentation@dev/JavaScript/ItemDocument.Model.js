define('ItemDocument.Model',[
	'Backbone',
	'underscore',
	'Utils'
], function ItemDocumentModel(
	Backbone,
	_,
	Utils
) {
	'use strict';

	return Backbone.Model.extend ({
		urlRoot: Utils.getAbsoluteUrl('services/ItemDocument.Service.ss')
	});
});