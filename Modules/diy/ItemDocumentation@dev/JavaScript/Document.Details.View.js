define('Document.Details.View', [
	'Backbone',
	'document_details.tpl',
	'Document.Configuration'
], function ItemDocumentDetailsView(
	Backbone,
	document_details_template,
	DocumentConfiguration
){
		return Backbone.View.extend({
			
			getContext: function(){
				var netsuitePDFLink = DocumentConfiguration.pdfLink;
				var documentType =  this.model.get('document_document_type');
				var documentLink = this.model.get('document_link');

				if (documentType === 'pdf') {
					documentLink = netsuitePDFLink + documentLink;
				}

				return {
					'id': this.model.get('internalid'),
					'name' : this.model.get('document_name'),
					'link' : documentLink,
					'target' : this.model.get('document_target_attribute'),
					'type' : documentType
				}
			},

			template: document_details_template
		});
	}
);