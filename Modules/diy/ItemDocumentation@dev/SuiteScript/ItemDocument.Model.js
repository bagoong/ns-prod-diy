define('ItemDocument.Model',[
    'SC.Model',
    'SearchHelper',
    'underscore'
], function ItemDocumentModel(
    SCModel,
    SearchHelper,
    _
) {
    'use strict';

    return SCModel.extend ({
        name: 'ItemDocument',

        record: 'customrecord_diy_item_documentation',

        fieldsets: {
            basic: [
                'internalid',
                'item',

                'document',
                'document_name',
                'document_display_description',
                'document_document_type',
                'document_link',
                'document_target_attribute',
                'document_source_video_identifier',
                'document_video_source',
                'document_tab_type'
               
            ]
        },

        sorting: [
            {fieldName: 'custrecord_diy_itemdoc_sortorder', sort:'asc'}
        ],

        filters: [
            {fieldName: 'isinactive', operator:'is', value1:'F'}],

        columns:{
            internalid:{fieldName:'internalid'},
            item:{fieldName:'custrecord_diy_itemdoc_item'},

            document:{fieldName:'custrecord_diy_itemdoc_doc'},
            document_name:{fieldName:'custrecord_diy_documentation_desc', joinKey: 'custrecord_diy_itemdoc_doc'},
            document_display_description:{fieldName:'custrecord_diy_documentation_desc', joinKey: 'custrecord_diy_itemdoc_doc'},
            document_document_type:{fieldName:'custrecord_diy_documentation_type', type:'getText', joinKey: 'custrecord_diy_itemdoc_doc'},
            document_link:{fieldName:'custrecord_diy_documentation_doc_link', joinKey: 'custrecord_diy_itemdoc_doc'},
            document_target_attribute:{fieldName:'custrecord_diy_html_target_attribute', type:'getText', joinKey: 'custrecord_diy_itemdoc_doc'},
            document_source_video_identifier:{fieldName:'custrecord_diy_documentation_vid_srcid', joinKey: 'custrecord_diy_itemdoc_doc'},
            document_video_source:{fieldName:'custrecord_diy_documentation_vid_src', type:'getText', joinKey: 'custrecord_diy_itemdoc_doc'},
            document_tab_type:{fieldName:'custrecord_diy_documentation_tab_type', type:'getText', joinKey: 'custrecord_diy_itemdoc_doc'}
        },

        list:function list(internalid, tabType) {

            if (internalid == null || internalid == '') {
                return [];
            }
            
            var filters = _.clone(this.filters);
            var Search = new SearchHelper(this.record, filters, this.columns, this.fieldsets.basic);

            Search.addFilter({
                fieldName: 'custrecord_diy_itemdoc_item',
                operator: 'anyof',
                value1: internalid
            });

            Search.addFilter({
                fieldName: 'custrecord_diy_documentation_tab_type',
                operator: 'anyof',
                value1: tabType,
                type:'getText',
                joinKey: 'custrecord_diy_itemdoc_doc'
            });

            Search.search();

            return Search.getResults();
        }

    });

});