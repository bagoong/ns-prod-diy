<li class="itemdocument-details-results-table-column">
    <a href="{{link}}" target="{{target}}">
         <div class="itemdocument-image {{type}}"></div>
         <div class="itemdocument-details-results-table-row-name">
            <span class="itemdocument-details-results-table-row-name-value">{{name}}</span>
        </div>
    </a>
</li>