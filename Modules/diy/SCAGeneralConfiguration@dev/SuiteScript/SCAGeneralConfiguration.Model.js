define('SCAGeneralConfiguration.Model', [
    'SC.Model',
    'Application',
    'underscore'
], function SCAGeneralConfigurationModel(
    SCModel,
    Application,
    _
) {
    'use strict';

    return SCModel.extend({

    name: 'SCAGeneralConfiguration.Model',

    /**
     * getDiyConfig: Retrieve DIY Config
	 *
	 * Assumption: if results set more than 1, only take the 1st one
	 *
	 * @param none
	 *
	 * @return {Array of objects} diyConfigObject
	 *
     */
    getReturnPolicy: function getReturnPolicy() {

	    var searchResults;
	    var diyConfig;

	    var filters = [];
	    filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	    var columns = [];
	    columns.push(new nlobjSearchColumn('custrecord_returnpolicy_title', null, null));
	    columns.push(new nlobjSearchColumn('custrecord_returnpolicy_bodytext', null, null));
	    columns.push(new nlobjSearchColumn('custrecord_returnpolicy_linktext', null, null));
	    columns.push(new nlobjSearchColumn('custrecord_returnpolicy_linkvalue', null, null));

	    searchResults = nlapiSearchRecord('customrecord_sca_general_config', null, filters, columns) || [];

	    if (searchResults != null && searchResults.length > 0)	{
			var returnPolicyTitle = searchResults[0].getValue('custrecord_returnpolicy_title');
			var returnPolicyBodyText = searchResults[0].getValue('custrecord_returnpolicy_bodytext');
			var returnPolicyLinkText = searchResults[0].getValue('custrecord_returnpolicy_linktext');
			var returnPolicyLinkValue = searchResults[0].getValue('custrecord_returnpolicy_linkvalue');
			var returnPolicy = {
				'title': returnPolicyTitle,
				'body': returnPolicyBodyText,
				'linktext': returnPolicyLinkText,
				'linkvalue': returnPolicyLinkValue
			};

	    	// Prepare return object
	    	diyConfig = returnPolicy;
	    }

	    return diyConfig;
    },
		
	getFreeShipping: function getFreeShipping() {
		var searchResults;
		var diyConfig;

		var filters = [];
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		var columns = [];
		columns.push(new nlobjSearchColumn('custrecord_freeshipping_title', null, null));
		columns.push(new nlobjSearchColumn('custrecord_freeshipping_bodytext', null, null));
		columns.push(new nlobjSearchColumn('custrecord_freeshipping_linktext', null, null));
		columns.push(new nlobjSearchColumn('custrecord_freeshipping_linkvalue', null, null));

		searchResults = nlapiSearchRecord('customrecord_sca_general_config', null, filters, columns) || [];

		if (searchResults != null && searchResults.length > 0)	{
			// Assumption: 1 and only 1

			var freeShippingTitle = searchResults[0].getValue('custrecord_freeshipping_title');
			var freeShippingBodyText = searchResults[0].getValue('custrecord_freeshipping_bodytext');
			var freeShippingLinkText = searchResults[0].getValue('custrecord_freeshipping_linktext');
			var freeShippingLinkValue = searchResults[0].getValue('custrecord_freeshipping_linkvalue');
			var freeShipping = {
				'title': freeShippingTitle,
				'body': freeShippingBodyText,
				'linktext': freeShippingLinkText,
				'linkvalue': freeShippingLinkValue
			};

			// Prepare return object
			diyConfig = freeShipping;
			
		}
		return diyConfig;
	},

	getSiteWideMessage: function getSitewideMessage() {

		var searchResults;
		var sitewideMessage;

		var filters = [];
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		filters.push(new nlobjSearchFilter('custrecord_diy_sitewide_effectivedate', null, 'onorbefore', 'today'));
		filters.push(new nlobjSearchFilter('custrecord_diy_sitewide_expirationdate', null, 'onorafter', 'today'));

		var columns = [];
		columns.push(new nlobjSearchColumn('custrecord_diy_sitewide_message', null, null));

		searchResults = nlapiSearchRecord('customrecord_diy_sitewide_msg', null, filters, columns) || [];

		if (searchResults != null && searchResults.length > 0)	{
			// Assumption: 1 and only 1
			sitewideMessage = searchResults[0].getValue('custrecord_diy_sitewide_message');
		}
		return sitewideMessage;
	}

  });
});