define('SCAGeneralConfiguration.Configuration', [
    'Configuration'
], function SCAGeneralConfigurationConfiguration(
    Configuration
) {
    'use strict';

    Configuration.publish.push({
        key: 'SITEWIDE_MSG',
        model: 'SCAGeneralConfiguration.Model',
        call: 'getSiteWideMessage'
    });

    Configuration.publish.push({
        key: 'RETURN_POLICY',
        model: 'SCAGeneralConfiguration.Model',
        call: 'getReturnPolicy'
    });

    Configuration.publish.push({
        key: 'FREE_SHIPPING',
        model: 'SCAGeneralConfiguration.Model',
        call: 'getFreeShipping'
    });

});
