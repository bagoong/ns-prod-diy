/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module OrderWizard.Module.Address
define(
	'CustomOrderWizard.Module.Address'
,	[	'Wizard.Module'
	,	'Profile.Model'
	,	'GlobalViews.Message.View'
	,	'LiveOrder.Model'
	,	'OrderWizard.Module.Address'

	,	'order_wizard_shipmethod_module.tpl'

	,	'underscore'
	,	'jQuery'
	]
,	function (
		WizardModule
	,	ProfileModel
	,	GlobalViewsMessageView
	,	LiveOrder
	,	OrderWizardModuleAddress

	,	order_wizard_shipmethod_module_tpl

	,	_
	,	jQuery
	)
{
	'use strict';

	// CUSTOMIZATION
	OrderWizardModuleAddress.prototype.selectAddress = function selectAddress(e)	{
		
		jQuery('.wizard-content .alert-error').hide();

		// Grabs the address id and sets it to the model
		// on the position in which our sub class is managing (billaddress or shipaddress)
		this.setAddress(jQuery(e.target).data('id').toString());

		// re render so if there is changes to be shown they are represented in the view
		this.render();

		// As we already set the address, we let the step know that we are ready
		this.trigger('ready', true);

		// CUSTOMIZATION
		//window.location.reload();
		//TODO - this was a quick fix -- figure out how to resync the Expedited Msg and Ships-To HI/AK error msg without reloading page.
	};

});