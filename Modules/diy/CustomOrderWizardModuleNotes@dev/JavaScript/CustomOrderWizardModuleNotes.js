define(
	'CustomOrderWizardModuleNotes'

,	[
		'Wizard.Module'
	,	'order_wizard_notes_module.tpl'	
	]

,	function (
		WizardModule
	,	order_wizard_notes_module_tpl
	)

{
	'use strict';

	// @class CustomOrderWizard.Module.Notes @extends WizardModule
	return WizardModule.extend({

		template: order_wizard_notes_module_tpl

		// @method submit @return CustomOrderWizard.Module.Notes.Context
		, submit: function()	{

			// @class CustomOrderWizard.Module.Notes.Context
			return	{
				// @property {Object} custbody_web_order_note
				custbody_web_order_note: this.custbody_web_order_note
			};
		}
	});
});