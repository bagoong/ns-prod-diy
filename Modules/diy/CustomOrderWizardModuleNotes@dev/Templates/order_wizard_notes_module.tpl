<h4 class="section-header">{{translate 'Notes: '}}</h4>
<div id="diy-checkout-notes">	
	<textarea 
		id="custbody_web_order_note"
		class="diy-checkoukt-notes-textarea"
		value="{{custbody_web_order_note}}"		 		
		placeholder="Add Notes or Special Instructions"
		rows="5">
	</textarea>
</div>