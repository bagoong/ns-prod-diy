define('ItemVideo.Model',[
	'Backbone',
	'underscore',
	'Utils'
], function ItemVideoModel(
	Backbone,
	_,
	Utils
) {
	'use strict';

	return Backbone.Model.extend ({
		urlRoot: Utils.getAbsoluteUrl('services/ItemVideo.Service.ss')
	});
});