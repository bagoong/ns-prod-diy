define('Video.Details.View', [
	'Backbone',
	'video_details.tpl',
	'Video.Configuration',
	'jQuery'
], function VideoDetailsView(
	Backbone,
	video_details_template,
	VideoConfiguration,
	jQuery
){
		return Backbone.View.extend({
			getContext: function(){
				var videoType = this.model.get('document_video_source');
				var videoSourceIdentifier = this.model.get('document_source_video_identifier');
				var img_src = "#";
				var iframeSourceLink = "#"
				var showSuggestedVideosWhenTheVideoFinishes = VideoConfiguration.youtube.showSuggestedVideosWhenTheVideoFinishes;
				var	showPlayerControls = VideoConfiguration.youtube.showPlayerControls;
				var	showVideoTitleAndPlayerActions = VideoConfiguration.youtube.showVideoTitleAndPlayerActions;
				var	enablePrivacyEnhancedMode = VideoConfiguration.youtube.enablePrivacyEnhancedMode;

				if (videoType === 'YouTube.com') {
					img_src = VideoConfiguration.youtube.youtubeThumbnailLink + videoSourceIdentifier + "/default.jpg";

					if (enablePrivacyEnhancedMode) {
						iframeSourceLink = "https://www.youtube-nocookie.com/embed/" + videoSourceIdentifier;
					} else {
						iframeSourceLink = "https://www.youtube.com/embed/" + videoSourceIdentifier;
					}

					var params = new Object();

					if (!showSuggestedVideosWhenTheVideoFinishes) {
						params.rel=0;
					}
					if (!showPlayerControls) {
						params.controls=0;
					}
					if (!showVideoTitleAndPlayerActions) {
						params.showinfo=0;
					}

					iframeSourceLink = iframeSourceLink + "?" +jQuery.param( params );

				} else if (videoType === 'Vimeo.com') {
					//Do Some API Calls
				}

				return {
					'id': this.model.get('internalid'),
					'name' : this.model.get('document_name'),
					'src' : img_src,
					'width':VideoConfiguration.width,
					'height':VideoConfiguration.height,
					'video_src':iframeSourceLink
				}
			},

			template: video_details_template
		});
	}
);