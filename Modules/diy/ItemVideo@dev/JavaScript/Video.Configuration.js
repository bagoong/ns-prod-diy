define('Video.Configuration', [
], function(
) {
    'use strict';
    var configuration = {
        height: 315,
        width: 560,
        youtube: {
            youtubeThumbnailLink:"http://img.youtube.com/vi/",
            showSuggestedVideosWhenTheVideoFinishes:true,
            showPlayerControls:true,
            showVideoTitleAndPlayerActions:true,
            enablePrivacyEnhancedMode:false
        },
        vimeo: {
            //Add here configuration specific to vimeo
        }
    };

    return configuration;

});