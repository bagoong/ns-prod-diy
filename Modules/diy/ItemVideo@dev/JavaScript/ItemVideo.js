define('ItemVideo', [
    'ItemDetails.View',
    'ItemVideo.Model',
    'ItemVideo.Collection',
    'Video.List.View',
    'SC.Configuration',
    'underscore',
    'jQuery',
    'Utils'
], function ItemVideo(
    ItemDetailsView,
    Model,
    ItemVideoCollection,
    VideoListView,
    Configuration,
    _,
    jQuery
) {
    'use strict';

    var sliderCSSWidth;

    _.extend(ItemDetailsView.prototype.events, {
        'click .view-video': 'showModal',
        'click .close': 'hideModal'
    });

    _.extend(ItemDetailsView.prototype, {
        showModal: function (e) {
            e.preventDefault();

            sliderCSSWidth =  jQuery( '#videoslider-items' ).css( 'width' );
            jQuery( '#videoslider-items' ).removeAttr( 'style' );


            var iframe_src = jQuery(e.currentTarget).attr('iframe-src');
            var iframe_height = jQuery(e.currentTarget).attr('iframe-height');
            var modal_width = jQuery(e.currentTarget).attr('modal-width');
            var modal_name = jQuery(e.currentTarget).attr('modal-name');

            jQuery('.modal-content-name').html(modal_name);
            jQuery('.modal-content').css('width', modal_width);
            jQuery('.modal-content-iframe').attr('src', iframe_src);
            jQuery('.modal-content-iframe').attr('height', iframe_height);

            jQuery('#myModal').addClass('show');


        },
        hideModal: function (e) {
            e.preventDefault();

            jQuery( '#videoslider-items' ).css("width", sliderCSSWidth);
            jQuery( '#videoslider-items' ).css("position", "relative");
            jQuery( '#videoslider-items' ).css("transition-duration", "0s");
            jQuery( '#videoslider-items' ).css("transform", "translate3d(-972px, 0px, 0px)");

            jQuery('#myModal').removeClass('show');

            jQuery('.modal-content-name').html("");
            jQuery('.modal-content').removeAttr('style');
            jQuery('.modal-content-iframe').removeAttr("src");
            jQuery('.modal-content-iframe').removeAttr("height");
        }
    });

    return {
        mountToApp: function () {
            // ItemDetailsView.prototype.initialize =
            //     _.wrap(ItemDetailsView.prototype.initialize, function wrapInitialize(fn) {
            //
            //         fn.apply(this, _.toArray(arguments).slice(1));
            //
            //         this.on('afterViewRender', function afterViewRender() {
            //             this.$el
            //                 .find('#item-details-info-tab-1')
            //                 .append('<div data-view="ItemVideo"></div>');
            //         });
            //     });

            _.extend(ItemDetailsView.prototype.childViews, {
                'ItemVideo': function addVideos() {
                    var self = this;
                    var itemVideoCollection = new ItemVideoCollection();

                    itemVideoCollection.fetch({
                        data: {
                            internalid: self.model.get('internalid'),
                            tabType: 2
                        }
                    });

                    return new VideoListView({
                        collection: itemVideoCollection,
                        application:self.application
                    });
                }
            });




        }
    }
});