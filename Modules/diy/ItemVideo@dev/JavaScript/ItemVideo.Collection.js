define('ItemVideo.Collection',[
  'Backbone.CachedCollection',
  'ItemVideo.Model',
  'underscore',
  'Utils'
], function ItemVideoCollection(
    BackboneCachedCollection,
    Model,
    _,
    Utils
) {

  'use strict';

  return BackboneCachedCollection.extend ({
        model: Model,
        url: Utils.getAbsoluteUrl('services/ItemVideo.Service.ss')
  });
});