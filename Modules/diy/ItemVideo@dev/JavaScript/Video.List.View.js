define('Video.List.View', [
		'Backbone',
		'SC.Configuration',
		'video_list.tpl',
		'Backbone.CollectionView',
		'Backbone.CompositeView',
		'Video.Details.View',
		'jQuery',
		'underscore'
	], function VideoListView(
		Backbone,
		Configuration,
		video_list_tpl,
		CollectionView,
		CompositeView,
		VideoDetailsView,
		jQuery,
		_
	) {
		var videoSliderInstance;
		return Backbone.View.extend({
			initialize: function(options){
				CompositeView.add(this);
				this.application = options.application;
				this.collection = options.collection;

				this.listenTo(this.collection, 'sync', jQuery.proxy(this.render,this));
				var self = this;
				this.listenTo(this, 'afterCompositeViewRender', function() {
					// _.defer(function() {
					// 	if (videoSliderInstance && videoSliderInstance.length) {
					// 		videoSliderInstance.destroySlider();
					// 	}
					// 	var videoCarousel = jQuery('#videoslider-items');
                    //
					// 	videoSliderInstance = _.initBxSlider(videoCarousel, Configuration.videoSliderDefaults);
                    //
					// 	jQuery( '#videoslider-items' ).css("transform", "translate3d(-972px, 0px, 0px)");
					// });

					if (self.collection.length > 0) {
						jQuery('.video-tab').show();
					} else {
						jQuery('.video-tab').hide();
					}
				});

			},

			childViews: {
				'ItemVideo.Collection': function(){
					return new CollectionView({
						'childView': VideoDetailsView,
						'collection': this.collection,
						'viewsPerRow': Infinity
					});
				}
			},

			template: video_list_tpl
		});
	}
);