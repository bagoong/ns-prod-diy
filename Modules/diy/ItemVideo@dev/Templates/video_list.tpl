<section class="video-list">
    <div class="video-list-results-container">
        <div id="myModal" class="modal videoModal">
            <div class="modal-content">
                <span class="close">x</span>
                <div class="modal-content-name"></div>
                <iframe class="modal-content-iframe" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
        <ul data-view="ItemVideo.Collection" class="videocarousel-items"></ul>
    </div>
</section>
