<li class="video-details-results-table-column">
    <div class="video-details-container">
        <!--<div id="myModal" class="modal videoModal">-->
            <!--<div class="modal-content" style="width:{{width}}px;">-->
                <!--<span class="close">x</span>-->
                <!--<div class="modal-content-name">{{name}}</div>-->
                <!--<iframe class="modal-content-iframe" height="{{height}}" src="{{video_src}}" frameborder="0" allowfullscreen></iframe>-->
            <!--</div>-->
        <!--</div>-->
        <div class="view-video" iframe-src="{{video_src}}" iframe-height="{{height}}" modal-width="{{width}}" modal-name="{{name}}">
            <div class="playbutton-overlay"></div>
            <div class="video-details-container">
                <img class="video-image" src="{{src}}">
                <div class="video-details-results-table-row-name">
                    <span class="video-details-results-table-row-name-value">{{name}}</span>
                </div>
            </div>
        </div>
    </div>
</li>