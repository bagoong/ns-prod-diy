{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="item-details">
	<div data-cms-area="item_details_banner" data-cms-area-filters="page_type"></div>

	<header class="item-details-header">
		<div id="banner-content-top" class="item-details-banner-top"></div>
	</header>
	<!--<div class="item-details-divider-desktop"></div>-->
	<article class="item-details-content" itemscope itemtype="http://schema.org/Product">
		<meta itemprop="url" content="{{model._url}}">
		<div id="banner-details-top" class="item-details-banner-top-details"></div>

		<section class="item-details-main-content">
			<div class="item-details-content-header">
				<h1 class="item-details-content-header-title" itemprop="name">{{model._pageHeader}}</h1>

				<!-- DIY - Manufacturer -->
				<div class="path">{{translate 'by '}} {{customManufacturer}}</div>

				<!-- DIY - Moved the Item ID / SKU -->
				<div class="item-details-sku-container">
					<span class="item-details-sku">
						{{translate 'SKU:'}}
					</span>
					<span class="item-details-sku-value" itemprop="sku">
						{{sku}}
					</span>
				</div>

				{{#if showReviews}}
				<div class="item-details-rating-header" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
					<div class="item-details-rating-header-rating" data-view="Global.StarRating"></div>
				</div>
				{{/if}}
				<div data-cms-area="item_info" data-cms-area-filters="path"></div>

				<div class="diy-item-details-info-mobile">
					<div class="diy-item-details-price-brand-container">
						<div data-view="Item.Price"></div>

						<!-- DIY: Brand Logo -->
						{{#if isBrandLogoPresent}}
						<div class="item-details-brand-logo-value">
							<!-- TODO: see mockup for delimiter '|'... please replace -->
							<img src="{{brandLogoUrl}}" alt="" width="125px" height="40px">
						</div>
						{{/if}}
						<div class="clearfix"></div>
					</div>


					<!-- DIY - On Sale -->
					{{#if isOnSale}}
					<div class="diy-item-details-onsale-message">
						<h4><span></span>{{isOnSaleHeader}}</h4>
						<span></span><div>{{isOnSaleMessage}}</div>
					</div>
					{{/if}}

					<div data-view="Item.Stock"></div>
				</div>
			</div>

			<!--<div class="item-details-divider"></div>-->

			<div class="item-details-image-gallery-container">

				<!-- DIY - Promo Overlay - TODO -->
				{{#if hasPromoOverlay}}
					<div class="diy-item-details-promo-overlay">
						{{promoOverlay}}
					</div>
				{{/if}}
				<div id="banner-image-top" class="content-banner banner-image-top"></div>
				<div data-view="ItemDetails.ImageGallery"></div>
				<div id="banner-image-bottom" class="content-banner banner-image-bottom"></div>
			</div>

			<!--<div class="item-details-divider"></div>-->

			<div class="item-details-main">

				<section class="item-details-info">
					<div id="banner-summary-bottom" class="item-details-banner-summary-bottom"></div>


						<div class="diy-item-details-info-desktop">
							<div class="diy-item-details-price-brand-container">
								<div data-view="Item.Price"></div>

								<!-- DIY: Brand Logo -->
								{{#if isBrandLogoPresent}}
								<div class="item-details-brand-logo-value">
									<!-- TODO: see mockup for delimiter '|'... please replace -->
									<img src="{{brandLogoUrl}}" alt="" width="125px" height="40px">
								</div>
								{{/if}}
								<div class="clearfix"></div>
							</div>


							<!-- DIY - On Sale -->
							{{#if isOnSale}}
							<div class="diy-item-details-onsale-message">
								<h4><span></span>{{isOnSaleHeader}}</h4>
								<span></span><div>{{isOnSaleMessage}}</div>
							</div>	
							{{/if}}

							<div data-view="Item.Stock"></div>
						</div>


				</section>

				{{#if showRequiredReference}}
					<div class="item-details-text-required-reference-container">
						<small>Required <span class="item-details-text-required-reference">*</span></small>
					</div>
				{{/if}}

				<!-- DIY: Volume Pricing + Shipping Promotion -->
				{{#if hasVolumePricing}}
					<div class="diy-item-details-vp-icon"><i></i>{{translate 'Volume Pricing'}}</div>
				{{/if}}
				{{#if shippingPromotion}}
					<div class="diy-item-details-shipping-promotions"><i></i>{{shippingPromotion}}</div>
				{{/if}}

				<section class="item-details-options">
					{{#if isItemProperlyConfigured}}
						{{#if hasAvailableOptions}}
							<button class="item-details-options-pusher" data-type="sc-pusher" data-target="item-details-options"> {{#if isReadyForCart}}{{ translate 'Options' }}{{else}}{{ translate 'Select Options' }}{{/if}} {{#if hasSelectedOptions}}:{{/if}} <i></i>
								{{#each selectedOptions}}
									{{#if @first}}
										<span> {{ label }} </span>
									{{else}}
										<span> , {{ label }} </span>
									{{/if}}
								{{/each}}
							</button>
						{{/if}}

						<div class="item-details-options-content" data-action="pushable" data-id="item-details-options">

							<div class="item-details-options-content-price" data-view="Item.Price"></div>

							<div class="item-details-options-content-stock"  data-view="Item.Stock"></div>

							<div data-view="ItemDetails.Options"></div>
						</div>
					{{else}}
						<div class="alert alert-error">
							{{translate '<b>Warning</b>: This item is not properly configured, please contact your administrator.'}}
						</div>

					{{/if}}
				</section>

				<!-- DIY: Custom Stock Status + Time To Ship -->
				<div class="item-details-stock-status-time-to-ship-container">
					<p class="item-details-stock-status-value {{stockStatusIcon}}">{{stockStatus}}</p>
					
					{{#if isInStock}}
						<p class="item-details-time-to-ship-value">{{timeToShip}}</p>
					{{else}}
						{{#if isBackordered}}
							<div>{{translate 'Expected Ship Date: '}}</div><p class="item-details-time-to-ship-value">{{backOrderedItemExpectedShipDate}}</p>
						{{/if}}
					{{/if}}
				</div>

				<!-- DIY: Important section -->
				{{#if showImportantSection}}
					<div class="item-details-important-container">
						<!-- Important Icon and Text -->
						<h4><span></span>{{translate 'IMPORTANT'}}</h4>
						<p class="message-body">{{{importantMessageBody}}}</p>
						<p class="">
							<a href="{{importantMessageLinkUrl}}">{{importantMessageLinkText}}</a>
						</p>
					</div>
				{{/if}}

				{{#if isPriceEnabled}}
				{{#if isItemProperlyConfigured}}
				{{#if canBeOrdered}}
					<section class="item-details-actions">

						<form action="#" class="item-details-add-to-cart-form" data-validation="control-group">
							{{#if showQuantity}}
								<input type="hidden" name="quantity" id="quantity" value="1">
							{{else}}
								<div class="item-details-options-quantity" data-validation="control">
									<label for="quantity" class="item-details-options-quantity-title">
										{{translate 'Qty'}}
									</label>
									<button class="item-details-quantity-remove" data-action="minus" {{#if isMinusButtonDisabled}}disabled{{/if}}>-</button>
									<input type="number" name="quantity" id="quantity" class="item-details-quantity-value" value="{{quantity}}" min="1">
									<button class="item-details-quantity-add" data-action="plus">+</button>

									{{#if showMinimumQuantity}}
										<small class="item-details-options-quantity-title-help">
											{{translate 'Minimum of $(0) required' minQuantity}}
										</small>
									{{/if}}
								</div>
							{{/if}}


							<div class="item-details-actions-container">
								<div class="item-details-add-to-cart">
									<button data-type="add-to-cart" data-action="sticky" class="item-details-add-to-cart-button" {{#unless isReadyForCart}}disabled{{/unless}}>
										{{translate 'Add to Cart'}}
									</button>
								</div>

								{{#if isReadyForWishList}}
									<div class="item-details-add-to-wishlist" data-type="product-lists-control"></div>
								{{else}}
									<div class="item-details-add-to-wishlist" data-type="product-lists-control" data-disabledbutton="true"></div>
								{{/if}}
							</div>

							{{#unless isReadyForCart}}
							{{#if showSelectOptionMessage}}
							<p class="item-details-add-to-cart-help">
								<i class="item-details-add-to-cart-help-icon"></i>
								<span class="item-details-add-to-cart-help-text">{{translate 'Please select options before adding to cart'}}</span>
							</p>
							{{/if}}
							{{/unless}}
						</form>

						<div data-type="alert-placeholder"></div>

					</section>
				{{/if}}
				{{/if}}
				{{/if}}

				<div class="item-details-main-bottom-banner mobile">
					<span>{{translate 'Share This Product'}}</span>
					<div data-view="SocialSharing.Flyout"></div>
					<div id="banner-summary-bottom" class="item-details-banner-summary-bottom"></div>
				</div>

				<!-- DIY: Our Return Policy -->
				<!--<div class="">&lt;!&ndash;TODO &ndash;&gt;{{translate 'Our Return Policy'}}</div>-->
				<a class="diy-item-details-return-policy-link" data-toggle="modal" data-target="#return-policy-modal">
					<i class="diy-item-details-return-policy-link-icon"></i>
					<span class="diy-item-details-return-policy-link-text">{{translate 'Our Return Policy'}}</span>
				</a>

				<!-- DIY: Our Return Policy -->
				<!-- Modal -->
				<div class="modal fade modal-return-policy" id="return-policy-modal" role="dialog">
					<div class="modal-dialog global-views-modal">
						<!-- Modal content-->
						<div class="global-views-modal-content">
							<div id="modal-header" class="global-views-modal-content-header">
								<button type="button" class="global-views-modal-content-header-close" data-dismiss="modal" aria-hidden="true"> × </button>
							</div>
							<div class="return-policy-view-container global-views-modal-content-body">

								<h2 class="return-policy-view-title">{{{returnPolicyTitle}}}</h2>
								<p>{{{returnPolicyBodyText}}}</p>

								<a href="{{returnPolicyLinkValue}}">{{{returnPolicyLinkText}}}</a>

								<button class="return-policy-view-button" data-dismiss="modal">{{translate 'Close'}}</button>
							</div>
						</div>

					</div>
				</div>



				<div class="item-details-main-bottom-banner desktop">
					<span>{{translate 'Share This Product'}}</span>
					<div data-view="SocialSharing.Flyout"></div>
					<div id="banner-summary-bottom" class="item-details-banner-summary-bottom"></div>
				</div>

			<div id="banner-details-bottom" class="item-details-banner-details-bottom" data-cms-area="item_info_bottom" data-cms-area-filters="page_type"></div>
			</div>
		</section>

		<section class="item-details-more-info-content">
			{{#if showDetails}}

				{{#each details}}
					{{!-- Mobile buttons --}}
					<button class="item-details-info-pusher" data-target="item-details-info-{{ @index }}" data-type="sc-pusher">
						{{ name }} <i></i>
						<!--<p class="item-details-info-hint"> {{{trimHtml content 150}}} </p>-->
					</button>
				{{/each}}

				<div class="item-details-more-info-content-container">

					<div id="banner-content-top" class="content-banner banner-content-top"></div>

					<div role="tabpanel">
						{{!-- When more than one detail is shown, these are the tab headers  --}}
						<ul class="item-details-more-info-content-tabs" role="tablist">
							{{#each details}}
								<li class="item-details-tab-title {{#if @first}} active {{/if}}  {{videosAdditionalClass}} {{infoAndGuidesAdditionalClass}}" role="presentation">
									<a href="#" data-target="#item-details-info-tab-{{@index}}" data-toggle="tab">{{name}}</a>
								</li>
							{{/each}}
						</ul>
						{{!-- Tab Contents --}}
						<div class="item-details-tab-content" >
							{{#each details}}
								<div role="tabpanel" class="item-details-tab-content-panel {{#if @first}}active{{/if}}" id="item-details-info-tab-{{@index}}" itemprop="{{itemprop}}" data-action="pushable" data-id="item-details-info-{{ @index }}">
									<!-- DIY - removed the H2 name -->
									
									<!-- DIY: Description Tab - START -->
									{{#if isDescriptionTab}}
										<div id="item-details-content-container-{{@index}}">

											<div class="item-details-content-main">

												<div class="item-details-logo-descriptions-container">

													<!-- DIY: Description Tab - Logo -->
													{{#if isDealerLogoPresent}}
													<img src="{{descriptionTabContentDealerLogoUrl}}" class="center-block" alt="" width="240px">
													{{/if}}
													<!-- DIY: Description Tab - Long Descriptions -->
													{{#each customDescriptions}}
													<p class="diy-pdp-descriptiontab-longdescription">{{{customDescription}}}</p>
													{{/each}}

												</div>

												<!-- DIY: Description Tab - Features -->
												{{#if isFeatures}}
												<div class="item-details-feature-bullet-container">
													<h4 class="item-details-tab-title">{{translate 'Features'}}</h4>
													<ul class="">
													{{#each features}}
													<li class="">{{{featureBullet}}}</li>
													{{/each}}
													</ul>
												</div>
												{{/if}}

											</div>


										</div>
									<!-- DIY: Description Tab - END -->									
									{{else}}
										<!-- DIY: Description Specs - START -->
										{{#if isSpecsTab}}

										<div class="item-details-specs-table-wrapper">
											<div class="item-details-specs-table-head">
												Specs
											</div>
											<table class="item-details-specs-table">
												<tbody class="item-details-specs-table-content">
												{{#each specs}}
												<tr class="">
													{{#each spec}}
													<td class="item-details-specs-table-label">{{{specLabel}}}</td>
													<td class="item-details-specs-table-value">{{{specValue}}}</td>
													{{/each}}
												</tr>
												{{/each}}
												</tbody>
											</table>

										</div>
										{{else}}
										<!-- DIY: Description Specs - END -->
											{{#if isVideosTab}}
												<div id="item-details-content-container-{{@index}}" class="{{videosAdditionalClass}}">
													<div class="item-video-container" data-view="ItemVideo"></div>
												</div>
											{{/if}}

											{{#if isInfoAndGuidesTab}}
												<div id="item-details-content-container-{{@index}}" class="{{infoAndGuidesAdditionalClass}}">
													<div class="item-documentation-container" data-view="ItemDocumentation"></div>
												</div>
											{{/if}}

											{{#if isPricingTab}}
											<div id="item-details-content-container-{{@index}}">
												<div data-view="Item.QuantityPricing"></div>
											</div>
											{{/if}}

										{{/if}}
									{{/if}}
								</div>
							{{/each}}
							<div class="item-details-action">
								<a href="#" class="item-details-more" data-action="show-more">{{translate 'See More'}}</a>
								<a href="#" class="item-details-less" data-action="show-more">{{translate 'See Less'}}</a>
							</div>
						</div>
					</div>
					<div id="banner-content-bottom" class="content-banner banner-content-bottom"></div>
				</div>
			{{/if}}
		</section>

		<!--<div class="item-details-divider-desktop"></div>-->

		<div class="item-details-content-related-items">
			<div data-view="Related.Items"></div>
		</div>

		<section class="item-details-product-review-content">
			{{#if showReviews}}
			<button class="item-details-product-review-pusher" data-target="item-details-review" data-type="sc-pusher">{{ translate 'Reviews' }}
				<div class="item-details-product-review-pusher-rating" data-view="Global.StarRating"></div><i></i>
			</button>
			<div class="item-details-more-info-content-container" data-action="pushable" data-id="item-details-review">
				<div data-view="ProductReviews.Center"></div>
			</div>
			{{/if}}
		</section>

		<div class="item-details-content-correlated-items">
			<div data-view="Correlated.Items"></div>
		</div>

		<div id="banner-details-bottom" class="content-banner banner-details-bottom" data-cms-area="item_details_banner_bottom" data-cms-area-filters="page_type"></div>
	</article>
</div>
