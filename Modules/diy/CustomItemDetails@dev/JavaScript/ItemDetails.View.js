define('ItemDetails.View', [
    'Facets.Translator',
    'ItemDetails.Collection',

    'item_details.tpl',
    'quick_view.tpl',


    'item_views_selected_option.tpl',
    'item_views_option_color.tpl',
    'item_views_selected_option_color.tpl',
    'item_views_option_text.tpl',
    'item_views_option_dropdown.tpl',
    'item_views_option_tile.tpl',

    'Backbone.CollectionView',
    'ItemDetails.ImageGallery.View',
    'ItemViews.Price.View',
    'GlobalViews.StarRating.View',
    'ProductReviews.Center.View',
    'ItemViews.Option.View',
    'ItemViews.Stock.View',
    'ItemViews.RelatedItem.View',
    'ItemRelations.Correlated.View',
    'ItemRelations.Related.View',
    'SocialSharing.Flyout.View',
    'Profile.Model',
    'Tracker',

    'SC.Configuration',
    'Backbone',
    'Backbone.CompositeView',
    'underscore',
    'jQuery',
    'RecentlyViewedItems.Collection',
    'LiveOrder.Model',

    'jquery.zoom',
    'jQuery.bxSlider',
    'jQuery.scPush',
    'jQuery.scSeeMoreLess',
    'jQuery.overflowing'

], function ItemDetailsView(
    FacetsTranslator,
	ItemDetailsCollection,

	itemDetailsTpl,
	quickViewTpl,


	itemViewsSelectedOptionTemplate,
    itemViewsOptionColorTemplate,
    itemViewsSelectedOptionColorTemplate,
    itemViewsOptionTextTemplate,
    itemViewsOptionDropdownTemplate,
    itemViewsOptionTilesTemplate,

	BackboneCollectionView,
	ItemDetailsImageGalleryView,
	ItemViewsPriceView,
	GlobalViewsStarRatingView,
	ProductReviewsCenterView,
	ItemViewsOptionView,
	ItemViewsStockView,
	ItemViewsRelatedItemView,
	ItemRelationsCorrelatedView,
	ItemRelationsRelatedView,
	SocialSharingFlyoutView,
	ProfileModel,
	Tracker,

	Configuration,
	Backbone,
	BackboneCompositeView,
	_,
	jQuery,
	RecentlyViewedItemsCollection,
	LiveOrderModel
) {
    'use strict';

    var colapsibles_states = {};

	// @class ItemDetails.View Handles the PDP and quick view @extend Backbone.View
    return Backbone.View.extend({

		// @property {String} title
        title: '',

		// @property {String} page_header
		page_header: '',

		// @property {Function} template
		template: itemDetailsTpl,

		// @property {String} modalClass
		modalClass: 'global-views-modal-large',

		// @property {Boolean} showModalPageHeader
		showModalPageHeader: false,

		// @property {Object} attributes List of HTML attributes applied by Backbone into the $el
		attributes: {
			'id': 'product-detail',
			'class': 'view product-detail'
		},

		// TODO ALWAYS USE [data-action=""] WHEN ATTACHING EVENTS INTO THE DOM!
		// @property {Object} events
		events: {
			'blur [data-toggle="text-option"]': 'setOption',
			'click [data-toggle="set-option"]': 'setOption',
			'change [data-toggle="select-option"]': 'setOption',

			'keydown [data-toggle="text-option"]': 'tabNavigationFix',
			'focus [data-toggle="text-option"]': 'tabNavigationFix',

			'click [data-action="plus"]': 'addQuantity',
			'click [data-action="minus"]': 'subQuantity',

			'change [name="quantity"]': 'updateQuantity',
			'keypress [name="quantity"]': 'submitOnEnter',

			'click [data-type="add-to-cart"]': 'addToCart',

			'shown .collapse': 'storeColapsiblesState',
			'hidden .collapse': 'storeColapsiblesState',

			'click [data-action="show-more"]': 'showMore'
		},

		// @method initialize
		// @param {ItemDetails.View.Initialize.Parameters}
		// @return {Void}
		initialize: function initialize(options) {
			this.application = options.application;
			this.reviews_enabled = SC.ENVIRONMENT.REVIEWS_CONFIG && SC.ENVIRONMENT.REVIEWS_CONFIG.enabled;

			BackboneCompositeView.add(this);
			Backbone.Validation.bind(this);

			if (!this.model) {
				throw new Error('A model is needed');
			}
		},

		// @method getBreadcrumbPages Returns the breadcrumb for the current page based on the current item
		// @return {Array<BreadcrumbPage>} breadcrumb pages
		getBreadcrumbPages: function getBreadcrumbPages() {
			return this.model.get('_breadcrumb');
		},

		// @property {Object} childViews
		childViews: {
			'ItemDetails.ImageGallery': function ()
			{
				return new ItemDetailsImageGalleryView({images: this.model.get('_images', true)});
			}
		,	'Item.Price': function ()
			{
				return new ItemViewsPriceView({
					model: this.model
				,	origin: this.inModal ? 'PDPQUICK' : 'PDPFULL'
				});
			}
		,	'Global.StarRating': function ()
			{
				return new GlobalViewsStarRatingView({
					model: this.model
				,	showRatingCount: true
				});
			}
		,	'ProductReviews.Center': function ()
			{
				return new ProductReviewsCenterView({ item: this.model, application: this.application });
			}
		
		,	'ItemDetails.Options': function ()
			{
				var options_to_render = this.model.getPosibleOptions();

				
				_.each(options_to_render, function (option)
				{
					// DIY - Customization - START
					var itemOptionDisplayType = this.model.get('custitem_pdp_option_method');

					switch (itemOptionDisplayType) {
					    case 'Option Boxes':
					        option.templates.selected = itemViewsSelectedOptionColorTemplate;
							option.templates.selector = itemViewsOptionColorTemplate;
					        break;
					    case 'Drop Down Lists':
					        option.templates.selected = itemViewsSelectedOptionTemplate;
							option.templates.selector = itemViewsOptionDropdownTemplate;
					        break;
				        case 'Tiles':
					        option.templates.selected = itemViewsSelectedOptionTemplate;
							option.templates.selector = itemViewsOptionTilesTemplate;
					        break;
					    default:
					        // let the original Item Options selector and selected templates be used, as defined in Config
					}

					// If it's a matrix it checks for valid combinations
					if (option.isMatrixDimension)
					{
						var available = this.model.getValidOptionsFor(option.itemOptionId);
						_.each(option.values, function (value)
						{
							value.isAvailable = _.contains(available, value.label);
						});
					}
				}, this);

				return new BackboneCollectionView({
					collection: new Backbone.Collection(options_to_render)
				,	childView: ItemViewsOptionView
				,	viewsPerRow: 1
				,	childViewOptions: {
						item: this.model
					}
				});
			}
		,	'Item.Stock': function ()
			{
				return new ItemViewsStockView({model: this.model});
			}
		,	'Correlated.Items': function ()
			{
				return new ItemRelationsCorrelatedView({ itemsIds: this.model.get('internalid'), application: this.application });
			}
		,	'Related.Items': function ()
			{
				return new ItemRelationsRelatedView({ itemsIds: this.model.get('internalid'), application: this.application });
			}
		,	'SocialSharing.Flyout': function ()
			{
				return new SocialSharingFlyoutView();
			}
		},

		// @method storeColapsiblesState
		// Since this view is re-rendered every time you make a selection we need to keep the state of the collapsed parts for the next render
		// This method save the status (collapsed/expanded) of all elements with the class .collapse
		storeColapsiblesState: function storeColapsiblesState() {
			//TODO This should be a plugin or be located in some external part!
			this.storeColapsiblesStateCalled = true;

			this.$('.collapse').each(function (index, element)
			{
				colapsibles_states[SC.Utils.getFullPathForElement(element)] = jQuery(element).hasClass('in');
			});
		},

		// @method resetColapsiblesState
		// As we keep track of the state (See storeColapsiblesState method), we need to reset the value of the collapsable items onces we render the first time
		resetColapsiblesState: function resetColapsiblesState() {
			//TODO This should be a plugin or be located in some external part!
			var self = this;
			_.each(colapsibles_states, function (is_in, element_selector)
			{
				self.$(element_selector)[is_in ? 'addClass' : 'removeClass']('in').css('height', is_in ? 'auto' : '0');
			});
		},

		//@method updateQuantity Updates the quantity of the model
		//@param {jQuery.Event} e
		//@return {Void}
		updateQuantity: function updateQuantity(e) {
			var new_quantity = parseInt(jQuery(e.target).val(), 10)
			,	current_quantity = this.model.get('quantity')
			,	isOptimistic = this.application.getConfig('addToCartBehavior') === 'showCartConfirmationModal'
			,	min_quantity =  this.model.get('_minimumQuantity', true);

			if (new_quantity < this.model.get('_minimumQuantity', true))
			{
				if (require('LiveOrder.Model').loadCart().state() === 'resolved') // TODO: resolve error with dependency circular.
				{
					var itemCart = SC.Utils.findItemInCart(this.model, require('LiveOrder.Model').getInstance()) // TODO: resolve error with dependency circular.
					,	total = itemCart && itemCart.get('quantity') || 0;

					if ((total + new_quantity) < this.model.get('_minimumQuantity', true))
					{
						new_quantity = min_quantity;
					}
				}
			}

			jQuery(e.target).val(new_quantity);

			if (new_quantity !== current_quantity)
			{
				this.model.setOption('quantity', new_quantity);

				if (!this.$containerModal || !isOptimistic)
				{
					this.refreshInterface(e);
				}
			}

			if (this.$containerModal)
			{
				this.refreshInterface(e);

				// need to trigger an afterAppendView event here because, like in the PDP, we are really appending a new view for the new selected matrix child
				this.application.getLayout().trigger('afterAppendView', this);
			}
		},

		// @method addQuantity Increase the product's Quantity by 1
		// @param {jQuery.Event} e
		// @return {Void}
		addQuantity: function addQuantity(e) {
			e.preventDefault();
			var input_quantity = this.$('[name="quantity"]')
			,	old_value = parseInt(input_quantity.val(), 10);

			input_quantity.val(old_value + 1);
			input_quantity.trigger('change');
		},

		// @method subQuantity Decreases the product's Quantity by 1
		// @param {jQuery.Event} e
		subQuantity: function subQuantity(e) {
			e.preventDefault();

			var input_quantity = this.$('[name="quantity"]')
			,	old_value = parseInt(input_quantity.val(), 10);

			input_quantity.val(old_value-1);
			input_quantity.trigger('change');

		},

		// @method submitOnEnter Submit the form when user presses enter in the quantity input text
		// @param {jQuery.Event} e
		// @return {Void}
		submitOnEnter: function submitOnEnter(e) {
			if (e.keyCode === 13)
			{
				e.preventDefault();
				e.stopPropagation();
				this.addToCart(e);
			}
		},

		// @method addToCart Updates the Cart to include the current model
		// also takes care of updating the cart if the current model is already in the cart
		// @param {jQuery.Event} e
		// @return {Void}
		addToCart: function addToCart(e) {
			e.preventDefault();

			// Updates the quantity of the model
			var quantity = this.$('[name="quantity"]').val();
			this.model.setOption('quantity', quantity);

			if (this.model.isValid(true) && this.model.isReadyForCart())
			{
				var self = this
				,	cart = LiveOrderModel.getInstance()
				,	layout = this.application.getLayout()
				,	cart_promise
				,	error_message = _('Sorry, there is a problem with this Item and can not be purchased at this time. Please check back later.').translate()
				,	isOptimistic = this.application.getConfig('addToCartBehavior') === 'showCartConfirmationModal';

				if (this.model.itemOptions && this.model.itemOptions.GIFTCERTRECIPIENTEMAIL)
				{
					if (!Backbone.Validation.patterns.email.test(this.model.itemOptions.GIFTCERTRECIPIENTEMAIL.label))
					{
						self.showError(_('Recipient email is invalid').translate());
						return;
					}
				}

				if (isOptimistic)
				{
					//Set the cart to use optimistic when using add to cart
					// TODO pass a param to the add to cart instead of using this hack!
					cart.optimistic = {
						item: this.model
					,	quantity: quantity
					};
				}

				// If the item is already in the cart
				if (this.model.cartItemId)
				{
					cart_promise = cart.updateItem(this.model.cartItemId, this.model).done(function ()
					{
						//If there is an item added into the cart
						if (cart.getLatestAddition())
						{
							if (self.$containerModal)
							{
								self.$containerModal.modal('hide');
							}

							if (layout.currentView instanceof require('Cart.Detailed.View'))
							{
								layout.currentView.showContent();
							}
						}
						else
						{
							self.showError(error_message);
						}
					});
				}
				else
				{
					cart_promise = cart.addItem(this.model).done(function ()
					{
						//If there is an item added into the cart
						if (cart.getLatestAddition())
						{
							if (!isOptimistic)
							{
								layout.showCartConfirmation();
							}
						}
						else
						{
							self.showError(error_message);
						}
					});

					if (isOptimistic)
					{
						cart.optimistic.promise = cart_promise;
						layout.showCartConfirmation();
					}
				}

				// Handle errors. Checks for rollback items.
				cart_promise.fail(function (jqXhr)
				{
					var error_details = null;
					try
					{
						var response = JSON.parse(jqXhr.responseText);
						error_details = response.errorDetails;
					}
					finally
					{
						if (error_details && error_details.status === 'LINE_ROLLBACK')
						{
							var new_line_id = error_details.newLineId;
							self.model.cartItemId = new_line_id;
						}

						self.showError(_('We couldn\'t process your item').translate());

						if (isOptimistic)
						{
							self.showErrorInModal(_('We couldn\'t process your item').translate());
						}
					}
				});

				// Disables the button while it's being saved then enables it back again
				if (e && e.target)
				{
					var $target = jQuery(e.target).attr('disabled', true);

					cart_promise.always(function ()
					{
						$target.attr('disabled', false);
					});
				}
			}
		},

		// @method refreshInterface
		// Computes and store the current state of the item and refresh the whole view, re-rendering the view at the end
		// This also updates the URL, but it does not generates a history point
		// @return {Void}
		refreshInterface: function refreshInterface() {
			var self = this;

			var focused_element = this.$(':focus').get(0);

			this.focusedElement = focused_element ? SC.Utils.getFullPathForElement(focused_element) : null;

			if (!this.inModal)
			{
				Backbone.history.navigate(this.options.baseUrl + this.model.getQueryString(), {replace: true});
			}

			// DIY Customization
			this.computeDetailsArea();

			//TODO This should be a render, as the render aim to re-paint a view and the showContent aims to navigations
			self.showContent({dontScroll: true});
		},

		// @method showInModal overrides the default implementation to take care of showing the PDP in a modal by changing the template
		// This doesn't trigger the after events because those are triggered by showContent
		// @param {Object} options Any options valid to show content
		// @return {jQuery.Deferred}
		showInModal: function showInModal(options) {
			this.template = quickViewTpl;

			return this.application.getLayout().showInModal(this, options);
		},

		// @method prepareViewToBeShown
		// Prepares the model and other internal properties before view.showContent
		// Prepares the view to be shown. Set its title, header and the items options are calculated
		// @return {Void}
		prepareViewToBeShown: function prepareViewToBeShown() {
			// TODO Change this. This method is called by the itemDetails.router before call showContent of this view.
			// As each time an option change the view is re-painted by using showContent, we cannot add this code in the showContent code
			// the correct way to do this is call render for each re-paint.
			this.title = this.model.get('_pageTitle');
			this.page_header = this.model.get('_pageHeader');

			this.computeDetailsArea();
		},

		// @method computeDetailsArea
		// Process what you have configured in itemDetails as item details.
		// In the PDP extra information can be shown based on the itemDetails property in the Shopping.Configuration.
		// These are extra field extracted from the item model
		// @return {Void}
		computeDetailsArea: function computeDetailsArea() {
			var	details = [];

			// Tab 1: Description - combination of Logo + Custom Description + Feature bullets fields
			details = this.generateDescriptionTab(details);

			// Tabs
			// _.each(this.application.getConfig('itemDetails', []), function (item_details)
			// {
			// 	var content = '';
            //
			// 	if (item_details.contentFromKey)
			// 	{
			// 		content = self.model.get(item_details.contentFromKey);
			// 	}
            //
			// 	if (jQuery.trim(content) || item_details.isInfoGuideTab)
			// 	{
			// 		details.push({
			// 			name: item_details.name
			// 		,	isOpened: item_details.opened
			// 		,	content: content
			// 		,	itemprop: item_details.itemprop
			// 		});
			// 	}
			// });

			// Tab 2 & 3: Videos || Info and Guides
			details = this.generateItemVideoAndDocumentationTab(details);

			// Tab 4: Specs
			details = this.generateSpecsTab(details);

			// Tab 5: Quantity Pricing
			details = this.generateQuantityPricingTab(details);

			this.details = details;
		},

		// PDP Tab 1
		generateDescriptionTab: function generateDescriptionTab(details) {
            var self = this;
			var descriptionTabContentDealerLogoUrl = '';
			var descriptionTabContentDealerLogo = self.model.get('custitem_pdp_secondary_logo');
			var isDealerLogoPresent = true;

			if (descriptionTabContentDealerLogo)	{
				descriptionTabContentDealerLogoUrl = "../site/logo-secondary/" + descriptionTabContentDealerLogo;
			} else {
				isDealerLogoPresent = false;
			}
			// Custom Description
			var customDescriptions = [];

			if (self.model.get('custitem_description_long'))	{
				customDescriptions.push({customDescription: self.model.get('custitem_description_long')});
			}
			for (var i = 2; i <= 5; i++)	{
				var customDescriptionFieldId = 'custitem_description_long_' + i;
				var descriptionTabContentDescription = self.model.get(customDescriptionFieldId);
				if (descriptionTabContentDescription)	{
					customDescriptions.push({
						customDescription: descriptionTabContentDescription
					});
				}
			}

			// Bullets
			var features = [];
			for (var bulletNumber = 1; bulletNumber <= 10; bulletNumber++)	{
				var featureBulletFieldId = 'custitem_feature_bullet_' + bulletNumber;
				var descriptionTabContentBullet = self.model.get(featureBulletFieldId);
				if (descriptionTabContentBullet)	{
					features.push({
						featureBullet: descriptionTabContentBullet
					});
				}
			}

			if (features.length > 0 || isDealerLogoPresent  || customDescriptions.length > 0)	{
				details.push({
					name: "Description"
					, isOpened: true
					, isDescriptionTab: true
					, isDealerLogoPresent: isDealerLogoPresent
					, descriptionTabContentDealerLogoUrl: descriptionTabContentDealerLogoUrl
					, customDescriptions: customDescriptions
					, features: features
					, isFeatures: features.length > 0
					, itemprop: 'description'
				});
			}

			return details;
		},

		// PDP Tab 2 and 3
		generateItemVideoAndDocumentationTab: function generateItemVideoAndDocumentationTab(details) {
			var hasItemDocumentation = this.model.get('custitem_item_documentation');
			if (hasItemDocumentation > 0) {
				details.push({
					name: 'Videos',
					isOpened: true,
					isVideosTab: true,
					videosAdditionalClass: 'video-tab'
				});

				details.push({
					name: 'Info & Guides ',
					isOpened: true,
					isInfoAndGuidesTab: true,
					infoAndGuidesAdditionalClass: 'info-and-guides-tab'
				});
			}

			return details;
		},

		// PDP Tab 4
		generateSpecsTab: function generateSpecsTab(details) {
			var specsRawData;
			var self = this;
			var isMatrixChildItem = self.model.getSelectedMatrixChilds().length === 1;
			if (isMatrixChildItem)	{
				specsRawData = self.model.getSelectedMatrixChilds()[0].get('custitem_web_spec_data');
			} else {
				specsRawData = self.model.get('custitem_web_spec_data');
			}

			if (specsRawData) {

				// TODO: Extract elsewhere -- '^' separates the attribute and its value; '|' separates the lines

				// 1) Split the lines
				var specs = [];
				var specLines = specsRawData.split('|');
				_.each(specLines, function (specLine)
				{
					var spec = [];
					var keyValuePair = specLine.split('^');
					var key = keyValuePair[0];
					var value = keyValuePair[1];
					spec.push({
						specLabel: key,
						specValue: value
					});
					specs.push({
						spec: spec
					});
				});

				details.push({
					name: 'Specs',
					isOpened: true,
					isSpecsTab: true,
					specs: specs,
					itemprop: 'description'
				});

			}

			return details;
		},

		// PDP Tab 5
		generateQuantityPricingTab: function generateQuantityPricingTab(details) {
			var self = this;
			var hasQtyPricing;
			var isMatrixChildItem = self.model.getSelectedMatrixChilds().length == 1;
			if (isMatrixChildItem)	{
				hasQtyPricing = self.model.getSelectedMatrixChilds()[0].get('onlinecustomerprice_detail').priceschedule == null ? false : true;
			} else {
				hasQtyPricing = self.model.get('onlinecustomerprice_detail').priceschedule == null ? false : true;
			}

			if (hasQtyPricing)	{
				details.push({
					name: 'Quantity Pricing',
					isOpened: true,
					itemprop: 'pricing',
					isPricingTab: true
				});
			}

			return details;
		},

		// @method getMetaKeywords
		// @return {String}
		getMetaKeywords: function getMetaKeywords() {
			// searchkeywords is for alternative search keywords that customers might use to find this item using your Web store's internal search
			// they are not for the meta keywords
			// return this.model.get('_keywords');
			return this.getMetaTags().filter('[name="keywords"]').attr('content') || '';
		},

		// @method getMetaTags
		// @return {Array<HTMLElement>}
		getMetaTags: function getMetaTags() {
			return jQuery('<head/>').html(
				jQuery.trim(
					this.model.get('_metaTags')
				)
			).children('meta');
		},

		// @method getMetaDescription
		// @return {String}
		getMetaDescription: function getMetaDescription() {
			return this.getMetaTags().filter('[name="description"]').attr('content') || '';
		},

		// @method tabNavigationFix
		// When you blur on an input field the whole page gets rendered,
		// so the function of hitting tab to navigate to the next input stops working
		// This solves that problem by storing a a ref to the current input
		// @param {jQuery.Event} e
		// @return {Void}
		tabNavigationFix: function tabNavigationFix(e) {
			var self = this;
			this.hideError();

			// We need this timeout because sometimes a view is rendered several times and we don't want to loose the tabNavigation
			if (!this.tabNavigationTimeout)
			{
				// If the user is hitting tab we set tabNavigation to true, for any other event we turn it off
				this.tabNavigation = e.type === 'keydown' && e.which === 9;
				this.tabNavigationUpsidedown = e.shiftKey;
				this.tabNavigationCurrent = SC.Utils.getFullPathForElement(e.target);
				if (this.tabNavigation)
				{
					this.tabNavigationTimeout = setTimeout(function ()
					{
						self.tabNavigationTimeout = null;
						this.tabNavigation = false;
					}, 5);
				}
			}
		},

		// @method showContent
		// @param {Object<dontScroll:Boolean>} options
		// @return {Void}
		showContent: function showContent(options) {
			var self = this;

			// Once the showContent has been called, this make sure that the state is preserved
			// REVIEW: the following code might change, showContent should receive an options parameter
			this.application.getLayout().showContent(this, options && options.dontScroll).done(function ()
			{
				Tracker.getInstance().trackProductView(self.model);

				self.afterAppend();
				self.initPlugins();

				self.$('[data-type="add-to-cart"]').attr('disabled', true);
				LiveOrderModel.loadCart().done(function()
				{
					if (self.model.isReadyForCart())
					{
						self.$('[data-type="add-to-cart"]').attr('disabled', false);
					}
				});
			});
		},

		// @method afterAppend
		// @return {Void}
		afterAppend: function showContent() {
			var overflow = false;
			this.focusedElement && this.$(this.focusedElement).focus();

			if (this.tabNavigation)
			{
				var current_index = this.$(':input').index(this.$(this.tabNavigationCurrent).get(0))
				,	next_index = this.tabNavigationUpsidedown ? current_index - 1 : current_index + 1;

				this.$(':input:eq('+ next_index +')').focus();
			}

			this.storeColapsiblesStateCalled ? this.resetColapsiblesState() : this.storeColapsiblesState();

			RecentlyViewedItemsCollection.getInstance().addHistoryItem(this.model);

			if (this.inModal)
			{
				var $link_to_fix = this.$el.find('[data-name="view-full-details"]');
				$link_to_fix.mousedown();
				$link_to_fix.attr('href', $link_to_fix.attr('href') + this.model.getQueryString());
			}

			this.$('#item-details-content-container-0').overflowing('#item-details-info-tab-0', function ()
			{
				overflow = true;
			});

			if(!overflow)
			{
				this.$('.item-details-more').hide();
			}
		},

		// @method initPlugins
		// @return {Void}
		initPlugins: function initPlugins() {
			this.$el.find('[data-action="pushable"]').scPush();
			this.$el.find('[data-action="tab-content"]').scSeeMoreLess();
		},

		// @method setOption When a selection is change, this computes the state of the item to then refresh the interface.
		// @param {jQuery.Event} e
		// @return {Void}
		setOption: function setOption(e) {
			var self = this
			,	$target = jQuery(e.currentTarget)
			,	value = $target.val() || $target.data('value') || null
			,	cart_option_id = $target.closest('[data-type="option"]').data('cart-option-id');

			// prevent from going away
			e.preventDefault();

			// if option is selected, remove the value
			if ($target.data('active'))
			{
				value = null;
			}

			// it will fail if the option is invalid
			try
			{
				this.model.setOption(cart_option_id, value);
			}
			catch (error)
			{
				// Clears all matrix options
				_.each(this.model.getPosibleOptions(), function (option)
				{
					option.isMatrixDimension && self.model.setOption(option.cartOptionId, null);
				});

				// Sets the value once again
				this.model.setOption(cart_option_id, value);
			}

 			this.refreshInterface(e);

			// Need to trigger an afterAppendView event here because, like in the PDP, we are really appending a new view for the new selected matrix child
			if (this.$containerModal)
			{
				this.application.getLayout().trigger('afterAppendView', this);
			}
		},

		// @method showMore Toggle the content of an options, and change the label Show Less and Show More by adding a class
		// @return {Void}
		showMore: function showMore() {
			this.$el.find('.item-details-tab-content').toggleClass('show');
		},

		// @method getContext
		// @return {ItemDetails.View.Context}
		getContext: function getContext() {
			var model = this.model
				,	thumbnail = model.get('_images', true)[0] || model.get('_thumbnail')
				,	selected_options = model.getSelectedOptions()

				,	quantity = model.get('quantity')
				,	min_quantity = model.get('_minimumQuantity', true)
				,	min_disabled = false;


			if (model.get('quantity') <= model.get('_minimumQuantity', true))
			{
				// TODO: resolve error with dependency circular.
				if (require('LiveOrder.Model').loadCart().state() === 'resolved')
				{
					// TODO: resolve error with dependency circular.
					var itemCart = SC.Utils.findItemInCart(model, require('LiveOrder.Model').getInstance())
						,	total = itemCart && itemCart.get('quantity') || 0;

					if ((total + model.get('quantity')) <= model.get('_minimumQuantity', true))
					{
						min_disabled = true;
					}
				}
				else
				{
					min_disabled = false;
				}
			}

			// DIY Customization
			var brandLogo = model.get('custitem_pdp_primary_logo');
			var isBrandLogoPresent = brandLogo ? true : false;
			var baseURL = window.location.origin;
			var brandLogoUrl = baseURL + "/site/logo-primary/" + brandLogo;
			var importantMessageBody = model.get('custitem_status_message_body');
			var importantMessageLinkText = model.get('custitem_status_message_link_text');
			var importantMessageLinkUrl = model.get('custitem_status_message_url_component');
			var showImportantSection = importantMessageBody || importantMessageLinkText ? true : false;

			// DIY: Configuration
			var returnPolicyConfig = SC.ENVIRONMENT.published.RETURN_POLICY;
			var returnPolicyTitle;
			var returnPolicyBodyText;
			var returnPolicyLinkText;
			var returnPolicyLinkValue;

			if (returnPolicyConfig)	{
				returnPolicyTitle = returnPolicyConfig.title;
				returnPolicyBodyText = returnPolicyConfig.body;
				returnPolicyLinkText = returnPolicyConfig.linktext;
				returnPolicyLinkValue = returnPolicyConfig.linkvalue;
			}

			// DIY: The following should take the attributes from the selected Matrix Child Item, if applicable
			var stockStatus;
			var shippingPromotion;
			var timeToShip;
			var hasVolumePricing;
			var backOrderedItemExpectedShipDate;

			var isMatrixChildItem = model.getSelectedMatrixChilds().length == 1;
			if (isMatrixChildItem)	{
				stockStatus = model.getSelectedMatrixChilds()[0].get('custitem_stock_status');
				shippingPromotion = model.getSelectedMatrixChilds()[0].get('custitem_shipping_promotion');
				timeToShip = model.getSelectedMatrixChilds()[0].get('custitem_time_to_ship');
				hasVolumePricing = model.getSelectedMatrixChilds()[0].get('onlinecustomerprice_detail').priceschedule == null ? false : true;
				backOrderedItemExpectedShipDate = model.getSelectedMatrixChilds()[0].get('custitem_backorder_expected_ship_date');

			} else {
				stockStatus = model.get('custitem_stock_status');
				shippingPromotion = model.get('custitem_shipping_promotion');
				timeToShip = model.get('custitem_time_to_ship');
				hasVolumePricing = model.get('onlinecustomerprice_detail').priceschedule == null ? false : true;
				backOrderedItemExpectedShipDate = model.get('custitem_backorder_expected_ship_date');

				// For Volume Pricing, show the icon if any of the children have Qty Pricing defined
				if (model.get('_matrixChilds'))	{
					_.each(model.get('_matrixChilds').models, function (childItemModel)
					{
						if (childItemModel.get('onlinecustomerprice_detail').priceschedule != null)	{
							hasVolumePricing = true;
						}
					});
				}
			}

			var stockStatusIcon;
			var canBeOrdered = true;
			var isInStock = false;
			var isBackordered = false;
			// TODO: Temp - should get ID instead of text value
			switch(stockStatus) {
				case 'In Stock':
					stockStatusIcon = 'instock';
					isInStock = true;
					break;
				case 'Backordered':
					stockStatusIcon = 'backordered';
					isBackordered = true;
					break;
				case 'Can Not Order':
					stockStatusIcon = 'cannotorder';
					canBeOrdered = false;
					break;
				default:
					stockStatusIcon = 'instock';
			}

			//@class ItemDetails.View.Context
			return {
				//@property {ItemDetails.Model} model
				model: model
				//@property {Boolean} isPriceEnabled
				,	isPriceEnabled: !ProfileModel.getInstance().hidePrices()
				//@property {Array<ItemDetailsField>} details
				,	details: this.details
				//@property {Boolean} showDetails
				,	showDetails: this.details.length > 0
				//@property {Boolean} isItemProperlyConfigured
				,	isItemProperlyConfigured: model.isProperlyConfigured()
				//@property {Boolean} showQuantity
				,	showQuantity: model.get('_itemType') === 'GiftCert'
				//@property {Boolean} showRequiredReference
				,	showRequiredReference: model.get('_itemType') === 'GiftCert'
				//@property {Boolean} showSelectOptionifOutOfStock
				,	showSelectOptionMessage : !model.isSelectionComplete() && model.get('_itemType') !== 'GiftCert'
				//@property {Boolean} showMinimumQuantity
				,	showMinimumQuantity: !! min_quantity && min_quantity > 1
				//@property {Boolean} isReadyForCart
				,	isReadyForCart: model.isReadyForCart()
				//@property {Boolean} showReviews
				,	showReviews: this.reviews_enabled
				//@property {String} itemPropSku
				,	itemPropSku: '<span itemprop="sku">' + model.get('_sku', true) + '</span>'
				//@property {String} item_url
				,	item_url : model.get('_url') + model.getQueryString()
				//@property {String} thumbnailUrl
				,	thumbnailUrl : this.options.application.resizeImage(thumbnail.url, 'main')
				//@property {String} thumbnailAlt
				,	thumbnailAlt : thumbnail.altimagetext
				//@property {String} sku
				,	sku : model.get('_sku', true)
				//@property {Boolean} isMinQuantityOne
				,	isMinQuantityOne : model.get('_minimumQuantity', true) === 1
				//@property {Number} minQuantity
				,	minQuantity : min_quantity
				//@property {Number} quantity
				,	quantity : quantity
				//@property {Boolean} isMinusButtonDisabled
				,	isMinusButtonDisabled: min_disabled || model.get('quantity') === 1
				//@property {Boolean} hasCartItem
				,	hasCartItem : !!model.cartItemId
				//@property {Array} selectedOptions
				,	selectedOptions: selected_options
				//@property {Boolean} hasSelectedOptions
				,	hasSelectedOptions: !!selected_options.length
				//@property {Boolean} hasAvailableOptions
				,	hasAvailableOptions: !!model.getPosibleOptions().length
				//@property {Boolean} isReadyForWishList
				,	isReadyForWishList: model.isReadyForWishList()

				// DIY - Custom Item Fields
				,	isOnSale: model.get('custitem_on_sale')
				,	isOnSaleHeader: model.get('custitem_on_sale_message_header')
				,	isOnSaleMessage: model.get('custitem_on_sale_message')

				,	promoOverlay: model.get('custitem_promotional_overlay')
				,	hasPromoOverlay: model.get('custitem_promotional_overlay') ? true : false

				,	customManufacturer: model.get('custitem_manufacturer')

				,	hasVolumePricing: hasVolumePricing
				,	shippingPromotion: shippingPromotion

				,	timeToShip: timeToShip
				,	stockStatus: stockStatus
				,	stockStatusIcon: stockStatusIcon
				,	canBeOrdered: canBeOrdered
				,	backOrderedItemExpectedShipDate: backOrderedItemExpectedShipDate
				,	isInStock: isInStock
				,	isBackordered: isBackordered

				,	brandLogoUrl: brandLogoUrl
				,	isBrandLogoPresent: isBrandLogoPresent

				,	showImportantSection: showImportantSection
				, 	importantMessageBody: importantMessageBody
				, 	importantMessageLinkText: importantMessageLinkText
				,	importantMessageLinkUrl: importantMessageLinkUrl

				,	returnPolicyTitle: returnPolicyTitle
				,	returnPolicyBodyText: returnPolicyBodyText
				,	returnPolicyLinkText: returnPolicyLinkText
				,	returnPolicyLinkValue: returnPolicyLinkValue

			};
		}
	});
});

