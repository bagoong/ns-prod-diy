
define('SitewideMessage.Model'
	, [
		'SC.Model', 
		'Application', 
		'underscore'
	]
	, function (
		SCModel, 
		Application, 
		_
	) {
  
  'use strict';

  return SCModel.extend({

    name: 'SitewideMessage.Model',

    /**
     * getSitewideMessage: Retrieve Sitewide Message
	 *
	 * Assumption: if results set more than 1, only take the 1st one
	 *
	 * @param none
	 *
	 * @return {Array of objects} sitewideMessageObject
	 *
     */
    getSitewideMessage: function () {      

	    var searchResults;
	    var sitewideMessage;

	    var filters = [];
	    filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	    filters.push(new nlobjSearchFilter('custrecord_diy_sitewide_effectivedate', null, 'onorbefore', 'today'));
    	filters.push(new nlobjSearchFilter('custrecord_diy_sitewide_expirationdate', null, 'onorafter', 'today'));

	    var columns = [];
	    columns.push(new nlobjSearchColumn('custrecord_diy_sitewide_message', null, null));

	    searchResults = nlapiSearchRecord('customrecord_diy_sitewide_msg', null, filters, columns) || [];

	    if (searchResults != null && searchResults.length > 0)	{
	    	// Assumption: 1 and only 1
	    	sitewideMessage = searchResults[0].getValue('custrecord_diy_sitewide_message');
	    }
	    return sitewideMessage;
    },

  });
});