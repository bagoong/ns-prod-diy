define('BrandSearch.Collection',[
  'Backbone.CachedCollection',
  'BrandSearch.Model',
  'underscore',
  'Utils'
], function BrandSearchCollection  (
    BackboneCachedCollection,
    Model,
    _,
    Utils
) {

  'use strict';

  return BackboneCachedCollection.extend ({
        model: Model,
        url: Utils.getAbsoluteUrl('services/BrandSearch.Service.ss')
  });
});