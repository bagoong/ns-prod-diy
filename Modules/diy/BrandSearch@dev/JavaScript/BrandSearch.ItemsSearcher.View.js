define('BrandSearch.ItemsSearcher.View', [
    'ItemsSearcher.View',
    'ItemsSearcher.Utils',
    'ItemsSearcher.Collection',
    'ItemsSearcher.Item.View',
    'PluginContainer',
	'SC.Configuration',

    'BrandSearch.Collection',

    'itemssearcher.tpl',

    'Backbone',
    'underscore',
    'jQuery'
],	function BrandSearchItemsSearcherView(
	ItemsSearcherView,
	ItemsSearcherUtils,
	ItemsSearcherCollection,
	ItemsSearcherItemView,
	PluginContainer,
	Configuration,

	BrandsSearcherCollection,

	itemsSearcherTpl,

	Backbone,
	_,
	jQuery
) {
    'use strict';

    ItemsSearcherView.prototype.defaultOptions = _.extend({}, ItemsSearcherView.prototype.defaultOptions, {
		// @property {Number} minLength
		minLength: 3,
		// @property {Number} maxLength
		maxLength: 10,
		// @property {Number} limit
		limit: 10,
		// @property {String} sort
		sort: 'relevance:asc',
		// @property {Array} labels
		labels: [],
		// @property {ItemsSearcher.Collection} collection
		collection: ItemsSearcherCollection,
		// @property {BrandsSearcher.Collection} collection
		brandsCollection: BrandsSearcherCollection,
		// @property {String} query
		query: '',
		// @property {Boolean} ajaxDone
		ajaxDone: false,
		// @property {Boolean} showMenuOnClick
		showMenuOnClick: false,
		// @property {ItemsSearcher.Item.View} itemView
		itemView: ItemsSearcherItemView,
		// @property {Boolean} highlight
		highlight: true,
		//@property {Function} template
		template: itemsSearcherTpl,
		//@property {Void} componentId
		componentId: void 0,
		//@property {Void} componentName
		componentName: void 0,
		//@property {Boolean} showSeeAll
		showSeeAll: true,
		//@property {Boolean} highlightFirst
		highlightFirst: true,
		//@property {ItemsSearcher.View.Options.Item.View.Option} itemViewOptions
		itemViewOptions: { },
		//@property {ItemsSearcher.View.Options.Collection.Option} collectionOptions
		collectionOptions: { },
		//@property {Function<ItemDetails.Model, String, String>} getItemDisplayName This function give the chance to
		//change the way items display name is returned
		getItemDisplayName: null

	});

    ItemsSearcherView.prototype.initialize = function initialize(options)	{
        this.options = _.defaults(options || {}, this.defaultOptions);
        this.collection = new this.options.collection(this.options.collectionOptions);
        this.brandsCollection = new this.options.brandsCollection(this.options.collectionOptions);
        this.template = this.options.template;
        this.on('afterViewRender', this.configureTypeahead, this);
        this.installPlugins();
    };

	ItemsSearcherView.prototype.onKeyUp = function onKeyUp(e) {
		this.trigger('keyUp'
			//@class ItemsSearcher.View.KeyDown.Properties
			,	{
				//@property {Array<ItemDetails.Model>} collection
				collection: this.collection.models
				//@property {jQueryEvent} eventObject
				,	eventObject: e
				//@property {String} currentQuery
				,	currentQuery: this.getCurrentQuery()
			});
	};

    ItemsSearcherView.prototype.getSuggestionItemTemplate = function getSuggestionItemTemplate(item_id)	{
        var model = this.collection.get(item_id);
        if (!model) {
			 _.each(this.brandsCollection.models, function eachModel(brandModel) {
				 if (brandModel.get('id') === item_id) {
					 model = brandModel;
				 }
			 });
		}
		var item_view_options = _.extend({}
			,	this.options.itemViewOptions
			,	{
				model: model
				,	query: this.query
				,	areResults: !!this.collection.length
				,	isAjaxDone: this.options.ajaxDone
			})
			,	items_searcher_item = new this.options.itemView(item_view_options);

		items_searcher_item.render();
		return items_searcher_item.$el;
	};

	ItemsSearcherView.prototype.onItemSelected = function onItemSelected(e, item_id) {
		var itemCollection = this.collection.get(item_id);
		if (itemCollection) {
			this.options.selectedItem = itemCollection;
			// @event itemSelected
			this.trigger('itemSelected'
				//@class ItemsSearcher.View.itemSelected.Properties
				,	{
					//@property {ItemDetails.Model?} selectedItem
					selectedItem: this.collection.get(item_id)
					//@property {Array<ItemDetails.Model>} collection
					,	collection: this.collection.models
					//@property {String} currentQuery
					,	currentQuery: this.options.query
				});
			// @class ItemsSearcher.View
		} else if (item_id === 'see-all-' + this.options.query) {
			this.trigger('itemSelected'
				//@class ItemsSearcher.View.itemSelected.Properties
				,	{
					//@property {Array<ItemDetails.Model>} collection
					collection: this.collection.models
					//@property {String} currentQuery
					,	currentQuery: this.options.query
				});
		} else if (this.brandsCollection) {
			_.each(this.brandsCollection.models, function eachModel(brandModel) {
				if (brandModel.get('id') == item_id) {
					var url = brandModel.get('url');
					var homeTouchPoint = Configuration.siteSettings.touchpoints.home;
						homeTouchPoint = homeTouchPoint.substring(0, homeTouchPoint.indexOf('?'));
					window.location.href = homeTouchPoint + "\\" + url;
				}
			});
		}

	};

	ItemsSearcherView.prototype.loadSuggestionItemsSource = function loadSuggestionItemsSource(query, callback)	{
		var self = this;

		self.options.ajaxDone = false;
		self.options.results = {};
		self.options.query = ItemsSearcherUtils.formatKeywords(query);
		self.collection.reset(undefined, {silent:true});

		// if the character length from the query is over the min length
		if (self.options.query.length >= self.options.minLength)
		{
			self.options.labels = ['see-all-' + self.options.query];
			callback(self.options.labels);

			self.$searchElement.data('ttTypeahead').dropdown.moveCursorDown();
		}

		self.collection.fetch(
			{
				data: {
					q: jQuery.trim(self.options.query)
					,	sort: self.options.sort
					,	limit: self.options.limit
					,	offset: 0
				}
				,	killerId: _.uniqueId('ajax_killer_')
			}
			,	{
				silent: true
			}
		).done(function ()
		{
			self.brandsCollection.fetch(
				{
					data: {
						q: jQuery.trim(self.options.query)
						,	sort: self.options.sort
						,	limit: self.options.limit
						,	offset: 0
					}
					,	killerId: _.uniqueId('ajax_killer_')
				}
				,	{
					silent: true
				}
			).done(function() {

				var userSearchQuery = self.options.query;
					userSearchQuery = userSearchQuery.toLowerCase();
				var brandCollection = [], itemCollection = [];

				if (userSearchQuery && self.brandsCollection.models)	{
					// Iterate through all 'Search Banner' custom records retrieved
					_.each(self.brandsCollection.models, function (brandModel)
					{
						var brandModelKeyWords = brandModel.get('keywords');
							brandModelKeyWords = brandModelKeyWords.toLowerCase();
						if (brandModelKeyWords && brandModelKeyWords.indexOf(userSearchQuery) !== -1)	{
							brandModel.set('isBrands','true');
							brandModel.set('_id',brandModel.get('id'));
							var url = Configuration.siteSettings.touchpoints.home + "#" + brandModel.get('url');
							brandModel.set('customURL', url);
							brandCollection.push(brandModel);
						}
					});
				}
				if (brandCollection.length > 0) {
					_.each(self.collection.models, function (itemModel)
					{
						itemCollection.push(itemModel);
					});
					self.collection.models = [];
					var count = 1;
					_.each(brandCollection, function (brandModel)
					{
						if (count == brandCollection.length) {
							brandModel.set('isLast','true');
						}
						self.collection.models.push(brandModel);
						count++;
					});
					_.each(itemCollection, function (itemModel)
					{
						self.collection.models.push(itemModel);
					});
				}

				self.collection = self.postItemsSuggestionObtained.executeAll(self.collection, self.options) || self.collection;

				self.options.ajaxDone = true;
				self.options.labels = self.options.showSeeAll ? ['see-all-' + self.options.query].concat(self.collection.pluck('_id')) : self.collection.pluck('_id');

				if (!self.options.labels.length)
				{
					self.options.labels = ['see-all-' + self.options.query];
				}

				callback(self.options.labels);

				self.selectFirstIfRequire();
			});
		});
	};
});