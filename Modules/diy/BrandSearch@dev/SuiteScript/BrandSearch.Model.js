define('BrandSearch.Model',[
	'SC.Model',
	'SearchHelper',
	'underscore'
], function BrandSearch(
	SCModel,
	SearchHelper,
	_
) {
	'use strict';

	return SCModel.extend ({
		name: 'BrandSearch',

		brandsSearcherRecord: 'customrecord_search_banners',

		brandsSearcherFieldSets: {
			basic: [
				'id',
				'name',
				'keywords',
				'imageurl',
				'url'
			]
		},

		brandsSearcherFilters: [
			{fieldName: 'isinactive', operator:'is', value1:'F'}],

		brandsSearcherColumns:{
			id:{fieldName:'internalid'},
			name:{fieldName:'name'},
			keywords:{fieldName:'custrecord_keywords'},
			imageurl:{fieldName:'custrecord_image_url', type:'file'},
			url:{fieldName:'custrecord_url'}
		},

		/**
		 * getAllBrandsSearchers: Retrieve all active Search Banner custom records
		 *
		 *
		 * @param none
		 * @return {Array of nlobjSearchResults} brandsSearcherSearchResults
		 *
		 */
		getAllBrandsSearches:function getAllBrandsSearches() {

			//nlapiLogExecution('DEBUG', 'getAllBrandsSearches - START');

			var filters = _.clone(this.brandsSearcherFilters);
			var Search = new SearchHelper(this.brandsSearcherRecord, filters, this.brandsSearcherColumns, this.brandsSearcherFieldSets.basic);

			Search.search();

			//nlapiLogExecution('DEBUG', 'getAllBrandsSearches: results', JSON.stringify(Search.getResults()));

			return Search.getResults();
		}

	});

});