function renderHTMLFromFile(request, response) {
    if(request.getMethod() == 'GET') {
        var fileId = request.getParameter('fileId');
        var file = nlapiLoadFile(fileId); //load the HTML file
        var contents = file.getValue(); //get the contents
        response.write(contents); //render it on the suitelet
    }
}