/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

var IS_PROCESSING = false;

/**
 * customFieldChanged: Triggered sublist change causing total to change
 *
 * @oaram {String} type - sublist internal ID
 * @oaram {String} name - sublist field name
 * @param {Stromg} linenum - sublist line number
 *
 * @return {boolean} true if accept the addition of the new item; false if reject.
 *
*/
function customFieldChanged(type, name, linenum)
{
    //nlapiLogExecution('DEBUG', '*** FIELD CHANGED TEST-START!! ***');

    if (nlapiGetContext().getExecutionContext() != 'webstore') {
        return true;
    }

    if (name == 'custbody_ns_ps_shipping_price' || name == 'custbody_ns_ps_shipping_carrier' || name == 'custbody_ns_ps_shipping_method')   {

        if (IS_PROCESSING) {
            return true;
        }
 
        IS_PROCESSING = true;

        // 1- Shipping Cost

        var currentCost = nlapiGetFieldValue('shippingcost');
        var costToSet = nlapiGetFieldValue('custbody_ns_ps_shipping_price');

        if (costToSet && costToSet != currentCost) {
            nlapiSetFieldValue('custbody_ns_ps_shipping_price', '', true, true);
            nlapiSetFieldValue('shippingcost', costToSet, true, true);
        }

        // 2 - Shipping Carrier

        var currentCarrier = nlapiGetFieldValue('shipcarrier');
        var carrierToSet = nlapiGetFieldValue('custbody_ns_ps_shipping_carrier');

        if (carrierToSet && carrierToSet != currentCarrier) {
            nlapiSetFieldValue('custbody_ns_ps_shipping_carrier', '', true, true);
            nlapiSetFieldValue('shipcarrier', carrierToSet, true, true);
        }

        // 3 - Shipping Method

        var currentMethod = nlapiGetFieldValue('shipmethod');
        var methodToSet = nlapiGetFieldValue('custbody_ns_ps_shipping_method');

        if (methodToSet && methodToSet != currentMethod) {
            nlapiSetFieldValue('custbody_ns_ps_shipping_method', '', true, true);
            nlapiSetFieldValue('shipmethod', methodToSet, true, true);
        }

        IS_PROCESSING = false;
    }

    //nlapiLogExecution('DEBUG', '*** FIELD CHANGED TEST-END!! ***');
}
