/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

/**
 * Entry point function that works on the Brand custom record.
 *
 * @param {String} type
 * @return none
 */
function beforeSubmit_populateLearnAbout(type)	{

	if (type == 'create' || type == 'edit')	{
		populateLearnAbout();		
	}
}


/**
 * Function that reads the Common Questions and Install Instructions files from the File Cabinet and populate their respective Long-Form Text fields.
 *
 * @param none
 * @return none
 */
function populateLearnAbout()	{

	var commonQuestionFileId = nlapiGetFieldValue('custrecord_diy_brand_questions_file');
	var installInstructionsFileId = nlapiGetFieldValue('custrecordcustrecord_diy_brand_f');

	var commonQuestionFile;
	var installInstructionsFile;

	var commonQuestionFileContents;
	var installInstructionsFileContents;


	//nlapiLogExecution('DEBUG', 'commonQuestionFileId', commonQuestionFileId);
	//nlapiLogExecution('DEBUG', 'installInstructionsFileId', installInstructionsFileId);

	// Extract contents of file, and set it into its respective Long-Form Text field
	if (commonQuestionFileId)	{
		commonQuestionFile = nlapiLoadFile(commonQuestionFileId);
		if (commonQuestionFile)	{
			commonQuestionFileContents = commonQuestionFile.getValue();
		}
		if (commonQuestionFileContents)	{
			nlapiSetFieldValue('custrecord_diy_brand_commonquestions', commonQuestionFileContents, null, true);
		}
	}
	if (installInstructionsFileId)	{
		installInstructionsFile = nlapiLoadFile(installInstructionsFileId);
		if (installInstructionsFile)	{
			installInstructionsFileContents = installInstructionsFile.getValue();
		}
		if (installInstructionsFileContents)	{
			nlapiSetFieldValue('custrecordcustrecord_diy_brand_instrct_p', installInstructionsFileContents, null, true);
		}
	}

}

