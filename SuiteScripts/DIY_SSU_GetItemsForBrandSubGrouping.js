/*
    © 2016 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/
  
  
/**
 *  getAllItemsByBrandSubGroupingId
 *
 *  @param - request
 *  @param - response
 *  
**/
function getAllItemsByBrandSubGroupingId(request, response) {
  
    nlapiLogExecution('DEBUG', 'BEGIN ACCESS TEST - ITEMS UNDER BRAND SUBGROUPING');
  
    if (request.getMethod() == 'GET')   {
  
        var brandSubGroupingId = request.getParameter('brandSubGroupingId');
        //nlapiLogExecution('DEBUG', 'brandSubGroupingId', brandSubGroupingId);
  
        if (brandSubGroupingId != null) {
  
            var searchResults;
 
            // MTA -- Method 1 for "OR" in SearchFilter
            var filters = [];
            filters.push([['matrixchild', 'IS', 'F']]);
            filters.push('AND');
            filters.push([
                ['custitem_diy_brand_subgrouping','IS', brandSubGroupingId],
                'OR',
                ['custitem_diy_brand_subgrouping_2','IS', brandSubGroupingId],
                'OR',
                ['custitem_diy_brand_subgrouping_3','IS', brandSubGroupingId],
                'OR',
                ['custitem_diy_brand_subgrouping_4','IS', brandSubGroupingId],
                'OR',
                ['custitem_diy_brand_subgrouping_5','IS', brandSubGroupingId]
            ]);
 
            var columns = [ 
                new nlobjSearchColumn('internalid', null, 'GROUP'),
                new nlobjSearchColumn('type', null, 'GROUP')
            ];
  
            searchResults = nlapiSearchRecord('item', null, filters, columns) || [];
  
            response.write(JSON.stringify(searchResults));
  
          nlapiLogExecution('DEBUG', 'ITEMS UNDER BRAND SUBGROUPING', JSON.stringify(searchResults));
          return response;
        }
    }
}