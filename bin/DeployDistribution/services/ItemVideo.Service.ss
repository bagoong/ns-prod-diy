/*
  © 2015 NetSuite Inc.
  User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
  provided, however, if you are an authorized user with a NetSuite account or log-in, you
  may use this code subject to the terms that govern your access and use.
*/

/*exported service*/
// Service to manage Item Document requests

function service (request) {
  'use strict';

  var Application = require('Application');

  try {
    var method = request.getMethod();
    var ItemVideo = require('ItemVideo.Model');
    var data = JSON.parse(request.getBody() || '{}');
    var itemId = request.getParameter('internalid')
    var tabType = request.getParameter('tabType')

    switch (method) {
      case 'GET':
        Application.sendContent(ItemVideo.list(itemId, tabType), {
          'cache': response.CACHE_DURATION_MEDIUM
        });
      break;

      default:
        // methodNotAllowedError is defined in ssp library commons.js
        Application.sendError(methodNotAllowedError);
    }
  } catch (e) {
    Application.sendError(e);
  }
}