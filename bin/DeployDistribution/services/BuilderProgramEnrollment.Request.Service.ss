/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// BuilderProgramEnrollment.Request.Service.ss
// ----------------
// Service to manage addresses requests
function service (request)
{
	'use strict';

	var Application = require('Application');

	try
	{
		var method = request.getMethod()
		,	response
		,	responseCode
		,	data = JSON.parse(request.getBody() || '{}');

		switch (method)
		{
			case 'POST':
				//Unpack input data and send to form
				//nlapiLogExecution('DEBUG', 'data', JSON.stringify(data));

				var firstname = data.firstname;
				var lastname = data.lastname;				
				var companyname = data.companyname;

				var address1 = data.address1;
				var address2 = data.address2;
				var city = data.city;
				var state = data.state;
				var zipcode = data.zipcode;

				var phone = data.phone;
				var email = data.email;

				var custentity_federal_tax_id = data.custentity_federal_tax_id;

				var title = 'Builder Program Request';
				var incomingmessage = 	'First Name: ' + firstname + ' -- ' +
										'Last Name: ' + lastname + ' -- ' +
										'Company Name: ' + companyname + ' -- ' +
										'Address 1: ' + address1 + ' -- ' +
										'City: ' + city + ' -- ' +
										'State: ' + state + ' -- ' +
										'Zip Code: ' + zipcode + ' -- ' +
										'Phone: ' + phone + ' -- ' +
										'Email: ' + email + ' -- ' +
										'Federal Tax ID: ' + custentity_federal_tax_id;

				var url = 'https://forms.na1.netsuite.com/app/site/crm/externalcasepage.nl?compid=530610&formid=6&h=AACffht__pmqX8hWU9jKfkeJtPVcS5WJmXU%3D';

				url += '&firstname=' + firstname;
				url += '&lastname=' + lastname;
				url += '&companyname=' + companyname;
				url += '&email=' + email;
				url += '&phone=' + phone;
				url += '&address1=' + address1;
				url += '&address2=' + address2;
				url += '&city=' + city;
				url += '&state=' + state;
				url += '&zipcode=' + zipcode;
				url += '&title=' + title;
				url += '&incomingmessage=' + incomingmessage;
				url += '&category=' + 5; // Builder Program Request, pulled from List

				url = url.replace(/ /g,"%20");

				//nlapiLogExecution('DEBUG', 'url', url);

				try {
					var headers = {};
                	headers['Content-Type'] = 'application/json';

	                response = nlapiRequestURL(url, data, headers);
	                responseCode = parseInt(response.getCode(), 10);

	                //nlapiLogExecution('DEBUG', 'responseCode', responseCode);

	                // Just in case someday it accepts the redirect. 206 is netsuite error ('partial content')
	                if (response === 200 || responseCode === 302 || responseCode === 201) {
	                    return Application.sendContent({
	                        successMessage: 'Thanks for contact us'
	                    });
	                } else if (response === 206) {
	                	nlapiLogExecution('ERROR', '206 error, partial content');
                	}
	            } catch (e) {
	                // If the form submit SUCCEEDS!!! it will throw an exception
	                // Because of the url redirect
	                //nlapiLogExecution('DEBUG', 'e', JSON.stringify(e));
	                if (e instanceof nlobjError && e.getCode().toString() === 'ILLEGAL_URL_REDIRECT') {
	                    return Application.sendContent({
	                        successMessage: 'You have successfully submitted your request for Builder Program Enrollment.  We will get back to you shortly, thank you for your patience.'
	                    });
	                }

	                return Application.sendError({
	                    // @property {Number} status
	                    status: 500,
	                    code: 'ERR_FORM',
	                    message: 'Something went wrong processing your form, please try again later.'
	                });
	            }
			break;

			default:
				// methodNotAllowedError is defined in ssp library commons.js
				Application.sendError(methodNotAllowedError);
		}
	}
	catch (e)
	{
		Application.sendError(e);
	}
}