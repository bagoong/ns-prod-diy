define('hotspot.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var template = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <div class=\"hotspot\" data-action=\"hotspot\" style=\"top:"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"top") || (depth0 != null ? compilerNameLookup(depth0,"top") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"top","hash":{},"data":data}) : helper)))
    + "; left:"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"left") || (depth0 != null ? compilerNameLookup(depth0,"left") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"left","hash":{},"data":data}) : helper)))
    + ";\" id=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"id") || (depth0 != null ? compilerNameLookup(depth0,"id") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\"></div> ";
},"3":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <div class=\"hotspot-part "
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"id") || (depth0 != null ? compilerNameLookup(depth0,"id") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" id=\"part-"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"id") || (depth0 != null ? compilerNameLookup(depth0,"id") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" style=\"top:"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"top") || (depth0 != null ? compilerNameLookup(depth0,"top") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"top","hash":{},"data":data}) : helper)))
    + "; left: "
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"left") || (depth0 != null ? compilerNameLookup(depth0,"left") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"left","hash":{},"data":data}) : helper)))
    + ";\"><div class=\"left-arrow\"></div><div class=\"item-contents-wrap\"><div class=\"item-image\"><img src=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"image") || (depth0 != null ? compilerNameLookup(depth0,"image") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"image","hash":{},"data":data}) : helper)))
    + "\" alt=\"\" width=\"146\"></div><div class=\"item-contents\"><p class=\"item-name\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"item-name") || (depth0 != null ? compilerNameLookup(depth0,"item-name") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"item-name","hash":{},"data":data}) : helper)))
    + "</p><p class=\"item-price\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"price") || (depth0 != null ? compilerNameLookup(depth0,"price") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"price","hash":{},"data":data}) : helper)))
    + "</p><p class=\"desc\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"desc") || (depth0 != null ? compilerNameLookup(depth0,"desc") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"desc","hash":{},"data":data}) : helper)))
    + "</p><p class=\"shop-now\"><a href=\"#\">Shop Now >></a></p></div><br class=\"clr\"></div></div> ";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = compilerNameLookup(helpers,"each").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"hotspots") : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " ";
  stack1 = compilerNameLookup(helpers,"each").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"parts") : depth0), {"name":"each","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"useData":true}); template.Name = 'hotspot'; return template;});