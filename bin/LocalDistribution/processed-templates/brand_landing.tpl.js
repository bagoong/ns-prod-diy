define('brand_landing.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var template = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = " <div class=\"brandgrouping-learn-navigation\"><h3>Learn About It</h3> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"hasCommonQuestions") : depth0), {"name":"if","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"hasInstallInstructions") : depth0), {"name":"if","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </div> ";
},"2":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <a href=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"urlcomponent") || (depth0 != null ? compilerNameLookup(depth0,"urlcomponent") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"urlcomponent","hash":{},"data":data}) : helper)))
    + "/common-questions\"><i class=\"infoIcon\"></i><span>Common Questions</span></a> ";
},"4":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <a href=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"urlcomponent") || (depth0 != null ? compilerNameLookup(depth0,"urlcomponent") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"urlcomponent","hash":{},"data":data}) : helper)))
    + "/install-instructions\"><i class=\"infoIcon\"></i><span>Install Instructions</span></a> ";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<div class=\"brandgrouping-logo-wrapper\"><a href=\"/"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"urlcomponent") || (depth0 != null ? compilerNameLookup(depth0,"urlcomponent") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"urlcomponent","hash":{},"data":data}) : helper)))
    + "\"><img class=\"brandgrouping-logo-image\" src=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"brandLogo") || (depth0 != null ? compilerNameLookup(depth0,"brandLogo") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"brandLogo","hash":{},"data":data}) : helper)))
    + "\"></a></div><nav class=\"brandgrouping-category-browse-facets\"> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"hasLearnAbout") : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </nav><nav class=\"brandgrouping-category-browse-facets\"><div class=\"brandgrouping-category-browse-navigation\"><h3>Categories</h3><div data-view=\"BrandGroup.Collection\"></div></div></nav>";
},"useData":true}); template.Name = 'brand_landing'; return template;});