define('brand_group_listing.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var template = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <div class=\"brandgrouping-logo-wrapper\"><img class=\"brandgrouping-logo-image\" src=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"logo") || (depth0 != null ? compilerNameLookup(depth0,"logo") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"logo","hash":{},"data":data}) : helper)))
    + "\"></div> ";
},"3":function(depth0,helpers,partials,data) {
  var stack1, buffer = " <div class=\"brandgrouping-learn-navigation\"><h3>Learn About It</h3> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"hasCommonQuestions") : depth0), {"name":"if","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"hasInstallInstructions") : depth0), {"name":"if","hash":{},"fn":this.program(6, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </div> ";
},"4":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <a href=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"urlcomponent") || (depth0 != null ? compilerNameLookup(depth0,"urlcomponent") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"urlcomponent","hash":{},"data":data}) : helper)))
    + "/common-questions\"><i class=\"infoIcon\"></i><span>Common Questions</span></a> ";
},"6":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <a href=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"urlcomponent") || (depth0 != null ? compilerNameLookup(depth0,"urlcomponent") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"urlcomponent","hash":{},"data":data}) : helper)))
    + "/install-instructions\"><i class=\"infoIcon\"></i><span>Install Instructions</span></a> ";
},"8":function(depth0,helpers,partials,data) {
  var stack1, buffer = " <div class=\"home-slider-container\"><div class=\"home-image-slider\"><ul data-slider class=\"home-image-slider-list\"> ";
  stack1 = compilerNameLookup(helpers,"each").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"brandSlider") : depth0), {"name":"each","hash":{},"fn":this.program(9, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </ul></div></div> ";
},"9":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda;
  return " <li><div class=\"home-slide-main-container\"><a href=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"url") || (depth0 != null ? compilerNameLookup(depth0,"url") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"url","hash":{},"data":data}) : helper)))
    + "\"><img alt=\"\" src=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"image") : depth0)) != null ? compilerNameLookup(stack1,"name") : stack1), depth0))
    + "\"><div class=\"bx-caption\"><span>"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"title") || (depth0 != null ? compilerNameLookup(depth0,"title") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "</span></div></a></div></li> ";
},"11":function(depth0,helpers,partials,data) {
  var stack1, buffer = " <h3>Learn About It</h3><ul> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"hasCommonQuestions") : depth0), {"name":"if","hash":{},"fn":this.program(12, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"hasInstallInstructions") : depth0), {"name":"if","hash":{},"fn":this.program(14, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </ul> ";
},"12":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <li><a href=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"urlcomponent") || (depth0 != null ? compilerNameLookup(depth0,"urlcomponent") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"urlcomponent","hash":{},"data":data}) : helper)))
    + "/common-questions\"><i class=\"infoIcon\"></i><span>Common Questions</span></a></li> ";
},"14":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <li><a href=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"urlcomponent") || (depth0 != null ? compilerNameLookup(depth0,"urlcomponent") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"urlcomponent","hash":{},"data":data}) : helper)))
    + "/install-instructions\"><i class=\"infoIcon\"></i><span>Install Instructions</span></a></li> ";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<div class=\"shopping-layout-breadcrumb\"><ul class=\"global-views-breadcrumb\" itemprop=\"breadcrumb\"><li class=\"global-views-breadcrumb-item\"><a href=\"/\" data-touchpoint=\"home\" data-hashtag=\"#\"> Home </a></li><li class=\"global-views-breadcrumb-divider\"><span class=\"global-views-breadcrumb-divider-icon\"></span></li><li class=\"global-views-breadcrumb-item\"><a href=\"/brands\"> Brands </a></li><li class=\"global-views-breadcrumb-divider\"><span class=\"global-views-breadcrumb-divider-icon\"></span></li><li class=\"global-views-breadcrumb-item-active\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"name") || (depth0 != null ? compilerNameLookup(depth0,"name") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</li></ul></div><div class=\"brandgrouping-content\"><div class=\"brandgrouping-container\"><div class=\"brandgrouping-left-container\"> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"logo") : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " <nav class=\"brandgrouping-category-browse-facets\"> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"hasLearnAbout") : depth0), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " </nav><nav class=\"brandgrouping-category-browse-facets\"><div class=\"brandgrouping-category-browse-navigation\"><h3>Categories</h3><div data-view=\"BrandGroup.Collection\"></div></div></nav></div><div class=\"brandgrouping-right-container\"> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"logo") : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " <div class=\"brandgrouping-tags\"><h1>"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"title") || (depth0 != null ? compilerNameLookup(depth0,"title") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "</h1></div><div class=\"brandgrouping-slider-container\"> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"brandSlider") : depth0), {"name":"if","hash":{},"fn":this.program(8, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " </div><div class=\"brandgrouping-brand-description\"><p>"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"description") || (depth0 != null ? compilerNameLookup(depth0,"description") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"description","hash":{},"data":data}) : helper)))
    + "</p></div><div class=\"brandgrouping-learn-navigation\"> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"hasLearnAbout") : depth0), {"name":"if","hash":{},"fn":this.program(11, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </div><div class=\"brandgrouping-main-collection\" data-view=\"BrandGroupMain.Collection\"></div></div></div></div>";
},"useData":true}); template.Name = 'brand_group_listing'; return template;});