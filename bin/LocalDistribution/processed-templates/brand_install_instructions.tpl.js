define('brand_install_instructions.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var template = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <div class=\"brandgrouping-logo-wrapper\"><a href=\"/"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"brandURLComponent") || (depth0 != null ? compilerNameLookup(depth0,"brandURLComponent") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"brandURLComponent","hash":{},"data":data}) : helper)))
    + "\"><img class=\"brandgrouping-logo-image\" src=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"logo") || (depth0 != null ? compilerNameLookup(depth0,"logo") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"logo","hash":{},"data":data}) : helper)))
    + "\"></a></div> ";
},"3":function(depth0,helpers,partials,data) {
  var stack1, buffer = " <div class=\"brandgrouping-learn-navigation\"><h3>Learn About It</h3> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"hasCommonQuestions") : depth0), {"name":"if","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"hasInstallInstructions") : depth0), {"name":"if","hash":{},"fn":this.program(6, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </div> ";
},"4":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <a href=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"brandURLComponent") || (depth0 != null ? compilerNameLookup(depth0,"brandURLComponent") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"brandURLComponent","hash":{},"data":data}) : helper)))
    + "/common-questions\"><i class=\"infoIcon\"></i><span>Common Questions</span></a> ";
},"6":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <a href=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"brandURLComponent") || (depth0 != null ? compilerNameLookup(depth0,"brandURLComponent") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"brandURLComponent","hash":{},"data":data}) : helper)))
    + "/install-instructions\"><i class=\"infoIcon\"></i><span>Install Instructions</span></a> ";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<div class=\"shopping-layout-breadcrumb\"><ul class=\"global-views-breadcrumb\" itemprop=\"breadcrumb\"><li class=\"global-views-breadcrumb-item\"><a href=\"/\" data-touchpoint=\"home\" data-hashtag=\"#\"> Home </a></li><li class=\"global-views-breadcrumb-divider\"><span class=\"global-views-breadcrumb-divider-icon\"></span></li><li class=\"global-views-breadcrumb-item\"><a href=\"/brands\"> Brands </a></li><li class=\"global-views-breadcrumb-divider\"><span class=\"global-views-breadcrumb-divider-icon\"></span></li><li class=\"global-views-breadcrumb-item\"><a href=\"/"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"brandURLComponent") || (depth0 != null ? compilerNameLookup(depth0,"brandURLComponent") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"brandURLComponent","hash":{},"data":data}) : helper)))
    + "\"> "
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"name") || (depth0 != null ? compilerNameLookup(depth0,"name") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + " </a></li><li class=\"global-views-breadcrumb-divider\"><span class=\"global-views-breadcrumb-divider-icon\"></span></li><li class=\"global-views-breadcrumb-item-active\">Install Instructions</li></ul></div><div class=\"brandgrouping-content\"><div class=\"brandgrouping-container\"><div class=\"brandgrouping-left-container\"> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"logo") : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " <nav class=\"brandgrouping-category-browse-facets\"> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"hasLearnAbout") : depth0), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " </nav><nav class=\"brandgrouping-category-browse-facets\"><div class=\"brandgrouping-category-browse-navigation\"><h3>Categories</h3><div data-view=\"BrandGroup.Collection\"></div></div></nav></div><div class=\"brandgrouping-right-container\"><div class=\"brand-install-instructions\">";
  stack1 = ((helper = (helper = compilerNameLookup(helpers,"installInstructions") || (depth0 != null ? compilerNameLookup(depth0,"installInstructions") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"installInstructions","hash":{},"data":data}) : helper));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</div></div></div></div>";
},"useData":true}); template.Name = 'brand_install_instructions'; return template;});