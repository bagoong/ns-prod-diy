define('newsletter_signup.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var template = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<form action=\"#\" data-validation=\"control-group\"><div><input type=\"email\" name=\"email\" class=\"footer-newsletter-email-form-input\" placeholder=\""
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "enter your email address", {"name":"translate","hash":{},"data":data})))
    + "\" data-validation=\"control\"><button type=\"submit\" class=\"footer-newsletter-email-form-submit\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Subscribe", {"name":"translate","hash":{},"data":data})))
    + "</button></div><div class=\"hide message\"></div></form> ";
},"useData":true}); template.Name = 'newsletter_signup'; return template;});