define('brandlanding_details.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var template = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, buffer = " <div class=\"brandgrouping-subgroup-description\"> ";
  stack1 = ((helper = (helper = compilerNameLookup(helpers,"brandSubGroupDescription") || (depth0 != null ? compilerNameLookup(depth0,"brandSubGroupDescription") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"brandSubGroupDescription","hash":{},"data":data}) : helper));
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </div> ";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<div class=\"brandgrouping-tags\"><h1>"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"brandSubGroupTitle") || (depth0 != null ? compilerNameLookup(depth0,"brandSubGroupTitle") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"brandSubGroupTitle","hash":{},"data":data}) : helper)))
    + "</h1></div> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"brandSubGroupDescription") : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"useData":true}); template.Name = 'brandlanding_details'; return template;});