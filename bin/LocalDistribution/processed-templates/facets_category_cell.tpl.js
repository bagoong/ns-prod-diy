define('facets_category_cell.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var template = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var stack1, helper, lambda=this.lambda, escapeExpression=this.escapeExpression, functionType="function", helperMissing=helpers.helperMissing, buffer = " <div class=\"subcat-wrap\" id=\"cat"
    + escapeExpression(lambda((data && compilerNameLookup(data,"index")), depth0))
    + "\"><script type=\"text/javascript\"> var cat = jQuery(\"#cat"
    + escapeExpression(lambda((data && compilerNameLookup(data,"index")), depth0))
    + "\").attr(\"id\"); </script><h2 class=\"title\"><a href=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"link") || (depth0 != null ? compilerNameLookup(depth0,"link") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"link","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"displayName") || (depth0 != null ? compilerNameLookup(depth0,"displayName") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"displayName","hash":{},"data":data}) : helper)))
    + "</a></h2><div class=\"subcats\"> ";
  stack1 = compilerNameLookup(helpers,"each").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"values") : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </div></div> ";
},"2":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda;
  return " <script type=\"text/javascript\"> var url = \""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"url") || (depth0 != null ? compilerNameLookup(depth0,"url") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"url","hash":{},"data":data}) : helper)))
    + "\";\n		        var link = url.replace(\"Home\",\"\");\n				var urlstring = url.replace(\"Home/\",\"\");\n				var thumb = urlstring.replace(new RegExp(\"/\", \"g\"), \"-\");\n				var thumbUrl = \"../site/categories/\" + thumb.substring(0, thumb.length-1) + \".jpg\";\n\n				//console.log(thumbUrl);\n\n                jQuery(document).ready( function (){\n                    jQuery(\"#\"+cat +\" .subcats .subcat .subcat-link"
    + escapeExpression(lambda((data && compilerNameLookup(data,"index")), depth0))
    + "\").attr(\"href\",link);\n					jQuery(\"#\"+cat +\" .subcats .subcat .subcat-link"
    + escapeExpression(lambda((data && compilerNameLookup(data,"index")), depth0))
    + " img\").attr(\"src\",thumbUrl);\n                }); </script><div class=\"subcat\"><a class=\"subcat-link"
    + escapeExpression(lambda((data && compilerNameLookup(data,"index")), depth0))
    + "\" href=\"#\"><img src=\"\"></a><p><a class=\"subcat-link"
    + escapeExpression(lambda((data && compilerNameLookup(data,"index")), depth0))
    + "\" href=\"#\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"label") || (depth0 != null ? compilerNameLookup(depth0,"label") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"label","hash":{},"data":data}) : helper)))
    + "</a></p></div> ";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = " <script type=\"text/javascript\"> jQuery(\".facets-category-browse-results .facets-facet-browse-header\").hide(); </script> ";
  stack1 = compilerNameLookup(helpers,"each").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"subcats") : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " ";
},"useData":true}); template.Name = 'facets_category_cell'; return template;});