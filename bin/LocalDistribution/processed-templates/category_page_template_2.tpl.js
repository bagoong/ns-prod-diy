define('category_page_template_2.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var template = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<img src=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"bannerImage") || (depth0 != null ? compilerNameLookup(depth0,"bannerImage") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"bannerImage","hash":{},"data":data}) : helper)))
    + "\" alt=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"bannerImageAlt") || (depth0 != null ? compilerNameLookup(depth0,"bannerImageAlt") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"bannerImageAlt","hash":{},"data":data}) : helper)))
    + "\">";
},"3":function(depth0,helpers,partials,data) {
  return " <div class=\"ecqs-subcategories-view\" data-view=\"Subcategory\"></div> ";
  },"5":function(depth0,helpers,partials,data) {
  var stack1, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = " <header class=\"facets-facet-browse-header\"><nav class=\"facets-facet-browse-list-header\"><div class=\"facets-facet-browse-list-header-actions\" data-view=\"Facets.ItemListDisplaySelector\"></div><div class=\"facets-facet-browse-list-header-expander\"><button class=\"facets-facet-browse-list-header-expander-button collapsed\" data-toggle=\"collapse\" data-target=\"#list-header-filters\" aria-expanded=\"true\" aria-controls=\"list-header-filters\"> "
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Sort & Filter", {"name":"translate","hash":{},"data":data})))
    + " <i class=\"facets-facet-browse-list-header-expander-icon\"></i></button></div><div class=\"facets-facet-browse-list-header-filters collapse\" id=\"list-header-filters\"><div class=\"facets-facet-browse-list-header-filters-wrapper\"><div class=\"facets-facet-browse-list-header-filters-row\"><div class=\"facets-facet-browse-list-header-filter-column\" data-view=\"Facets.ItemListShowSelector\"></div> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"showSortAndFacets") : depth0), {"name":"if","hash":{},"fn":this.program(6, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " </div></div></div></nav></header><div class=\"facets-facet-browse-content\"></div><div class=\"facets-facet-browse-results\"><div id=\"banner-section-top\" class=\"content-banner banner-section-top\" data-cms-area=\"item_list_banner_top\" data-cms-area-filters=\"path\"></div> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isEmptyList") : depth0), {"name":"if","hash":{},"fn":this.program(9, data),"inverse":this.program(11, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </div><div class=\"facets-facet-browse-pagination\" data-view=\"GlobalViews.Pagination\"></div></div> ";
},"6":function(depth0,helpers,partials,data) {
  var stack1, buffer = " <div class=\"facets-facet-browse-list-header-filter-column\" data-view=\"Facets.ItemListSortSelector\"></div> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"hasItemsAndFacets") : depth0), {"name":"if","hash":{},"fn":this.program(7, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " ";
},"7":function(depth0,helpers,partials,data) {
  var helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <div class=\"facets-facet-browse-list-header-filter-column\"><button class=\"facets-facet-browse-list-header-filter-facets\" data-type=\"sc-pusher\" data-target=\"product-search-facets\"> "
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Narrow By", {"name":"translate","hash":{},"data":data})))
    + " <i class=\"facets-facet-browse-list-header-filter-facets-icon\"></i></button></div> ";
},"9":function(depth0,helpers,partials,data) {
  return " <div data-view=\"Facets.Items.Empty\"></div> ";
  },"11":function(depth0,helpers,partials,data) {
  return " <div class=\"facets-facet-browse-items\" data-view=\"Facets.Items\"></div> ";
  },"13":function(depth0,helpers,partials,data) {
  return " ";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<section class=\"facets-facet-browse\"><div data-cms-area=\"item_list_banner\" data-cms-area-filters=\"page_type\"></div><div id=\"ecqs-category-banner\" class=\"content-banner banner-section-top\"> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"hasBannerImage") : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " <h1>"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"title") || (depth0 != null ? compilerNameLookup(depth0,"title") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "</h1><p>";
  stack1 = ((helper = (helper = compilerNameLookup(helpers,"pageContent") || (depth0 != null ? compilerNameLookup(depth0,"pageContent") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"pageContent","hash":{},"data":data}) : helper));
  if (stack1 != null) { buffer += stack1; }
  buffer += "</p></div> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"hasSubcategories") : depth0), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"showResults") : depth0), {"name":"if","hash":{},"fn":this.program(5, data),"inverse":this.program(13, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " <div class=\"ecft-cm-merchandizing-zone\"><div data-id=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"merchandisingZone") || (depth0 != null ? compilerNameLookup(depth0,"merchandisingZone") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"merchandisingZone","hash":{},"data":data}) : helper)))
    + "\" data-type=\"merchandising-zone\"></div></div><div id=\"banner-section-bottom\" class=\"content-banner banner-section-bottom\" data-cms-area=\"item_list_banner_bottom\" data-cms-area-filters=\"page_type\"></div></section>";
},"useData":true}); template.Name = 'category_page_template_2'; return template;});