define('item_details.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var template = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  return " <div class=\"item-details-rating-header\" itemprop=\"aggregateRating\" itemscope itemtype=\"http://schema.org/AggregateRating\"><div class=\"item-details-rating-header-rating\" data-view=\"Global.StarRating\"></div></div> ";
  },"3":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <div class=\"item-details-brand-logo-value\"><img src=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"brandLogoUrl") || (depth0 != null ? compilerNameLookup(depth0,"brandLogoUrl") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"brandLogoUrl","hash":{},"data":data}) : helper)))
    + "\" alt=\"\" width=\"125px\" height=\"40px\"></div> ";
},"5":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <div class=\"diy-item-details-onsale-message\"><h4><span></span>"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"isOnSaleHeader") || (depth0 != null ? compilerNameLookup(depth0,"isOnSaleHeader") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"isOnSaleHeader","hash":{},"data":data}) : helper)))
    + "</h4><span></span><div>"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"isOnSaleMessage") || (depth0 != null ? compilerNameLookup(depth0,"isOnSaleMessage") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"isOnSaleMessage","hash":{},"data":data}) : helper)))
    + "</div></div> ";
},"7":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <div class=\"diy-item-details-promo-overlay\"> "
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"promoOverlay") || (depth0 != null ? compilerNameLookup(depth0,"promoOverlay") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"promoOverlay","hash":{},"data":data}) : helper)))
    + " </div> ";
},"9":function(depth0,helpers,partials,data) {
  return " <div class=\"item-details-text-required-reference-container\"><small>Required <span class=\"item-details-text-required-reference\">*</span></small></div> ";
  },"11":function(depth0,helpers,partials,data) {
  var helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <div class=\"diy-item-details-vp-icon\"><i></i>"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Volume Pricing", {"name":"translate","hash":{},"data":data})))
    + "</div> ";
},"13":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <div class=\"diy-item-details-shipping-promotions\"><i></i>"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"shippingPromotion") || (depth0 != null ? compilerNameLookup(depth0,"shippingPromotion") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"shippingPromotion","hash":{},"data":data}) : helper)))
    + "</div> ";
},"15":function(depth0,helpers,partials,data) {
  var stack1, buffer = " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"hasAvailableOptions") : depth0), {"name":"if","hash":{},"fn":this.program(16, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " <div class=\"item-details-options-content\" data-action=\"pushable\" data-id=\"item-details-options\"><div class=\"item-details-options-content-price\" data-view=\"Item.Price\"></div><div class=\"item-details-options-content-stock\"  data-view=\"Item.Stock\"></div><div data-view=\"ItemDetails.Options\"></div></div> ";
},"16":function(depth0,helpers,partials,data) {
  var stack1, buffer = " <button class=\"item-details-options-pusher\" data-type=\"sc-pusher\" data-target=\"item-details-options\"> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isReadyForCart") : depth0), {"name":"if","hash":{},"fn":this.program(17, data),"inverse":this.program(19, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"hasSelectedOptions") : depth0), {"name":"if","hash":{},"fn":this.program(21, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " <i></i> ";
  stack1 = compilerNameLookup(helpers,"each").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"selectedOptions") : depth0), {"name":"each","hash":{},"fn":this.program(23, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </button> ";
},"17":function(depth0,helpers,partials,data) {
  var helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Options", {"name":"translate","hash":{},"data":data})));
  },"19":function(depth0,helpers,partials,data) {
  var helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Select Options", {"name":"translate","hash":{},"data":data})));
  },"21":function(depth0,helpers,partials,data) {
  return ":";
  },"23":function(depth0,helpers,partials,data) {
  var stack1, buffer = " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (data && compilerNameLookup(data,"first")), {"name":"if","hash":{},"fn":this.program(24, data),"inverse":this.program(26, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " ";
},"24":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <span> "
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"label") || (depth0 != null ? compilerNameLookup(depth0,"label") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"label","hash":{},"data":data}) : helper)))
    + " </span> ";
},"26":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <span> , "
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"label") || (depth0 != null ? compilerNameLookup(depth0,"label") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"label","hash":{},"data":data}) : helper)))
    + " </span> ";
},"28":function(depth0,helpers,partials,data) {
  var helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <div class=\"alert alert-error\"> "
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "<b>Warning</b>: This item is not properly configured, please contact your administrator.", {"name":"translate","hash":{},"data":data})))
    + " </div> ";
},"30":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <p class=\"item-details-time-to-ship-value\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"timeToShip") || (depth0 != null ? compilerNameLookup(depth0,"timeToShip") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"timeToShip","hash":{},"data":data}) : helper)))
    + "</p> ";
},"32":function(depth0,helpers,partials,data) {
  var stack1, buffer = " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isBackordered") : depth0), {"name":"if","hash":{},"fn":this.program(33, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " ";
},"33":function(depth0,helpers,partials,data) {
  var helper, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function";
  return " <div>"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Expected Ship Date: ", {"name":"translate","hash":{},"data":data})))
    + "</div><p class=\"item-details-time-to-ship-value\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"backOrderedItemExpectedShipDate") || (depth0 != null ? compilerNameLookup(depth0,"backOrderedItemExpectedShipDate") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"backOrderedItemExpectedShipDate","hash":{},"data":data}) : helper)))
    + "</p> ";
},"35":function(depth0,helpers,partials,data) {
  var stack1, helper, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function", buffer = " <div class=\"item-details-important-container\"><h4><span></span>"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "IMPORTANT", {"name":"translate","hash":{},"data":data})))
    + "</h4><p class=\"message-body\">";
  stack1 = ((helper = (helper = compilerNameLookup(helpers,"importantMessageBody") || (depth0 != null ? compilerNameLookup(depth0,"importantMessageBody") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"importantMessageBody","hash":{},"data":data}) : helper));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</p><p class=\"\"><a href=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"importantMessageLinkUrl") || (depth0 != null ? compilerNameLookup(depth0,"importantMessageLinkUrl") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"importantMessageLinkUrl","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"importantMessageLinkText") || (depth0 != null ? compilerNameLookup(depth0,"importantMessageLinkText") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"importantMessageLinkText","hash":{},"data":data}) : helper)))
    + "</a></p></div> ";
},"37":function(depth0,helpers,partials,data) {
  var stack1, buffer = " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isItemProperlyConfigured") : depth0), {"name":"if","hash":{},"fn":this.program(38, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " ";
},"38":function(depth0,helpers,partials,data) {
  var stack1, buffer = " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"canBeOrdered") : depth0), {"name":"if","hash":{},"fn":this.program(39, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " ";
},"39":function(depth0,helpers,partials,data) {
  var stack1, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = " <section class=\"item-details-actions\"><form action=\"#\" class=\"item-details-add-to-cart-form\" data-validation=\"control-group\"> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"showQuantity") : depth0), {"name":"if","hash":{},"fn":this.program(40, data),"inverse":this.program(42, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " <div class=\"item-details-actions-container\"><div class=\"item-details-add-to-cart\"><button data-type=\"add-to-cart\" data-action=\"sticky\" class=\"item-details-add-to-cart-button\" ";
  stack1 = compilerNameLookup(helpers,"unless").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isReadyForCart") : depth0), {"name":"unless","hash":{},"fn":this.program(43, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "> "
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Add to Cart", {"name":"translate","hash":{},"data":data})))
    + " </button></div> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isReadyForWishList") : depth0), {"name":"if","hash":{},"fn":this.program(47, data),"inverse":this.program(49, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " </div> ";
  stack1 = compilerNameLookup(helpers,"unless").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isReadyForCart") : depth0), {"name":"unless","hash":{},"fn":this.program(51, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </form><div data-type=\"alert-placeholder\"></div></section> ";
},"40":function(depth0,helpers,partials,data) {
  return " <input type=\"hidden\" name=\"quantity\" id=\"quantity\" value=\"1\"> ";
  },"42":function(depth0,helpers,partials,data) {
  var stack1, helper, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function", buffer = " <div class=\"item-details-options-quantity\" data-validation=\"control\"><label for=\"quantity\" class=\"item-details-options-quantity-title\"> "
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Qty", {"name":"translate","hash":{},"data":data})))
    + " </label><button class=\"item-details-quantity-remove\" data-action=\"minus\" ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isMinusButtonDisabled") : depth0), {"name":"if","hash":{},"fn":this.program(43, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += ">-</button><input type=\"number\" name=\"quantity\" id=\"quantity\" class=\"item-details-quantity-value\" value=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"quantity") || (depth0 != null ? compilerNameLookup(depth0,"quantity") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"quantity","hash":{},"data":data}) : helper)))
    + "\" min=\"1\"><button class=\"item-details-quantity-add\" data-action=\"plus\">+</button> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"showMinimumQuantity") : depth0), {"name":"if","hash":{},"fn":this.program(45, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </div> ";
},"43":function(depth0,helpers,partials,data) {
  return "disabled";
  },"45":function(depth0,helpers,partials,data) {
  var helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <small class=\"item-details-options-quantity-title-help\"> "
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Minimum of $(0) required", (depth0 != null ? compilerNameLookup(depth0,"minQuantity") : depth0), {"name":"translate","hash":{},"data":data})))
    + " </small> ";
},"47":function(depth0,helpers,partials,data) {
  return " <div class=\"item-details-add-to-wishlist\" data-type=\"product-lists-control\"></div> ";
  },"49":function(depth0,helpers,partials,data) {
  return " <div class=\"item-details-add-to-wishlist\" data-type=\"product-lists-control\" data-disabledbutton=\"true\"></div> ";
  },"51":function(depth0,helpers,partials,data) {
  var stack1, buffer = " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"showSelectOptionMessage") : depth0), {"name":"if","hash":{},"fn":this.program(52, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " ";
},"52":function(depth0,helpers,partials,data) {
  var helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <p class=\"item-details-add-to-cart-help\"><i class=\"item-details-add-to-cart-help-icon\"></i><span class=\"item-details-add-to-cart-help-text\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Please select options before adding to cart", {"name":"translate","hash":{},"data":data})))
    + "</span></p> ";
},"54":function(depth0,helpers,partials,data) {
  var stack1, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = " ";
  stack1 = compilerNameLookup(helpers,"each").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"details") : depth0), {"name":"each","hash":{},"fn":this.program(55, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " <div class=\"item-details-more-info-content-container\"><div id=\"banner-content-top\" class=\"content-banner banner-content-top\"></div><div role=\"tabpanel\">  <ul class=\"item-details-more-info-content-tabs\" role=\"tablist\"> ";
  stack1 = compilerNameLookup(helpers,"each").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"details") : depth0), {"name":"each","hash":{},"fn":this.program(57, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " </ul>  <div class=\"item-details-tab-content\" > ";
  stack1 = compilerNameLookup(helpers,"each").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"details") : depth0), {"name":"each","hash":{},"fn":this.program(60, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " <div class=\"item-details-action\"><a href=\"#\" class=\"item-details-more\" data-action=\"show-more\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "See More", {"name":"translate","hash":{},"data":data})))
    + "</a><a href=\"#\" class=\"item-details-less\" data-action=\"show-more\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "See Less", {"name":"translate","hash":{},"data":data})))
    + "</a></div></div></div><div id=\"banner-content-bottom\" class=\"content-banner banner-content-bottom\"></div></div> ";
},"55":function(depth0,helpers,partials,data) {
  var helper, lambda=this.lambda, escapeExpression=this.escapeExpression, functionType="function", helperMissing=helpers.helperMissing;
  return "  <button class=\"item-details-info-pusher\" data-target=\"item-details-info-"
    + escapeExpression(lambda((data && compilerNameLookup(data,"index")), depth0))
    + "\" data-type=\"sc-pusher\"> "
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"name") || (depth0 != null ? compilerNameLookup(depth0,"name") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + " <i></i></button> ";
},"57":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda, buffer = " <li class=\"item-details-tab-title ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (data && compilerNameLookup(data,"first")), {"name":"if","hash":{},"fn":this.program(58, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " "
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"videosAdditionalClass") || (depth0 != null ? compilerNameLookup(depth0,"videosAdditionalClass") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"videosAdditionalClass","hash":{},"data":data}) : helper)))
    + " "
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"infoAndGuidesAdditionalClass") || (depth0 != null ? compilerNameLookup(depth0,"infoAndGuidesAdditionalClass") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"infoAndGuidesAdditionalClass","hash":{},"data":data}) : helper)))
    + "\" role=\"presentation\"><a href=\"#\" data-target=\"#item-details-info-tab-"
    + escapeExpression(lambda((data && compilerNameLookup(data,"index")), depth0))
    + "\" data-toggle=\"tab\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"name") || (depth0 != null ? compilerNameLookup(depth0,"name") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</a></li> ";
},"58":function(depth0,helpers,partials,data) {
  return " active ";
  },"60":function(depth0,helpers,partials,data) {
  var stack1, helper, lambda=this.lambda, escapeExpression=this.escapeExpression, functionType="function", helperMissing=helpers.helperMissing, buffer = " <div role=\"tabpanel\" class=\"item-details-tab-content-panel ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (data && compilerNameLookup(data,"first")), {"name":"if","hash":{},"fn":this.program(61, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "\" id=\"item-details-info-tab-"
    + escapeExpression(lambda((data && compilerNameLookup(data,"index")), depth0))
    + "\" itemprop=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"itemprop") || (depth0 != null ? compilerNameLookup(depth0,"itemprop") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"itemprop","hash":{},"data":data}) : helper)))
    + "\" data-action=\"pushable\" data-id=\"item-details-info-"
    + escapeExpression(lambda((data && compilerNameLookup(data,"index")), depth0))
    + "\"> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isDescriptionTab") : depth0), {"name":"if","hash":{},"fn":this.program(63, data),"inverse":this.program(71, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </div> ";
},"61":function(depth0,helpers,partials,data) {
  return "active";
  },"63":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = " <div id=\"item-details-content-container-"
    + escapeExpression(lambda((data && compilerNameLookup(data,"index")), depth0))
    + "\"><div class=\"item-details-content-main\"><div class=\"item-details-logo-descriptions-container\"> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isDealerLogoPresent") : depth0), {"name":"if","hash":{},"fn":this.program(64, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " ";
  stack1 = compilerNameLookup(helpers,"each").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"customDescriptions") : depth0), {"name":"each","hash":{},"fn":this.program(66, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " </div> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isFeatures") : depth0), {"name":"if","hash":{},"fn":this.program(68, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </div></div> ";
},"64":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <img src=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"descriptionTabContentDealerLogoUrl") || (depth0 != null ? compilerNameLookup(depth0,"descriptionTabContentDealerLogoUrl") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"descriptionTabContentDealerLogoUrl","hash":{},"data":data}) : helper)))
    + "\" class=\"center-block\" alt=\"\" width=\"240px\"> ";
},"66":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, buffer = " <p class=\"diy-pdp-descriptiontab-longdescription\">";
  stack1 = ((helper = (helper = compilerNameLookup(helpers,"customDescription") || (depth0 != null ? compilerNameLookup(depth0,"customDescription") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"customDescription","hash":{},"data":data}) : helper));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</p> ";
},"68":function(depth0,helpers,partials,data) {
  var stack1, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = " <div class=\"item-details-feature-bullet-container\"><h4 class=\"item-details-tab-title\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Features", {"name":"translate","hash":{},"data":data})))
    + "</h4><ul class=\"\"> ";
  stack1 = compilerNameLookup(helpers,"each").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"features") : depth0), {"name":"each","hash":{},"fn":this.program(69, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </ul></div> ";
},"69":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, buffer = " <li class=\"\">";
  stack1 = ((helper = (helper = compilerNameLookup(helpers,"featureBullet") || (depth0 != null ? compilerNameLookup(depth0,"featureBullet") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"featureBullet","hash":{},"data":data}) : helper));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</li> ";
},"71":function(depth0,helpers,partials,data) {
  var stack1, buffer = " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isSpecsTab") : depth0), {"name":"if","hash":{},"fn":this.program(72, data),"inverse":this.program(76, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " ";
},"72":function(depth0,helpers,partials,data) {
  var stack1, buffer = " <div class=\"item-details-specs-table-wrapper\"><div class=\"item-details-specs-table-head\"> Specs </div><table class=\"item-details-specs-table\"><tbody class=\"item-details-specs-table-content\"> ";
  stack1 = compilerNameLookup(helpers,"each").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"specs") : depth0), {"name":"each","hash":{},"fn":this.program(73, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </tbody></table></div> ";
},"73":function(depth0,helpers,partials,data) {
  var stack1, buffer = " <tr class=\"\"> ";
  stack1 = compilerNameLookup(helpers,"each").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"spec") : depth0), {"name":"each","hash":{},"fn":this.program(74, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </tr> ";
},"74":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, buffer = " <td class=\"item-details-specs-table-label\">";
  stack1 = ((helper = (helper = compilerNameLookup(helpers,"specLabel") || (depth0 != null ? compilerNameLookup(depth0,"specLabel") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"specLabel","hash":{},"data":data}) : helper));
  if (stack1 != null) { buffer += stack1; }
  buffer += "</td><td class=\"item-details-specs-table-value\">";
  stack1 = ((helper = (helper = compilerNameLookup(helpers,"specValue") || (depth0 != null ? compilerNameLookup(depth0,"specValue") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"specValue","hash":{},"data":data}) : helper));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</td> ";
},"76":function(depth0,helpers,partials,data) {
  var stack1, buffer = " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isVideosTab") : depth0), {"name":"if","hash":{},"fn":this.program(77, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isInfoAndGuidesTab") : depth0), {"name":"if","hash":{},"fn":this.program(79, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isPricingTab") : depth0), {"name":"if","hash":{},"fn":this.program(81, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " ";
},"77":function(depth0,helpers,partials,data) {
  var helper, lambda=this.lambda, escapeExpression=this.escapeExpression, functionType="function", helperMissing=helpers.helperMissing;
  return " <div id=\"item-details-content-container-"
    + escapeExpression(lambda((data && compilerNameLookup(data,"index")), depth0))
    + "\" class=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"videosAdditionalClass") || (depth0 != null ? compilerNameLookup(depth0,"videosAdditionalClass") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"videosAdditionalClass","hash":{},"data":data}) : helper)))
    + "\"><div class=\"item-video-container\" data-view=\"ItemVideo\"></div></div> ";
},"79":function(depth0,helpers,partials,data) {
  var helper, lambda=this.lambda, escapeExpression=this.escapeExpression, functionType="function", helperMissing=helpers.helperMissing;
  return " <div id=\"item-details-content-container-"
    + escapeExpression(lambda((data && compilerNameLookup(data,"index")), depth0))
    + "\" class=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"infoAndGuidesAdditionalClass") || (depth0 != null ? compilerNameLookup(depth0,"infoAndGuidesAdditionalClass") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"infoAndGuidesAdditionalClass","hash":{},"data":data}) : helper)))
    + "\"><div class=\"item-documentation-container\" data-view=\"ItemDocumentation\"></div></div> ";
},"81":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return " <div id=\"item-details-content-container-"
    + escapeExpression(lambda((data && compilerNameLookup(data,"index")), depth0))
    + "\"><div data-view=\"Item.QuantityPricing\"></div></div> ";
},"83":function(depth0,helpers,partials,data) {
  var helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <button class=\"item-details-product-review-pusher\" data-target=\"item-details-review\" data-type=\"sc-pusher\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Reviews", {"name":"translate","hash":{},"data":data})))
    + " <div class=\"item-details-product-review-pusher-rating\" data-view=\"Global.StarRating\"></div><i></i></button><div class=\"item-details-more-info-content-container\" data-action=\"pushable\" data-id=\"item-details-review\"><div data-view=\"ProductReviews.Center\"></div></div> ";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, lambda=this.lambda, escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, functionType="function", buffer = " <div class=\"item-details\"><div data-cms-area=\"item_details_banner\" data-cms-area-filters=\"page_type\"></div><header class=\"item-details-header\"><div id=\"banner-content-top\" class=\"item-details-banner-top\"></div></header><article class=\"item-details-content\" itemscope itemtype=\"http://schema.org/Product\"><meta itemprop=\"url\" content=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"_url") : stack1), depth0))
    + "\"><div id=\"banner-details-top\" class=\"item-details-banner-top-details\"></div><section class=\"item-details-main-content\"><div class=\"item-details-content-header\"><h1 class=\"item-details-content-header-title\" itemprop=\"name\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"_pageHeader") : stack1), depth0))
    + "</h1><div class=\"path\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "by ", {"name":"translate","hash":{},"data":data})))
    + " "
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"customManufacturer") || (depth0 != null ? compilerNameLookup(depth0,"customManufacturer") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"customManufacturer","hash":{},"data":data}) : helper)))
    + "</div><div class=\"item-details-sku-container\"><span class=\"item-details-sku\"> "
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "SKU:", {"name":"translate","hash":{},"data":data})))
    + " </span><span class=\"item-details-sku-value\" itemprop=\"sku\"> "
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"sku") || (depth0 != null ? compilerNameLookup(depth0,"sku") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"sku","hash":{},"data":data}) : helper)))
    + " </span></div> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"showReviews") : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " <div data-cms-area=\"item_info\" data-cms-area-filters=\"path\"></div><div class=\"diy-item-details-info-mobile\"><div class=\"diy-item-details-price-brand-container\"><div data-view=\"Item.Price\"></div> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isBrandLogoPresent") : depth0), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " <div class=\"clearfix\"></div></div> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isOnSale") : depth0), {"name":"if","hash":{},"fn":this.program(5, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " <div data-view=\"Item.Stock\"></div></div></div><div class=\"item-details-image-gallery-container\"> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"hasPromoOverlay") : depth0), {"name":"if","hash":{},"fn":this.program(7, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " <div id=\"banner-image-top\" class=\"content-banner banner-image-top\"></div><div data-view=\"ItemDetails.ImageGallery\"></div><div id=\"banner-image-bottom\" class=\"content-banner banner-image-bottom\"></div></div><div class=\"item-details-main\"><section class=\"item-details-info\"><div id=\"banner-summary-bottom\" class=\"item-details-banner-summary-bottom\"></div><div class=\"diy-item-details-info-desktop\"><div class=\"diy-item-details-price-brand-container\"><div data-view=\"Item.Price\"></div> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isBrandLogoPresent") : depth0), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " <div class=\"clearfix\"></div></div> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isOnSale") : depth0), {"name":"if","hash":{},"fn":this.program(5, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " <div data-view=\"Item.Stock\"></div></div></section> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"showRequiredReference") : depth0), {"name":"if","hash":{},"fn":this.program(9, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"hasVolumePricing") : depth0), {"name":"if","hash":{},"fn":this.program(11, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"shippingPromotion") : depth0), {"name":"if","hash":{},"fn":this.program(13, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " <section class=\"item-details-options\"> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isItemProperlyConfigured") : depth0), {"name":"if","hash":{},"fn":this.program(15, data),"inverse":this.program(28, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " </section><div class=\"item-details-stock-status-time-to-ship-container\"><p class=\"item-details-stock-status-value "
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"stockStatusIcon") || (depth0 != null ? compilerNameLookup(depth0,"stockStatusIcon") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"stockStatusIcon","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"stockStatus") || (depth0 != null ? compilerNameLookup(depth0,"stockStatus") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"stockStatus","hash":{},"data":data}) : helper)))
    + "</p> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isInStock") : depth0), {"name":"if","hash":{},"fn":this.program(30, data),"inverse":this.program(32, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " </div> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"showImportantSection") : depth0), {"name":"if","hash":{},"fn":this.program(35, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isPriceEnabled") : depth0), {"name":"if","hash":{},"fn":this.program(37, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " <div class=\"item-details-main-bottom-banner mobile\"><span>"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Share This Product", {"name":"translate","hash":{},"data":data})))
    + "</span><div data-view=\"SocialSharing.Flyout\"></div><div id=\"banner-summary-bottom\" class=\"item-details-banner-summary-bottom\"></div></div><a class=\"diy-item-details-return-policy-link\" data-toggle=\"modal\" data-target=\"#return-policy-modal\"><i class=\"diy-item-details-return-policy-link-icon\"></i><span class=\"diy-item-details-return-policy-link-text\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Our Return Policy", {"name":"translate","hash":{},"data":data})))
    + "</span></a><div class=\"modal fade modal-return-policy\" id=\"return-policy-modal\" role=\"dialog\"><div class=\"modal-dialog global-views-modal\"><div class=\"global-views-modal-content\"><div id=\"modal-header\" class=\"global-views-modal-content-header\"><button type=\"button\" class=\"global-views-modal-content-header-close\" data-dismiss=\"modal\" aria-hidden=\"true\"> × </button></div><div class=\"return-policy-view-container global-views-modal-content-body\"><h2 class=\"return-policy-view-title\">";
  stack1 = ((helper = (helper = compilerNameLookup(helpers,"returnPolicyTitle") || (depth0 != null ? compilerNameLookup(depth0,"returnPolicyTitle") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"returnPolicyTitle","hash":{},"data":data}) : helper));
  if (stack1 != null) { buffer += stack1; }
  buffer += "</h2><p>";
  stack1 = ((helper = (helper = compilerNameLookup(helpers,"returnPolicyBodyText") || (depth0 != null ? compilerNameLookup(depth0,"returnPolicyBodyText") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"returnPolicyBodyText","hash":{},"data":data}) : helper));
  if (stack1 != null) { buffer += stack1; }
  buffer += "</p><a href=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"returnPolicyLinkValue") || (depth0 != null ? compilerNameLookup(depth0,"returnPolicyLinkValue") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"returnPolicyLinkValue","hash":{},"data":data}) : helper)))
    + "\">";
  stack1 = ((helper = (helper = compilerNameLookup(helpers,"returnPolicyLinkText") || (depth0 != null ? compilerNameLookup(depth0,"returnPolicyLinkText") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"returnPolicyLinkText","hash":{},"data":data}) : helper));
  if (stack1 != null) { buffer += stack1; }
  buffer += "</a><button class=\"return-policy-view-button\" data-dismiss=\"modal\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Close", {"name":"translate","hash":{},"data":data})))
    + "</button></div></div></div></div><div class=\"item-details-main-bottom-banner desktop\"><span>"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Share This Product", {"name":"translate","hash":{},"data":data})))
    + "</span><div data-view=\"SocialSharing.Flyout\"></div><div id=\"banner-summary-bottom\" class=\"item-details-banner-summary-bottom\"></div></div><div id=\"banner-details-bottom\" class=\"item-details-banner-details-bottom\" data-cms-area=\"item_info_bottom\" data-cms-area-filters=\"page_type\"></div></div></section><section class=\"item-details-more-info-content\"> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"showDetails") : depth0), {"name":"if","hash":{},"fn":this.program(54, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " </section><div class=\"item-details-content-related-items\"><div data-view=\"Related.Items\"></div></div><section class=\"item-details-product-review-content\"> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"showReviews") : depth0), {"name":"if","hash":{},"fn":this.program(83, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </section><div class=\"item-details-content-correlated-items\"><div data-view=\"Correlated.Items\"></div></div><div id=\"banner-details-bottom\" class=\"content-banner banner-details-bottom\" data-cms-area=\"item_details_banner_bottom\" data-cms-area-filters=\"page_type\"></div></article></div> ";
},"useData":true}); template.Name = 'item_details'; return template;});