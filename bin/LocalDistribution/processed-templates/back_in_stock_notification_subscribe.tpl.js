define('back_in_stock_notification_subscribe.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var template = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function", buffer = "<button class=\"back-in-stock-notification-show-form-button\" data-action=\"show-bis-control\"> "
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Email me when item is back in stock", {"name":"translate","hash":{},"data":data})))
    + " </button><div class=\"dropdown-menu back-in-stock-notification-form-wrapper\" data-type=\"bis-control\" ";
  stack1 = ((helper = (helper = compilerNameLookup(helpers,"isVisible") || (depth0 != null ? compilerNameLookup(depth0,"isVisible") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"isVisible","hash":{},"data":data}) : helper));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "><form novalidate><fieldset><div class=\"back-in-stock-notification-message back-in-stock-notification-message-success\" data-confirm-bin-message=\"\"></div><div class=\"back-in-stock-notification-message\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "To be notified when this item is back in stock, please enter your name and email address", {"name":"translate","hash":{},"data":data})))
    + "</div><div class=\"back-in-stock-notification-control-wrapper\" data-validation=\"control-group\"><label for=\"firstname\" class=\"back-in-stock-notification-label\"> "
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Name", {"name":"translate","hash":{},"data":data})))
    + " <small class=\"back-in-stock-notification-required\"> "
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "(required)", {"name":"translate","hash":{},"data":data})))
    + "</small></label><input type=\"text\" name=\"firstname\" id=\"firstname\" class=\"back-in-stock-notification-input\" placeholder=\""
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "First Name", {"name":"translate","hash":{},"data":data})))
    + "\" data-validation=\"control\"><input type=\"text\" name=\"lastname\" id=\"lastname\" class=\"back-in-stock-notification-input\" placeholder=\""
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Last Name", {"name":"translate","hash":{},"data":data})))
    + "\" data-validation=\"control\"></div><div class=\"back-in-stock-notification-control-wrapper\" data-validation=\"control-group\"><label for=\"bis-email\" class=\"back-in-stock-notification-label\"> "
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Email", {"name":"translate","hash":{},"data":data})))
    + " <small class=\"back-in-stock-notification-required\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "(required)", {"name":"translate","hash":{},"data":data})))
    + "</small></label><input type=\"email\" name=\"email\" id=\"bis-email\" class=\"back-in-stock-notification-input\" placeholder=\""
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "your@email.com", {"name":"translate","hash":{},"data":data})))
    + "\" data-validation=\"control\"></div><input type=\"hidden\" name=\"item\" id=\"item\" value=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"selectedProduct") || (depth0 != null ? compilerNameLookup(depth0,"selectedProduct") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"selectedProduct","hash":{},"data":data}) : helper)))
    + "\"><button type=\"submit\" class=\"back-in-stock-notification-submit-button\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Notify Me!", {"name":"translate","hash":{},"data":data})))
    + "</button></fieldset></form></div> ";
},"useData":true}); template.Name = 'back_in_stock_notification_subscribe'; return template;});