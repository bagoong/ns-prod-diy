define('brand_sub_group_main_details.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var template = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <img class=\"brandgrouping-sub-brands-logo-image\" src=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"image") || (depth0 != null ? compilerNameLookup(depth0,"image") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"image","hash":{},"data":data}) : helper)))
    + "\"> ";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<div class=\"brandgrouping-sub-brands "
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"width") || (depth0 != null ? compilerNameLookup(depth0,"width") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"width","hash":{},"data":data}) : helper)))
    + "\"><a href=\"/"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"brandURLComponent") || (depth0 != null ? compilerNameLookup(depth0,"brandURLComponent") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"brandURLComponent","hash":{},"data":data}) : helper)))
    + "/"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"urlcomponent") || (depth0 != null ? compilerNameLookup(depth0,"urlcomponent") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"urlcomponent","hash":{},"data":data}) : helper)))
    + "\"><div class=\"brandgrouping-sub-brands-image\"> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"image") : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </div><p class=\"title\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"name") || (depth0 != null ? compilerNameLookup(depth0,"name") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</p></a></div> ";
},"useData":true}); template.Name = 'brand_sub_group_main_details'; return template;});