define('order_wizard_notes_module.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var template = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function";
  return "<h4 class=\"section-header\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Notes: ", {"name":"translate","hash":{},"data":data})))
    + "</h4><div id=\"diy-checkout-notes\"><textarea \r\n		id=\"custbody_web_order_note\"\r\n		class=\"diy-checkoukt-notes-textarea\"\r\n		value=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"custbody_web_order_note") || (depth0 != null ? compilerNameLookup(depth0,"custbody_web_order_note") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"custbody_web_order_note","hash":{},"data":data}) : helper)))
    + "\"		 		\r\n		placeholder=\"Add Notes or Special Instructions\"\r\n		rows=\"5\"></textarea></div>";
},"useData":true}); template.Name = 'order_wizard_notes_module'; return template;});