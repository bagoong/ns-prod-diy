define('recommendedaccessories_details.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var template = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<li class=\"recommendedaccessories-list-details-results-table-column\"><a class=\"item-views-recommended-accessories-thumbnail\" href=\"/"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"url_component") || (depth0 != null ? compilerNameLookup(depth0,"url_component") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"url_component","hash":{},"data":data}) : helper)))
    + "\"><div class=\"recommendedaccessories-list-details-container\"><img class=\"recommendedaccessories-img\" src=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"img_src") || (depth0 != null ? compilerNameLookup(depth0,"img_src") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"img_src","hash":{},"data":data}) : helper)))
    + "?resizeid=4&resizeh=175&resizew=175\"><div class=\"recommendedaccessories-title\"><span>"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"name") || (depth0 != null ? compilerNameLookup(depth0,"name") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</span></div><div class=\"recommendedaccessories-price\"><span>"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"price") || (depth0 != null ? compilerNameLookup(depth0,"price") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"price","hash":{},"data":data}) : helper)))
    + "</span></div></div></a></li>";
},"useData":true}); template.Name = 'recommendedaccessories_details'; return template;});