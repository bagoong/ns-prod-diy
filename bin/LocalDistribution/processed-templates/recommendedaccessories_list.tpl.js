define('recommendedaccessories_list.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var template = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <h1 class=\"recommendedaccessories-header\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Recommended Accessories", {"name":"translate","hash":{},"data":data})))
    + "</h1><div class=\"recommendedaccessories-list-results\"><ul data-view=\"RecommendedAccessories.Collection\" class=\"recommendedaccessories-items\" id=\"recommendedaccessoriesslider-items\"></ul></div> ";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<aside class=\"recommendedaccessories-container\"> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isRecommendedAccessories") : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </aside> ";
},"useData":true}); template.Name = 'recommendedaccessories_list'; return template;});