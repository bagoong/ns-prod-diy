define('video_details.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var template = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<li class=\"video-details-results-table-column\"><div class=\"video-details-container\"><div class=\"view-video\" iframe-src=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"video_src") || (depth0 != null ? compilerNameLookup(depth0,"video_src") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"video_src","hash":{},"data":data}) : helper)))
    + "\" iframe-height=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"height") || (depth0 != null ? compilerNameLookup(depth0,"height") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"height","hash":{},"data":data}) : helper)))
    + "\" modal-width=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"width") || (depth0 != null ? compilerNameLookup(depth0,"width") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"width","hash":{},"data":data}) : helper)))
    + "\" modal-name=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"name") || (depth0 != null ? compilerNameLookup(depth0,"name") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "\"><div class=\"playbutton-overlay\"></div><div class=\"video-details-container\"><img class=\"video-image\" src=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"src") || (depth0 != null ? compilerNameLookup(depth0,"src") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"src","hash":{},"data":data}) : helper)))
    + "\"><div class=\"video-details-results-table-row-name\"><span class=\"video-details-results-table-row-name-value\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"name") || (depth0 != null ? compilerNameLookup(depth0,"name") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</span></div></div></div></div></li>";
},"useData":true}); template.Name = 'video_details'; return template;});