define('quantity_pricing_rows.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var template = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var stack1, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = " <div class=\"quantity-pricing\"><h4 class=\"quantity-pricing-title\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Quantity Pricing", {"name":"translate","hash":{},"data":data})))
    + "</h4><div class=\"quantity-pricing-cms-intro\" data-cms-area=\"quantity_pricing_intro\" data-cms-area-filters=\"page_type\"></div><div class=\"quantity-pricing-table-wrapper\"><table class=\"quantity-pricing-table\"><thead class=\"quantity-pricing-quantities\"><tr> ";
  stack1 = compilerNameLookup(helpers,"each").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"pricingSchedule") : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " </tr></thead><tbody class=\"quantity-pricing-prices\"><tr> ";
  stack1 = compilerNameLookup(helpers,"each").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"pricingSchedule") : depth0), {"name":"each","hash":{},"fn":this.program(7, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </tr></tbody></table></div></div> ";
},"2":function(depth0,helpers,partials,data) {
  var stack1, buffer = " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (data && compilerNameLookup(data,"last")), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.program(5, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " ";
},"3":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <th class=\"quantity-pricing-table-cell\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"minimum") || (depth0 != null ? compilerNameLookup(depth0,"minimum") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"minimum","hash":{},"data":data}) : helper)))
    + " +</th> ";
},"5":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <th class=\"quantity-pricing-table-cell\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"minimum") || (depth0 != null ? compilerNameLookup(depth0,"minimum") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"minimum","hash":{},"data":data}) : helper)))
    + " - "
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"maximum") || (depth0 != null ? compilerNameLookup(depth0,"maximum") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"maximum","hash":{},"data":data}) : helper)))
    + "</th> ";
},"7":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <td class=\"quantity-pricing-table-cell\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"price") || (depth0 != null ? compilerNameLookup(depth0,"price") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"price","hash":{},"data":data}) : helper)))
    + "</td> ";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1;
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"pricingSchedule") : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { return stack1; }
  else { return ''; }
  },"useData":true}); template.Name = 'quantity_pricing_rows'; return template;});