define('quantity_pricing_paras.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var template = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var stack1, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = " <div class=\"quantity-pricing\"><h4 class=\"quantity-pricing-title\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Quantity Pricing", {"name":"translate","hash":{},"data":data})))
    + "</h4><div class=\"quantity-pricing-cms-intro\" data-cms-area=\"quantity_pricing_intro\" data-cms-area-filters=\"page_type\"></div> ";
  stack1 = compilerNameLookup(helpers,"each").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"pricingSchedule") : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </div> ";
},"2":function(depth0,helpers,partials,data) {
  var stack1, buffer = " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (data && compilerNameLookup(data,"last")), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.program(5, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " ";
},"3":function(depth0,helpers,partials,data) {
  var helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <p class=\"quantity-pricing-price-each\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Buy $(0) or above and pay only $(1) each", (depth0 != null ? compilerNameLookup(depth0,"minimum") : depth0), (depth0 != null ? compilerNameLookup(depth0,"price") : depth0), {"name":"translate","hash":{},"data":data})))
    + "</p> ";
},"5":function(depth0,helpers,partials,data) {
  var helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <p class=\"quantity-pricing-price-each\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Buy $(0) - $(1) and pay only $(2) each", (depth0 != null ? compilerNameLookup(depth0,"minimum") : depth0), (depth0 != null ? compilerNameLookup(depth0,"maximum") : depth0), (depth0 != null ? compilerNameLookup(depth0,"price") : depth0), {"name":"translate","hash":{},"data":data})))
    + "</p> ";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1;
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"pricingSchedule") : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { return stack1; }
  else { return ''; }
  },"useData":true}); template.Name = 'quantity_pricing_paras'; return template;});