define('brand_listing.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var template = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <li data-value=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"letter") || (depth0 != null ? compilerNameLookup(depth0,"letter") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"letter","hash":{},"data":data}) : helper)))
    + "\" class=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"enabled") || (depth0 != null ? compilerNameLookup(depth0,"enabled") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"enabled","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"letter") || (depth0 != null ? compilerNameLookup(depth0,"letter") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"letter","hash":{},"data":data}) : helper)))
    + "</li> ";
},"3":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = " <div id=\"brandletter-"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"letter") || (depth0 != null ? compilerNameLookup(depth0,"letter") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"letter","hash":{},"data":data}) : helper)))
    + "\" class=\"brandlisting-footer-alphabetical-result\"><div class=\"brand-listing-letter-container\"><span class=\"result-container-letter\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"letter") || (depth0 != null ? compilerNameLookup(depth0,"letter") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"letter","hash":{},"data":data}) : helper)))
    + "</span></div><div class=\"brand-listing-brandgroup-container\"><div class=\"brand-listing-brand-container\"> ";
  stack1 = compilerNameLookup(helpers,"each").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"brandGroupList") : depth0), {"name":"each","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </div></div></div> ";
},"4":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <div class=\"brand-listing-brand-details\"><a href=\"/"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"urlComponent") || (depth0 != null ? compilerNameLookup(depth0,"urlComponent") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"urlComponent","hash":{},"data":data}) : helper)))
    + "\"><img class=\"brand-cell-image\"\n                                src=\""
    + escapeExpression(((compilerNameLookup(helpers,"resizeImage") || (depth0 && compilerNameLookup(depth0,"resizeImage")) || helperMissing).call(depth0, ((stack1 = (depth0 != null ? compilerNameLookup(depth0,"logo") : depth0)) != null ? compilerNameLookup(stack1,"logoName") : stack1), "thumbnail", {"name":"resizeImage","hash":{},"data":data})))
    + "\"\n                           ><p>"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"name") || (depth0 != null ? compilerNameLookup(depth0,"name") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</p></a></div> ";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div class=\"shopping-layout-breadcrumb\"><ul class=\"global-views-breadcrumb\" itemprop=\"breadcrumb\"><li class=\"global-views-breadcrumb-item\"><a href=\"/\" data-touchpoint=\"home\" data-hashtag=\"#\"> Home </a></li><li class=\"global-views-breadcrumb-divider\"><span class=\"global-views-breadcrumb-divider-icon\"></span></li><li class=\"global-views-breadcrumb-item-active\"> Brands </li></ul></div><div class=\"brandlisting-content\"><div class=\"brandlisting-header\" data-cms-area=\"brandlisting_header\" data-cms-area-filters=\"path\"></div><div class=\"brandlisting-body\" data-cms-area=\"brandlisting_body\" data-cms-area-filters=\"path\"></div><div class=\"brandlisting-footer\"><p>Alphabetical Brand Index</p><div class=\"brandlisting-footer-alphabetical-index\"><ul class=\"alphabetical-index\"><li>#</li> ";
  stack1 = compilerNameLookup(helpers,"each").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"letters") : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " </ul></div> ";
  stack1 = compilerNameLookup(helpers,"each").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"brandAlphabeticalGroupList") : depth0), {"name":"each","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </div></div>";
},"useData":true}); template.Name = 'brand_listing'; return template;});