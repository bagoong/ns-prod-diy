define('backinstocknotification_list_details.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var template = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda, functionType="function";
  return "<tr><td class=\"back-in-stock-notification-td-first\"><div class=\"back-in-stock-notification-thumbnail\"><img src=\""
    + escapeExpression(((compilerNameLookup(helpers,"resizeImage") || (depth0 && compilerNameLookup(depth0,"resizeImage")) || helperMissing).call(depth0, ((stack1 = (depth0 != null ? compilerNameLookup(depth0,"thumbnail") : depth0)) != null ? compilerNameLookup(stack1,"url") : stack1), "thumbnail", {"name":"resizeImage","hash":{},"data":data})))
    + "\" alt=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"thumbnail") : depth0)) != null ? compilerNameLookup(stack1,"altimagetext") : stack1), depth0))
    + "\"></div></td><td class=\"back-in-stock-notification-td-middle\"><div class=\"back-in-stock-notification-name\"><span class=\"back-in-stock-notification-name-viewonly\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"itemName") || (depth0 != null ? compilerNameLookup(depth0,"itemName") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"itemName","hash":{},"data":data}) : helper)))
    + "</span></div><div class=\"back-in-stock-notification-price\"><span class=\"back-in-stock-notification-price-lead\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"itemPrice") || (depth0 != null ? compilerNameLookup(depth0,"itemPrice") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"itemPrice","hash":{},"data":data}) : helper)))
    + "</span></div><div class=\"back-in-stock-notification-data\"><span class=\"back-in-stock-notification-data-label\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Date", {"name":"translate","hash":{},"data":data})))
    + ": </span><span class=\"back-in-stock-notification-data-value\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"created") || (depth0 != null ? compilerNameLookup(depth0,"created") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"created","hash":{},"data":data}) : helper)))
    + "</span></div><div class=\"back-in-stock-notification-data\"><span class=\"back-in-stock-notification-data-label\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Contact", {"name":"translate","hash":{},"data":data})))
    + ": </span><span class=\"back-in-stock-notification-data-value\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"firstName") || (depth0 != null ? compilerNameLookup(depth0,"firstName") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"firstName","hash":{},"data":data}) : helper)))
    + " "
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"lastName") || (depth0 != null ? compilerNameLookup(depth0,"lastName") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"lastName","hash":{},"data":data}) : helper)))
    + " &lt;"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"email") || (depth0 != null ? compilerNameLookup(depth0,"email") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"email","hash":{},"data":data}) : helper)))
    + "&gt;</span></div></td><td class=\"back-in-stock-notification-td-last\"><div class=\"back-in-stock-notification-button-container\"><button class=\"back-in-stock-notification-delete\" data-type=\"backinstock-delete\" data-id=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"internalId") || (depth0 != null ? compilerNameLookup(depth0,"internalId") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"internalId","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Delete", {"name":"translate","hash":{},"data":data})))
    + "</button></div></td></tr> ";
},"useData":true}); template.Name = 'backinstocknotification_list_details'; return template;});