define('header.tpl', ['Handlebars','Handlebars.CompilerNameLookup','header_sidebar.tpl','header_sidebar.tpl'], function (Handlebars, compilerNameLookup){ var template = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  return "header-diy-pro-builder-row";
  },"3":function(depth0,helpers,partials,data) {
  return " wprobuilder ";
  },"5":function(depth0,helpers,partials,data) {
  return " wprobuilderphone ";
  },"7":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <a href=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"wishListLink") || (depth0 != null ? compilerNameLookup(depth0,"wishListLink") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"wishListLink","hash":{},"data":data}) : helper)))
    + "\"><div class=\"image-wrap\"><img src=\"/c.530610/site/images/star-icon.png\" class=\"star-icon\" align=\"absmiddle\"/></div><p>Wish Lists</p></a> ";
},"9":function(depth0,helpers,partials,data) {
  return " <div class=\"col build-button\"><a href=\"#\" class=\"action-build-button\"><div class=\"image-wrap\"><img src=\"/c.530610/site/images/check-white.png\"/></div><p>Pro Builder</p></a></div> ";
  },"11":function(depth0,helpers,partials,data) {
  return "header-main-nav-logged-in";
  },"13":function(depth0,helpers,partials,data) {
  var stack1, buffer = " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"userName") : depth0), {"name":"if","hash":{},"fn":this.program(14, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " ";
},"14":function(depth0,helpers,partials,data) {
  var helper, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function";
  return " <div class=\"header-menu-welcome-message-tablet\"> "
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Welcome", {"name":"translate","hash":{},"data":data})))
    + " "
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"userName") || (depth0 != null ? compilerNameLookup(depth0,"userName") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"userName","hash":{},"data":data}) : helper)))
    + " </div> ";
},"16":function(depth0,helpers,partials,data) {
  var stack1, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = " <div class=\"header-menu-settings\"><a href=\"#\" class=\"header-menu-settings-link\" data-toggle=\"dropdown\" title=\""
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Settings", {"name":"translate","hash":{},"data":data})))
    + "\"><i class=\"header-menu-settings-icon\"></i><i class=\"header-menu-settings-carret\"></i></a><div class=\"header-menu-settings-dropdown\"><h5 class=\"header-menu-settings-dropdown-title\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Site Settings", {"name":"translate","hash":{},"data":data})))
    + "</h5> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"showLanguages") : depth0), {"name":"if","hash":{},"fn":this.program(17, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"showCurrencies") : depth0), {"name":"if","hash":{},"fn":this.program(19, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " </div></div> ";
},"17":function(depth0,helpers,partials,data) {
  return " <div data-view=\"Global.HostSelector\"></div> ";
  },"19":function(depth0,helpers,partials,data) {
  return " <div data-view=\"Global.CurrencySelector\"></div> ";
  },"21":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " <div class=\"site-message\"> "
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"sitewideMsg") || (depth0 != null ? compilerNameLookup(depth0,"sitewideMsg") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"sitewideMsg","hash":{},"data":data}) : helper)))
    + " </div> ";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = " <div class=\"header-message\" data-type=\"message-placeholder\"></div><div class=\"header-top-wrap\"><div class=\"header-top-content ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"showProBuilder") : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "\"><div class=\"col first ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"showProBuilder") : depth0), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "\"><a data-toggle=\"modal\" data-target=\"#header-free-shipping-modal\"><p>Free Shipping On All Products*</p></a></div><div class=\"col ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"showProBuilder") : depth0), {"name":"if","hash":{},"fn":this.program(5, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "\"><a href=\"#\"><div class=\"image-wrap\"><img src=\"/c.530610/site/images/phone-icon.png\" align=\"absmiddle\"/></div><p>888-349-4660 <span>M-F | 8-5 CST</span></p></a></div><div class=\"col ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"showProBuilder") : depth0), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "\"><a href=\"/builder-program\"><div class=\"image-wrap\"><img src=\"/c.530610/site/images/hammer-icon.png\" align=\"absmiddle\"/></div><p>Pro Rewards</p></a></div><div class=\"col last wish-mobile ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"showProBuilder") : depth0), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "\"> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isHome") : depth0), {"name":"if","hash":{},"fn":this.program(7, data),"inverse":this.program(7, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " </div> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"showProBuilder") : depth0), {"name":"if","hash":{},"fn":this.program(9, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " <br class=\"clr\"></div></div><div class=\"header-main-wrapper\"><nav class=\"header-main-nav ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isLoggedIn") : depth0), {"name":"if","hash":{},"fn":this.program(11, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "\"><div id=\"banner-header-top\" class=\"content-banner banner-header-top\" data-cms-area=\"header_banner_top\" data-cms-area-filters=\"global\"></div><div class=\"header-sidebar-toggle-wrapper\"><button class=\"header-sidebar-toggle\" data-action=\"header-sidebar-show\"><i class=\"header-sidebar-toggle-icon\"></i></button></div><div class=\"header-content\"><div class=\"header-logo-wrapper\"><div data-view=\"Header.Logo\"></div></div><div class=\"header-right-menu\"><div class=\"header-menu-profile\" data-view=\"Header.Profile\"></div> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isLoggedIn") : depth0), {"name":"if","hash":{},"fn":this.program(13, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"showLanguagesOrCurrencies") : depth0), {"name":"if","hash":{},"fn":this.program(16, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " <div class=\"header-menu-quote\" data-view=\"RequestQuoteWizardHeaderLink\"></div><div class=\"header-menu-cart\"><div class=\"header-menu-cart-dropdown\" ><div data-view=\"Header.MiniCart\"></div></div></div></div><div class=\"site-search-wrapper pull-right\"><div data-view=\"SiteSearch\" data-type=\"SiteSearch\"></div></div></div><div id=\"banner-header-bottom\" class=\"content-banner banner-header-bottom\" data-cms-area=\"header_banner_bottom\" data-cms-area-filters=\"global\"></div></nav></div><div class=\"header-sidebar-overlay\" data-action=\"header-sidebar-hide\"></div><div class=\"header-secondary-wrapper\" data-view=\"Header.Menu\" data-phone-template=\"header_sidebar\" data-tablet-template=\"header_sidebar\"></div> ";
  stack1 = compilerNameLookup(helpers,"if").call(depth0, (depth0 != null ? compilerNameLookup(depth0,"isSitewideMsgAvailable") : depth0), {"name":"if","hash":{},"fn":this.program(21, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " <div class=\"modal fade modal-return-policy\" id=\"header-free-shipping-modal\" role=\"dialog\"><div class=\"modal-dialog global-views-modal\"><div class=\"global-views-modal-content\"><div id=\"modal-header\" class=\"global-views-modal-content-header\"><button type=\"button\" class=\"global-views-modal-content-header-close\" data-dismiss=\"modal\" aria-hidden=\"true\"> × </button></div><div class=\"return-policy-view-container global-views-modal-content-body\"><h2 class=\"return-policy-view-title\">";
  stack1 = ((helper = (helper = compilerNameLookup(helpers,"freeShippingTitle") || (depth0 != null ? compilerNameLookup(depth0,"freeShippingTitle") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"freeShippingTitle","hash":{},"data":data}) : helper));
  if (stack1 != null) { buffer += stack1; }
  buffer += "</h2><p>";
  stack1 = ((helper = (helper = compilerNameLookup(helpers,"freeShippingBodyText") || (depth0 != null ? compilerNameLookup(depth0,"freeShippingBodyText") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"freeShippingBodyText","hash":{},"data":data}) : helper));
  if (stack1 != null) { buffer += stack1; }
  buffer += "</p><a href=\""
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"freeShippingLinkValue") || (depth0 != null ? compilerNameLookup(depth0,"freeShippingLinkValue") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"freeShippingLinkValue","hash":{},"data":data}) : helper)))
    + "\">";
  stack1 = ((helper = (helper = compilerNameLookup(helpers,"freeShippingLinkText") || (depth0 != null ? compilerNameLookup(depth0,"freeShippingLinkText") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"freeShippingLinkText","hash":{},"data":data}) : helper));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</a><button class=\"return-policy-view-button\" data-dismiss=\"modal\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "Close", {"name":"translate","hash":{},"data":data})))
    + "</button></div></div></div></div> ";
},"useData":true}); template.Name = 'header'; return template;});