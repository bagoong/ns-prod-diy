/*
  © 2015 NetSuite Inc.
  User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
  provided, however, if you are an authorized user with a NetSuite account or log-in, you
  may use this code subject to the terms that govern your access and use.
*/

/*exported service*/
// Service to manage Brands Searcher requests

function service (request) {
  'use strict';

  var Application = require('Application');

  try {

    //nlapiLogExecution('DEBUG', 'BrandsSearcher.Service - START');

    var method = request.getMethod();
    var BrandSearch = require('BrandSearch.Model');
    var data = JSON.parse(request.getBody() || '{}');

    switch (method) {
      case 'GET':
        Application.sendContent(BrandSearch.getAllBrandsSearches(), {
              'cache': response.CACHE_DURATION_MEDIUM
          });
      break;

      default:
        // methodNotAllowedError is defined in ssp library commons.js
        Application.sendError(methodNotAllowedError);
    }
  } catch (e) {
    Application.sendError(e);
  }
}